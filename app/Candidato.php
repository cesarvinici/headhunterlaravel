<?php

namespace App;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidato extends Authenticatable
{
    //
    use softDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'candidatos';

}
