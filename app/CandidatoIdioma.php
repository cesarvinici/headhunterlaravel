<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidatoIdioma extends Model
{
    //
    protected $table = 'candidatos_idiomas';
    public $timestamps = false;
}
