<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidatoRelatorio extends Model
{
    protected $table = 'candidatos_relatorios';
    public $timestamps = false;
}
