<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

    /**
     * Funções estão em app\http\emails.php
     */        
        $schedule->call(function()
        {
            enviaEmailVagaVencidaEmpresa();
        })->daily();

        $schedule->call(function()
        {
            enviaEmailVagaVencidaHeadhunter();
        })->daily();

        $schedule->call(function()
        {
            finalizaVagasEmpresas();
        })->daily();

        $schedule->call(function()
        {
            finalizaVagasHeadhunter();
        })->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
