<?php

namespace App\CustomClass\Candidatos;

use App\CustomClass\Uteis;

use App\CandidatoIdioma;
use App\Candidato;
use App\CandidatoRelatorio;
use App\CandidatoExperiencia;
use Session;

class Candidatos
{
    // Headhunter Cadastrar o CV de um candidato
    public static function gerenciaCandidato($cdd, $request)
    {
        try
        {
            $cdd = new Candidato();
            $cdd->pcd  = $request->input('pcd') == 'ON' ? 1 : 0;
            $cdd->nome = $request->input('nome');
            $cdd->cpf  = $request->input('cpf');
            $cdd->email = $request->input('email');
            $cdd->telefone = $request->input('telefone');
            $cdd->celular = $request->input('celular');
            $cdd->estado_civil = $request->input('estadoCivil');
            $cdd->nascimento = Uteis::dataToMysql($request->input('nascimento'));
            $cdd->cep = $request->input('cep');
            $cdd->endereco = $request->input('endereco');
            $cdd->cidade = $request->input('cidade');
            $cdd->instagram = $request->input('instagram');
            $cdd->twitter = $request->input('twitter');
            $cdd->objetivo = $request->input('objetivo');
            $cdd->resumo_qualidades = $request->input('resumoQualidades');
            $cdd->trabalha_atualmente = $request->input('trabAtual');
            $cdd->disponivel_contratacao = $request->input('dispContratacao');
            $cdd->instituicao = $request->input('instituicao');
            $cdd->curso = $request->input('curso');
            $cdd->duracao = $request->input('duracao');
            $cdd->escolaridade = $request->input('nivel');
            $cdd->status_escolaridade = $request->input('status');
           
            $cdd->pretensao_salarial = Uteis::moneyToInt($request->input('pretensaoSalarial'));
            $cdd->nivel_hierarquico = $request->input('nivelHierar');
            $cdd->disposicao_viagem = $request->input('disViag');
            $cdd->frequencia_viagens = $request->input('qualFreq');
            $cdd->publicidade_curriculum = $request->input('cv');                

            $cdd->save();
        }
        catch(\Exception $e)
        {
            
            SESSION::flash('cad_cdd_fail', 'Ocorreu um erro '.$e->getMessage());
            return redirect()->back();
        }
        try
        {
            //relatorios
            if($request->hasFile('relatorios'))
            {
                for ($i=0; $i < sizeof($request->file('relatorios')) ; $i++)
                {                    
                    if($request->file('relatorios')[$i])
                    {                       
                        $relatorio  = new CandidatoRelatorio();
                        $relatorio->candidato = $cdd->id;
                        $rel = $request->file('relatorios')[$i];
                        $nomearquivo  = $cdd->id."_".$i.".". $rel->getClientOriginalExtension();
                        $request->file('relatorios')[$i]->move(public_path('./admin/upload/candidatos/relatorios'), $nomearquivo);
                        $relatorio->relatorio = $nomearquivo;
                        $relatorio->save();       
                    }
                }
            }

            //Experiencia
            if($request->input('idCargo'))
            {
                print_r($request->input('idCargo'));
                for ($i=0; $i < sizeof($request->input('idCargo')) ; $i++)
                {
                    if(!$request->idCargo[$i]) continue;
                    $exp = new CandidatoExperiencia();
                    $exp->candidato = $cdd->id;
                    $exp->empresa = $request->input('empresa')[$i];
                    $exp->cargo = $request->input('idCargo')[$i];
                    $exp->atividades = $request->input('atividades')[$i];
                    $exp->admissao = $request->input('admissao')[$i] ? Uteis::dataToMysql($request->input('admissao')[$i]) : null;
                    $exp->desligamento = $request->input('desligamento')[$i] ? Uteis::datatoMysql($request->input('desligamento')[$i]) : null;
                    $exp->save();
                }
            }

            //curriculum
            if($request->hasFile('curriculo'))
            {                    
                $cv = $request->file('curriculo');
                $nomearquivo  = $cdd->id.".". $cv->getClientOriginalExtension();
                $request->file('curriculo')->move(public_path('./admin/curriculos/candidatos'), $nomearquivo);
                $cdd->curriculum = $nomearquivo;
                
            }

            //img perfil
            if($request->hasFile('imgPerfil'))
            {                    
                $cv = $request->file('imgPerfil');
                $nomearquivo  = $cdd->id.".". $cv->getClientOriginalExtension();
                $request->file('imgPerfil')->move(public_path('./admin/upload/candidatos'), $nomearquivo);
                $cdd->foto_perfil = $nomearquivo;
                
            }

            for ($i=0; $i < sizeof($request->input('idioma')) ; $i++)
            { 
                if($request->input('idioma')[$i] )
                {
                    $idioma = new CandidatoIdioma();
                    $idioma->candidato = $cdd->id;
                    $idioma->idioma = $request->input('idioma')[$i];
                    $idioma->nivel = $request->input('idiomaNivel'.($i+1));
                    $idioma->save();                
                }
            }  
            $cdd->save();
            return $cdd;
        }
        catch(\Exception $e)
        {
            $cdd->delete();
            dd($e);
            SESSION::flash('cad_cdd_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
            //return redirect()->back();
        }
    }

    // Editar o CV de um candidato
    public static function editCandidato($cdd, $request)
    {
        try
        {            
            $cdd->pcd  = $request->input('pcd') == 'on' ? 1 : 0;
            $cdd->nome = $request->input('nome');
            $cdd->cpf  = $request->input('cpf');
            $cdd->email = $request->input('email');
            $cdd->telefone = $request->input('telefone');
            $cdd->celular = $request->input('celular');
            $cdd->estado_civil = $request->input('estadoCivil');
            $cdd->nascimento = Uteis::dataToMysql($request->input('nascimento'));
            $cdd->cep = $request->input('cep');
            $cdd->endereco = $request->input('endereco');
            $cdd->cidade = $request->input('cidade');
            $cdd->instagram = $request->input('instagram');
            $cdd->twitter = $request->input('twitter');
            $cdd->objetivo = $request->input('objetivo');
            $cdd->resumo_qualidades = $request->input('resumoQualidades');
            $cdd->trabalha_atualmente = $request->input('trabAtual');
            $cdd->disponivel_contratacao = $request->input('dispContratacao');
            $cdd->instituicao = $request->input('instituicao');
            $cdd->curso = $request->input('curso');
            $cdd->duracao = $request->input('duracao');
            $cdd->escolaridade = $request->input('nivel');
            $cdd->status_escolaridade = $request->input('status');
           
            $cdd->pretensao_salarial = Uteis::moneyToInt($request->input('pretensaoSalarial'));
            $cdd->nivel_hierarquico = $request->input('nivelHierar');
            $cdd->disposicao_viagem = $request->input('disViag');
            $cdd->frequencia_viagens = $request->input('qualFreq');
            $cdd->publicidade_curriculum = $request->input('cv');                

            $cdd->save();
        }
        catch(\Exception $e)
        {
            //dd($e);
            SESSION::flash('cad_cdd_fail', 'Ocorreu um erro: '.$e->getMessage());
            return redirect()->back();
        }
        try
        {
            //relatorios
            if($request->hasFile('relatorios'))
            {
                for ($i=0; $i < sizeof($request->file('relatorios')) ; $i++)
                {                    
                    if($request->file('relatorios')[$i])
                    {                      
                        $relatorio = new CandidatoRelatorio();
                        $relatorio->candidato = $cdd->id;
                        $rel = $request->file('relatorios')[$i];
                        $nomearquivo  = $cdd->id."_".$i.".". $rel->getClientOriginalExtension();
                        $request->file('relatorios')[$i]->move(public_path('./admin/upload/candidatos/relatorios'), $nomearquivo);
                        $relatorio->relatorio = $nomearquivo;
                        $relatorio->save();       
                    }
                }
            }

            //Experiencia           
            if($request->input('idCargo'))
            {
                for ($i=0; $i < sizeof($request->input('idCargo')) ; $i++)
                { 
                    if(isset($request->input('idExp')[$i]))
                    {
                        $exp = CandidatoExperiencia::find($request->input('idExp')[$i]);
                    }
                    else
                    {
                        $exp = new CandidatoExperiencia();
                    }
                    $exp->candidato = $cdd->id;
                    $exp->empresa = $request->input('empresa')[$i];
                    $exp->cargo = $request->input('idCargo')[$i];
                    $exp->atividades = $request->input('atividades')[$i];
                    $exp->admissao = $request->input('admissao')[$i] ? Uteis::dataToMysql($request->input('admissao')[$i]) : null;
                    $exp->desligamento = $request->input('desligamento')[$i] ? Uteis::datatoMysql($request->input('desligamento')[$i]) : null;
                    $exp->save();
                                        
                }
            }
            //curriculum
            if($request->hasFile('curriculo'))
            {
                $cv = $request->file('curriculo');
                $nomearquivo  = $cdd->id.".". $cv->getClientOriginalExtension();
                $request->file('curriculo')->move(public_path('./admin/curriculos/candidatos'), $nomearquivo);
                $cdd->curriculum = $nomearquivo;
                
                
            }

            //img perfil
            if($request->hasFile('imgPerfil'))
            {                    
                $cv = $request->file('imgPerfil');
                $nomearquivo  = $cdd->id.".". $cv->getClientOriginalExtension();
                $request->file('imgPerfil')->move(public_path('./admin/upload/candidatos'), $nomearquivo);
                $cdd->foto_perfil = $nomearquivo;
                
            }

            for ($i=0; $i < sizeof($request->input('idioma')) ; $i++)
            { 
                if($request->input('idioma')[$i] )
                {
                    if(isset($request->input('idIdioma')[$i]))
                    {
                        $idioma = CandidatoIdioma::find($request->input('idIdioma')[$i]);
                    }
                    else
                    {
                        $idioma = new CandidatoIdioma();
                    }
                    $idioma->candidato = $cdd->id;
                    $idioma->idioma = $request->input('idioma')[$i];
                    $idioma->nivel = $request->input('idiomaNivel'.($i+1));
                    $idioma->save();                
                }
            }  
            
            $cdd->save();
            return $cdd;
        }
        catch(\Exception $e)
        {
            //dd($e);
            SESSION::flash('cad_cdd_fail', 'Ocorreu um erro, '.$e->getMessage());
            return redirect()->back();
        }

    }
}