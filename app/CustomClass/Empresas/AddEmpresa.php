<?php
namespace App\CustomClass\Empresas;


use App\Empresa;

class AddEmpresa
{
    public function empresaAdd($empresa, $request)
    {   
        
        $empresa->nome_fantasia = $request->input('nomeFantasia');
        $empresa->razao_social = $request->input('razaoSocial');
        $empresa->cnpj = $request->input('cnpj');
        $empresa->telefone1 = $request->input('telefone');
        $empresa->endereco_logradouro = $request->input('endereco');
        $empresa->endereco_numero = $request->input('numero');
        $empresa->endereco_bairro = $request->input('bairro');
        $empresa->endereco_cidade = $request->input('cidade');
        $empresa->endereco_cep = $request->input('cep');
        $empresa->website = $request->input('website');
        $empresa->segmento = $request->input('segmento');
        $empresa->numero_funcionarios = $request->input('nFuncionarios');
        $empresa->responsavel_financeiro_nome = $request->input('nomeResp');
        $empresa->responsavel_financeiro_cargo = $request->input('cargoResp');
        $empresa->responsavel_financeiro_email = $request->input('emailResp');
        $empresa->responsavel_financeiro_telefone = $request->input('telResp');
        
        if($empresa->save())
        {
            if($request->hasFile('logotipo'))
            {               
                $imagem = $request->file('logotipo');
                @unlink(public_path('admin/upload/logotipo/'.$empresa->logotipo));
                $nomearquivo  = $empresa->id.".". $imagem->getClientOriginalExtension();
                $request->file('logotipo')->move(public_path('admin/upload/logotipo/'), $nomearquivo);
                $empresa->logotipo = $nomearquivo;
                $empresa->save();               
               
            }

            return $empresa;
        }  

        return false;
    }

}