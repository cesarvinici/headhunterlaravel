<?php

namespace App\CustomClass;

class Uteis
{
    public static function moneyToInt($valor)
    {
        $valor = str_replace("R$", "", $valor);
		$valor = str_replace(',','.',str_replace('.','',$valor));
		return $valor;
    }

    public static function intToMoney($valor)
	{
		return number_format(floatval($valor),2,",",".");
    }
    
    public static function dataToMysql($data)
	{
		return implode('-', array_reverse(explode("/", $data)));
    }
    
    public static function MysqlToData($data)
    {
        return implode('/', array_reverse(explode("-", $data)));
    }
}
