<?php

namespace App\CustomClass\Vagas;

use App\CustomClass\Uteis;

use App\Vaga;

class Vagas
{
    public static function gerenciaVaga($vaga, $request)
    {
       
        $vaga->vaga_pcd = $request->vagaPCD == 'ON' ? 1 : 0;
        $vaga->empresa = $request->empresa;
        $vaga->tituloCargo = $request->idCargo;
        $vaga->numVagas = $request->numVagas;
        $vaga->sigilo = $request->sigilosos;
        $vaga->formacao_exigida = $request->formacaoExigida;
        $vaga->principais_atividades = $request->principaisAtividades;
        $vaga->requisitos = $request->requisitos;
        $vaga->disposicao_viagens = $request->disponViagem;
        $vaga->frequencia_viagem = $request->frequenciaViagem;
        $vaga->naoExibirSalario = $request->NaoExibirSalario == 'ON' ? 1 : 0;
        $vaga->salario =  Uteis::moneyToInt($request->salarioMensal);
        $vaga->faixa_salarial = $request->faixaSalarial;
        $vaga->naoExibirBeneficio = $request->NaoExibirBeneficios == 'ON' ? 1 : 0;
        $vaga->comissao = $request->comissao;
        $vaga->beneficios = $request->beneficios;
        $vaga->regime_contratacao = $request->regContr;
        $vaga->horario_de_trabalho = $request->horarioTrab;
        $vaga->informacoes_adicionais_vaga = $request->infoAdicionaisVagas;
        $vaga->candidatos_outro_estado = $request->candEstados;
        $vaga->estados_e_regioes = $request->estadoseRegioes;
        $vaga->auxilio_mudanca = $request->auxMudanca;
        $vaga->dias_para_contratacao = $request->dataLimite;
        $vaga->data_limite = Date("Y-m-d", strtotime("+".$vaga->dias_para_contratacao." days"));
        $vaga->etapas_processo_seletivo = json_encode($request->etapasProcesso);
        $vaga->etapas_processo_seletivo_outros = $request->etapasProcessoOutros;
        $vaga->feeRecrutamento = $request->feeRecruts;
        $vaga->limite_curriculum_headhunter = $request->limitCV;
        $vaga->documentos_requeridos = $request->documentosRequeridos;
        $vaga->comentarios_e_recomendacoes = $request->comentariosRecomendacoes;
        $vaga->responsavel_recrutamento = $request->responsavelRecrutamento;
        $vaga->status_vaga = 1;
        $vaga->save();
        return $vaga;
    
    }
}