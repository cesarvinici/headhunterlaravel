<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Headhunter extends Authenticatable
{
    protected $table = 'headhunters';
}
