<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeadhunterAlerta extends Model
{
    protected $table  = 'headhunters_alertas';
    public $timestamps = false;
}
