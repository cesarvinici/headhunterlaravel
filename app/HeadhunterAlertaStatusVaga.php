<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeadhunterAlertaStatusVaga extends Model
{
    protected $table = 'headhunter_alerta_status_vaga';
    public $timestamps = false;
}
