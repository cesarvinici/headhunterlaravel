<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class HeadhunterUsuario extends Authenticatable
{
    //
    protected $table = 'headhunters_usuarios';
}
