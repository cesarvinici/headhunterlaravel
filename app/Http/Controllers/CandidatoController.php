<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\CandidatoVaga;
use App\Candidato;
use App\CustomClass\Candidatos\Candidatos;
use Illuminate\Http\Request;
use Session;
class CandidatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        if(auth('cdd')->check())
        {
            return redirect()->route('dashboardCandidato');
        }
        
        return view('page-candidatos');
    }

    public function create()
    {
        return view('cadastro.cadastroCandidato');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'email' => 'required|
                unique:headhunters|
                unique:candidatos|
                unique:empresas_usuarios|
                unique:empresas,responsavel_financeiro_email|
                unique:headhunters_usuarios',
            'cpf' => 'required|
                unique:candidatos|
                unique:headhunters',       
            'novaSenha'=> 'required|min:6']);
        try
        {
            $candidato = new Candidato();
            $candidato->nome = $request->nome;
            $candidato->cpf = $request->cpf;
            $candidato->email = $request->email;
            
            if($request->novaSenha == $request->novaSenhaC)
            {
                $candidato->password = Hash::make($request->novaSenha);
            }
            else
            {
                Session::flash('error', 'As senhas não conferem, tente novamente.');
                return redirect()->back();
            }

            $candidato->save();
            
            $user = [
                'email' => $request->email,
                'password' => $request->novaSenha,
            ];

            if(auth('cdd')->attempt($user))
            {
                Session::flash('cadCdd_ok', '');
                return redirect()->back();
            }
        }
        catch(Exception $e )
        {
            Session::flash('error', 'Ocorreu um erro, tente novamente mais tarde');
            return redirect()->back();
        }   
    }

    public function edit(Request $request)
    {
        if(auth('cdd')->check())
        {    
            $candidato = auth('cdd')->user();
            $cidades = array();
            // Caso o cdd esteja vinculado a um estado então será retornado lista de todas as cidades referente aquele estado
            if(!empty($candidato->cidade))
            {                
                $cidades = DB::table('cidades')->where('CT_UF', function($query) use ($candidato)
                {
                    $query->select('CT_UF')->from("cidades")->where('CT_ID', $candidato->cidade)->first();
                })->get();
            }

            $estados = DB::table("estados")->get();
            return view('dashboard.candidatos.editarPerfil', array(
                                                                'candidato' => $candidato, 
                                                                'estados' => $estados, 
                                                                'cidades' => $cidades));
        }
    }

    // Recebe dados do formulário e edita o caadastro do candidato;
    public function update(Request $request)
    {
        
        if(auth('cdd')->check())
        {
            $this->validate($request, [
                'nome'=> 'required',
                'cpf' => 'required|unique:candidatos,cpf,'.Auth('cdd')->id().'|
                       unique:headhunters',
                'email'=> 'required|unique:candidatos,email,'.Auth('cdd')->id().'|',
                'nascimento'=> 'required',
                'endereco'=>'required',
                'estado'=> 'required',
                'cidade'=>'required'
            ]);
            try
            {
                $candidato = Candidato::find(auth('cdd')->id());
                $candidato->nome = $request->nome;
                $candidato->cpf = $request->cpf;
                $candidato->email = $request->email;
                $candidato->nascimento = DataToMySql($request->nascimento);
                $candidato->telefone = $request->telefone;
                $candidato->celular = $request->celular;
                $candidato->estado_civil = $request->estadoCivil;
                $candidato->cep = $request->cep;
                $candidato->endereco = $request->endereco;
                $candidato->cidade = $request->cidade;
                $candidato->instagram = $request->instagram;
                $candidato->twitter = $request->twitter;               

                 //img perfil
                if($request->hasFile('imgPerfil'))
                {                    
                    $cv = $request->file('imgPerfil');
                    $nomearquivo  = $candidato->id.".". $cv->getClientOriginalExtension();
                    $request->file('imgPerfil')->move(public_path('./admin/upload/candidatos/'), $nomearquivo);
                    $candidato->foto_perfil = $nomearquivo;                    
                }

                if($request->senha)
                {
                    if(Hash::check($request->senha, $candidato->password))
                    {
                        $this->validate($request, ['password' => 'required|confirmed|min:6']);
                        $candidato->password = Hash::make($request->password);

                    }
                    else
                    {
                        Session::flash('error', 'Senha Incorreta, tente novamente');
                        return redirect()->back();
                    }                    
                }
                
                
                $candidato->save();

                Session::flash('success', 'Perfil editado com sucesso!');
                return redirect()->back();

            }    
            catch(Exception $e)
            {
                dd($e);
            }            
        }
            
    }


    public function cadastraCv()
    {
        if(auth('cdd')->check())
        {
            try
            {
                $candidato = DB::table('candidatos as cdd')->select('cdd.*', 'ct.CT_UF')
                ->leftJoin('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
                ->where('cdd.id', auth('cdd')->id())->first();
                   
                $cddExperiencia = DB::table('candidato_experiencia as ce')
                    ->select('ce.id', 'ce.empresa', 'ce.atividades', 'ce.cargo as idcargo', 'c.cargo', 'ce.admissao', 'ce.desligamento')
                    ->join('cargos as c', 'c.id', 'ce.cargo')
                    ->where('candidato', auth('cdd')->id())
                    ->orderBy('admissao', 'desc')->get();
    
                $cddIdiomas = DB::table('candidatos_idiomas')->where('candidato', auth('cdd')->id())->get();
                $relatorios = DB::table('candidatos_relatorios')->where('candidato', auth('cdd')->id())->get();
                $estados = DB::table('estados')->get();
                $cidades = DB::table('cidades')->where('CT_UF', $candidato->CT_UF)->get();
                $formacoes = DB::table('formacao_nivel')->get();
                $statusForm = DB::table('formacao_status')->get();
                $idiomas = DB::table('idiomas')->get();
                $publicidade = DB::table('curriculum_publicidade')->get();
                $nivelHierarquico = DB::table('candidatoNivelHierarquico')->get();
    
                return view('dashboard.candidatos.cadastraCv', array('candidato' => $candidato,
                                                                        'cddIdiomas' => $cddIdiomas,
                                                                        'relatorios' => $relatorios,
                                                                        'estados' => $estados,
                                                                        'cidades' => $cidades,
                                                                        'formacoes' => $formacoes,
                                                                        'statusForm' => $statusForm,
                                                                        'idiomas' => $idiomas,
                                                                        'publicidade' => $publicidade,
                                                                        'cddExperiencia' => $cddExperiencia,
                                                                        'nivelHierarquico' =>  $nivelHierarquico));

            }
            catch(\Exception $e)
            {
                return redirect()->route('dashboardCandidato')->withErrors("Erro ".$e->getMessage());
            }
            
        }
        else
        {
            return redirect('candidatos');
        }
    }

    public function editaCv(Request $request)
    {
        if(auth('cdd')->check())
        {
            try
            {
              
                Candidatos::editCandidato(auth('cdd')->user(), $request);
                Session::flash('success', 'Curriculum cadastrado com sucesso.');
                return redirect()->back();
            }
            catch(\Exception $e)
            {
                Session::flash('error', 'Ocorreu um erro.'.$e->getMessage());
                return redirect()->back();
            }
            
        }
        else
        {
            return redirect('candidatos');
        }
         

    }

    public function login(Request $request)
    {
        $user = [
            'email' => $request->email,
            'password'=> $request->senha
        ];

        if(auth('cdd')->attempt($user))
        {
            return redirect()->back();
        }

        Session::flash('error', 'Usuário ou senha incorreto.');
        return redirect()->back();
    }

    public function logoutCdd()
    {
        auth('cdd')->logout();
        return redirect()->route('candidatos');
    }

    public function dashboard()
    {
        return view('dashboard.candidatos.index', array('user' => auth('cdd')->user()));
    }

    //Mostra as Vagas para o Candidato
    public function vagas()
    {
        if(auth('cdd')->check())
        {
            $cdd = Auth('cdd')->user();
            if(empty($cdd->celular) || empty($cdd->nascimento) || empty($cdd->endereco)
                || empty($cdd->cidade))
            {
                return redirect()->route('editarCdd')->withErrors('É necessário completar seu cadastro antes de se candidatar à uma vaga.');
            }

            $vagas = DB::table('vagas as v')->select('v.*', 'cg.cargo', 'ct.CT_NOME', 'emp.nome_fantasia', 'est.*', 'seg.segmento')
                ->join('empresas as emp', 'emp.id', 'v.empresa')
                ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
                ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
                ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->join('segmentos as seg', 'seg.id', 'emp.segmento')->get();
            
            $regimeContratacao = DB::table('regime_contratacao')->get();
            $segmentos = DB::table("segmentos")->get();

            return view('dashboard.candidatos.vagas', array('vagas' => $vagas, 'segmentos' => $segmentos, 'regimeContratacao' => $regimeContratacao));
        }

        return redirect('candidatos');
    }

    // Candidato pode filtrar vagas
    public function filtroVagas(Request $request)
    { 
        if(auth('cdd')->check())
        {
            $segmentos = DB::table("segmentos")->get();
            $regimeContratacao = DB::table('regime_contratacao')->get();
            $vagas = DB::table('vagas as v')
            ->select('v.*', 'cg.cargo', 'emp.segmento', 'emp.nome_fantasia', 'ct.CT_NOME', 'est.UF_UF', 'fee.fee')
            ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
            ->join('fee_recrutamento as fee', 'fee.id', 'v.feeRecrutamento')
            ->join('empresas as emp', 'emp.id', 'v.empresa')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where(function ($query) use($request)
            {
               
                if($request->areaAtuacao)
                    $query->where('emp.segmento', $request->areaAtuacao);
                if($request->salario && moneyToInt($request->salario) > 100)
                    $query->where('v.salario', '<=', moneyToInt($request->salario));
                    // $query->where('v.naoExibirSalario', 0);
                if($request->regime)
                    $query->where('v.regime_contratacao', $request->regime);
                if($request->localidade)
                {
                    $cidade = DB::table('cidades')->select('CT_ID')->where('CT_NOME', 'like',  '%'.urlencode($request->localidade).'%')->get();
                    if(sizeof($cidade))
                    {
                        for ($i=0; $i < sizeof($cidade) ; $i++)
                        { 
                            if($i == 0)
                                $query->where('emp.endereco_cidade', $cidade[$i]->CT_ID);
                            else
                                $query->orWhere('emp.endereco_cidade', $cidade[$i]->CT_ID);
                        }
                    }                   
                }
                if($request->idCargo)
                    $query->where('v.tituloCargo', $request->idCargo);
            })
            ->get();
            return view('dashboard.candidatos.vagas', array('vagas' => $vagas, 'segmentos' => $segmentos, 'regimeContratacao' => $regimeContratacao));
        }
        return redirect('candidatos');
    }

    /**
    * classe pela qual um candidato irá se candidatar a uma vaga
    */
    public function candidatoVaga($vaga)
    {   
       if(auth('cdd')->check())
       {
           //Verifica se o candidato já não se candidatou para a vaga;
            $procura = DB::table('candidato_vagas')
                                ->where('candidato', auth('cdd')->id())
                                ->where('vaga', $vaga)->count();
            if($procura)
            {
                echo "existe";
                die;
            }
            //Vincula o candidato à vaga
           $cddVaga = new CandidatoVaga();
           $cddVaga->candidato = auth('cdd')->id();
           $cddVaga->vaga = $vaga;
           if($cddVaga->save())
           {
               echo "OK";
           }
       }
    }

    public function servicos()
    {
        return view('dashboard.candidatos.servicos');
    }

    public function cancelarConta()
    {
        if(auth('cdd')->check())
        {
            return view('dashboard.candidatos.cancelarconta');
        }
    }

    public function destroy()
    {
        if(auth('cdd')->check())
        {
            Candidato::destroy(auth('cdd')->id());
            return redirect()->route('logoutCdd');
        }

        return redirect('candidatos');
    }

}
