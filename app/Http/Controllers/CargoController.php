<?php

namespace App\Http\Controllers;

use App\Cargo;
use Illuminate\Http\Request;

class CargoController extends Controller
{

    public function getCargos($cargo)
    {
        $cargos = Cargo::select('cargo', 'id')->where('cargo', 'like', '%'.urlencode($cargo).'%')->get();
        $array = array();
	    if(sizeof($cargos))
	    {   
		    for ($i=0; $i < sizeof($cargos) ; $i++)
		    { 
		    	array_push($array, urldecode($cargos[$i]['cargo']));
		    }
        }
        
        header('Content-Type: application/json');
	    echo json_encode($array);


    }

    public function getIdCargo($cargo)
    {
        $cargo = Cargo::select('id')->where('cargo', urlencode($cargo))->first();
        if(!empty($cargo->id))
        {
            return $cargo->id;
        }
        return 0;
        
    }


}
