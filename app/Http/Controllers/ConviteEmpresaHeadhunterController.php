<?php

namespace App\Http\Controllers;

use App\VagaHeadhunter;
use App\ConviteEmpresaHeadhunter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use Session;


class ConviteEmpresaHeadhunterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($vaga, $headhunter)
    {

        if(auth('emp')->check())
        {
            $convite = ConviteEmpresaHeadhunter::where('vaga', $vaga)
                                    ->where('headhunter', $headhunter)
                                    ->where('empresa', auth('emp')->user()->empresa)
                                    ->first();
            if(!empty($convite->id))
            {
                echo "existe";
                die;
            }

            $convite = new ConviteEmpresaHeadhunter();
            $convite->empresa = auth("emp")->user()->empresa;
            $convite->headhunter = $headhunter;
            $convite->vaga = $vaga;
            $convite->status_convite = 0;
            
            if($convite->save())
            {
                emailConviteHeadhunter($convite->id);
                echo 'ok';
            }
            else
            {
                echo 0;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ConviteEmpresaHeadhunter  $conviteEmpresaHeadhunter
     * @return \Illuminate\Http\Response
     */
    public function show(ConviteEmpresaHeadhunter $conviteEmpresaHeadhunter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ConviteEmpresaHeadhunter  $conviteEmpresaHeadhunter
     * @return \Illuminate\Http\Response
     */
    public function edit(ConviteEmpresaHeadhunter $conviteEmpresaHeadhunter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ConviteEmpresaHeadhunter  $conviteEmpresaHeadhunter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConviteEmpresaHeadhunter $conviteEmpresaHeadhunter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConviteEmpresaHeadhunter  $conviteEmpresaHeadhunter
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConviteEmpresaHeadhunter $conviteEmpresaHeadhunter)
    {
        //
    }

    public function getConvites()
    {
        if(Auth('head')->check())
        {
            $convites = DB::table('convite_empresa_headhunters as conv')
                        ->select('cg.cargo', 'conv.*', 'emp.nome_fantasia as empresa', 'ct.CT_NOME as cidade', 'est.UF_UF as estado')
                        ->join('empresas as emp', 'emp.id', 'conv.empresa')
                        ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
                        ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
                        ->join('vagas as v', 'v.id', 'conv.vaga')
                        ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
                        ->where('headhunter', Auth('head')->id())
                        ->where('status_convite', 0)->get();
            return view('dashboard.headhunter.mensagens-caixa-entrada', array('convites' => $convites));
        }
    }

    public function aceitaConvite($convite)
    {
        if(Auth('head')->check())
        {
            $convite = ConviteEmpresaHeadhunter::find($convite);
            $convite->status_convite = 1;
            if($convite->save())
            {
                $vaga = new VagaHeadhunter();
                $vaga->vaga = $convite->vaga;
                $vaga->headhunter = Auth('head')->id();
                $vaga->criado = 0;
                if($vaga->save())
                {
                    respostaConvite($convite->id);
                    echo "ok";
                }
                
            }
        }
    }

    public function recusaConvite($convite)
    {
        if(Auth('head')->check())
        {
            $convite = ConviteEmpresaHeadhunter::find($convite);
            $convite->status_convite = 2;
            if($convite->save())
            {
                respostaConvite($convite->id);
                echo "ok";
            }
        }
    }

    public function mostaHeadhunters($vaga)
    {
        $headhunters  = DB::table('headhunters as head')
            ->whereNotIn('head.id', function($query) use ($vaga)
            {
                $query->select('headhunter')->from('fila_candidatura')->where('vaga', $vaga);
            })->get();
        return view('dashboard.empresas.listaheadhuntersconvite', array('headhunters' => $headhunters));
    }
}
