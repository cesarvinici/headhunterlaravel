<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;


class EmailController extends Controller
{
    public function vagasVencendoEmpresa($hash)
    {
        
        //acd605afae83e8a91a133a1447c9dd1662ee24be
        if($hash == sha1('4rk4nSyst3m!@#$'))
        {
            $vaga = array(
                'id' => '1',
                'cargo' => 'Programador Laravel',
                'data_limite' => '2018-06-21',
            );
            return view('emails.vagasVencendo', array('vaga' => $vaga));
        }
       return redirect('/');
      
    }
}
