<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\CustomClass\Empresas\AddEmpresa;
use App\Empresa;
use App\EmpresaUsuario;
use Illuminate\Http\Request;
use App\Notifications\EnviaEmail;
use Session;
use Mail;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth('emp')->check())
        { 
            return view('dashboard.empresas.index', array('user' => Auth('emp')->user()));
        }
        else
        {
            return redirect('empresas');
        }        

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth('emp')->check())
        {
            $estados = DB::table('estados')->get();            
            $segmentos = DB::table('segmentos')->orderBy('id')->get();
            $nFunc = DB::table('num_funcionarios')->orderBy('id')->get();

            
            if(empty(Auth('emp')->user()->empresa))
                return view('dashboard.empresas.empresaAdd', array( 'estados' => $estados,
                                                                    'segmentos' => $segmentos,
                                                                    'nFuncionarios' =>  $nFunc));
            else
            {
                $empresa = DB::table('empresas as emp')
                    ->select('emp.*', 'est.UF_ID as estado')
                    ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
                    ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
                    ->where('emp.id', Auth('emp')->user()->empresa)->first();
                $cidades = DB::table('cidades')->where('CT_UF', $empresa->estado)->get();
                return view('dashboard.empresas.empresaEdit',  array( 'estados' => $estados,
                                                                    'segmentos' => $segmentos,
                                                                    'nFuncionarios' =>  $nFunc,
                                                                    'empresa' => $empresa,
                                                                'cidades' => $cidades));
            }
            
        }
        else
        {
            return redirect('empresas');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth('emp')->check())
        {
             // Valida os dados recebidos no formulário
             $this->validate($request, [
                'razaoSocial' => 'required',
                'nomeFantasia' => 'required',
                'cnpj' => 'required|unique:empresas|size:18',
                'cidade' => 'required',
                'nomeResp' => 'required',
                'cargoResp' => 'required', 
                                 
            ]);

            try
            {
                $empresa = new Empresa();
                $addEmpresa = new AddEmpresa();
                $empresa = $addEmpresa->empresaAdd($empresa, $request);
                if($empresa->id)
                {
                    $empresaUsuario = EmpresaUsuario::find(auth('emp')->id());
                    $empresaUsuario->empresa = $empresa->id;
                    $empresaUsuario->save();
                    Session::flash('success', 'Empresa cadastrada com sucesso!');
                    return redirect()->back();    
                }
                
                Session::flash('error', 'Empresa cadastrada com sucesso, porém houve um erro ao cadastrar o logotipo');
                return redirect()->back();
            }
            catch(\Exception $e)
            {
                
                Session::flash('error', 'Ocorreu um erro, '.$e->getMessage());
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function show(Empresa $empresa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function edit(Empresa $empresa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empresa $empresa)
    {
        if(Auth('emp')->check())
        {
            $empresa = Empresa::find(auth('emp')->user()->empresa);
            
            // Valida os dados recebidos no formulário
             $this->validate($request, [
                'razaoSocial' => 'required',
                'nomeFantasia' => 'required',
                'cnpj' => 'required|unique:empresas,cnpj,'.Auth('emp')->user()->empresa.'|size:18',
                'nomeResp' => 'required',
                'cargoResp' => 'required',                            
            ]);
            
            try
            {
                $addEmpresa = new AddEmpresa();
                $empresa = $addEmpresa->empresaAdd($empresa, $request);
                if($empresa->id)
                {
                    SESSION::flash('success', 'Empresa editada com sucesso!');
                    return redirect()->back();
    
                }
                SESSION::flash('error', 'Ocorreu um erro: '.$e->getMessage());
                return redirect()->back();
            }
            catch(\Exception $e)
            {
                SESSION::flash('error', 'Ocorreu um erro: '.$e->getMessage());
                return redirect()->back();
            }           
        }
        else
        {
            return redirect('empresas');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }

    public function pageEmpresas()
    {
        if(Auth('emp')->check())
        {
            return redirect('dashboard/empresa');
        }
        else
        {
            return view('page-empresas');
        }
    }

    public function buscaInfoEmpresa($id)
    {
        if(Auth('head')->check())
        {
            $empresa = DB::table('empresas as emp')
                ->select('ct.CT_NOME as cidade', 'est.UF_NOME as estado', 'seg.segmento')
                ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
                ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->join('segmentos as seg', 'seg.id', 'emp.segmento')
                ->where('emp.id', $id)->first();
            echo urldecode(json_encode($empresa));
        }
    }
}
