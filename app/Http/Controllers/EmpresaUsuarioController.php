<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use App\EmpresaUsuario;
use Session;


class EmpresaUsuarioController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth('emp')->check())
        {
            if(empty(Auth('emp')->user()->empresa))
            {
                Session::flash("error", 'É necessário realizar o cadastro da empresa antes de adicionar um novo usuário');
                return redirect('/dashboard/empresa/cadastro-empresa');
            }

            $usuarios = EmpresaUsuario::where('id', '<>', Auth('emp')->id())
            ->where('empresa', Auth('emp')->user()->empresa)->get();


            return view('dashboard.empresas.usuariosConta', array('usuarios' => $usuarios));
        }

        return redirect('empresas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cadastro.cadastroEmpresa');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'email' => 'required|
                unique:headhunters|
                unique:candidatos|
                unique:empresas_usuarios|
                unique:empresas,responsavel_financeiro_email|
                unique:headhunters_usuarios',       
            'novaSenha'=> 'required|min:6']);
        
        try
        {
            $usuario = new EmpresaUsuario();
            $usuario->nome = $request->nome;
            $usuario->email = $request->email;
            
            if($request->novaSenha == $request->novaSenhaC)
            {
                $usuario->password = Hash::make($request->novaSenha);
            }
            else
            {
                Session::flash('error', 'As senhas não conferem, tente novamente.');
                return redirect()->back();
            }

            if(auth('emp')->check())
            {
                //Caso o usuário seja criado por alguém logado a execução do código para aqui
                $usuario->cargo = $request->cargo;
                $usuario->tipo = 2;
                $usuario->empresa = Auth('emp')->user()->empresa;
                $usuario->save();

                if($request->hasFile('imgPerfil'))
                {                    
                    $imagem = $request->file('imgPerfil');
                    $nomearquivo  = $usuario->id.".". $imagem->getClientOriginalExtension();
                    $request->file('imgPerfil')->move(public_path('./admin/upload/empresas/'), $nomearquivo);
                    $usuario->imagem_perfil = $nomearquivo;                
                }

                $usuario->save();
                Session::flash('success', 'Usuário cadastrado com sucesso.');
                return redirect()->back();
            }
            
            $usuario->save();

            $user =
            [
                'email' => $usuario->email,
                'password' => $request->novaSenha,
            ];

            if(Auth('emp')->attempt($user))
            {
                Session::flash('cadEmpresa_ok', '');
                return redirect()->back();
            }
      
        }
        catch(\Exception $e)
        {
            //dd($e);
            Session::flash('error', 'Ocorreu um erro, '.$e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        if(Auth('emp')->check())
        {
            return view('dashboard.empresas.meus-dados', array('user' => Auth('emp')->user()));
        }

        return redirect("empresas");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(auth('emp')->check())
        {
            $this->validate($request, [
                'nome' => 'required',
                'email' => 'required|
                    unique:headhunters|
                    unique:candidatos|
                    unique:empresas_usuarios,email,'.Auth('emp')->id().'|
                    unique:empresas,responsavel_financeiro_email|
                    unique:headhunters_usuarios',
                ]);            
                

            try
            {
                $usuario = EmpresaUsuario::find(Auth('emp')->id());
                $usuario->nome = $request->nome;
                $usuario->email = $request->email;
                $usuario->cargo = $request->cargo;
                $usuario->tipo = $request->tipoUsuario;                
                
                if($request->hasFile('imgPerfil'))
                {
                    
                    $imagem = $request->file('imgPerfil');
                    $nomearquivo  = Auth('emp')->id().".". $imagem->getClientOriginalExtension();
                    $request->file('imgPerfil')->move(public_path('./admin/upload/empresas/'), $nomearquivo);
                    $usuario->imagem_perfil = $nomearquivo;
                
                }
                
                if($request->senhaAtual)
                {   
                    if(Hash::check($request->senhaAtual, Auth('emp')->user()->password))
                    {
                        $this->validate($request, [
                            'novaSenha' => 'required|min:6',
                            'novaSenhaC' => 'required|min:6',
                            ]);  

                        if(($request->novaSenha && $request->novaSenhaC)  && $request->novaSenha == $request->novaSenhaC)
                        {
                            $usuario->password = Hash::make($request->novaSenha);
                        }
                        else
                        {
                            Session::flash('error', 'As senhas não conferem, tente novamente.');
                            return redirect()->back();
                        }

                    }
                    else
                    {
                        Session::flash('error', 'Senha incorreta, tente novamente.');
                        return redirect()->back();
                    }
                }
                $usuario->save();
                Session::flash('success', "Cadastro editado com sucesso");
                return redirect()->back();

            }
            catch(Exception $e)
            {
                Session::flash('error', 'Ocorreu um erro, '.$e->getMessage());
                return redirect()->back();
            }

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       if(auth('emp')->check())
       {
           if(auth('emp')->user()->tipo == 1)
           {    
                $usuario = EmpresaUsuario::find($id);
                $usuario->delete();
                Session::flash('success', 'Usuário removido com sucesso.');
           }
           else
           {
                Session::flash('error', 'Você não tem permissão para remover usuários.');                
           }
            return redirect()->back();
       }

       return redirect('empresas');
    }

    public function logout()
    {
        Auth('emp')->logout();
        return redirect('empresas');        
    }

    public function loginUsuario(Request $request)
    {
        $user = [
            'email' => $request->login,
            'password' => $request->senha,
        ];

        if(Auth('emp')->attempt($user))
        {
            return redirect('dashboard/empresa');
        }
        else
        {
            Session::flash('error', 'Email ou senha incorreto, tente novamente');
            return redirect()->back();
        }

    }

    public function editaUsuario($id)
    {
        if(Auth('emp')->check())
        {   
            $user = EmpresaUsuario::find($id);
            return view('dashboard.empresas.usuarioEdit', array('user' => $user));
        }

        return redirect("empresas");
    }

    public function updateUsuario(Request $request)
    {
        if(auth('emp')->check())
        {
            $this->validate($request, [
                'nome' => 'required',
                'email' => 'required|
                    unique:headhunters|
                    unique:candidatos|
                    unique:empresas_usuarios,email,'.$request->usuario.'|
                    unique:empresas,responsavel_financeiro_email|
                    unique:headhunters_usuarios',
                ]);          

            try
            {
                $usuario = EmpresaUsuario::find($request->usuario);
                $usuario->nome = $request->nome;
                $usuario->email = $request->email;
                $usuario->cargo = $request->cargo;            
                
                if($request->hasFile('imgPerfil'))
                {                    
                    $imagem = $request->file('imgPerfil');
                    $nomearquivo  = $usuario->id.".". $imagem->getClientOriginalExtension();
                    $request->file('imgPerfil')->move(public_path('./admin/upload/empresas/'), $nomearquivo);
                    $usuario->imagem_perfil = $nomearquivo;
                }              
               
                if(isset($request->novaSenha))
                {
                    if($request->novaSenha == $request->novaSenhaC)
                    {
                        $usuario->password = Hash::make($request->novaSenha);
                    }
                    else
                    {
                        echo "erro";
                        Session::flash('error', 'As senhas não conferem, tente novamente.');
                        return redirect()->back();
                    }         
                }                        
                $usuario->save();
                Session::flash('success', "Cadastro editado com sucesso");
                return redirect()->back();
            }
            catch(Exception $e)
            {
                Session::flash('error', 'Ocorreu um erro, '.$e->getMessage());
                return redirect()->back();
            }
        }
        return redirect('empresas');
    }
}
