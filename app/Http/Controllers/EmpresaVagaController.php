<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\CustomClass\Vagas\Vagas;
use App\VagaIdioma;
use App\VagaAptidao;
use App\Vaga;
use App\EvolucaoCandidato;
use Illuminate\Http\Request;
use Session;


class EmpresaVagaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth('emp')->check())
        {
            $vagas = DB::table('vagas as v')->select('v.*', 'cg.cargo', 'ct.CT_NOME as cidade', 'est.UF_UF as estado')
            ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
            ->join('empresas as emp', 'v.empresa', 'emp.id')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where('empresa', auth('emp')->user()->empresa)
            ->where('status_vaga', 1)->get();
            $etapas = DB::table('etapas_processo_seletivo')->get();
            return view('dashboard.empresas.minhasVagas', array('vagas' => $vagas, 'etapas' => $etapas));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function show(Vaga $vaga)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function edit(Vaga $vaga)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vaga $vaga)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vaga $vaga)
    {
        //
    }

    public function inviteRecrut()
    {
        if(auth('emp')->check())
        {   
            $vagas = DB::table('vagas as v')->select('v.*', 'cg.cargo', 'ct.CT_NOME as cidade', 'est.UF_UF as estado')
            ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
            ->join('empresas as emp', 'v.empresa', 'emp.id')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where('empresa', auth('emp')->user()->empresa)
            ->where('status_vaga', 1)->get();
            return view('dashboard.empresas.convidar-recrutador', array('vagas' => $vagas));
        }

        return redirect("empresas");
    }

    public function filtraParaConvite(Request $request)
    {
        if(auth('emp')->check())
        {
            $vagas = DB::table('vagas as v')->select('v.*', 'cg.cargo', 'ct.CT_NOME as cidade', 'est.UF_UF as estado')
            ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
            ->join('empresas as emp', 'v.empresa', 'emp.id')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where('empresa', auth('emp')->user()->empresa)
            ->where(function($query) use ($request)
            {
                if($request->has('idCargo'))
                {
                    $query->where('v.tituloCargo', $request->idCargo);
                }
            })->where('status_vaga', 1)->get();
            return view('dashboard.empresas.convidar-recrutador', array('vagas' => $vagas));
        }
        return redirect("empresas");
    }

    public function evolucaoCdd(Request $request)
    {
        if(auth('emp')->check())
        {  
           $obs = $request->aux;
           $evolucao = new EvolucaoCandidato();
           $evolucao->etapa = $request->etapa;
           $evolucao->data = dataToMySql($request->data);
           $evolucao->status = $request->status;
           $evolucao->observacoes = $request->input('obs'.$obs);
           $evolucao->candidato = $request->cdd;
           $evolucao->vaga = $request->vaga;
           $evolucao->headhunter = $request->hh;
           if($evolucao->save())
            {
                // Apos editar evolução será enviado um email para o headhunter responsável
                $email = DB::table('evolucao_candidatos as ec')
                ->select('ec.*', 'cdd.nome as candidato', 'emp.nome_fantasia as empresa', 'cg.cargo' ,'head.email', 'etapa.etapa')
                ->join('headhunters as head', 'ec.headhunter', 'head.id')
                ->join('candidatos as cdd', 'cdd.id', 'ec.candidato')
                ->join('vagas as v', 'v.id', 'ec.vaga')
                ->join('empresas as emp', 'emp.id', 'v.empresa')
                ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
                ->join('etapas_processo_seletivo as etapa', 'etapa.id', 'ec.etapa')
                ->where('ec.id', $evolucao->id)->first();                
                 emailEvolucaoCandidato($email);
                echo "ok";
           }
           
        }
    }
}
