<?php

namespace App\Http\Controllers;

use App\FilaCandidatura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FilaCandidaturaController extends Controller
{
    public function mostraCandidatosVaga($vaga)
    {
        if(auth('head')->check())
        {
            $candidatos = carregaCandidatosdaFila($vaga);

            return view('dashboard.headhunter.filaCdd', array('candidatos' => $candidatos)); 
        }
    }

    //Mostra todos os candidatos que ainda não estão participando de nenhum processo
    public function filtroFilaCdd(Request $request)
    {        

        if(auth('head')->check())
       {    
        //    echo "<pre>";
        //    print_r($request->input());
        //    echo "</pre>";
           
            if($request->input('candidatos'))
            {
                $candidatos = DB::table('headhunter_candidatos as hc')
                ->select('cdd.*', 'ct.CT_NOME', 'est.UF_UF')
                ->join('candidatos as cdd', 'hc.candidato', 'cdd.id')
                ->leftJoin('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
                ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->where(function ($query) use ($request)
                {
                    if($request->pessoas)
                        $query->where('cdd.nome', 'like', '%'.$request->pessoas.'%');
                    if($request->formacao)
                        $query->where('cdd.escolaridade', $request->formacao);
                    if($request->estado)
                        $query->where('est.UF_ID', $request->estado);
                    if($request->cidade)
                        $query->where('ct.CT_ID', $request->cidade);
                })
                ->where(function($query) use ($request)
                {                
                    $query->WhereIn('cdd.id', function($sql) use ($request)
                    {
                        $sql->select('candidato')->from('candidato_vagas')->where('vaga', $request->idvaga);
                    });

                    $query->orWhereIn('cdd.id', function($sql)
                    {
                        $sql->select("candidato")->from('headhunter_candidatos');
                    });

                })->whereNotIn('cdd.id', function ($query) use ($request)
                {
                    $query->select('candidato')->from('fila_candidatura')->where('vaga', $request->idvaga);
                })
                ->where('hc.headhunter', Auth('head')->id())->get();

                return view('dashboard.headhunter.filaGeral', array('candidatos' => $candidatos));   
            }else
            {
                $candidatos = DB::table('candidatos as cdd')
                ->select('cdd.*', 'ct.CT_NOME', 'est.UF_UF')
                ->leftJoin('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
                ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->where(function ($query) use ($request)
                {
                    if($request->pessoas)
                        $query->where('cdd.nome', 'like', '%'.$request->pessoas.'%');
                    if($request->formacao)
                        $query->where('cdd.escolaridade', $request->formacao);
                    if($request->estado)
                        $query->where('est.UF_ID', $request->estado);
                    if($request->cidade)
                        $query->where('ct.CT_ID', $request->cidade);
                })
               ->where(function($query) use ($request)
                {                
                    $query->whereIn('cdd.id', function($sql) use ($request)
                    {
                        $sql->select('candidato')->from('candidato_vagas')->where('vaga', $request->idvaga);
                    });

                    $query->orWhereIn('cdd.id', function($sql)
                    {
                        $sql->select("candidato")->from('headhunter_candidatos');
                    });
                    
                })->whereNotIn('cdd.id', function ($query) use ($request)
                {
                    $query->select('candidato')->from('fila_candidatura')->where('vaga', $request->idvaga);
                })->get();
                
                return view('dashboard.headhunter.filaGeral', array('candidatos' => $candidatos)); 
            }                  
        }        
    }

    // Retorna os candidatos participando de uma vaga espeficida
    public function filtroCddParticipantes(Request $request)
    {        
        if(auth('head')->check())
        {
            //Filtro caso esteja selecionada a opção 'meus candidatos'
            if($request->input('candidatos'))
            {
                $candidatos = DB::table('fila_candidatura as fc')
                ->select('cdd.*', 'ct.CT_NOME', 'est.UF_UF')
                ->join('candidatos as cdd', 'fc.candidato', 'cdd.id')
                ->leftJoin('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
                ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->where(function ($query) use ($request)
                {
                    if($request->pessoas)
                        $query->where('cdd.nome', 'like', '%'.$request->pessoas.'%');
                    if($request->formacao)
                        $query->where('cdd.escolaridade', $request->formacao);
                    if($request->estado)
                        $query->where('est.UF_ID', $request->estado);
                    if($request->cidade)
                        $query->where('ct.CT_ID', $request->cidade);
                })
                ->where('vaga', $request->idvaga)
                ->where('fc.headhunter', Auth('head')->id())->get();
                return view('dashboard.headhunter.filaCdd', array('candidatos' => $candidatos));   
            }
            //filtro da fila geral
            else
            {
                $candidatos = DB::table('fila_candidatura as fc')
                ->select('cdd.*', 'ct.CT_NOME', 'est.UF_UF')
                ->join('candidatos as cdd', 'cdd.id', 'fc.candidato')
                ->leftJoin('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
                ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->where(function ($query) use ($request)
                {
                    if($request->pessoas)
                        $query->where('cdd.nome', 'like', '%'.$request->pessoas.'%');
                    if($request->formacao)
                        $query->where('cdd.escolaridade', $request->formacao);
                    if($request->estado)
                        $query->where('est.UF_ID', $request->estado);
                    if($request->cidade)
                        $query->where('ct.CT_ID', $request->cidade);
                })
                ->where('vaga', $request->idvaga)->get();

                return view('dashboard.headhunter.filaCdd', array('candidatos' => $candidatos)); 
            }
        }
    }

    public function mostraCddAnexos($id)
    {
        if(auth('head')->check())
        {
            $candidatos = DB::table('fila_candidatura as fc')
                ->select('cdd.*', 'ct.CT_NOME', 'est.UF_UF', 'fc.created_at as adicionado_em')
                ->join('candidatos as cdd', 'cdd.id', 'fc.candidato')
                ->leftJoin('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
                ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->where('fc.vaga', $id)->get();

                return view('dashboard.headhunter.anexosFila', array('candidatos' => $candidatos));
        }
    }

    public function filtroCddAnexos(Request $request)
    {       
        if(auth('head')->check())
        {
            if($request->input('candidatos'))
            {
                $candidatos = DB::table('fila_candidatura as fc')
                ->select('cdd.*', 'ct.CT_NOME', 'est.UF_UF', 'fc.created_at as adicionado_em')
                ->join('candidatos as cdd', 'cdd.id', 'fc.candidato')
                ->leftJoin('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
                ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->where(function ($query) use ($request)
                {
                    if($request->pessoas)
                        $query->where('cdd.nome', 'like', '%'.$request->pessoas.'%');
                    if($request->formacao)
                        $query->where('cdd.escolaridade', $request->formacao);
                    if($request->estado)
                        $query->where('est.UF_ID', $request->estado);
                    if($request->cidade)
                        $query->where('ct.CT_ID', $request->cidade);
                })
                ->where('fc.vaga', $request->idvaga)
                ->where('fc.headhunter', Auth('head')->id())->get();
                return view('dashboard.headhunter.anexosFila', array('candidatos' => $candidatos));   
            }else
            {
                $candidatos = DB::table('fila_candidatura as fc')
                ->select('cdd.*', 'ct.CT_NOME', 'est.UF_UF', 'fc.created_at as adicionado_em')
                ->join('candidatos as cdd', 'cdd.id', 'fc.candidato')
                ->leftJoin('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
                ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->where(function ($query) use ($request)
                {
                    if($request->pessoas)
                        $query->where('cdd.nome', 'like', '%'.$request->pessoas.'%');
                    if($request->formacao)
                        $query->where('cdd.escolaridade', $request->formacao);
                    if($request->estado)
                        $query->where('est.UF_ID', $request->estado);
                    if($request->cidade)
                        $query->where('ct.CT_ID', $request->cidade);
                })
                ->where('fc.vaga', $request->idvaga)->get();
                
                return view('dashboard.headhunter.anexosFila', array('candidatos' => $candidatos)); 
            }                  

        }
        

    }



    public function addCddFila($vaga, $candidato)
    {
        if(auth('head')->check())
        {   
            // Verifica se candidato já não está na fila
            $existe = DB::table('fila_candidatura')->where('candidato', $candidato)->where('vaga', $vaga)->count();
           
            if(!$existe)
            {
                $fila = new FilaCandidatura();
                $fila->vaga = $vaga;
                $fila->candidato = $candidato;
                $fila->headhunter = auth('head')->id();
                $fila->save();
                echo 1;
            }
            echo 0;    
            
           
        }
    }

}
