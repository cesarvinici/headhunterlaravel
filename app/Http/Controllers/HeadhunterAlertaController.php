<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\HeadhunterAlerta;
use Illuminate\Http\Request;
use App\HeadhunterAlertaStatusVaga;

class HeadhunterAlertaController extends Controller
{
    public function criaAlerta(Request $request)
    {
        if(Auth('head')->check())
        {
            //Verifica se o headhunter quer receber alerta sempre que for alterado status da vaga
            if($request->alertaStatusVaga && $request->alertaStatusVaga != 0)
            {
                // Caso esteja selecionado, verifica se este alerta já existe
                if(!verificaAlertaStatus($request->idvaga, Auth('head')->id()))
                {
                    //Salva o alerta
                    $alerta = new HeadhunterAlertaStatusVaga();
                    $alerta->recebeAlerta = 1;
                    $alerta->headhunter = Auth('head')->id();
                    $alerta->vaga = $request->idvaga;
                    $alerta->save();                    
                }
            }
            else
            {
                /**
                 * Caso existe um registro no banco de alerta de status, mas não veio a informação 
                 * no formulário ou veio como 0, significa que o headhunter quer cancelar o alerta.
                 */
                if(verificaAlertaStatus($request->idvaga, Auth('head')->id()))
                {
                    $apaga = HeadhunterAlertaStatusVaga::where('headhunter', Auth('head')->id())
                                                        ->where('vaga', $request->idvaga)->first();
                    $apaga->delete();
                }
            }   

            /**
             * Se a váriavel editaAlerta existir é porque já existiam alertas criados 
             * no banco de dados
             */
            if(isset($request->editaAlerta))
            {
                $db = getAlertas(auth('head')->id(), $request->idvaga);
                /**
                 * Caso a váriavel $db seja maior que $request->alerta, significa que algum alerta foi desativado
                 * e precisa ser excluido da base de dados
                */
                if(sizeof($db) && !isset($request->alerta) || sizeof($db) > sizeof($request->alerta))
                {   
                    //Separa a informação alerta do objeto $db para poder comparar no if abaixo
                     $array = [];
                     foreach($db as $aux)
                     {
                         array_push($array, $aux->alerta);
                     }                     

                    foreach($array as $aux)
                    {   
                        /**
                        * Se a váriavel $request->alerta estiver vazia significa que o usuário 
                        * desativou as notificações
                        * */ 
                        if(empty($request->alerta))
                        {
                            HeadhunterAlerta::where('headhunter', auth('head')->id())
                                ->where('vaga', $request->idvaga)->delete();
                        } 
                        // O alerta que não estiver em $request->alerta será excluído do banco                    
                        elseif(!in_array($aux, $request->alerta))
                        { 
                            HeadhunterAlerta::where('alerta', $aux)
                                ->where('headhunter', auth('head')->id())
                                ->where('vaga', $request->idvaga)->delete();
                        } 
                    }
                }
                /**
                 * Caso a váriavel $request->alerta seja maior que $db significa que 
                 * o usuário criou um novo alerta
                 */
                elseif (sizeof($db) < sizeof($request->alerta))
                {
                    //Separa a informação alerta do objeto $db para poder comparar no if abaixo
                    $array = [];
                    foreach($db as $aux)
                    {
                        array_push($array, $aux->alerta);
                    }

                    foreach ($request->alerta as $alerta)
                    {   
                        // Se não existir o $alerta no array ele precisa ser adicionado ao banco
                        if(!in_array($alerta, $array))
                        {
                            $alert = new HeadhunterAlerta();
                            $alert->headhunter = Auth('head')->id();
                            $alert->vaga = $request->idvaga;
                            $alert->alerta = $alerta;
                            $alert->save();
                        }
                    }
                }
            }
            /**
             * Se editaAlerta não existir é porque será criado um novo alerta para o headhunter 
             */	
            elseif($request->alerta)
            {               
                foreach ($request->alerta as $alerta)
                {
                    // Pega os dados do formulário
                    $alert = new HeadhunterAlerta();
                    $alert->headhunter = Auth('head')->id();
                    $alert->vaga = $request->idvaga;
                    $alert->alerta = $alerta;
                    $alert->save();
                }
            }
            echo "ok";
        }
        else
        {
            echo "Favor realizar login no sistema";
        }

    }
}
