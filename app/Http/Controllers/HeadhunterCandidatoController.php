<?php

namespace App\Http\Controllers;

use App\HeadhunterCandidato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\CustomClass\Uteis;
use App\CandidatoIdioma;
use App\Candidato;
use App\CandidatoRelatorio;
use App\FilaCandidatura;
use App\CustomClass\Candidatos\Candidatos;
use Session;


class HeadhunterCandidatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidatos = DB::table('headhunter_candidatos as hc')->select('cdd.id', 'cdd.nome', 'cdd.email')
                        ->join('candidatos as cdd', 'cdd.id', 'hc.candidato')
                        ->where('hc.headhunter', auth('head')->id())->get();

        return view('dashboard.headhunter.candidatos', array('candidatos' => $candidatos));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth('head')->check())
        {
            $estados = DB::table('estados')->get();
            $formacoes = DB::table('formacao_nivel')->get();
            $statusForm = DB::table('formacao_status')->get();
            $idiomas = DB::table('idiomas')->get();
            $publicidade = DB::table('curriculum_publicidade')->get();
            $nivelHierarquico = DB::table('candidatoNivelHierarquico')->get();
            return view('dashboard.headhunter.candidatoAdd', array('estados' => $estados,
                                                                   'formacoes' => $formacoes,
                                                                   'statusForm' => $statusForm,
                                                                   'idiomas' => $idiomas,
                                                                   'publicidade' => $publicidade,
                                                                    'nivelHierarquico' => $nivelHierarquico));
        }
        else
        {
            return redirect('headhunters');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {                
        
        if(Auth('head')->check())
        {
            $this->validate($request,[
                'nome' => 'required',
                'nascimento' => 'required',
                'email' => 'required|
                            unique:candidatos|
                            unique:headhunters|
                            unique:headhunters_usuarios|
                            unique:empresas,responsavel_financeiro_email|
                            unique:empresas_usuarios|',
                'cidade' => 'required',
                'cpf' => 'required|
                         unique:candidatos|
                         unique:headhunters|',
                'pretensaoSalarial' => 'required',
            ]);
            
            try
            {
                $cdd = new Candidato();
                $cdd = Candidatos::gerenciaCandidato($cdd, $request);
                $hhCandidato = new HeadhunterCandidato();
                $hhCandidato->candidato = $cdd->id;
                $hhCandidato->headhunter = auth('head')->id();
                $hhCandidato->save();
                SESSION::flash('cad_cdd_ok', 'Candidato cadastrado com sucesso.');
                return redirect()->back();

            }
            catch(\Exception $e)
            {
                $cdd->delete();
                //dd($e);
                SESSION::flash('cad_cdd_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
                
            }
        }
        else
        {
           return redirect('headhunters');
        }
        
        die;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HeadhunterCandidato  $headhunterCandidato
     * @return \Illuminate\Http\Response
     */
    public function show(HeadhunterCandidato $headhunterCandidato)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HeadhunterCandidato  $headhunterCandidato
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth('head')->check())
        {
            $candidato = DB::table('headhunter_candidatos as hc')->select('cdd.*', 'ct.CT_UF')
            ->join('candidatos as cdd', 'cdd.id', 'hc.candidato')
            ->join('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
            ->where('hc.headhunter', auth('head')->id())
            ->where('hc.candidato', $id )->first();

            if(!$candidato) return redirect()->back();

            $cddExperiencia = DB::table('candidato_experiencia as ce')
                ->select('ce.id', 'ce.empresa', 'ce.atividades', 'ce.cargo as idcargo', 'c.cargo', 'ce.admissao', 'ce.desligamento')
                ->join('cargos as c', 'c.id', 'ce.cargo')
                ->where('candidato', $candidato->id)
                ->orderBy('admissao', 'desc')->get();

            $cddIdiomas = DB::table('candidatos_idiomas')->where('candidato', $candidato->id)->get();
            $relatorios = DB::table('candidatos_relatorios')->where('candidato', $candidato->id)->get();
            $estados = DB::table('estados')->get();
            $cidades = DB::table('cidades')->where('CT_UF', $candidato->CT_UF)->get();
            $formacoes = DB::table('formacao_nivel')->get();
            $statusForm = DB::table('formacao_status')->get();
            $idiomas = DB::table('idiomas')->get();
            $publicidade = DB::table('curriculum_publicidade')->get();
            $nivelHierarquico = DB::table('candidatoNivelHierarquico')->get();

            return view('dashboard.headhunter.editCandidato', array('candidato' => $candidato,
                                                                    'cddIdiomas' => $cddIdiomas,
                                                                    'relatorios' => $relatorios,
                                                                    'estados' => $estados,
                                                                    'cidades' => $cidades,
                                                                    'formacoes' => $formacoes,
                                                                    'statusForm' => $statusForm,
                                                                    'idiomas' => $idiomas,
                                                                    'publicidade' => $publicidade,
                                                                    'cddExperiencia' => $cddExperiencia,
                                                                    'nivelHierarquico' => $nivelHierarquico));
        }
        else
        {
            return redirect('headhunters');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HeadhunterCandidato  $headhunterCandidato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo "<pre>";
        //     print_r($request->input());
        // echo "</pre>";
        
        if(Auth('head')->check())
        {
            $this->validate($request,[
                'nome' => 'required',
                'nascimento' => 'required',
                'email' => 'required|
                            unique:candidatos,email,'.$id.'|
                            unique:headhunters|
                            unique:headhunters_usuarios|
                            unique:empresas,responsavel_financeiro_email|
                            unique:empresas_usuarios|',
                'cidade' => 'required',
                'cpf' => 'required|
                         unique:candidatos,cpf,'.$id.'|
                         unique:headhunters|',
                'pretensaoSalarial' => 'required',
            ]);


            try
            {
                $candidato = HeadhunterCandidato::where('candidato' ,$id)->where('headhunter', auth('head')->id())->first();
                if(!$candidato) return redirect()->back();
                $cdd = Candidato::find($candidato->candidato);
                $cdd = Candidatos::editCandidato($cdd, $request);               
                SESSION::flash('cad_cdd_ok', 'Candidato editado com sucesso.');
                return redirect()->back();

            }
            catch(Exception $e)
            {
                //dd($e);
                SESSION::flash('cad_cdd_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
                
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HeadhunterCandidato  $headhunterCandidato
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth('head')->check())
        {
            // Verifica se o candidato que está sendo removido está vinculado com o headhunter
            $candidato = DB::table('headhunter_candidatos as hc')->select('cdd.id', 'cdd.foto_perfil', 'cdd.curriculum')
                        ->join('candidatos as cdd', 'cdd.id', 'hc.candidato')
                        ->where('hc.headhunter', auth('head')->id())
                        ->where('hc.candidato', $id )->first();
            $relatorios = CandidatoRelatorio::where('candidato', $candidato->id)->get();
            try
            {
                foreach($relatorios as $relatorio)
                {
                    @unlink(public_path('./admin/upload/candidatos/relatorios/'.$relatorio->relatorio));
                }
                @unlink(public_path('./admin/curriculos/candidatos/'.$candidato->curriculum));
                @unlink(public_path('./admin/upload/candidatos/'.$candidato->foto_perfil));
                DB::table('candidatos')->where('id', $id)->delete();

                SESSION::flash('cad_cdd_ok', 'Candidato removido com sucesso.');
                return redirect()->back();                
            }
            catch(Exception $e)
            {
                SESSION::flash('cad_cdd_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
                //dd($e);
            }
        }
    }

    // public function getCandidatos()
    // {
    //     if(Auth('head')->check())
    //     {
    //         $candidatos = Candidato::select('candidatos.*', 'cp.publicidade', 'est.UF_UF', 'ec.estado_civil', 'ct.CT_NOME')
    //                     ->leftJoin('curriculum_publicidade as cp', 'cp.id', 'candidatos.publicidade_curriculum')
    //                     ->leftJoin('cidades as ct', 'ct.CT_ID', 'candidatos.cidade')
    //                     ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
    //                     ->leftJoin('estado_civil as ec', 'ec.id', 'candidatos.estado_civil')
    //                     ->simplePaginate(1);
    //         $estados = DB::table('estados')->get();
    //         $idiomas = DB::table('idiomas')->get();
    //         $escolaridade = DB::table('formacao_nivel')->get();
                        
    //         return view('dashboard.headhunter.painel-candidatos', array('candidatos' => $candidatos, 
    //                                                                     'estados' => $estados, 
    //                                                                     'idiomas' => $idiomas,
    //                                                                     'escolaridade' => $escolaridade));
            
    //     }
    //     else
    //     {
    //         return redirect('headhunters');
    //     }
    // }
    
    public function filtro(Request $request)
    {        
        if(Auth('head')->check())
        {
        
            $candidatos = Candidato::select('candidatos.*', 'cp.publicidade', 'est.UF_UF', 'ct.CT_NOME', 'ec.estado_civil')
                        ->leftJoin('curriculum_publicidade as cp', 'cp.id', 'candidatos.publicidade_curriculum')
                        ->leftJoin('cidades as ct', 'ct.CT_ID', 'candidatos.cidade')
                        ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
                        ->leftJoin('estado_civil as ec', 'ec.id', 'candidatos.estado_civil')
                        ->where(function($query) use ($request)
                        {
                            if($request->pretensaoSalarial)
                                $query->where('candidatos.pretensao_salarial', '>=', moneyToInt($request->pretensaoSalarial));
                            if($request->cidade)
                                $query->where('candidatos.cidade', $request->cidade);
                            if($request->estado)
                                $query->where('ct.CT_UF', $request->estado);
                            if($request->dispViagem)
                                $query->where('candidatos.disposicao_viagem', $request->dispViagem);
                            if($request->formacao)
                                $query->where("candidatos.escolaridade", $request->formacao);
                            if($request->idioma)
                            {
                                $query->whereIn('candidatos.id', function($subQuery) use ($request)
                                {
                                    $subQuery->select('candidato')->from('candidatos_idiomas')
                                        ->where('idioma', $request->idioma);
                                });
                            }
                        })->simplePaginate(50);

                        $estados = DB::table('estados')->get();
                        $idiomas = DB::table('idiomas')->get();
                        $escolaridade = DB::table('formacao_nivel')->get();
                                    
                        return view('dashboard.headhunter.painel-candidatos', array('candidatos' => $candidatos, 
                                                                                    'estados' => $estados, 
                                                                                    'idiomas' => $idiomas,
                                                                                    'escolaridade' => $escolaridade));
                        //return view('dashboard.headhunter.listaCandidatos', array('candidatos' => $candidatos));
        }
        else
        {
            return redirect('headhunters');
        }

    }

}
