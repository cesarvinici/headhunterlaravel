<?php

namespace App\Http\Controllers;

use App\HeadhunterCliente;
use App\Headhunter;
use App\Cidade;
use App\Estado;
use App\HeadhunterEmpresa;
use App\Segmento;
use App\NumeroFuncionarios;
use App\Empresa;
use App\CustomClass\Empresas\AddEmpresa;
use Session;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HeadhunterClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth('head')->check())
        {
            $empresas = DB::table('headhunter_cliente as hc')
                            ->select('emp.id', 'emp.razao_social', 'emp.nome_fantasia', 'emp.cnpj')
                            ->join('empresas as emp', 'emp.id', 'hc.empresa')
                            ->join('headhunters as h', 'h.id', 'hc.headhunter')
                            ->where('h.id', auth('head')->id())->paginate(20);
                                               
            return view('dashboard.headhunter.clientes', array('empresas' => $empresas ) );
        }

        return redirect('headhunters');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth('head')->check())
        {
            $estados = Estado::all();
            $segmentos = Segmento::all()->sortBy('id');
            $nFunc = NumeroFuncionarios::all()->sortBy('id');
            return view('dashboard.headhunter.adicionaCliente', array('estados' => $estados, 
                                                                      'segmentos' => $segmentos,
                                                                      'nFuncionarios' => $nFunc));
        }

        return redirect('headhunters');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth('head')->check())
        {
             // Valida os dados recebidos no formulário
             $this->validate($request, [
                'razaoSocial' => 'required',
                'nomeFantasia' => 'required',
                'cnpj' => 'required|unique:empresas|size:18',
                'nomeResp' => 'required',
                'cargoResp' => 'required',                            
            ]);

            try
            {
                $empresa = new Empresa();
                $addEmpresa = new AddEmpresa();
                $empresa = $addEmpresa->empresaAdd($empresa, $request);
                if($empresa->id)
                {
                    $headhunterCliente = new HeadhunterCliente();
                    $headhunterCliente->headhunter = auth('head')->id();
                    $headhunterCliente->empresa = $empresa->id;
                    $headhunterCliente->save();
                    SESSION::flash('cad_empresa_ok', 'Cliente cadastrado com sucesso!');
                    return redirect()->back();
    
                }
                SESSION::flash('cad_empresa_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
            }
            catch(\Exception $e)
            {
                SESSION::flash('cad_empresa_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
            }
        }

        return redirect('headhunters');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HeadhunterCliente  $headhunterCliente
     * @return \Illuminate\Http\Response
     */
    public function show(HeadhunterCliente $headhunterCliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HeadhunterCliente  $headhunterCliente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth('head')->check())
        {
            $empresa = DB::table('headhunter_cliente as hc')
            ->select('emp.*', 'est.UF_ID as estado')
            ->join('empresas as emp', 'emp.id', 'hc.empresa')
            ->join('headhunters as h', 'h.id', 'hc.headhunter')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where('hc.headhunter', auth('head')->id())
            ->where('hc.empresa', $id)->first();
            if(!$empresa)
            {
                return redirect('dashboard/headhunter/clientes');
            }
            $estados = Estado::all();
            $cidades = Cidade::where('CT_UF', $empresa->estado)->get();
            $segmentos = Segmento::all()->sortBy('id');
            $nFunc = NumeroFuncionarios::all()->sortBy('id');

            return view('dashboard.headhunter.clienteEdit', array('empresa' => $empresa,
                                                                  'cidades' => $cidades,
                                                                  'estados' => $estados,
                                                                  'nFuncionarios' => $nFunc,
                                                                  'segmentos' => $segmentos ) );
        }
        return redirect('headhunters');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HeadhunterCliente  $headhunterCliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth('head')->check())
        {
            $empresa = Empresa::find($id);
            
            // Valida os dados recebidos no formulário
             $this->validate($request, [
                'razaoSocial' => 'required',
                'nomeFantasia' => 'required',
                'cnpj' => 'required|unique:empresas,cnpj,'.$empresa->id.'|size:18',
                'nomeResp' => 'required',
                'cargoResp' => 'required',                            
            ]);
            
            try
            {
                $addEmpresa = new AddEmpresa();
                $empresa = $addEmpresa->empresaAdd($empresa, $request);
                if($empresa->id)
                {
                    SESSION::flash('cad_empresa_ok', 'Cliente editado com sucesso!');
                    return redirect()->back();
    
                }
                SESSION::flash('cad_empresa_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
            }
            catch(\Exception $e)
            {
                SESSION::flash('cad_empresa_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
            }

           
        }
        else
        {
            return redirect('headhunters');
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HeadhunterCliente  $headhunterCliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth('head')->check())
        {
            $empresa = DB::table('headhunter_cliente as hc')
            ->join('empresas as emp', 'emp.id', 'hc.empresa')
            ->join('headhunters as h', 'h.id', 'hc.headhunter')
            ->where('hc.headhunter', auth('head')->id())
            ->where('hc.empresa', $id)->count();
           if($empresa)
           { 
                Empresa::where('id', $id)->delete();
                SESSION::flash('empresa_excluir_ok', 'Cliente excluído com sucesso.');
                return redirect()->back();
           }
            
        }

        return redirect('headhunters'); 
    }

    public function filtro(Request $request)
    {
        if(Auth('head')->check())
        {
            $headhunter = Auth('head')->id();
            $empresas = DB::table('headhunter_cliente as hc')->select('emp.id', 'emp.razao_social', 'emp.nome_fantasia', 'emp.cnpj')
                        ->join('empresas as emp', 'emp.id' ,'hc.empresa')
                        ->where(function($query) use ($request)
                        {
                            if($request->nomeFantasia)
                                $query->where('emp.nome_fantasia', 'like', '%'.$request->nomeFantasia.'%');
                            if($request->razaoSocial)
                                $query->where('emp.razao_social', 'like', '%'.$request->razaoSocial.'%');
                            if($request->cnpj)
                                $query->where('emp.cnpj', $request->cnpj);
                            if($request->cod)
                                $query->where('emp.id', $request->cod);
                            if($request->cidade)
                            {
                                $cidade = DB::table("cidades")->select("CT_ID")->where('CT_NOME', urlencode($request->cidade))->first();
                                $query->where('endereco_cidade', $cidade->CT_ID);
                            }
                            if($request->segmento)
                            {
                                $segmento = DB::table("segmentos")->select('id')->where('segmento', urlencode($request->segmento))->first();
                                if(empty($segmento->id))
                                {
                                    $query->where('emp.segmento', 0);
                                }
                                else
                                {
                                    $query->where('emp.segmento', $segmento->id);
                                }
                                    
                            }
                        })
                        ->where('hc.headhunter' , $headhunter )->paginate(15);
                        
            return view('dashboard.headhunter.listaClientes', array('empresas' => $empresas));
        }
        else
        {
            return redirect('headhunters');
        }

    }
}