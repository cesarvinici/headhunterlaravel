<?php

namespace App\Http\Controllers;

use App\Headhunter;
use App\Cidade;
use App\Estado;
use App\HeadhunterEmpresa;
use App\Segmento;
use App\NumeroFuncionarios;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class HeadhunterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth('head')->check())
        {
           $id = Auth('head')->id();
           $vagas = DB::table('vagas as v')
                        ->select('vh.headhunter','v.id', 'cg.cargo as tituloCargo', 'emp.nome_fantasia',
                            'emp.endereco_cidade', 'rc.regime', 'v.status_vaga', 'seg.segmento')
                        ->join('cargos as cg', 'v.tituloCargo', 'cg.id')
                        ->join('empresas as emp', 'v.empresa', 'emp.id')
                        ->join('segmentos as seg', 'emp.segmento', 'seg.id')
                        ->join('regime_contratacao as rc', 'v.regime_contratacao', 'rc.id')
                        ->join('vaga_status as vs', 'v.status_vaga', 'vs.id')
                        ->leftJoin('vagas_headhunter as vh', 'v.id', 'vh.vaga')
                        ->where('v.status_vaga', 1)
                        ->inRandomOrder('v.id')->get();
            $user = Auth('head')->user();
            $convites = DB::table('convite_empresa_headhunters')->where('headhunter', $id)->get();
            $entrada = DB::table('convite_empresa_headhunters')->where('status_convite', 0)->where('headhunter', $id)->count();
            return view('dashboard.headhunter.index', array('user' => $user, 'vagasGerais'=>$vagas, 'convites' => $convites, 'entrada' => $entrada));
        }
        else
        {
            return redirect('headhunters');
        }
        
    }

    public function page_headhunter()
    {
        if(Auth('head')->check())
        {
            return redirect('dashboard/headhunter/');
        }
        return view('page-headhunters');
    }


    public function cadastro()
    {
        if(Auth('head')->check())
        {   
            return view('dashboard.headhunter.cadastros', array('user'=> Auth('head')->user()));
        }
        else
        {
            return redirect('headhunters');
        }
    }
    
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
        'nome' => 'required',
        'email' => 'required|unique:headhunters|
            unique:candidatos|
            unique:empresas_usuarios|
            unique:headhunters_usuarios',
            
        'cpf' => 'required|unique:headhunters|unique:candidatos|min:14',            
        'novaSenha'=> 'required|min:6',
        'novaSenhaC' => 'required|min:6']);


        $headhunter = new Headhunter();
        $headhunter->nome = $request->input('nome');
        $headhunter->cpf = $request->input('cpf');
        $headhunter->email = $request->input('email');
        $headhunter->password = Hash::make($request->input('novaSenha'));

        if($headhunter->save())
        {
            $user = 
            [
                'email' => $headhunter->email,
                'password' => $request->input('novaSenha')
            ];
            try
            {
                if(Auth('head')->attempt($user))
                {
                    SESSION::flash('cadHeadHunter_ok', '');
                    return redirect()->back();   
                }
            }
            catch(\Exception $e)
            {
                dd($e);
            }
        }
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Headhunter  $headhunter
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        if(Auth('head')->check())
        {
            if(Auth('head')->user()->role == 2)
            {
                return view('dashboard.headhunter.meus-dados', array('user' => Auth('head')->user()));
            }

           $user = DB::table('headhunters as h')
                        ->select('h.*', 'c.CT_ID', 'est.UF_ID')
                        ->leftJoin("cidades as c", 'h.endereco_cidade', 'c.CT_ID')
                        ->leftJoin('estados as est', 'c.CT_UF', 'est.UF_ID')
                        ->where('h.id', auth('head')->id())
                        ->first();
                        
            $cidades = DB::table('cidades')
                        ->select('CT_ID', 'CT_NOME')
                        ->where('CT_UF', $user->UF_ID)
                        ->get();
           
            
                            
            $estados = Estado::all();
            return view('dashboard.headhunter.meus-dados', array('user'=> $user, 'cidades'=> $cidades, 'estados'=>$estados));
        }
        else
        {
            return redirect('headhunters');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Headhunter  $headhunter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //Editar o headhunter
        if(Auth('head')->check())
        {

            $this->validate($request, [
            'nome' => 'required',
            'email' => 'required|unique:headhunters,email,'.Auth('head')->id().'|
                unique:candidatos|
                unique:empresas_usuarios|
                unique:headhunters_usuarios',
            'cpf'=>'required']);
            

            try
            {
                $headhunter = Headhunter::find(auth('head')->id());
                $headhunter->nome = $request->input("nome");
                $headhunter->cpf = $request->input('cpf');
                $headhunter->telefone = $request->input('telefone');
                $headhunter->celular = $request->input('celular');
                $headhunter->email = $request->input('email');
                $headhunter->endereco_logradouro = $request->input('endereco');
                $headhunter->endereco_numero = $request->input('numero');
                $headhunter->endereco_bairro = $request->input('bairro');
                $headhunter->endereco_cidade = $request->input('cidade');
                $headhunter->endereco_complemento = $request->input('complemento');
                $headhunter->endereco_cep = $request->input("cep");

                if($request->hasFile('imgPerfil'))
                {
                    
                    $imagem = $request->file('imgPerfil');
                    $nomearquivo  = $headhunter->id.".". $imagem->getClientOriginalExtension();
                    $request->file('imgPerfil')->move(public_path('./admin/upload/'), $nomearquivo);
                    $headhunter->imagem_perfil = $nomearquivo;
                
                }

                if($request->hasFile('curriculum'))
                {
                    $cv = $request->file('curriculum');
                    $nomearquivo  = $headhunter->id.".". $cv->getClientOriginalExtension();
                    $request->file('curriculum')->move(public_path('./admin/curriculos/'), $nomearquivo);
                    $headhunter->curriculo = $nomearquivo;
                }

                $headhunter->facebook = $request->input('facebook');
                $headhunter->linkedin = $request->input("linkedin");
                $headhunter->twitter = $request->input('twitter');
                $headhunter->instagram = $request->input('instagram');

                if(!empty($request->input('senhaAtual')))
                {                   
                    if(Hash::check($request->input('senhaAtual'), Auth('head')->user()->password))
                    {
                        if($request->input('novaSenha') == $request->input('novaSenha_confirma'))
                        {
                            $headhunter->password = Hash::make($request->input('novaSenha'));
                        }
                        else
                        {
                          SESSION::flash('erroNvSenha', 'Nova senha não confere.');
                          return redirect()->back();
                        }                     
                    }
                    else
                    {
                        SESSION::flash('erroNvSenha', 'Senha Incorreta');
                        return redirect()->back();
                    }
                }

                $headhunter->save();
                SESSION::flash('cadastro', 'Headhunter alterado com sucesso!');
                return redirect()->back();
            }
            catch(\Exception $e)
            {
               return redirect()->back()->withErrors('Ocorreu um problema. '.$e->getMessage());
            }
          
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Headhunter  $headhunter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Headhunter $headhunter)
    {
        //
    }

    //Login de headhunter
    public function loginHeadhunter(Request $request)
    {       

       $user =
       [
           'email' => $request->login,
           'password' => $request->senha
       ];
             
      try
      {     
           
            if(Auth('head')->attempt($user))
            {  
               return redirect('dashboard/headhunter/');
            }
            else
            {
                Session::flash('login_fail', 'Usuário ou senha incorreto');
                return redirect()->back();
            }                
            //}
      }
      catch(\Exception $e)
      {
        Session::flash('login_fail', 'Erro');
        return redirect()->back();
      }
    }

    public function logout()
    {
        Auth('head')->logout();
        return redirect('headhunters');
    }

    //Verifica se existe algum convite para trabalhar em vagas
    public function mensagens()
    {
        if(Auth('head')->check())
        {
            $id = Auth('head')->id();
            $convites = DB::table('convite_empresa_headhunters')
                            ->where('headhunter', $id)
                            ->where('status_convite', 0)
                            ->get();
            return view('dashboard.headhunter.mensagens', array('convites' => $convites));   
        }
        
    }
}
