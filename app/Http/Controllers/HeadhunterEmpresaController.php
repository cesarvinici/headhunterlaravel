<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use App\CustomClass\Empresas\AddEmpresa;
use App\Headhunter;
use App\Cidade;
use App\Estado;
use App\Segmento;
use App\NumeroFuncionarios;
use App\Empresa;
use App\HeadhunterEmpresa;
use App\Candidato;
use App\EmpresaUsuario;
use App\HeadhunterUsuario;
use Session;

class HeadhunterEmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retorna ou não a empresa vinculada ao headhunter

        if(Auth('head')->check())
        {
            $empresa = DB::table('headhunters_empresa as he')
                ->select('emp.*', 'he.tipo_atuacao', 'ct.CT_ID as cidade', 'est.UF_ID as estado')
                ->join('empresas as emp', 'he.empresa', 'emp.id')
                ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
                ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->where('he.headhunter', Auth('head')->id())->first();
            
            if(!$empresa)
            {
              $empresa = new Empresa();              
            }           

            $estados = Estado::all();
            $cidades = Cidade::where('CT_UF', $empresa->estado)->get();
            $segmentos = Segmento::all()->sortBy('id');
            $nFunc = NumeroFuncionarios::all()->sortBy('id');

            return view('dashboard.headhunter.empresa', 
                array(
                    'empresa' => $empresa, 
                    'estados' => $estados,
                    'segmentos' => $segmentos,
                    'nFuncionarios' => $nFunc,
                    'cidades' => $cidades,
                ));
        }
        else
        {
            return redirect('headhunters');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(Auth('head')->check())
        {
            // Valida os dados recebidos no formulário
            $this->validate($request, [
                'razaoSocial' => 'required',
                'nomeFantasia' => 'required',
                'cnpj' => 'required|unique:empresas|size:18',
                'nomeResp' => 'required',
                'cargoResp' => 'required',
                'emailResp' => 'required',                            
            ]);

            try
            {
                $empresa = new Empresa();
                $addEmpresa = new AddEmpresa();
                $empresa = $addEmpresa->empresaAdd($empresa, $request);
                
                if($empresa->id)
                {
                    $hhEmpresa = new HeadHunterEmpresa();
                    $hhEmpresa->headhunter = Auth('head')->id();
                    $hhEmpresa->empresa = $empresa->id;
                    $hhEmpresa->tipo_atuacao = $request->input('atuacao');
                    $hhEmpresa->save();
                    SESSION::flash('cad_empresa_ok', 'Empresa Cadastrada com sucesso!');
                    return redirect()->back();
                }
                else
                {
                    //SESSION::flash('cad_empresa_fail', '');
                    return redirect()->back()->withError('Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                }
                
            }
            catch(\Exception $e)
            {
                return redirect()->back()->withError('Ocorreu um erro, '.$e->getMessage());
            }
            
        }
        else
        {
            return redirect('headhunters');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HeadhunterEmpresa  $headhunterEmpresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            //Se o usuário alterar o id da empresa o sistema irá bloquear a alteração
            if(!DB::table('headhunters_empresa')->where('headhunter', auth('head')->id())->where('empresa', $id)->count())
            {
                return redirect()->back()->withErrors('Você não está vinculado com esta empresa.');
            }
            

            $empresa = Empresa::find($id);
            $this->validate($request, [
                'razaoSocial' => 'required',
                'nomeFantasia' => 'required',
                'cnpj' => 'required|unique:empresas,cnpj,'.$empresa->id.'|size:18',
                'nomeResp' => 'required',
                'cargoResp' => 'required',
                'emailResp' => 'required',                            
            ]);
            
            $addEmpresa = new AddEmpresa();
            $addEmpresa->empresaAdd($empresa, $request);
            
            $hhEmpresa = HeadhunterEmpresa::where('headhunter', auth('head')->check())->first();
            $hhEmpresa->tipo_atuacao = $request->input('atuacao');
            $hhEmpresa->save();
    
            SESSION::flash('cad_empresa_ok', 'Empresa editada com sucesso!');
            return redirect()->back();
            
             
        }
        catch(\Exception $e)
        {
            dd($e);
            die;
            SESSION::flash('cad_empresa_fail', 'Ocorreu um erro, '.$e->getMessage());
            return redirect()->back();

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HeadhunterEmpresa  $headhunterEmpresa
     * @return \Illuminate\Http\Response
     */
    public function destroy(HeadhunterEmpresa $headhunterEmpresa)
    {
        //
    }

   
}
