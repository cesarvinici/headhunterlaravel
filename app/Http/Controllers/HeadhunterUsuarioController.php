<?php

namespace App\Http\Controllers;

use App\HeadhunterUsuario;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class HeadhunterUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if(Auth('head')->check())
       {
           $usuarios = HeadhunterUsuario::where('headhunter', auth('head')->id())->get();
           return view('dashboard.headhunter.usuariosConta', array('usuarios' => $usuarios));
       }
       return redirect('headhunters');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth('head')->check())
        {
            $this->validate($request,[
                'nome' => 'required',
                'email' => 'required|
                            unique:candidatos|
                            unique:headhunters|
                            unique:headhunters_usuarios|
                            unique:empresas,responsavel_financeiro_email|
                            unique:empresas_usuarios|',
               'senha' => 'required|min:5'
            ]);


            try
            {
                $user = new HeadhunterUsuario();
                $user->headhunter = auth('head')->id();
                $user->nome = $request->input('nome');
                $user->email = $request->input('email');
                $user->tipo = $request->input('tipoUsuario');
                $user->password = hash::make($request->input('senha'));
                $user->save();
                if($request->hasFile('imgPerfil'))
                {                    
                    $imagem = $request->file('imgPerfil');
                    $nomearquivo  = $user->id.".". $imagem->getClientOriginalExtension();
                    $request->file('imgPerfil')->move(public_path('./admin/upload/users/'), $nomearquivo);
                    $user->imagem_perfil = $nomearquivo;
                    $user->save();
                
                }
                
                SESSION::flash('cad_user_ok', 'Usuário cadastrado com sucesso.');
                return redirect()->back();
                
            }
            catch(Exception $e)
            {
                SESSION::flash('cad_user_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
            }
        }
        return redirect('headhunters');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HeadhunterUsuario  $headhunterUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(HeadhunterUsuario $headhunterUsuario)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HeadhunterUsuario  $headhunterUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth('head')->check())
        {
            $usuarios = HeadhunterUsuario::where('headhunter', auth('head')->id())->get();
            $user = HeadhunterUsuario::where('headhunter', auth('head')->id())->where('id', $id)->first();
            return view('dashboard.headhunter.usuariosConta', array('usuarios' => $usuarios, 'user' => $user));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HeadhunterUsuario  $headhunterUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth('head')->check())
        {            
            $this->validate($request,[
                'nome' => 'required',
                'email' => 'required|
                            unique:candidatos|
                            unique:headhunters|
                            unique:headhunters_usuarios,email,'.$id.'|
                            unique:empresas,responsavel_financeiro_email|
                            unique:empresas_usuarios|',
                ]);
            // Caso usuario da conta tentar editar outro usuário ele será redirecionado para outra tela
            if(auth('head')->user()->role == 2 && $id != auth('head')->id())
            {                
                SESSION::flash('cad_user_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
            }
           
            try
            {
                $user =  HeadhunterUsuario::find($id);
                $headhunter = auth('head')->user()->role == 1 ? auth('head')->id() : auth('head')->user()->headhunter;
                $user->headhunter = $headhunter;
                $user->nome = $request->input('nome');
                $user->email = $request->input('email');
                $user->tipo = $request->input('tipoUsuario');

                if(!empty($request->input('senha')))
                {
                    $this->validate($request,[
                        'senha' => 'min:6',
                        ]);

                    $user->password = hash::make($request->input('senha'));
                }

                $user->save();
                if($request->hasFile('imgPerfil'))
                {                    
                    $imagem = $request->file('imgPerfil');
                    $nomearquivo  = $user->id.".". $imagem->getClientOriginalExtension();
                    $request->file('imgPerfil')->move(public_path('./admin/upload/users/'), $nomearquivo);
                    $user->imagem_perfil = $nomearquivo;
                    $user->save();
                
                }
                
                SESSION::flash('cad_user_ok', 'Usuário editado com sucesso.');
                return redirect()->back();
                
            }
            catch(Exception $e)
            {
                SESSION::flash('cad_user_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
            } 
        }

        return redirect('headhunters');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HeadhunterUsuario  $headhunterUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth('head')->check() && auth('head')->user()->role == 1 )
        {
            try
            {
                $user = HeadhunterUsuario::where('headhunter', auth('head')->id())->where('id', $id)->first();
                if(isset($user->imagem_perfil))
                {
                    @unlink(public_path('./admin/upload/users/'.$user->imagem_perfil));
                }                  
                $user->delete();
                        
                SESSION::flash('cad_user_ok', 'Usuário removido com sucesso.');
                return redirect()->back(); 

            }
            catch(Exception $e)
            {
                SESSION::flash('cad_user_fail', 'Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema.');
                return redirect()->back();
            }
        }   
        
        return redirect('headhunters');
    }

    public function filtro(Request $request)
    {
        if(Auth('head')->check() && auth('head')->user()->role == 1 )
        {
            print_r($request);
        }           
        return redirect('headhunters');

    }
}
