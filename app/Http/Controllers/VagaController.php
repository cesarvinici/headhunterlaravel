<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\CustomClass\Vagas\Vagas;
use App\Vaga;
use App\VagaIdioma;
use App\VagaAptidao;
use Illuminate\Http\Request;
use Session;

class VagaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if(Auth('emp')->check())
       {
           $vagas = DB::table('vagas as v')->select('v.*', 'cg.cargo', 'ct.CT_Nome as cidade', 'reg.regime')
           ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
           ->join('empresas as emp', 'emp.id', 'v.empresa')
           ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
           ->join('regime_contratacao as reg', 'reg.id', 'v.regime_contratacao')
           ->where('empresa', Auth('emp')->user()->empresa)->get();
           return view('dashboard.empresas.manutencaoVagas', array('vagas' => $vagas));
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth('emp')->check())
        {
            if(empty(Auth('emp')->user()->empresa))
            {
                Session::flash("error", 'É necessário realizar o cadastro da empresa antes de criar vagas');
                return redirect('/dashboard/empresa/cadastro-empresa');
            }

            $empresa =  DB::table('empresas as emp')->select('emp.nome_fantasia', 'emp.cnpj', 'seg.segmento', 'ct.CT_NOME as cidade', 'est.UF_NOME as estado')
            ->join('segmentos as seg', 'seg.id', 'emp.segmento')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where('emp.id', auth('emp')->user()->empresa)->first();
            $idiomas =  DB::table('idiomas')->orderBy('idioma')->get();
            $regimes =  DB::table('regime_contratacao')->get();
            $etapas  =  DB::table('etapas_processo_seletivo')->get();
            $fee     =  DB::table('fee_recrutamento')->get();
            $users   =  DB::table('headhunters_usuarios')->select('id', 'nome')->where('headhunter', auth('head')->id())->get();       
            
            return view('dashboard.empresas.adicionaVaga', array('empresa' => $empresa, 'idiomas' => $idiomas, 
                                                              'regimes' => $regimes,
                                                              'etapas' => $etapas,
                                                              'fee' => $fee,
                                                              'users' => $users));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth('emp')->check())
        {
            $request->empresa = Auth('emp')->user()->empresa;

            $this->validate($request, [
                'idCargo' => 'required',
                'numVagas' => 'required',
                'formacaoExigida' => 'required',
                'principaisAtividades' => 'required',
                'requisitos' => 'required',
                'conhecimento[*]' => 'required',
                'beneficios' => 'required',
                'horarioTrab' => 'required',                    
            ]);
        
            try
            {
                // echo "<pre>";
                // print_r($request->input());
                // echo "<pre>";
                // die;               

                $vaga = new Vaga();
                $vaga = Vagas::gerenciaVaga($vaga, $request);
                
               
                for ($i=0; $i < sizeof($request->input('conhecimentoIdioma')) ; $i++)
                { 
                    if($request->input('conhecimentoIdioma')[$i] )
                    {
                        $idioma = new VagaIdioma();
                        $idioma->vaga = $vaga->id;
                        $idioma->idioma = $request->input('conhecimentoIdioma')[$i];
                        $idioma->nivel = $request->input('nivelIdioma'.($i+1));
                        $idioma->save();                
                    }
                }
                
                            
                for ($i=0; $i < sizeof($request->input('conhecimento')) ; $i++)
                { 
                    $conhecimento = new VagaAptidao();
                    $conhecimento->vaga = $vaga->id;
                    $conhecimento->aptidoes = $request->input('conhecimento')[$i];
                    $conhecimento->nivel_requerido = $request->input('nivelConhecimento'.($i+1));
                    $conhecimento->save();
                }
               
                SESSION::flash('success', 'Vaga Cadastrada com sucesso');
                return redirect()->back();

            }
            catch(\Exception $e)
            {
                $vaga->delete();
                SESSION::flash('error', "Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema");
                return redirect()->back();
            }
        }
        else
        {
            return redirect('empresas');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function show($vaga)
    {
       if(auth('head')->check())
       {
            $vaga = DB::table('vagas as v')
            ->select('v.*', 'ct.CT_NOME', 'emp.nome_fantasia', 'seg.segmento', 'cg.cargo', 'est.UF_UF', 'fee.fee')
            ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
            ->join('fee_recrutamento as fee', 'fee.id', 'v.feeRecrutamento')
            ->join('empresas as emp', 'emp.id', 'v.empresa')
            ->join('segmentos as seg', 'seg.id', 'emp.segmento')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where('v.id', $vaga)->first();

            //Irá carregar todos os candidatos que são filiados headhunter e os que se interessaram pela vaga
            $candidatos = DB::table('candidatos as cdd')
            ->select('cdd.*', 'ct.CT_NOME', 'est.UF_UF')
            ->leftJoin('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
            ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where(function($query) use ($vaga)
            {                
                $query->WhereIn('cdd.id', function($sql) use ($vaga)
                {
                    $sql->select('candidato')->from('candidato_vagas')->where('vaga', $vaga->id);
                });

                $query->orWhereIn('cdd.id', function($sql)
                {
                    $sql->select("candidato")->from('headhunter_candidatos');
                });
            })->whereNotIn('cdd.id', function ($query) use ($vaga)
            {
                $query->select('candidato')->from('fila_candidatura')->where('vaga', $vaga->id)->get();
            })->get();
            
             
            $aptidoes = DB::table('vagas_aptidoes')->where('vaga', $vaga->id)->get();
            $idiomas = DB::table("vagas_idiomas as vi")->join('idiomas as i', 'i.id', 'vi.idioma')->where('vi.vaga', $vaga->id)->get();
            $escolaridade = DB::table('formacao_nivel')->get();
            $estados = DB::table('estados')->get();
            $alertas = DB::table('vaga_alerta')->get();
            $hhAlertas = DB::table('headhunters_alertas')->where('vaga', $vaga->id)->where('headhunter', auth('head')->id())->get();            
            $alertaStatus = DB::table('headhunter_alerta_status_vaga')->where('headhunter', auth('head')->id())->where('vaga', $vaga->id)->first();
            return view('dashboard.headhunter.mostraVaga', array('vaga' => $vaga, 'aptidoes' => $aptidoes, 
                                                            'idiomas' => $idiomas, 'escolaridade' => $escolaridade, 
                                                            'estados' => $estados,
                                                            'alertas' => $alertas,
                                                            'user' => Auth('head')->user(),
                                                            'hhAlertas' => $hhAlertas,
                                                            'alertaStatus' => $alertaStatus,
                                                            'candidatos' => $candidatos));            
       }

       return redirect('headhunters');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth('emp')->check())
        {
            $empresa=  DB::table('empresas as emp')->select('emp.nome_fantasia', 'emp.cnpj', 'seg.segmento', 'ct.CT_NOME as cidade', 'est.UF_NOME as estado')
            ->join('segmentos as seg', 'seg.id', 'emp.segmento')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where('emp.id', auth('emp')->user()->empresa)->first();
            $idiomas =  DB::table('idiomas')->orderBy('idioma')->get();
            $regimes =  DB::table('regime_contratacao')->get();
            $etapas  =  DB::table('etapas_processo_seletivo')->get();
            $fee     =  DB::table('fee_recrutamento')->get();
            $vaga    =  DB::table('vagas as v')->select('v.*', 'c.cargo')
                                                           ->join('cargos as c', 'c.id', 'v.tituloCargo')
                                                           ->where('v.id', $id )
                                                           ->where('v.id', $id)->first();
            
            $vaga->salario = intToMoney($vaga->salario);
            $conhecimentos = DB::table('vagas_aptidoes')->where('vaga', $id)->get();
            $vidiomas = DB::table('vagas_idiomas')->where('vaga', $id)->get();

            return view('dashboard.empresas.editaVaga', array('vaga' => $vaga,
                                                               'empresa' => $empresa,
                                                               'idiomas' => $idiomas, 
                                                               'regimes' => $regimes,
                                                               'etapas' => $etapas,
                                                               'fee' => $fee,
                                                               'conhecimentos' => $conhecimentos,
                                                               'vidiomas' => $vidiomas,
                                                            ));
        }
        
        return redirect('empresas');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth('emp')->check())
        {
            $request->empresa = Auth('emp')->user()->empresa;

            $this->validate($request, [
                'idCargo' => 'required',
                'numVagas' => 'required',
                'formacaoExigida' => 'required',
                'principaisAtividades' => 'required',
                'requisitos' => 'required',
                'conhecimento[*]' => 'required',
                'beneficios' => 'required',
                'horarioTrab' => 'required',                    
            ]);

            $vaga = Vaga::find($id);
            $vaga = Vagas::gerenciaVaga($vaga, $request);            
           
            for ($i=0; $i < sizeof($request->input('conhecimentoIdioma')) ; $i++)
            { 
                if($request->input('conhecimentoIdioma')[$i])
                {
                    $idioma = $request->input('idVIdioma')[$i] ? VagaIdioma::find($request->input('idVIdioma')[$i]) : new VagaIdioma();
                    $idioma->vaga = $vaga->id;
                    $idioma->idioma = $request->input('conhecimentoIdioma')[$i];
                    $idioma->nivel = $request->input('nivelIdioma'.($i+1));
                    $idioma->save();                
                }
            }
                       
            for ($i=0; $i < sizeof($request->input('conhecimento')) ; $i++)
            { 
                $conhecimento = $request->input('idConhecimento')[$i] ? VagaAptidao::find($request->input('idConhecimento')[$i]) : new VagaAptidao();
                $conhecimento->vaga = $vaga->id;
                $conhecimento->aptidoes = $request->input('conhecimento')[$i];
                $conhecimento->nivel_requerido = $request->input('nivelConhecimento'.($i+1));
                $conhecimento->save();
            }
        
            SESSION::flash('success', 'Vaga editada com sucesso');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vaga  $vaga
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth('head')->check() || Auth('emp')->check())
        {
            $vaga = Vaga::find($id);
            $vaga->delete();
            SESSION::flash('success', 'Vaga removida com sucesso');
            return redirect()->back();
        }
    }


    //Ajax
    public function alteraStatus($id, $status)
    {
        if(Auth('head')->check() || Auth('emp')->check())
        {
            try
            {
                $vaga = Vaga::find($id);
                $vaga->status_vaga = $status;
                $vaga->save();
                
                alertaStatusAlterado($vaga->id);

                echo 1;
            }
            catch(\Exception $e)
            {
                echo "Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema";
            }
            
        }
        else
        {
            echo "Favor realizar login no sistema";
        }       
    }

    public function filtraVagasEmp(Request $request)
    {
       if(Auth('emp')->check())
       {
            $vagas = DB::table('vagas as v')->select('v.*', 'cg.cargo', 'ct.CT_Nome as cidade', 'reg.regime')
            ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
            ->join('empresas as emp', 'emp.id', 'v.empresa')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('regime_contratacao as reg', 'reg.id', 'v.regime_contratacao')
            ->where('empresa', Auth('emp')->user()->empresa)
            ->where(function ($query) use ($request)
            {
                if($request->cod)
                    $query->where('v.id', $request->cod);
                if($request->status)
                    $query->where('v.status_vaga', $request->status);   
                if($request->idCargo)
                    $query->where('v.tituloCargo', $request->idCargo);
                if($request->regime)
                {
                    $regime = DB::table("regime_contratacao")->select('id')->where('regime', 'LIKE', '%'.urlencode($request->regime).'%')->get();
                    for ($i=0; $i < sizeof($regime) ; $i++)
                    { 
                        if($i == 0)
                            $query->where('v.regime_contracao', $regime[$i]->id);
                        else
                            $query->orWhere('v.regime_contracao', $regime[$i]->id);
                    }
                }
                if($request->cidade)
                {
                    $cidade = DB::table('cidades')->select('CT_ID')->where('CT_NOME', 'like',  '%'.urlencode($request->cidade).'%')->get();
                    if(sizeof($cidade))
                    {
                        for ($i=0; $i < sizeof($cidade) ; $i++)
                        { 
                            if($i == 0)
                                $query->where('emp.endereco_cidade', $cidade[$i]->CT_ID);
                            else
                                $query->orWhere('emp.endereco_cidade', $cidade[$i]->CT_ID);
                        }
                    }                   
                }
            })->get();

            return view('dashboard.empresas.vagasEmp', array('vagas' => $vagas));
       }
    }
}
