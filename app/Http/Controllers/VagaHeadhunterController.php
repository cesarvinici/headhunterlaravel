<?php

namespace App\Http\Controllers;

use App\VagaHeadhunter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Empresa;
use App\Vaga;
use App\VagaIdioma;
use App\VagaAptidao;
use App\CustomClass\Uteis;
use App\CustomClass\Vagas\Vagas;
use Session;

class VagaHeadhunterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth('head')->check())
        {
           
            $vagas = DB::table("vagas_headhunter as vh")->select('v.id', 'c.cargo', 'emp.nome_fantasia', 'v.status_vaga', 'ct.CT_NOME as cidade', 'rc.regime')
                                            ->join('vagas as v', 'v.id', 'vh.vaga')
                                            ->join('cargos as c', 'c.id', 'v.tituloCargo')
                                            ->join('empresas as emp', 'emp.id', 'v.empresa')
                                            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
                                            ->join('regime_contratacao as rc', 'rc.id', 'v.regime_contratacao')
                                            ->where('vh.headhunter', auth('head')->id())
                                            ->where('vh.criado', 1)->get();
            $status = DB::table('vaga_status')->get();
            return view('dashboard.headhunter.vagas', array('vagas' => $vagas, 'status' => $status));
        }

        return redirect("page-headhunters");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth('head')->check())
        {
            $empresas = DB::table('headhunter_cliente as hc')
                            ->select('emp.id','emp.cnpj', 'emp.nome_fantasia')
                            ->join('empresas as emp', 'emp.id', 'hc.empresa')
                            ->where('hc.headhunter', auth('head')->id())->get();
            $idiomas =  DB::table('idiomas')->orderBy('idioma')->get();
            $regimes =  DB::table('regime_contratacao')->get();
            $etapas =   DB::table('etapas_processo_seletivo')->get();
            $fee =      DB::table('fee_recrutamento')->get();
            $users =    DB::table('headhunters_usuarios')->select('id', 'nome')->where('headhunter', auth('head')->id())->get();       
            return view('dashboard.headhunter.addVaga', array('empresas' => $empresas, 'idiomas' => $idiomas, 
                                                              'regimes' => $regimes,
                                                              'etapas' => $etapas,
                                                              'fee' => $fee,
                                                              'users' => $users));
        }
        return redirect('headhunters');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth('head')->check())
        {
            $this->validate($request, [
                'idCargo' => 'required',
                'empresa' => 'required',
                'numVagas' => 'required',
                'formacaoExigida' => 'required',
                'principaisAtividades' => 'required',
                'requisitos' => 'required',
                'conhecimento[*]' => 'required',
                'beneficios' => 'required',
                'horarioTrab' => 'required',                    
            ]);
            
            try
            {
                // echo "<pre>";
                // print_r($request->input());
                // echo "<pre>";
                // die;               

                $vaga = new Vaga();
                $vaga = Vagas::gerenciaVaga($vaga, $request);
                
               
                    for ($i=0; $i < sizeof($request->input('conhecimentoIdioma')) ; $i++)
                    { 
                        if($request->input('conhecimentoIdioma')[$i] )
                        {
                            $idioma = new VagaIdioma();
                            $idioma->vaga = $vaga->id;
                            $idioma->idioma = $request->input('conhecimentoIdioma')[$i];
                            $idioma->nivel = $request->input('nivelIdioma'.($i+1));
                            $idioma->save();                
                        }
                    }
                
                            
                for ($i=0; $i < sizeof($request->input('conhecimento')) ; $i++)
                { 
                    $conhecimento = new VagaAptidao();
                    $conhecimento->vaga = $vaga->id;
                    $conhecimento->aptidoes = $request->input('conhecimento')[$i];
                    $conhecimento->nivel_requerido = $request->input('nivelConhecimento'.($i+1));
                    $conhecimento->save();
                }

                $vagaHeadhunter = new VagaHeadhunter();
                $vagaHeadhunter->headhunter = auth('head')->id();
                $vagaHeadhunter->vaga = $vaga->id;
                $vagaHeadhunter->criado = 1;
                $vagaHeadhunter->save();
                SESSION::flash('cad_vaga_ok', 'Vaga Cadastrada com sucesso');
                return redirect()->back();

            }
            catch(\Exception $e)
            {
                $vaga->delete();
                // dd($e);
                // die;
                SESSION::flash('cad_vaga_fail', "Ocorreu um erro, tente novamente ou entre em contato com o administrador do sistema");
                return redirect()->back();
            }
        }
        return redirect('headhunters');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VagaHeadhunter  $vagaHeadhunter
     * @return \Illuminate\Http\Response
     */
    public function show(VagaHeadhunter $vagaHeadhunter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VagaHeadhunter  $vagaHeadhunter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth('head')->check())
        {         
            $empresas = DB::table('headhunter_cliente as hc')
                            ->select('emp.id','emp.cnpj', 'emp.nome_fantasia')
                            ->join('empresas as emp', 'emp.id', 'hc.empresa')
                            ->where('hc.headhunter', auth('head')->id())->get();
            $idiomas =  DB::table('idiomas')->orderBy('idioma')->get();
            $regimes =  DB::table('regime_contratacao')->get();
            $etapas =   DB::table('etapas_processo_seletivo')->get();
            $fee =      DB::table('fee_recrutamento')->get();
            $users =    DB::table('headhunters_usuarios')->select('id', 'nome')->where('headhunter', auth('head')->id())->get();
            $vaga =     DB::table('vagas_headhunter as vh')->select('v.*', 'c.cargo')
                                                        ->join('vagas as v', 'v.id', 'vh.vaga')
                                                        ->join('cargos as c', 'c.id', 'v.tituloCargo')
                                                        ->where('vh.headhunter', auth('head')->id())
                                                        ->where('vh.vaga', $id)->first();
        
            $vaga->salario = Uteis::intToMoney($vaga->salario);
            $conhecimentos = DB::table('vagas_aptidoes')->where('vaga', $id)->get();
            $vidiomas = DB::table('vagas_idiomas')->where('vaga', $id)->get();

            return view('dashboard.headhunter.editVaga', array('vaga' => $vaga,
                                                               'empresas' => $empresas,
                                                               'idiomas' => $idiomas, 
                                                               'regimes' => $regimes,
                                                               'etapas' => $etapas,
                                                               'fee' => $fee,
                                                               'users' => $users,
                                                               'conhecimentos' => $conhecimentos,
                                                               'vidiomas' => $vidiomas,
                                                            ));
        }
        return redirect('headhunters');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VagaHeadhunter  $vagaHeadhunter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        if(Auth('head')->check())
        {
            // echo "<pre>";
            // print_r($request->input());
            // echo "<pre>";

            $this->validate($request, [
                'idCargo' => 'required',
                'empresa' => 'required',
                'numVagas' => 'required',
                'formacaoExigida' => 'required',
                'principaisAtividades' => 'required',
                'requisitos' => 'required',
                'conhecimento[*]' => 'required',
                'beneficios' => 'required',
                'horarioTrab' => 'required',                    
            ]);

            $vaga = Vaga::find($id);
            $vaga = Vagas::gerenciaVaga($vaga, $request);
            
           
                for ($i=0; $i < sizeof($request->input('conhecimentoIdioma')) ; $i++)
                { 
                    if($request->input('conhecimentoIdioma')[$i])
                    {
                        $idioma = $request->input('idVIdioma')[$i] ? VagaIdioma::find($request->input('idVIdioma')[$i]) : new VagaIdioma();
                        $idioma->vaga = $vaga->id;
                        $idioma->idioma = $request->input('conhecimentoIdioma')[$i];
                        $idioma->nivel = $request->input('nivelIdioma'.($i+1));
                        $idioma->save();                
                    }
                }
                       
            for ($i=0; $i < sizeof($request->input('conhecimento')) ; $i++)
            { 
                $conhecimento = $request->input('idConhecimento')[$i] ? VagaAptidao::find($request->input('idConhecimento')[$i]) : new VagaAptidao();
                $conhecimento->vaga = $vaga->id;
                $conhecimento->aptidoes = $request->input('conhecimento')[$i];
                $conhecimento->nivel_requerido = $request->input('nivelConhecimento'.($i+1));
                $conhecimento->save();
            }
        
            SESSION::flash('cad_vaga_ok', 'Vaga editada com sucesso');
            return redirect()->back();
        }
        return redirect('headhunters');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VagaHeadhunter  $vagaHeadhunter
     * @return \Illuminate\Http\Response
     */
    public function destroy(VagaHeadhunter $vagaHeadhunter)
    {
        //
    }

    public function filtroVagasMktPlace(Request $request)
    {
        if(Auth('head')->check())
        {
            $vagas = DB::table('vagas as v')
            ->select('v.id','v.created_at', 'v.salario', 'cg.cargo', 'v.naoExibirSalario', 'ct.CT_NOME', 'est.UF_UF', 'fee.fee')
            ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
            ->join('fee_recrutamento as fee', 'fee.id', 'v.feeRecrutamento')
            ->join('empresas as emp', 'emp.id', 'v.empresa')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where(function ($query) use($request)
            {
                if($request->taxarecut)
                    $query->where('v.feeRecrutamento', $request->taxarecut);
                if($request->areaAtuacao)
                    $query->where('emp.segmento', $request->areaAtuacao);
                if($request->salario)
                    $query->where('v.salario', '>=', moneyToInt($request->salario));
                    // $query->where('v.naoExibirSalario', 0);
                if($request->localidade)
                {
                    $cidade = DB::table('cidades')->select('CT_ID')->where('CT_NOME', 'like',  '%'.urlencode($request->localidade).'%')->get();
                    if(sizeof($cidade))
                    {
                        for ($i=0; $i < sizeof($cidade) ; $i++)
                        { 
                            if($i == 0)
                                $query->where('emp.endereco_cidade', $cidade[$i]->CT_ID);
                            else
                                $query->orWhere('emp.endereco_cidade', $cidade[$i]->CT_ID);
                        }
                    }                   
                }
                if($request->idCargo)
                    $query->where('v.tituloCargo', $request->idCargo);
                if($request->empresa)
                    $query->where("emp.razao_social", 'like', '%'.$request->empresa.'%')
                           ->orWhere("emp.nome_fantasia", 'like', '%'.$request->empresa.'%');
                // if($request->palavraChave)
                // {
                //     $query->where('v.feeRecrutamento', $request->palavraChave)
                //     ->orWhere();
                // }

            })
            
            ->whereNotIn('v.id', function ($query)
            {
                $query->select('vaga')->from('vagas_headhunter as vh');
            })->simplePaginate(30);

            return view('dashboard.headhunter.listaVagas', array('vagas' => $vagas));
            
        }
        else
        {
            echo "Favor realizar login no sistema";
        }       
    }

    public function filtroVagasInternas(Request $request)
    { 
        if(Auth('head')->check())
        {
            $headhunter = DB::table('vagas as v')
            ->select('v.id','v.created_at', 'v.salario', 'cg.cargo', 'v.naoExibirSalario', 'ct.CT_NOME', 'est.UF_UF', 'fee.fee')
            ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
            ->join('fee_recrutamento as fee', 'fee.id', 'v.feeRecrutamento')
            ->join('empresas as emp', 'emp.id', 'v.empresa')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where(function ($query) use($request)
            {
                if($request->taxarecut)
                    $query->where('v.feeRecrutamento', $request->taxarecut);
                if($request->areaAtuacao)
                    $query->where('emp.segmento', $request->areaAtuacao);
                if($request->salario)
                    $query->where('v.salario', '>=', moneyToInt($request->salario));
                if($request->localidade)
                {
                    $cidade = DB::table('cidades')->select('CT_ID')->where('CT_NOME', 'like',  '%'.urlencode($request->localidade).'%')->get();
                    if(sizeof($cidade))
                    {
                        for ($i=0; $i < sizeof($cidade) ; $i++)
                        { 
                            if($i == 0)
                                $query->where('emp.endereco_cidade', $cidade[$i]->CT_ID);
                            else
                                $query->orWhere('emp.endereco_cidade', $cidade[$i]->CT_ID);
                        }
                    }                   
                }
                if($request->idCargo)
                    $query->where('v.tituloCargo', $request->idCargo);
                if($request->empresa)
                    $query->where("emp.razao_social", 'like', '%'.$request->empresa.'%')
                           ->orWhere("emp.nome_fantasia", 'like', '%'.$request->empresa.'%');
                // if($request->palavraChave)
                // {
                //     $query->where('v.feeRecrutamento', $request->palavraChave)
                //     ->orWhere();
                // }

            })
            ->whereIn('v.id', function ($query)
            {
                $query->select('vaga')->from('vagas_headhunter as vh')->where('headhunter', auth('head')->id());
            })->get();

            return view('dashboard.headhunter.listaVagasInternas', array('headhunter' => $headhunter));
            
        }
        else
        {
            echo "Favor realizar login no sistema";
        }       
    }

    public function painelVagas()
    {
        if(Auth('head')->check())
        {
            $id = Auth('head')->id();

            // Seleciona as vagas criadas pelo headhunter
            $headhunter = DB::table('vagas_headhunter as vh')->select('v.*', 'cg.cargo', 'ct.CT_NOME', 'est.UF_UF', 'fee.fee')
                ->join('vagas as v', 'v.id', 'vh.vaga')
                ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
                ->join('fee_recrutamento as fee', 'fee.id', 'v.feeRecrutamento')
                ->join('empresas as emp', 'emp.id', 'v.empresa')
                ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
                ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
                ->where('vh.headhunter' , $id)->get();

            // Seleciona todas as vagas que não estiverem vinculadas com o headhunter conectado
            $vagas = DB::table('vagas as v')
            ->select('v.id','v.created_at', 'v.salario', 'cg.cargo', 'v.naoExibirSalario', 'ct.CT_NOME', 'est.UF_UF', 'fee.fee')
            ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
            ->join('fee_recrutamento as fee', 'fee.id', 'v.feeRecrutamento')
            ->join('empresas as emp', 'emp.id', 'v.empresa')
            ->join('cidades as ct', 'ct.CT_ID', 'emp.endereco_cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->whereNotIn('v.id', function ($query)
            {
                $query->select('vaga')->from('vagas_headhunter as vh')->where('headhunter', auth('head')->id());
            })
            ->simplePaginate(30);
           
            $fee = DB::table("fee_recrutamento")->get();
            $segmentos = DB::table('segmentos')->get();
            $entrada = DB::table("convite_empresa_headhunters")
                        ->where('headhunter', auth('head')->id())
                        ->where('status_convite', 0)->count();
            $minhasVagas = DB::table("vagas_headhunter")->where('headhunter', auth('emp')->id())->count();
            $alertas = DB::table("headhunter_alerta_status_vaga")->where('headhunter', auth('head')->id())->count();
            return view('dashboard.headhunter.painelVagas', array('vagas' => $vagas, 
                                                                  'headhunter' => $headhunter,
                                                                  'fee' => $fee,
                                                                  'segmentos' => $segmentos,
                                                                  'entrada' => $entrada,
                                                                  'minhasVagas' => $minhasVagas,
                                                                  'alertas' => $alertas));
        }

        return redirect('headhunters');
    }



}
