<?php

function statusVaga()
{
    return DB::table('vaga_status')->get();
}
//Conta os headhunters que estão participando da vaga.
function headhuntersVaga($vaga)
{
    return DB::table('fila_candidatura')->select('headhunter')->where('vaga', $vaga)->distinct('headhunter')->count('headhunter');
}
// Conta candidatos que que estão participando do processo seletivo.
function contaCandidatos($vaga)
{
    return DB::table('fila_candidatura')->where('vaga', $vaga)->count('candidato');
}
//Retorna informações do headhunter para a vaga especificada.
function infoHeadhunterVaga($vaga)
{
    return  DB::table("fila_candidatura as vh")->select('hh.id', 'hh.nome', 'hh.imagem_perfil', 'vh.created_at as engajamento', 'ct.CT_NOME as cidade', 'est.UF_UF as estado')
        ->join('headhunters as hh', 'hh.id', 'vh.headhunter')
        ->join('cidades as ct', 'ct.CT_ID', 'hh.endereco_cidade')
        ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')->where('vaga', $vaga)->orderBy('hh.id')->get();

}

function convidaHeadhunter($vagas)  
{
    // Retorna headhunters que não estão atualmente trabalhando na vaga selecionada
    return DB::table('headhunters as head')
    ->where(function($query) use ($vagas)
    {
        foreach($vagas as $vaga)
        {
            $query->whereNotIn('head.id', function($db) use ($vaga)
            {
               $db->select("headhunter")->from('fila_candidatura')->where('vaga', $vaga);
            });
        }
    })->get();

}

function getCddHeadhunterVaga($vaga, $headhunter)
{
    return DB::table("fila_candidatura as fc")->select('cdd.id', 'cdd.nome', 'fc.created_at')
                ->join('candidatos as cdd', 'fc.candidato', 'cdd.id')
                ->where('fc.vaga', $vaga)
                ->where('fc.headhunter', $headhunter)->get();
}

function candidatosVaga($vaga)
{
    return DB::table('fila_candidatura')->where('vaga', $vaga)->count();
}

function getCandidatosvaga($vaga)
{
    return DB::table('fila_candidatura as fc')->select('cdd.id', 'cdd.nome', 'cdd.foto_perfil', 'ct.CT_NOME as cidade', 'est.UF_UF as estado')
            ->join('candidatos as cdd', 'cdd.id', 'fc.candidato')
            ->join('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
            ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
            ->where('fc.vaga', $vaga)->get();
}

function buscaHeadhunter($candidato, $vaga)
{
    $headhunter = DB::table('fila_candidatura as fc')->select('hh.nome')
                ->join('headhunters as hh', 'hh.id', 'fc.headhunter')
                ->where('candidato', $candidato)
                ->where('vaga', $vaga)->first();
    return $headhunter->nome;
}

function buscaHeadhunterId($candidato, $vaga)
{
    $hh = DB::table('fila_candidatura as fc')->select('fc.headhunter')
                ->join('headhunters as hh', 'hh.id', 'fc.headhunter')
                ->where('candidato', $candidato)
                ->where('vaga', $vaga)->first();
    return $hh->headhunter;
}

function contaConviteAceitos($headhunter)
{
    return DB::table('convite_empresa_headhunters')->where('status_convite', 1)->where('headhunter', $headhunter)->count();
}

function countVagasStatus($status)
{
    return DB::table('vagas')->where('status_vaga', $status)->count();
}
function getVagasExpiram()
{
    return DB::table('vagas')->whereRaw('DATEDIFF(NOW(), DATA_LIMITE) < 5')->where('status_vaga', 1)->count();
}
function getCandidatosEnviados($headhunter)
{
    return DB::table('fila_candidatura')->where('headhunter', $headhunter)->count();
}

function getMinhasVagas($headhunter)
{
    return DB::table('vagas_headhunter as vh')
            ->join('vagas as v', 'v.id', 'vh.vaga')
            ->where('v.status_vaga', 1)
            ->where('vh.criado', 1)
            ->where('vh.headhunter', $headhunter)->count();
}

function getMinhasVagasExpiram($headhunter)
{
    return DB::table('vagas_headhunter as vh')
            ->join('vagas as v', 'v.id', 'vh.vaga')
            ->where('v.status_vaga', 1)
            ->where('vh.criado', 1)
            ->where('vh.headhunter', $headhunter)
            ->whereRaw('DATEDIFF(NOW(), v.DATA_LIMITE) < 5')->count();
}

function minhasVagas($headhunter)
{
    return DB::table('vagas_headhunter')
            ->join('vagas as v', 'v.id', 'vagas_headhunter.vaga')
            ->where('headhunter', $headhunter)
            ->where('v.status_vaga', 1)
            ->where('criado', 1)->count();
}

function minhasVagasAbertas($headhunter)
{
    return DB::table('vagas_headhunter as vh')
                ->join('vagas as v', 'v.id', 'vh.vaga')
                ->where('vh.headhunter', $headhunter)
                ->where('vh.criado', 1)
                ->where('v.status_vaga', 1)->count();
}

function minhasVagasAbertasQuinzeDias($headhunter)
{
    return DB::table('vagas_headhunter as vh')
                ->join('vagas as v', 'v.id', 'vh.vaga')
                ->where('vh.headhunter', $headhunter)
                ->where('vh.criado', 1)
                ->whereRaw('DATEDIFF(NOW(), v.DATA_LIMITE) > 15')->count();
}

function minhasVagasFinalizadas($headhunter)
{
    return DB::table('vagas_headhunter as vh')
        ->join('vagas as v', 'v.id', 'vh.vaga')
        ->where('vh.headhunter', $headhunter)
        ->where('vh.criado', 3)
        ->where('v.status_vaga', 1)->count();
}

function getAptidoes($vaga)
{
    return DB::table('vagas_aptidoes')->where('vaga', $vaga)->get();
}
function getIdiomas($vaga)
{
    return DB::table('vagas_idiomas as v')->select('v.*', 'i.*')
                ->join('idiomas as i', 'i.id', 'v.idioma')
                ->where('v.vaga', $vaga)
                ->get();
}

function minhasVagasCanceladas($headhunter)
{
    return DB::table('vagas_headhunter as vh')
        ->join('vagas as v', 'v.id', 'vh.vaga')
        ->where('vh.headhunter', $headhunter)
        ->where('vh.criado', 2)
        ->where('v.status_vaga', 1)->count();
}
function getEvolucaoCdd($candidato, $vaga)
{
    return DB::table('evolucao_candidatos')->where('candidato', $candidato)->where('vaga', $vaga)->get();
}

function verificaAlertaStatus($vaga, $headhunter)
{
    return DB::table('headhunter_alerta_status_vaga')->select('alerta_status')->where('vaga', $vaga)->where('headhunter', $headhunter)->count();
}
function getAlertas($headhunter, $vaga)
{
    return  DB::table('headhunters_alertas')->where('vaga', $vaga)->where('headhunter', $headhunter)->get();
}
function contaAlerta($alerta, $headhunter, $vaga)
{
    return DB::table('headhunters_alertas')->where('alerta', $alerta)->where('headhunter', $headhunter)->where('vaga', $vaga)->count();
}

function estadoCivil()
{
    return DB::table("estado_civil")->get();
}
function dispContratacao()
{
    return DB::table('disp_contratacao')->get();
}