<?php
use Illuminate\Support\Facades\DB;

/*
* Envia email para as empresas informando sobre as vagas que estão prestes a vencer
*/ 
function enviaEmailVagaVencidaEmpresa()
{
    $empresas = App\Vaga::select('vagas.empresa')->join('cargos as cg', 'cg.id', 'vagas.tituloCargo')->whereRaw('DATEDIFF(DATA_LIMITE, NOW()) <= 5')->where('status_vaga', 1)->groupBy('empresa')->get();
    foreach($empresas as $empresa)
    {
        $vagas = App\Vaga::select('cg.cargo', 'vagas.data_limite', 'vagas.id')->join('cargos as cg', 'cg.id', 'vagas.tituloCargo')->whereRaw('DATEDIFF(DATA_LIMITE, NOW()) <= 5')->where('status_vaga', 1)->where('vagas.empresa', $empresa->empresa)->get();
        $usuario = App\EmpresaUsuario::where('empresa', $empresa->id)->where('tipo', 1)->first();
        //return new App\Mail\EmpVagaVencendo($vagas);
        Mail::to('cesarvinici@gmail.com')->send(new App\Mail\EmpVagaVencendo($vagas));
    }
}

/*
* Envia email para para empresa informando sobre as vagas que foram finalizadas 
* pois esgotou o prazo para contratação
*/ 
function finalizaVagasEmpresas()
{
    $empresas = App\EmpresaUsuario::select('empresa')->whereIn('empresa', function($query)
    {
        $query->select('empresa')->from('vagas')
                    ->whereRaw('DATEDIFF(DATA_LIMITE, NOW()) < 0')
                    ->where('vagas.status_vaga', 1)
                    ->get();
    })->where('tipo', 1)->groupBy('empresa')->get();

    if(sizeof($empresas))
    {
        foreach($empresas as $empresa)
        {
            $vagas = App\Vaga::select('cg.cargo', 'vagas.data_limite', 'vagas.id')->join('cargos as cg', 'cg.id', 'vagas.tituloCargo')->whereRaw('DATEDIFF(DATA_LIMITE, NOW()) < 0')->where('status_vaga', 1)->where('vagas.empresa', $empresa->empresa)->get();
            foreach($vagas as $vaga)
            {
                $aux = App\Vaga::find($vaga->id);
                $aux->status_vaga = 2;
                $aux->save(); 
            }
            $usuario = App\EmpresaUsuario::where('empresa', $empresa->id)->where('tipo', 1)->first();
            //return new App\Mail\EmpresaVagaFinalizada($vagas);
            Mail::to('cesarvinici@gmail.com')->send(new App\Mail\EmpresaVagaFinalizada($vagas));
        }
    }   
}

/*
* Envia email para os headhunters informando sobre as vagas que estão prestes a vencer
*/ 
function enviaEmailVagaVencidaHeadhunter()
{
    $headhunters =  App\VagaHeadhunter::select('vagas_headhunter.headhunter', 'head.email')
    ->join('vagas as v', 'v.id', 'vagas_headhunter.vaga')
    ->join('headhunters as head', 'head.id', 'vagas_headhunter.headhunter')
    ->whereRaw('DATEDIFF(v.DATA_LIMITE, NOW()) <= 5')
    ->where('v.status_vaga', 1)
    ->where('vagas_headhunter.criado', 1)
    ->groupBy('vagas_headhunter.headhunter')->get();
   
    foreach($headhunters as $headhunter)
    {
        $vagas = App\VagaHeadhunter::select('cg.cargo', 'v.data_limite', 'v.id', 'emp.nome_fantasia')
            ->join('vagas as v', 'v.id', 'vagas_headhunter.vaga')
            ->join('cargos as cg', 'v.tituloCargo', 'cg.id')
            ->join('empresas as emp', 'emp.id', 'v.empresa')          
            ->whereRaw('DATEDIFF(v.DATA_LIMITE, NOW()) <= 5')
            ->where('v.status_vaga', 1)
            ->where('vagas_headhunter.criado', 1)
            ->where('vagas_headhunter.headhunter', $headhunter->headhunter)->get();
        // return new App\Mail\HeadVagasVencendo($vagas);
        Mail::to('cesarvinici@gmail.com')->send(new App\Mail\HeadVagasVencendo($vagas));
    }
}
/*
* Envia email para para headhunter informando sobre as vagas que foram finalizadas 
* pois esgotou o prazo para contratação
*/ 
function finalizaVagasHeadhunter()
{
    $headhunters =  App\VagaHeadhunter::select('vagas_headhunter.headhunter', 'head.email')
    ->join('vagas as v', 'v.id', 'vagas_headhunter.vaga')
    ->join('headhunters as head', 'head.id', 'vagas_headhunter.headhunter')
    ->whereRaw('DATEDIFF(v.DATA_LIMITE, NOW()) < 0')
    ->where('v.status_vaga', 1)
    ->where('vagas_headhunter.criado', 1)
    ->groupBy('vagas_headhunter.headhunter')->get();
    foreach($headhunters as $headhunter)
    {
        $vagas = App\VagaHeadhunter::select('cg.cargo', 'v.data_limite', 'v.id', 'emp.nome_fantasia')
            ->join('vagas as v', 'v.id', 'vagas_headhunter.vaga')
            ->join('cargos as cg', 'v.tituloCargo', 'cg.id')
            ->join('empresas as emp', 'emp.id', 'v.empresa')          
            ->whereRaw('DATEDIFF(v.DATA_LIMITE, NOW()) < 0')
            ->where('v.status_vaga', 1)
            ->where('vagas_headhunter.criado', 1)
            ->where('vagas_headhunter.headhunter', $headhunter->headhunter)->get();
        foreach($vagas as $vaga)
        {
            $aux = App\Vaga::find($vaga->id);
            $aux->status_vaga = 2;
            $aux->save();
        }
        //return new App\Mail\HeadhunterVagaFinalizada($vagas);
        Mail::to('cesarvinici@gmail.com')->send(new App\Mail\HeadhunterVagaFinalizada($vagas));
    }
}
/*
* Envia email para headhunter sempre que a empresa evoluir um candidato 
*/ 
function emailEvolucaoCandidato($evolucao)
{
    //return new App\Mail\EvolucaoCandidato($evolucao);
    Mail::to('cesarvinici@gmail.com')->send(new App\Mail\EvolucaoCandidato($evolucao));
}
/*
* Envia email para headhunter que for convidado pela empresa para trabalhar em uma vaga
*/ 
function emailConviteHeadhunter($id)
{
    $convite = App\ConviteEmpresaHeadhunter::select('head.email', 'data_limite', 'cg.cargo', 'emp.nome_fantasia')
    ->join('headhunters as head', 'head.id', 'convite_empresa_headhunters.headhunter')
    ->join('vagas as v', 'v.id', 'convite_empresa_headhunters.vaga')
    ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
    ->join('empresas as emp', 'emp.id', 'v.empresa')
    ->where('convite_empresa_headhunters.id', $id)->first(); 
 
    //return new App\Mail\ConvidaHeadhunter($convite);    
    Mail::to('cesarvinici@gmail.com')->send(new App\Mail\ConvidaHeadhunter($convite));      
}
/*
* Envia email para empresa sempre que um headhunter responder a um convite
*/ 
function respostaConvite($id)
{
    $convite = App\ConviteEmpresaHeadhunter::select('head.email', 'head.nome','v.id', 'data_limite', 'emp.id as empresa', 'cg.cargo', 'convite_empresa_headhunters.status_convite')
    ->join('headhunters as head', 'head.id', 'convite_empresa_headhunters.headhunter')
    ->join('vagas as v', 'v.id', 'convite_empresa_headhunters.vaga')
    ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
    ->join('empresas as emp', 'emp.id', 'v.empresa')
    ->where('convite_empresa_headhunters.id', $id)->first();

    $empresa = App\EmpresaUsuario::where('empresa', $convite->empresa)->where('tipo', 1)->first();
    
    //return new App\Mail\RespostaConvite($convite);    
   Mail::to('cesarvinici@gmail.com')->send(new App\Mail\RespostaConvite($convite)); 
}

/**
 * Alerta para headhunters sempre que o status da vaga sofrer modificações
 */
function alertaStatusAlterado($id)
{
    $vagas = App\HeadhunterAlertaStatusVaga::select('head.email', 'vs.status_vaga', 'cg.cargo', 'emp.nome_fantasia', 'v.id')
    ->join('headhunters as head', 'headhunter_alerta_status_vaga.headhunter', 'head.id')
    ->join('vagas as v', 'v.id', 'headhunter_alerta_status_vaga.vaga')
    ->join('empresas as emp', 'emp.id', 'v.empresa')
    ->join('vaga_status as vs', 'vs.id', 'v.status_vaga')
    ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
    ->where('v.id', $id )->get();

    //return new App\Mail\AlertaStatusVaga($vaga);
    foreach($vagas as $vaga)
    {
        Mail::to('cesarvinici@gmail.com')->send(new App\Mail\AlertaStatusVaga($vaga));
    }
}