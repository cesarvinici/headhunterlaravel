<?php

use Illuminate\Support\Facades\DB;

// function carregaFilaCandidatura()
// {
//     $candidatos = DB::table('candidatos as cdd')
//         ->select('cdd.*', 'ct.CT_NOME', 'est.UF_UF')
//         ->join('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
//         ->join('estados as est', 'est.UF_ID', 'ct.CT_UF')
//         ->whereIn('cdd.id', function($query)
//         {
//             $query->select('candidato')->from('candidato_vagas');
//         })
//         ->whereNotIn('cdd.id', function ($query)
//         {
//             $query->select('candidato')->from('fila_candidatura');
//         })->get();
   
//     return $candidatos;
// }

function carregaCandidatosdaFila($vaga)
{
    $candidatos = DB::table('fila_candidatura as fc')
        ->select('cdd.*', 'ct.CT_NOME', 'est.UF_UF', 'fc.created_at as adicionado_em')
        ->join('candidatos as cdd', 'cdd.id', 'fc.candidato')
        ->leftJoin('cidades as ct', 'ct.CT_ID', 'cdd.cidade')
        ->leftJoin('estados as est', 'est.UF_ID', 'ct.CT_UF')
        ->where('fc.vaga', $vaga)
        ->get();

    return $candidatos;

}

function candidatoExperiencia($candidato)
{
    $experiencia = DB::table('candidato_experiencia as exp')->select("exp.*", 'cg.cargo')
                    ->join('cargos as cg', 'cg.id', 'exp.cargo')
                    ->where('exp.candidato', $candidato)
                    ->orderBy('exp.admissao', 'desc')->limit(2)->get();
    
    return $experiencia;
}

function candidatoIdiomas($candidato)
{
    $idiomas = DB::table('candidatos_idiomas as ci')->select('ci.*', 'i.idioma')
                ->join('idiomas as i', 'i.id', 'ci.idioma' )->where('candidato', $candidato)->get();
    return $idiomas;
}

function processoSelecaoCdd($candidato)
{
    $processo = DB::table('fila_candidatura as fc')->select('cg.cargo', 'h.nome', DB::raw('DATEDIFF(now(), fc.created_at) as data'))
    ->join('headhunters as h', 'h.id', 'fc.headhunter')
    ->join('vagas as v', 'v.id', 'fc.vaga')
    ->join('cargos as cg', 'cg.id', 'v.tituloCargo')
    ->where('fc.candidato', $candidato)
    ->get();

    return  $processo;
}