<?php

function intToMoney($valor)
{
    return number_format(floatval($valor),2,",",".");
}

function moneyToInt($valor)
{
    $valor = str_replace("R$", "", $valor);
    $valor = str_replace(',','.',str_replace('.','',$valor));
    return $valor;
}


function nivelAptidao($nivel)
{
    switch ($nivel)
    {
        case '1':
            return "BÁSICO";
            break;
        case '2':
            return "INTERMEDIÁRIO";
            break;
        case '3':
            return "AVANÇADO";
            break;
        default:
            break;
    }
}

function nivelIdioma($nivel)
{
    switch ($nivel)
    {
        case '0':
            return "Basico";
            break;
        case '1':
            return "Intermediário";
            break;
        case '2':
            return "Avançado";
            break;
        default:
            break;
    }
}

function getIdade($data)
{
    if(!$data) return "";
    // Separa em dia, mês e ano
    list($dia, $mes, $ano) = explode('/', $data);

    // Descobre que dia é hoje e retorna a unix timestamp
    $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
    // Descobre a unix timestamp da data de nascimento do fulano
    $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);

    // Depois apenas fazemos o cálculo já citado :)
    $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

    return $idade;
}

function diaDaSemana($dia)
{
    switch ($dia)
    {
        case 1:
            return "Segunda-feira";
            break;
        case 2:
            return "Terça-feira";
            break;
        case 3:
            return "Quarta-feira";
            break;
        case 4:
            return "Quinta-feira";
            break;
        case 5:
            return "Sexta-feira";
            break;
        case 6:
            return "Sábado";
            break;
        case 7:
            return "Domingo";
            break;
    }
}

function dataToMysql($data)
{
    return implode('-', array_reverse(explode("/", $data)));
}

function MysqlToData($data)
{
    if($data)
        return date('d/m/Y', strtotime($data));
}

function mesAno($data)
{
    if(!$data)
    {
        return "";
    }
    return date('m/Y', strtotime($data));
}

function statusEvolucao($status)
{
    switch ($status)
    {
        case 1:
            return "Aprovado";
            break;
        case 2:
            return "Em Analise";
            break;
        case 3:
            return "Reprovado";
            break;
    }

}

function getStatusConvite($status)
{
    switch($status)
    {
        case 1:
            return "Aceito";
        case 2:
            return "Recusado";
    }
}