<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AlertaStatusVaga extends Mailable
{
    use Queueable, SerializesModels;
    public $vaga;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($vaga)
    {
        $this->vaga = $vaga;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('E-Headhunters  - Alteração Status Vaga')->markdown('vendor.notifications.alertaheadhunter');
    }
}
