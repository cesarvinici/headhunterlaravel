<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConvidaHeadhunter extends Mailable
{
    use Queueable, SerializesModels;
    public $convite;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($convite)
    {
        $this->convite = $convite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('E-Headhunters  - Convite para trabalhar vaga')->markdown('vendor.notifications.conviteHeadhunter');

    }
}
