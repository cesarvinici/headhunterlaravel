<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\EmpresaUsuario;
use App\Vaga;

class EmpVagaVencendo extends Mailable
{
    use Queueable, SerializesModels;

    public $empresas;
    public $vagas;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($vagas)
    {
        $this->vagas = $vagas;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        $this->subject('E-Headhunters  - Aviso vencimento vagas')->markdown('vendor.notifications.empresaVagaVencendo');
    }
}
