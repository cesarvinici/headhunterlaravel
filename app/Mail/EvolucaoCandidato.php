<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EvolucaoCandidato extends Mailable
{
    use Queueable, SerializesModels;
    public $evolucao;
    /**
     * Create a new message instance. 
     *
     * @return void
     */
    public function __construct($evolucao)
    {
        $this->evolucao = $evolucao;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('E-Headhunters  - Aviso evolução de candidato')->markdown('vendor.notifications.evolucaoCandidato');
    }
}
