<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RespostaConvite extends Mailable
{
    use Queueable, SerializesModels;
    public $convite;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($convite)
    {
        $this->convite = $convite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('E-Headhunters - Resposta Convite')->markdown('vendor.notifications.respostaConvite');
    }
}
