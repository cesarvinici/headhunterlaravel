<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VagaAptidao extends Model
{
    //
    protected $table = 'vagas_aptidoes';
    public $timestamps = false;
}
