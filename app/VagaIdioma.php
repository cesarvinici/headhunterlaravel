<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VagaIdioma extends Model
{
    //
    protected $table = "vagas_idiomas";
    public $timestamps = false;
}
