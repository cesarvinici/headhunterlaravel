<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCandidatosIdiomasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('candidatos_idiomas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('candidato')->nullable()->index('candidato');
			$table->string('idioma', 50)->nullable();
			$table->string('nivel', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('candidatos_idiomas');
	}

}
