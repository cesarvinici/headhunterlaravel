<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCandidatosRelatoriosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('candidatos_relatorios', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('candidato')->nullable()->index('candidato');
			$table->string('relatorio', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('candidatos_relatorios');
	}

}
