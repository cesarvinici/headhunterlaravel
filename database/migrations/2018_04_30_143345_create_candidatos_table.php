<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCandidatosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('candidatos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('pcd')->nullable();
			$table->string('nome', 150)->nullable();
			$table->string('cpf', 14)->unique('cpf');
			$table->string('email', 50)->nullable()->unique('email');
			$table->string('telefone', 30)->nullable();
			$table->string('celular', 30)->nullable();
			$table->string('estado_civil', 20)->nullable();
			$table->date('nascimento')->nullable();
			$table->string('endereco', 150)->nullable();
			$table->integer('cidade')->nullable();
			$table->string('cep', 100)->nullable();
			$table->string('pais', 20)->nullable();
			$table->string('instagram', 50)->nullable();
			$table->string('twitter', 50)->nullable();
			$table->text('objetivo', 65535)->nullable();
			$table->text('resumo_qualidades', 65535)->nullable();
			$table->boolean('trabalha_atualmente')->nullable();
			$table->integer('disponivel_contratacao')->nullable();
			$table->integer('escolaridade')->nullable();
			$table->integer('status_escolaridade')->nullable();
			$table->string('instituicao', 100)->nullable();
			$table->string('curso', 100)->nullable();
			$table->string('duracao', 20)->nullable();
			$table->string('ultima_experiencia', 100)->nullable();
			$table->string('cargo_ultima_experiencia', 100)->nullable();
			$table->text('atividades_ultima_experiencia', 65535)->nullable();
			$table->date('admissao_ultima_experiencia')->nullable();
			$table->date('desligamento_ultima_experiencia')->nullable();
			$table->string('penultima_experiencia', 100)->nullable();
			$table->string('cargo_penultima_experiencia', 100)->nullable();
			$table->text('atividades_penultima_experiencia', 65535)->nullable();
			$table->date('admissao_penultima_experiencia')->nullable();
			$table->date('desligamento_penultima_experiencia')->nullable();
			$table->string('antepenultima_experiencia', 100)->nullable();
			$table->string('cargo_antepenultima_experiencia', 100)->nullable();
			$table->text('atividades_antepenultima_experiencia', 65535)->nullable();
			$table->date('admissao_antepenultima_experiencia')->nullable();
			$table->date('desligamento_antepenultima_experiencia')->nullable();
			$table->decimal('pretensao_salarial', 10)->nullable();
			$table->integer('nivel_hierarquico')->nullable();
			$table->boolean('disposicao_viagem')->nullable();
			$table->string('frequencia_viagens', 20)->nullable();
			$table->integer('publicidade_curriculum')->nullable()->index('publicidade_curriculum');
			$table->string('foto_perfil', 100)->nullable();
			$table->string('curriculum')->nullable();
			$table->dateTime('dataCriacao')->nullable();
			$table->dateTime('ultimaAtualizacao')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('candidatos');
	}

}
