<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpresasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empresas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome_fantasia', 100)->nullable();
			$table->string('razao_social', 100)->nullable();
			$table->string('cnpj', 20)->nullable();
			$table->string('telefone1', 20)->nullable();
			$table->string('endereco_logradouro', 50)->nullable();
			$table->integer('endereco_numero')->nullable();
			$table->string('endereco_bairro', 50)->nullable();
			$table->integer('endereco_cidade')->nullable();
			$table->string('endereco_complemento', 50)->nullable();
			$table->string('endereco_cep', 10)->nullable();
			$table->string('website', 100)->nullable();
			$table->string('segmento', 50)->nullable();
			$table->integer('numero_funcionarios')->nullable();
			$table->string('logotipo', 50)->nullable();
			$table->string('responsavel_financeiro_nome', 50)->nullable();
			$table->string('responsavel_financeiro_cargo', 50)->nullable();
			$table->string('responsavel_financeiro_email', 50)->nullable();
			$table->string('responsavel_financeiro_telefone', 50)->nullable();
			$table->dateTime('data_criacao')->nullable();
			$table->dateTime('ultima_modificacao')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empresas');
	}

}
