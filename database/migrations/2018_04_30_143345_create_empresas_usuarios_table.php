<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpresasUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empresas_usuarios', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('empresa')->nullable()->index('empresa');
			$table->string('nome', 50)->nullable();
			$table->string('nome_sobrenome', 50)->nullable();
			$table->string('cargo', 50)->nullable();
			$table->string('email', 50)->nullable();
			$table->string('tipo', 20)->nullable();
			$table->string('imagem_perfil', 50)->nullable();
			$table->string('pass', 50)->nullable();
			$table->integer('atividade')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empresas_usuarios');
	}

}
