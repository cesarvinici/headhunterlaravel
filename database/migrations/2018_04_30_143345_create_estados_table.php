<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estados', function(Blueprint $table)
		{
			$table->integer('UF_ID', true);
			$table->string('UF_NOME', 75)->nullable();
			$table->string('UF_UF', 2)->nullable();
			$table->integer('UF_IBGE')->nullable();
			$table->integer('UF_SL')->nullable();
			$table->string('UF_DDD', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estados');
	}

}
