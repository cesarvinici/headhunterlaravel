<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFilaCandidaturaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fila_candidatura', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('vaga')->nullable()->index('vaga');
			$table->integer('candidato')->nullable()->index('candidato');
			$table->integer('headhunter')->nullable()->index('headhunter');
			$table->timestamp('adicionadoEm')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fila_candidatura');
	}

}
