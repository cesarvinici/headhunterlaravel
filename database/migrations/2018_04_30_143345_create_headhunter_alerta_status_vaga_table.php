<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHeadhunterAlertaStatusVagaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('headhunter_alerta_status_vaga', function(Blueprint $table)
		{
			$table->boolean('recebeAlerta')->nullable();
			$table->integer('headhunter')->nullable()->index('headhunter');
			$table->integer('vaga')->nullable()->index('vaga');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('headhunter_alerta_status_vaga');
	}

}
