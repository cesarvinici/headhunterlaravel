<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHeadhunterCandidatosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('headhunter_candidatos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('candidato')->nullable()->index('candidato');
			$table->integer('headhunter')->nullable()->index('headhunter');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('headhunter_candidatos');
	}

}
