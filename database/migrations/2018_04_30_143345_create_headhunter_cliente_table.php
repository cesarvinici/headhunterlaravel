<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHeadhunterClienteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('headhunter_cliente', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('headhunter')->nullable()->index('headhunter');
			$table->integer('empresa')->nullable()->index('empresa');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('headhunter_cliente');
	}

}
