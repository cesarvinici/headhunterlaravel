<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHeadhuntersAlertasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('headhunters_alertas', function(Blueprint $table)
		{
			$table->integer('alerta')->nullable()->index('alerta');
			$table->integer('headhunter')->nullable()->index('headhunter');
			$table->integer('vaga')->nullable()->index('vaga');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('headhunters_alertas');
	}

}
