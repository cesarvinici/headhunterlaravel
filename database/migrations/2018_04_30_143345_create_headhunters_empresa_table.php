<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHeadhuntersEmpresaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('headhunters_empresa', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('headhunter')->nullable()->index('headhunter');
			$table->integer('empresa')->nullable()->index('empresa');
			$table->string('tipo_atuacao', 100)->nullable();
			$table->text('segmentos', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('headhunters_empresa');
	}

}
