<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHeadhuntersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('headhunters', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 50);
			$table->string('cpf', 11)->unique('cpf');
			$table->string('telefone', 20)->nullable();
			$table->string('celular', 20)->nullable();
			$table->string('email', 50)->nullable();
			$table->string('endereco_logradouro', 50)->nullable();
			$table->integer('endereco_numero')->nullable();
			$table->string('endereco_bairro', 50)->nullable();
			$table->integer('endereco_cidade')->nullable();
			$table->string('endereco_complemento', 50)->nullable();
			$table->string('endereco_cep', 10)->nullable();
			$table->string('endereco_pais', 50)->nullable();
			$table->string('imagem_perfil', 50)->nullable();
			$table->string('curriculo', 50)->nullable();
			$table->string('facebook', 50)->nullable();
			$table->string('linkedin', 50)->nullable();
			$table->string('instagram', 50)->nullable();
			$table->string('twitter', 50)->nullable();
			$table->string('site_pass', 50)->nullable();
			$table->dateTime('data_cadastro')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('headhunters');
	}

}
