<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHeadhuntersUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('headhunters_usuarios', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('headhunter')->nullable()->index('headhunter');
			$table->string('nome', 50)->nullable();
			$table->string('nome_sobrenome', 50)->nullable();
			$table->string('email', 50)->nullable();
			$table->integer('tipo')->nullable();
			$table->string('imagem_perfil', 100)->nullable();
			$table->string('pass', 50)->nullable();
			$table->boolean('ativo')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('headhunters_usuarios');
	}

}
