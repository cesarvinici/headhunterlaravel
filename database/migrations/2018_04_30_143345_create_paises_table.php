<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaisesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('paises', function(Blueprint $table)
		{
			$table->integer('SL_ID', true);
			$table->string('SL_NOME', 60)->nullable();
			$table->string('SL_NOME_PT', 60)->nullable();
			$table->string('SL_SIGLA', 2)->nullable();
			$table->integer('SL_BACEN')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('paises');
	}

}
