<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVagasAptidoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vagas_aptidoes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('vaga')->nullable()->index('vaga');
			$table->text('aptidoes', 65535)->nullable();
			$table->integer('nivel_requerido')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vagas_aptidoes');
	}

}
