<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVagasHeadhunterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vagas_headhunter', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('headhunter')->nullable()->index('headhunter');
			$table->integer('vaga')->nullable()->index('vaga');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vagas_headhunter');
	}

}
