<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVagasIdiomasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vagas_idiomas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('idioma', 20);
			$table->integer('vaga')->nullable()->index('vaga');
			$table->boolean('desejavel')->nullable();
			$table->integer('nivel')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vagas_idiomas');
	}

}
