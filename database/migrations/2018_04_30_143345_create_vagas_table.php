<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVagasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vagas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('vaga_pcd');
			$table->integer('empresa')->nullable()->index('empresa');
			$table->string('tituloCargo', 50)->nullable();
			$table->integer('numVagas')->nullable();
			$table->boolean('sigilo')->nullable();
			$table->string('formacao_exigida', 300)->nullable();
			$table->text('principais_atividades', 65535)->nullable();
			$table->text('requisitos', 65535)->nullable();
			$table->string('disposicao_viagens', 50)->nullable();
			$table->string('frequencia_viagem', 50)->nullable();
			$table->boolean('naoExibirSalario');
			$table->integer('salario')->nullable();
			$table->string('faixa_salarial', 50)->nullable();
			$table->text('comissao', 65535)->nullable();
			$table->boolean('naoExibirBeneficio');
			$table->text('beneficios', 65535)->nullable();
			$table->integer('regime_contratacao')->nullable()->index('regime_contratacao');
			$table->string('horario_de_trabalho', 50)->nullable();
			$table->text('informacoes_adicionais_vaga', 65535)->nullable();
			$table->boolean('candidatos_outro_estado')->nullable();
			$table->string('estados_e_regioes', 50)->nullable();
			$table->string('auxilio_mudanca')->nullable();
			$table->integer('dias_para_contratacao')->nullable();
			$table->date('data_limite')->nullable();
			$table->string('etapas_processo_seletivo', 150)->nullable();
			$table->string('etapas_processo_seletivo_outros', 100)->nullable();
			$table->integer('feeRecrutamento')->nullable();
			$table->integer('limite_curriculum_headhunter')->nullable();
			$table->string('documentos_requeridos')->nullable();
			$table->string('comentarios_e_recomendacoes', 300)->nullable();
			$table->integer('responsavel_recrutamento')->nullable();
			$table->boolean('ativa')->nullable()->default(1);
			$table->dateTime('data_criacao')->nullable();
			$table->dateTime('ultima_modificacao')->nullable();
			$table->integer('status_vaga')->nullable()->index('status_vaga');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vagas');
	}

}
