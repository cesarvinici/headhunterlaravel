<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCandidatosRelatoriosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('candidatos_relatorios', function(Blueprint $table)
		{
			$table->foreign('candidato', 'candidatos_relatorios_ibfk_1')->references('id')->on('candidatos')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('candidatos_relatorios', function(Blueprint $table)
		{
			$table->dropForeign('candidatos_relatorios_ibfk_1');
		});
	}

}
