<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCandidatosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('candidatos', function(Blueprint $table)
		{
			$table->foreign('publicidade_curriculum', 'candidatos_ibfk_1')->references('id')->on('curriculum_publicidade')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('candidatos', function(Blueprint $table)
		{
			$table->dropForeign('candidatos_ibfk_1');
		});
	}

}
