<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmpresasUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('empresas_usuarios', function(Blueprint $table)
		{
			$table->foreign('empresa', 'empresas_usuarios_ibfk_1')->references('id')->on('empresas')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('empresas_usuarios', function(Blueprint $table)
		{
			$table->dropForeign('empresas_usuarios_ibfk_1');
		});
	}

}
