<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFilaCandidaturaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fila_candidatura', function(Blueprint $table)
		{
			$table->foreign('vaga', 'fila_candidatura_ibfk_1')->references('id')->on('vagas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('candidato', 'fila_candidatura_ibfk_2')->references('id')->on('candidatos')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('headhunter', 'fila_candidatura_ibfk_3')->references('id')->on('headhunters')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fila_candidatura', function(Blueprint $table)
		{
			$table->dropForeign('fila_candidatura_ibfk_1');
			$table->dropForeign('fila_candidatura_ibfk_2');
			$table->dropForeign('fila_candidatura_ibfk_3');
		});
	}

}
