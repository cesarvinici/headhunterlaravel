<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHeadhunterAlertaStatusVagaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('headhunter_alerta_status_vaga', function(Blueprint $table)
		{
			$table->foreign('headhunter', 'headhunter_alerta_status_vaga_ibfk_1')->references('id')->on('headhunters')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('vaga', 'headhunter_alerta_status_vaga_ibfk_2')->references('id')->on('vagas')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('headhunter_alerta_status_vaga', function(Blueprint $table)
		{
			$table->dropForeign('headhunter_alerta_status_vaga_ibfk_1');
			$table->dropForeign('headhunter_alerta_status_vaga_ibfk_2');
		});
	}

}
