<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHeadhunterCandidatosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('headhunter_candidatos', function(Blueprint $table)
		{
			$table->foreign('candidato', 'headhunter_candidatos_ibfk_3')->references('id')->on('candidatos')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('headhunter', 'headhunter_candidatos_ibfk_4')->references('id')->on('headhunters')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('candidato', 'headhunter_candidatos_ibfk_5')->references('id')->on('candidatos')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('headhunter_candidatos', function(Blueprint $table)
		{
			$table->dropForeign('headhunter_candidatos_ibfk_3');
			$table->dropForeign('headhunter_candidatos_ibfk_4');
			$table->dropForeign('headhunter_candidatos_ibfk_5');
		});
	}

}
