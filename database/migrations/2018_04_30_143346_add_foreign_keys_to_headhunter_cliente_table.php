<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHeadhunterClienteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('headhunter_cliente', function(Blueprint $table)
		{
			$table->foreign('headhunter', 'headhunter_cliente_ibfk_1')->references('id')->on('headhunters')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('empresa', 'headhunter_cliente_ibfk_2')->references('id')->on('empresas')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('headhunter_cliente', function(Blueprint $table)
		{
			$table->dropForeign('headhunter_cliente_ibfk_1');
			$table->dropForeign('headhunter_cliente_ibfk_2');
		});
	}

}
