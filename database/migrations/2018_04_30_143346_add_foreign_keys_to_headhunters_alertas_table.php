<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHeadhuntersAlertasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('headhunters_alertas', function(Blueprint $table)
		{
			$table->foreign('alerta', 'headhunters_alertas_ibfk_1')->references('id')->on('vaga_alerta')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('headhunter', 'headhunters_alertas_ibfk_2')->references('id')->on('headhunters')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('vaga', 'headhunters_alertas_ibfk_3')->references('id')->on('vagas')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('headhunters_alertas', function(Blueprint $table)
		{
			$table->dropForeign('headhunters_alertas_ibfk_1');
			$table->dropForeign('headhunters_alertas_ibfk_2');
			$table->dropForeign('headhunters_alertas_ibfk_3');
		});
	}

}
