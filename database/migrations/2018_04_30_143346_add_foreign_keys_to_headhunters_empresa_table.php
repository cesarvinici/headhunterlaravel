<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHeadhuntersEmpresaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('headhunters_empresa', function(Blueprint $table)
		{
			$table->foreign('headhunter', 'headhunters_empresa_ibfk_1')->references('id')->on('headhunters')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('empresa', 'headhunters_empresa_ibfk_2')->references('id')->on('empresas')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('headhunters_empresa', function(Blueprint $table)
		{
			$table->dropForeign('headhunters_empresa_ibfk_1');
			$table->dropForeign('headhunters_empresa_ibfk_2');
		});
	}

}
