<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHeadhuntersUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('headhunters_usuarios', function(Blueprint $table)
		{
			$table->foreign('headhunter', 'headhunters_usuarios_ibfk_1')->references('id')->on('headhunters')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('headhunter', 'headhunters_usuarios_ibfk_2')->references('id')->on('headhunters')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('headhunter', 'headhunters_usuarios_ibfk_3')->references('id')->on('headhunters')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('headhunters_usuarios', function(Blueprint $table)
		{
			$table->dropForeign('headhunters_usuarios_ibfk_1');
			$table->dropForeign('headhunters_usuarios_ibfk_2');
			$table->dropForeign('headhunters_usuarios_ibfk_3');
		});
	}

}
