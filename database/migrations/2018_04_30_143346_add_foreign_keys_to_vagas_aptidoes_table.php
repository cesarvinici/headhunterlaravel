<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVagasAptidoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vagas_aptidoes', function(Blueprint $table)
		{
			$table->foreign('vaga', 'vagas_aptidoes_ibfk_1')->references('id')->on('vagas')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vagas_aptidoes', function(Blueprint $table)
		{
			$table->dropForeign('vagas_aptidoes_ibfk_1');
		});
	}

}
