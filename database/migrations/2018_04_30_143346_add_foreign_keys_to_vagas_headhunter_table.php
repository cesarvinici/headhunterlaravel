<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVagasHeadhunterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vagas_headhunter', function(Blueprint $table)
		{
			$table->foreign('headhunter', 'vagas_headhunter_ibfk_1')->references('id')->on('headhunters')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('vaga', 'vagas_headhunter_ibfk_2')->references('id')->on('vagas')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vagas_headhunter', function(Blueprint $table)
		{
			$table->dropForeign('vagas_headhunter_ibfk_1');
			$table->dropForeign('vagas_headhunter_ibfk_2');
		});
	}

}
