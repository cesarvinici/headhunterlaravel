<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVagasIdiomasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vagas_idiomas', function(Blueprint $table)
		{
			$table->foreign('vaga', 'vagas_idiomas_ibfk_1')->references('id')->on('vagas')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vagas_idiomas', function(Blueprint $table)
		{
			$table->dropForeign('vagas_idiomas_ibfk_1');
		});
	}

}
