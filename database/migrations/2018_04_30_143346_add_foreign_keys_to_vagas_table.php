<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVagasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vagas', function(Blueprint $table)
		{
			$table->foreign('empresa', 'vagas_ibfk_1')->references('id')->on('empresas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('regime_contratacao', 'vagas_ibfk_3')->references('id')->on('regime_contratacao')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('status_vaga', 'vagas_ibfk_4')->references('id')->on('vaga_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vagas', function(Blueprint $table)
		{
			$table->dropForeign('vagas_ibfk_1');
			$table->dropForeign('vagas_ibfk_3');
			$table->dropForeign('vagas_ibfk_4');
		});
	}

}
