<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableHeadhunterempresasCreatedUpdatedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('headhunters_empresa', function (Blueprint $table) {
            $table->dateTime('created_at')->nullable($value = true);		
            $table->dateTime('updated_at')->nullable($value = true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('headhunters_empresa', function (Blueprint $table) {
            //
        });
    }
}
