<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableCandidatoExperiencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidato_experiencia', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('candidato');
            $table->string('empresa', 100);
            $table->unsignedInteger('cargo');
            $table->string('atividades', 255);
            $table->date('admissao');
            $table->date('desligamento');
            $table->timestamps();
            $table->foreign('candidato')->references('id')->on('candidatos')->onDelete('cascade');
            $table->foreign('cargo')->references('id')->on('cargos');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidato_experiencia');
    }
}
