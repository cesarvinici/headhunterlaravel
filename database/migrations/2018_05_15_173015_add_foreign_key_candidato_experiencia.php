<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyCandidatoExperiencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidato_experiencia', function (Blueprint $table) {
            $table->integer('candidato')->nullable()->change();
            $table->integer('cargo')->nullable()->change();
            $table->foreign('candidato', 'candidato_experiencia_fk')->references('id')->on('candidatos')->onDelete('cascade');
            $table->foreign('cargo', 'cargo_experiencia_fk')->references('id')->on('cargos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidato_experiencia', function (Blueprint $table) {
            //
        });
    }
}
