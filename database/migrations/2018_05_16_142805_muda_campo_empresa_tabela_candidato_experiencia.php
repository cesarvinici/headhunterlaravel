<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MudaCampoEmpresaTabelaCandidatoExperiencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidato_experiencia', function (Blueprint $table) {
            $table->string('empresa', 100)->nullable()->change();
            $table->string('atividades', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidato_experiencia', function (Blueprint $table) {
            //
        });
    }
}
