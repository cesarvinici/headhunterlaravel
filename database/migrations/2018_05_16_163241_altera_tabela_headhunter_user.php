<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlteraTabelaHeadhunterUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('headhunters_usuarios', function (Blueprint $table) {
            
            //Cria novas colunas
            $table->rememberToken();
            $table->timestamps();
            $table->string('password', 100);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('headhunters_usuarios', function (Blueprint $table) {
            //
        });
    }
}
