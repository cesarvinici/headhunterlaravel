<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsereColunaRoleHeadhuntersUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('headhunters_usuarios', function (Blueprint $table) {
            $table->unsignedInteger('role');
           // $table->foreign('role')->references('id')->on('role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('headhunters_usuarios', function (Blueprint $table) {
            //
        });
    }
}
