<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AtualizaTabelaEmpresaUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas_usuarios', function (Blueprint $table) {
            //
            $table->string('password', 255);
            $table->rememberToken();
            $table->timestamps();
            $table->dropColumn('pass');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas_usuarios', function (Blueprint $table) {
            //
        });
    }
}
