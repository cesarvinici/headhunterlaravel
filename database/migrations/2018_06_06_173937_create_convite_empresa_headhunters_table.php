<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConviteEmpresaHeadhuntersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convite_empresa_headhunters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa')->nullable();
            $table->integer('headhunter')->nullable();
            $table->integer('vaga')->nullable();
            $table->string('assunto', 300);
            $table->text('mensagem');
            $table->integer('status_convite')->nullable();
            $table->timestamps();
            $table->foreign('empresa')->references('id')->on('empresas')
                        ->onDelete('cascade');
            $table->foreign('headhunter')->references('id')->on('headhunters')
                        ->onDelete('cascade');
            $table->foreign('vaga')->references('id')->on('vagas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convite_empresa_headhunters');
    }
}
