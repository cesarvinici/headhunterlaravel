<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvolucaoCandidatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evolucao_candidatos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vaga')->nullable();
            $table->integer('headhunter')->nullable();
            $table->integer('candidato')->nullable();
            $table->integer('etapa')->nullable();
            $table->integer('status')->nullable();
            $table->text('observacoes');
            $table->date('data');          
            $table->timestamps();

            $table->foreign('vaga')->references('id')->on('vagas');
            $table->foreign('headhunter')->references('id')->on('headhunters');
            $table->foreign('candidato')->references('id')->on('candidatos');
            $table->foreign('etapa')->references('id')->on('etapas_processo_seletivo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evolucao_candidatos');
    }
}
