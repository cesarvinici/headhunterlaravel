<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApagaColunasTableConvitesHeadhunter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convite_empresa_headhunters', function (Blueprint $table) {
            $table->dropColumn(['assunto', 'mensagem']);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convite_empresa_headhunters', function (Blueprint $table) {
            //
        });
    }
}
