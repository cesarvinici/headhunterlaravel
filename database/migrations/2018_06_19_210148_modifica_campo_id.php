<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificaCampoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('headhunter_alerta_status_vaga', function (Blueprint $table) {
            $table->bigIncrements('id')->before('recebeAlerta')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('headhunter_alerta_status_vaga', function (Blueprint $table) {
            //
        });
    }
}
