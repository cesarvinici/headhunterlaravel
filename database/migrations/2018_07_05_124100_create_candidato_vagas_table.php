<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatoVagasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidato_vagas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidato')->nullable();
            $table->integer('vaga')->nullable();
            $table->timestamps();

            $table->foreign('candidato')->references('id')->on('candidatos')->onDelete('cascade');
            $table->foreign('vaga')->references('id')->on('vagas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidato_vagas');
    }
}
