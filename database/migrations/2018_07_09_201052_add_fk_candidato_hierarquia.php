<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkCandidatoHierarquia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidatos', function (Blueprint $table) {
            $table->integer('nivel_hierarquico')->unsigned()->nullable()->change();

        });
        Schema::table('candidatos', function (Blueprint $table) {
            
            $table->foreign('nivel_hierarquico')->references('id')->on('candidatoNivelHierarquico');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidatos', function (Blueprint $table) {
            //
        });
    }
}
