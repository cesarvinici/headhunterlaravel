<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CandidatoNivelHierarquico extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       if(!sizeof(DB::table('candidatoNivelHierarquico')->get()))
       {
            $niveis = ['PROFISSIONAL ENSINO MÉDIO', 'PROFISSIONAL NÍVEL TÉCNICO', 'PROFISSIONAL NÍVEL SUPERIOR',
            'SUPERVISÃO', 'GERÊNCIA', 'DIRETORIA'];
            foreach($niveis as $nivel)
            {
                DB::table('candidatoNivelHierarquico')->insert([
                    'nivel' => $nivel,
                ]);
            }
       }
       
       
    }
}
