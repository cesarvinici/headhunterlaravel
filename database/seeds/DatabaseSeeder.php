<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CandidatoNivelHierarquico::class);
        $this->call(EstadoCivil::class);
        $this->call(alimenta_disp_contratacao::class);
    }
}
