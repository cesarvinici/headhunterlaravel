<?php

use Illuminate\Database\Seeder;

class EstadoCivil extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         if(!sizeof(DB::table('estado_civil')->get()))
         {
            $estados = ['CASADO(A)', 'DIVORCIADO(A)', 'SEPARADO(A)', 'SOLTEIRO(A)', 'UNIÃO ESTÁVEL', 'VIÚVO(A)'];
            foreach($estados as $estado)
            {
                DB::table('estado_civil')->insert(['estado_civil' => $estado]);
            }
            
         }
    }
}
