<?php

use Illuminate\Database\Seeder;

class alimenta_disp_contratacao extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!sizeof(DB::table('disp_contratacao')->get()))
        {
            $disponibilidade = ['IMEDIATA', '7 DIAS', '15 DIAS', '30 DIAS'];
            foreach($disponibilidade as $aux)
            {
                DB::table('disp_contratacao')->insert(['disponibilidade' => $aux]);
            }
        }
    }
}
