$(function()
{

	$("#tituloCargo, .tituloCargo").on('input', function()
	{
		if($(this).val().length >= 5)
		{
			let nextMsg = $(this).next().next();
			$.get("/dashboard/ajax/cargos/"+$(this).val(), function(data)
			{
				data = JSON.parse(data);
				var arr = $.map(data, function(el) { return el });
				$("#tituloCargo, .tituloCargo").autocomplete({
					source : arr,
					response: function(event, ui) {
						if (ui.content.length == 0) {
							nextMsg.text("Cargo não encontrado");
							// setTimeout(function(){$("#tituloCargo").val("")},5000)
							
						} else {
							nextMsg.empty();
						}
					}
				});				
			})	
		}		
	})

	$("#tituloCargo, .tituloCargo").change(function()
	{
		let elemento = $(this).next();
		
		let cargo = $(this).val() != "" ? $(this).val() : 0;

		$.get('/dashboard/ajax/cargos/buscaid/'+cargo, function(data)
		{
			if(data != '0')
			{
				
				elemento.val(data);
			}
			else
			{
				$("[name='idCargo']").val("");
			}
		})
	})


	// $("input").blur(function()
	// {
	// 	pattern = /senha/
	// 	let classes = $(this).parent().parent().attr("class");
	// 	if(!pattern.test(classes) && $(this).attr("id") != 'acaoSubmit')
	// 	{
	// 		$(this).val($(this).val().toUpperCase())	
	// 	}
	// })

	// $("textarea").blur(function()
	// {
	// 	$(this).val($(this).val().toUpperCase())
	// })

	$("#curriculo").change(function()
	{
		
		if(!validaPdf($(this)) || !validaTamArquivo(this))
		{
			$(this).val("");
		}		
	})

	$(".tabelaEmpresas").tablesorter({
		//pass the headers argument and assing a object 
        headers: { 
            
            4: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }, 
            
        } 
	});

	$(".tabelaVagas").tablesorter({
		//pass the headers argument and assing a object 
        headers: { 
            
            6: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }, 
            
        } 
	});

	$(".btn-voltar").click(function()
	{
		history.back();
	})

	$(".btn-voltar-cad").click(function()
	{
		location.href="/dashboard/headhunter/cadastros";
	})

	$(".filtroCliente").click(function()
	{ 
		event.preventDefault();
		$.ajax(
		{
			type: "POST",
			url: '/dashboard/ajax/filtroClienteConsulta',
			data: $("#filtro-clientes").serialize(),
			success: function(data)
			{
				$(".listaClientes").children().remove();
				$(".listaClientes").html(data);
				//$(".resultEnc").text($(".cont").length);
				
				$(".tabelaEmpresas").tablesorter({
					//pass the headers argument and assing a object 
					headers: { 
						
						4: { 
							// disable it by setting the property sorter to false 
							sorter: false 
						}, 
						
					} 
				});
				console.log(data);
			},
		});
	})
	
	$(".filtroVagaEmp").click(function()
	{
		event.preventDefault();
		let empresaId = $("#editEmpresa").val();
		let filtro = $(this).siblings().val();
		let campo  = filtro ? $(this).siblings().attr('name') : "";
		$.post("/dashboard/ajax/filtraVagasEmp", $("#filtroVagasEmp").serialize(), function(data)
		{
			console.log(data);
			$(".tbody").children().remove();
			$(".manut-vagas-page").html(data);
			$(".tabelaVagas").tablesorter({
				//pass the headers argument and assing a object 
				headers: { 
					
					6: { 
						// disable it by setting the property sorter to false 
						sorter: false 
					}, 
					
				} 
			});
		})
	})

	$(".datadeslig").blur(function()
	{
		let data_deslg = $(this).val();
		let data_adm = $(this).parent().parent().find(".data-adm").val();
		if(Date.parse(data_adm) > Date.parse(data_deslg))
		{
			$(this).parent().find(".data-erro").remove();
			$(this).parent().append('<span class="small text-danger data-erro">Data de admissão maior que da data de desligamento</span>')
		}
		else
		{
			$(this).parent().find(".data-erro").remove();
		}
	})

}) // fim funcão

function alteraStatus(id, elemento)
{
	let status = $(elemento).val();
	if(confirm("Confirma a mudança no status da vaga?"))
	{
		$.ajax(
		{
			type: "GET",
			url: '/dashboard/ajax/alteraStatusVaga/'+id+'/'+status,
			success: function(data)
			{
				console.log(data);
				if(parseInt(data))
				{
					alert("Status alterado com sucesso");
					//nviaEmailStatusVaga(id, status);
				}
			},
		});
	}
}

// Retorna informações de empresa selecionada no cadastro de vagas
function getEmpresa()
{
	let idEmp = $("#empresa option:selected").val();
	$.ajax(
	{
		type: "GET",
		url: '/dashboard/ajax/getInfoEmpresa/'+idEmp,
		success: function(data)
		{
			let emp = data;
			if(emp.length)
			{
				emp = JSON.parse(emp);
				$("#cidade").val(decodeURI(emp.cidade).toUpperCase());
				$("#estado").val(emp.estado.toUpperCase());
				$("#segmento").val(emp.segmento.toUpperCase());
			}
			
		},
	});
}
function deletaVaga(id)
{
	if(confirm("Confirma a exclusão da vaga?"))
	{
		location.href='/dashboard/ajax/deletaVaga/'+id;
	}
}

function editUserEmpresa(id)
{
	$(".conteudo-painel").children().remove();
	$.get('/dashboard/empresa/editUser/'+id, function(data)
	{
		$(".conteudo-painel").html(data);
	})

}

function novaGuia(evt, form)
{
	
	
		// Declare all variables
		hide();
		removeActive();
    	// Show the current tab, and add an "active" class to the button that opened the tab
    	document.getElementById(form).style.display = "block";
    	evt.currentTarget.className += " form-perfil__menu-link-ativo";

}	

function excluirHeadHunterUser(id)
{
	if(confirm("Confirma a exclusão deste usuário?"))
	{
		location.href="/dashboard/headhunter/usuarios/"+id+'/delete';
	}
}

function prosseguir(form, guia, page="")
{
	let validasal = true;
	
	let formulario = validaForm(page)
	let salario = validaSalario(page)
	let cargo = validaCargo(page)
	if(formulario && salario && cargo)
	{
		
		hide()
		removeActive()
		// Show the current tab, and add an "active" class to the button that opened the tab
		document.getElementById(form).style.display = "block";
		//evt.currentTarget.className += " form-perfil__menu-link-ativo";
		let element = document.getElementById(guia);
		//console.log(element);
		element.classList.add("form-perfil__menu-link-ativo");
		return true
	}
	
}

function validaCargo(page)
{
	if(page == 1 && $("[name='idCargo']").val() == "")
	{
		$("#tituloCargo").css("border", "solid red");
		return false
	}
	return true
}

function validaSalario(page="")
{
	if(page == 2 || page == "")
	{
		if($("[name='faixaSalarial']").val() == "" && $("[name='salarioMensal']").val() == "")
		{
			alert("Favor informar um salário ou faixa salarial");
			return false;
		}
		else
		{
			$(".divSalario").html("");
			return true;
		}
	}
	return true;
}

function hide()
{
	let i, tabcontent, tablinks;
	// Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tab");
	    for (i = 0; i < tabcontent.length; i++) {
	    	tabcontent[i].style.display = "none";
	    }
}

function removeActive()
{
	let i, tabcontent, tablinks;
	// Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	    	tablinks[i].className = tablinks[i].className.replace(" form-perfil__menu-link-ativo", "");
	    }
}


function validaDatas()
{
	let adm = document.getElementsByClassName("data-adm");
	let desl = document.getElementsByClassName("datadeslig");
	for (var i = 0; i < adm.length; i++)
	{
		if(Date.parse(adm[i].value) > Date.parse(desl[i].value))
		{
			$(desl[i]).parent().append('<span class="small text-danger data-erro">Data Incorreta</span>');
				
			return false;
		}
	}
	return true;
}

function deletaCliente(idCliente)
{
	if(confirm("Confirma a exclusão da empresa bem como todas as vagas e e informações vinculadas a ela?"))
	{
		location.href="/dashboard/headhunter/cliente/remover/"+idCliente;
	}
	
}
function excluirCandidato(id)
{
	if(confirm("Confirma a exclusão do candidato?"))
	{
		location.href="/dashboard/headhunter/candidatos/"+id+"/delete";
	}
}

function hideInfo()
{
	$(".vagas__resul-row").toggleClass("esconder");
}

function alertaHeahHunter()
{
	event.preventDefault();
	$.post('/dashboard/headhunters/criaAlerta', $("#form-alerta").serialize(), function(data)
	{
		if(data == 'ok')
		{
			alert("Alerta Salvo");
			location.reload();
		}
	})
}

function addCddFila(elemento, candidato)
{
	if(confirm("Confirma inclusão do candidato na fila de candidatura?"))
	{

		$.ajax(
		{
			type: "GET",
			url: '/dashboard/ajax/addCddFila/'+$("#idvaga").val()+'/'+candidato,
			success: function(data)
			{				
				if(parseInt(data))
				{
					alert("Candidato adicionado a fila com sucesso!");
					elemento.parentNode.parentNode.parentNode.remove();
					atualizaResults();
				}
				else				{
					alert("Não é possível adicionar candidato a fila pois o mesmo ja está em processo");
				}
			}
		});
	}
}

function filtrofilaGeral()
{
	event.preventDefault();
	$(".filaCandidaturaGeral").children().remove();

	$.ajax(
	{
		type: "POST",
		url: '/dashboard/ajax/filtroFilaCdd',
		data: $("#formFiltroFilaCdd").serialize(),
		success: function(data)
		{
			//console.log(data);
			//return true;
			
			$(".filaCandidaturaGeral").append(data);
			atualizaResults();
			$(".control").remove();
			//paginacao();
			
		}
	});
}

function filtrofilaCddParticipantes()
{
	event.preventDefault();
	$(".filtrofilaParticipantes").children().remove();

	$.ajax(
	{
		type: "POST",
		url: '/dashboard/ajax/filtrofilaCddParticipantes',
		data: $("#formFiltroCddParticipante").serialize(),
		complete: function(r)
		{
			console.log(r.responseText);
			$(".filtrofilaParticipantes").append(r.responseText);
			atualizaResults()
		}
	});
}

function carregaAnexoCddParticipantes(idVaga)
{
	$.get('/dashboard/ajax/mostraCddAnexos/'+idVaga, function(data)
	{
		$("#anexos").html(data);
		atualizaResults();		
	})
}

function filtroAnexoCdd()
{
	event.preventDefault();

	$.post('/dashboard/ajax/filtrocddAnexos', $("#form-cdd-anexos").serialize(), function(data)
	{
		$("#anexos").html(data);
		atualizaResults();		
	})
}

//Carrega lista de Cidades de acordo com estado selecionado no combobox
function carregaCidades(element, n)
{
	let estado = element.value;
	let id = n == 1 ? '#listaCidades' : "#listaCidades"+n;
	if(estado == "")
	{
		html = '<option value="" selected>CIDADES</option>';
		$(id).html(html);
		return;
	}
	$.ajax(
	{
		type: "GET",
		url: '/dashboard/ajax/listaCidades/'+estado,
		success: function(data)
		{			
			data = JSON.parse(data);
			html = '<option value="" selected>Cidade</option>';
			for (var i = 0; i < data.length; i++)
			{			
				html += '<option value="'+data[i]['CT_ID']+'">'+data[i]['CT_NOME']+'</option>'                        
				$(id).html(html);
			}	
		}
	});
}

function carregaCidadeCEP(estado, ibge)
{

	$.ajax(
	{
		type: "GET",
		url: '/dashboard/ajax/listaCidades/'+estado,
		
		complete: function(r)
		{
			let dados = r.responseText;
			dados = JSON.parse(dados);
			html = '<option value="">Cidade</option>';
			for (var i = 0; i < dados.length; i++)
			{
				let selected = ibge == dados[i]["CT_IBGE"] ? 'selected' : '';
				html += '<option '+ selected +' value="'+dados[i]['CT_ID']+'">'+dados[i]['CT_NOME'].toUpperCase()+'</option>'                        
				$("#listaCidades").html(html);

			}	
		}
	});

}

function carregaCddParticipantes(idVaga)
{
	$.ajax(
	{
		type: "GET",
		url: '/dashboard/ajax/carregaFilaParticipantes/'+idVaga,
		success: function(data)
		{
			$(".filtrofilaParticipantes").html(data);
			atualizaResults();
			$(".control").remove();		
			//paginacao();		
		}
	});
}



function atualizaResults()
{ // Atualiza campo resultados encontrados em 
	$("#resultadosFilaPartic").text($(".resultFiltroFilaPartic").length);
	$("#resultadosEnc").text($(".resultFiltro").length);
	$("#resultAnexoCddEnc").text($(".resultadoAnexosCddPartc").length);
}
function removeFotoPerfil(idUser)
{
	event.preventDefault();
	html ='<span>Imagem De Perfil</span><input type="file" name="imgPerfil" class="">';
    $(".form-perfil__img").html(html);
}

function excluirUsuarioEmpresa(id)
{
	if(confirm("Confirma a exclusão do usuário?"))
	{
		location.href='/dashboard/empresas/removeUsuario/'+id;
	}
}

function removeLogo()
{
	event.preventDefault();
	html ='<input type="file" name="logo" class="form-perfil__input-col" id="logotipo">';
    $(".changeLogo").html(html);
}

// empresas/minhas-vagas
function mostaDados(elemt, vaga)
{           
	$(elemt).find(".setaVaga").toggleClass("icn-resul-acesso-p-branco");
	$(elemt).find(".setaVaga").toggleClass("icn-resul-acesso-p-branco-invert");
	$(".vaga"+vaga).toggleClass("esconder");

	//carregaDivInfoVagas(vaga)
}

function filtroVagasMktPlace()
{
	event.preventDefault();
	$.post("/dashboard/ajax/filtroVagasMktPlace", $("#FormVagasMKtPlace").serialize(),function(data)
	{
		$("#listaVagasmktplace").html(data);
		//$(".qtdVagasmktplace").html($(".vagasmktplace .vagas__marketplace-titulo").length);
		$(".controlsmk").remove();
		//paginacao();
	} )
}

function filtroVagasInternas()
{
	event.preventDefault();
	$.post("/dashboard/ajax/filtroVagasInternas", $("#FormVagasInternas").serialize(), function(data)
	{
		$("#listaVagasInternas").html(data);
	})
}
function validaPdf(obj)
{
		let arquivo = $(obj).val()
		let patter = /(.[\w]{3}$)/
		let match = patter.exec(arquivo);
		if(!match || match[0] != '.pdf')
		{
			alert("Favor carregar um arquivo .pdf")
			return false
		}
		return true
}
function validaTamArquivo(obj)
{
	let tamanho = obj.files[0].size;
	if(tamanho > 2097152)
	{
		alert("Favor inserir arquivo menor que 2MB")
		return false;
	}
	return true
}


//submenus abas
//headhunter usuario
$("#gerenciamento-permissoes").hide();
$("#cadastro-usuarios").hide();
$("#submenu-usuarios-gerenciamento-permissoes").click(function () {
	$("#gerenciamento-permissoes").show();
	$("#cadastro-usuarios").hide();
});

$("#submenu-usuarios-cadastro-usuarios").click(function () {
	$("#cadastro-usuarios").show();
	$("#gerenciamento-permissoes").hide();
});

//headhunter taxa de sucesso
$("#servicos-marketplace").hide();
$("#dados-bancarios").hide();
$("#submenu-taxa-sucesso-servicos-marketplace").click(function () {
	$("#servicos-marketplace").show();
	$("#dados-bancarios").hide();
});

$("#submenu-taxa-sucesso-dados-bancarios").click(function () {
	$("#dados-bancarios").show();
	$("#servicos-marketplace").hide();
});