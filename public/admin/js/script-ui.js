
// Box pesquisa/consulta
$( function() {
  $( "#range-slider" ).slider({
    range: "min",
    value: 0,
    min: 1,
    max: 5000,
    slide: function( event, ui ) {
      $( "#amount" ).val( "R$ " + ui.value );
    }
  });
  $( "#amount" ).val( "R$ " + $( "#range-slider" ).slider( "value" ) );

  $( "#range-sliderMKT" ).slider({
    range: "min",
    value: 0,
    min: 1,
    max: 2500,
    slide: function( event, ui ) {
      $( "#amountMKT" ).val( "R$ " + ui.value );
    }
  });
  $( "#amountMKT" ).val( "R$ " + $( "#range-sliderMKT" ).slider( "value" ) );

  $( "#range-sliderInt" ).slider({
    range: "min",
    value: 0,
    min: 1,
    max: 2500,
    slide: function( event, ui ) {
      $( "#amountInt" ).val( "R$ " + ui.value );
    }
  });
  $( "#amountInt" ).val( "R$ " + $( "#range-sliderInt" ).slider( "value" ) );


} );


  // candidatos/index
  $(function(){
    //candidatos/index/perfil
    var item = '.candidatos-perfil .box__conteudo-nav-item';
    var acao = '.candidatos-perfil .icn-select-menu-m-branco';
    $(item).hide();
    $(acao).click(function(){
       $(item).toggle('500');
    });

    // candidatos/vagas
    $(function(){
      var item = '#candidatos-vagas .resul__mostra-conteudo';
      var acao = '#candidatos-vagas .resul__col-visualizar-1';
      $(item).hide();
      // $(acao).click(function () {
      //   $(item).toggle('500');
      // });
    });
  });

  function visualizar($vaga)
  {
    $('.mostra'+$vaga).toggle('500');
  }

  // empresas/minhas-vagas
// $(function () {
//   $("#headhunter-vagas #accordion").accordion(
//     {
//       active: false,
//       collapsible: true
//     }    
//   );
// });

$(function(){
  $('.vagas__informacoes-resultado-box-conteudo').hide();
});
$(function () {
  $('.headHunterPerfil-conteudo').hide();
  $('.headHunterPerfil-box').click(function () {
    $('.headHunterPerfil-conteudo').toggle('300');
    $('.headHunterPerfil-box').hide('300');
  });
});
function mostraInfoBox(el, $id)
{
    $(el).next('.vagas__informacoes-resultado-box-conteudo').toggle('300');
    $(el).hide('300');
}