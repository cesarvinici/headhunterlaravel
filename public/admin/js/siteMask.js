$(function()
{
		$('#cnpj').mask('00.000.000/0000-00', {reverse: true});
		$('#cnpjemp').mask('00.000.000/0000-00', {reverse: true});
  	$('#cpf').mask('000.000.000-00', {reverse: true});
  	$('.money').mask('#.##0,00', {reverse: true});
  	$('#cep').mask('00000-000');
  	$('#telefone, .telefone').mask('(00) 0000-0000');
  	$("#celular01").mask("(00) 00000-0000");
    $("#nascimento").mask("00/00/0000");
    $(".data").mask("00/00/0000");
    
  	$("#range-slider").change(function()
  	{
  		$('#amount').mask('#.##0,00', {reverse: true});
  	})
})