function validaForm(page = "")
{
    switch (page)
    {
		case 1:
			page = 'validatePage1';
			break;
		case 2:
			page = 'validatePage2';
			break;
		default:
			page = 'validate';
	}

	let forms = document.getElementsByClassName(page);
	let count = 0;
	// Pega todos os inputs com a classe validate e verifica quais estão com o value vazio
    for (let i = forms.length - 1; i >= 0; i--)
    {
		if (forms[i].value == "") {
			forms[i].style.border = "solid red";
			count++;
		}
		else {
			forms[i].style.border = "1px solid #ccc";
		}
	}
    if (count == 0)
    {
		$(".mensagem div").remove();
		return true;
	}
    else
    {

		$(".mensagem div").remove();
		let msgDanger = '<div class="mensagem__conteudo mensagem__conteudo-bg-red">';
		msgDanger += '<div class="mensagem__conteudo-texto">';
		msgDanger += '<p>Atenção! Existem campos obrigatórios que não foram preenchidos</p>';
		msgDanger += '</div></div>';
		$(".mensagem").append(msgDanger);
		return false;
	}
}



