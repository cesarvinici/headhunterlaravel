(function ($) {
  $(function () {

    $('.sidenav').sidenav();
    $('.parallax').parallax();
    // $('.modal').modal();
    $('.tabs').tabs();
    $('select').formSelect();

    $("#form-vinculado-empresa").hide();
    $("input[id$='vinculado-empresa']").click(function () {
      $("#form-vinculado-empresa").show();
    });

    $("input[id$='profissional-autonomo']").click(function () {
      $("#form-vinculado-empresa").hide();
    });
    
});
})(jQuery);
