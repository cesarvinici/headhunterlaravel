@extends('layouts.cadastro')
@section('content')
<div style="margin-top:10px" class="row col-md-6 col-md-offset-3">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger">
            <ul>
                <li>
                    {{Session::get('error')}}
                </li>
            </ul>				
        </div>
    @endif
    @if(Session::has('cadEmpresa_ok'))
        <script>parent.jQuery.fancybox.getInstance().close()</script>
    @endif
</div>
<div class="form-group container">
    <form class="form-perfil" action="/nova-empresa" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-perfil__conteudo">
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="nomeCompleto" class="form-perfil__label">Nome Completo*</label>
                <input type="text" value="{{old('nome')}}" class="form-perfil__input form-control validate" name="nome" id="nomeCompleto"  >
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="email" class="form-perfil__label">E-mail*</label>
                    <input type="email" value="{{old('email')}}" name="email" class="form-perfil__input form-control validate" id="email"  >
                </div>
            </div>

            <div class="row form-perfil__row form-perfil__row-bg senha">
                <div class="form-perfil__titulo">
                    <h3>Senha de Acesso</h3>
                </div>
                <div class="col-xs-6">
                    <label for="novaSenha" class="form-perfil__label">Senha</label>
                    <input type="password" class="form-perfil__input-col02 form-control validate" name="novaSenha" id="novaSenha">
                </div>
                <div class="col-xs-6">
                    <label for="repetirSenha" class="form-perfil__label">Repetir Senha</label>
                    <input type="password" class="form-perfil__input-col02 form-control validate" name="novaSenhaC" id="repetirSenha">
                </div>

            </div>
            <div class="mensagem"></div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <div class="form-perfil__acao">
                        <div class="form-perfil__acao-submit">
                            <input type="submit" id="acaoSubmit" class="" name="salvar" value="Salvar">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
