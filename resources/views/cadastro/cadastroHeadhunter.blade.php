@extends('layouts.site')
@section('content')
<div class="container">
    <div class="row">
        <div class="col s12 m12 l12 center">
            <h1>Cadastro Recrutador</h1>
            <form class="form grey lighten-3">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="row">
                    <div class="input-field col s12 m12 l8">
                        <input id="nome" type="text" class="validate">
                        <label for="nome">Nome completo</label>
                    </div>
                    <div class="input-field col s12 m12 l4">
                        <input id="cpf" type="text" class="validate">
                        <label for="cpf">CPF</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m6 l4">
                        <input id="cep" type="text" class="validate">
                        <label for="cep">CEP</label>
                    </div>
                    <div class="input-field col s12 m6 l8">
                        <input id="endereco" type="text" class="validate">
                        <label for="endereco">Endereço</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s1">
                        <input id="numero" type="text" class="validate">
                        <label for="numero">Número</label>
                    </div>
                    <div class="input-field col s7">
                        <input id="complemento" type="text" class="validate">
                        <label for="complemento">Complemento</label>
                    </div>
                    <div class="input-field col s4">
                        <input id="bairro" type="text" class="validate">
                        <label for="bairro">Bairro</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s3">
                        <select id="estado">
                            <option value="" disabled selected>Selecione o estado</option>
                            <option value="1">Ceará</option>
                            <option value="2">Rio Grande do Sul</option>
                            <option value="3">Amazonas</option>
                        </select>
                    </div>
                    <div class="input-field col s3">
                        <input id="cidade" type="text" class="validate">
                        <label for="cidade">Cidade</label>
                    </div>
                    <div class="input-field col s3">
                        <input id="telefone" type="text" class="validate">
                        <label for="telefone">Telefone</label>
                    </div>
                    <div class="input-field col s3">
                        <input id="celular" type="text" class="validate">
                        <label for="celular">Celular</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <input id="linkedin" type="text" class="validate">
                        <label for="linkedin">Linkedin</label>
                    </div>
                    <div class="input-field col s4">
                        <input id="facebook" type="text" class="validate">
                        <label for="facebook">Facebook</label>
                    </div>
                    <div class="input-field col s4">
                        <input id="instagram" type="text" class="validate">
                        <label for="instagram">Instagram</label>
                    </div>
                </div>
                <div class="row center">
                    <div class="input-field col s12">
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Foto do Perfil</span>
                                <input id="foto-perfil" name="foto-perfil" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Escolha sua foto do perfil...">
                            </div>
                            <br>
                        </div>
                        <button type="submit" class="btn btn-large btn-default indigo darken-3 center">Baixar Meu Currículum</button>
                    </div>
                </div>
                <div class="card gray well">
                    <div class="row">
                        <div class="input-field col s4">
                            Como você atuará no marketplace?
                        </div>
                        <div class="input-field col s4">
                            <label class="radio-label">
                            <input id="profissional-autonomo" name="tipo-usuario" type="radio" />
                            <span>Profissional Autônomo</span>
                            </label>
                        </div>
                        <div class="input-field col s4">
                            <label class="radio-label">
                            <input id="vinculado-empresa" name="tipo-usuario" type="radio" />
                            <span>Vinculado a Consultoria de RH (Empresa)</span>
                            </label>
                        </div>
                    </div>
                    <br><br>
                    <div id="form-vinculado-empresa">
                        <div class="row">
                            <div class="input-field col s8">
                                <input id="ve-razao-social" type="text" class="validate">
                                <label for="ve-razao-social">Razão Social*</label>
                            </div>
                            <div class="input-field col s4">
                                <input id="ve-cnpj" type="text" class="validate">
                                <label for="ve-cnpj">CNPJ*</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s8">
                                <input id="ve-nome-fantasia" type="text" class="validate">
                                <label for="ve-nome-fantasia">Nome Fantasia*</label>
                            </div>
                            <div class="file-field input-field col s4">
                                <div class="btn">
                                    <span>Logo</span>
                                    <input id="ve-logo" name="ve-logo" type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Selecione a logo...">
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6 l4">
                                    <input id="ve-cep" type="text" class="validate">
                                    <label for="cep">CEP</label>
                                </div>
                                <div class="input-field col s12 m6 l8">
                                    <input id="ve-endereco" type="text" class="validate">
                                    <label for="ve-endereco">Endereço</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s1">
                                    <input id="numero" type="text" class="validate">
                                    <label for="numero">Número</label>
                                </div>
                                <div class="input-field col s7">
                                    <input id="complemento" type="text" class="validate">
                                    <label for="complemento">Complemento</label>
                                </div>
                                <div class="input-field col s4">
                                    <input id="bairro" type="text" class="validate">
                                    <label for="bairro">Bairro</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s3">
                                    <select id="estado">
                                        <option value="" disabled selected>Selecione o estado</option>
                                        <option value="1">Ceará</option>
                                        <option value="2">Rio Grande do Sul</option>
                                        <option value="3">Amazonas</option>
                                    </select>
                                </div>
                                <div class="input-field col s3">
                                    <input id="cpf" type="text" class="validate">
                                    <label for="cpf">Cidade</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="telefone" type="text" class="validate">
                                    <label for="telefone">Telefone</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="ve-site" type="text" class="validate">
                                    <label for="ve-site">Site</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <select id="segmento">
                                        <option value="" disabled selected>Segmento</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                                <div class="input-field col s6">
                                    <select id="n-funcionarios">
                                        <option value="" disabled selected>Número de funcionários</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <p>Escolha o(s) segmento(s) que deseja atuar como recrutador na E-Headhunter:*</p>
                    <div class="col s4">
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Comercial e Marketing</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Finanças e Contabilidade</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Supply Chain | Logística</span>
                        </label>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Publicidade e Propaganda</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Engenharia & Facilities</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Auditoria</span>
                        </label>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Jurídico</span>
                        </label> 
                    </div>
                    <div class="col s4">
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Comunicação</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Tecnologia da Informação</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Manutenção Industrial</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Segurança do Trabalho</span>
                        </label> <br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Área da Saúde</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Administração</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Todos Segmentos</span>
                        </label><br>
                    </div>
                    <div class="col s4">
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Recursos Humanos e DP</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Atendimento & Suporte</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Desempenho Empresarial
                        </span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Produção & Industrial
                        </span>
                        </label> <br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Auditoria e Compliance</span>
                        </label><br>
                        <label class="left">
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        <span>Trade & Inteligência de Mercado</span>
                        </label><br>
                        <label class="left">
                        <span>+</span>
                        <input name="segmentos" type="checkbox" class="filled-in" />
                        </label><br>
                    </div>
                </div>
        </div>
        <div class="row">
        <div class="col s12 m6 l3"></div>
        <div class="col s12 m6 l6 indigo darken-3 white-text form">
        <p class="center">Criar Sua Senha de Acesso</p>
        <form>
        <div class="input-field col s12">
        <input id="login" type="text" class="validate" placeholder="email ex: seunome@email.com">
        <label for="login">Login*</label>
        </div>
        <div class="input-field col s6">
        <input id="senha" type="password" class="validate">
        <label for="senha">Senha*</label>
        </div>
        <div class="input-field col s6">
        <input id="repetir-senha" type="password" class="validate">
        <label for="repetir-senha">Repetir Senha*</label>
        </div>
        <div class="center">
        <a href="#" class="waves-effect grey lighten-2 black-text btn-large center">Escolha o Plano</a>
        </div><br>
        </form>
        </div>
        <div class="col s12 m6 l3"></div>
        </div>
        </form>
    </div><br>
</div>
</div>
@endsection