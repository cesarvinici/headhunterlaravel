@extends('layouts.candidatoHeader')
@section('title', 'Excluir Conta')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Cancelament de Conta</h2>
                    </div>
                </div>
                <div class="col-xs-3 btn-voltar">
                    <div class="conteudo-header__voltar">
                        <div class="conteudo-header__voltar-icone">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <div class="mensagem">
                <div class="mensagem__conteudo mensagem__conteudo-bg-red">
                    <div class="mensagem__conteudo-texto">
                        <p>Se você tem certeza de que não pretende mais usar o Marketplace E-HeadHunter e deseja excluir sua conta, cuidaremos disto para você. Lembramos que após a confirmação do encerramento você não poderá mais utilizar nossos recursos ou recuperar esta conta assim como os dados e informações nela adicionados</p>
                        <p>Se ainda assim deseja continuar é só clicar em Excluir Minha Conta!</p>
                    </div>
                </div>
                <div class="mensagem__acao">
                    <div class="mensagem__acao-confirma">
                        <a href="{{route('deletarConta')}}">Excluir Minha Conta</a>
                    </div>
                    <div class="mensagem__acao-cancelar">
                        <a href="#">Cancelar</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection