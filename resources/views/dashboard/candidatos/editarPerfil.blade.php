@extends('layouts.candidatoHeader')
@section('title', 'Editar Perfil')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            @if (Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{{ Session::get('success') }}</li>                        
                    </ul>
                </div>
            @endif 
            @if (Session::has('error'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{ Session::get('error') }}</li>                        
                </ul>
            </div>
        @endif 
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif 
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Meu Perfil</h2>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__voltar btn-voltar">
                        <div class="conteudo-header__voltar-icone">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <form class="form-perfil" action="{{route('editarCdd')}}" method="post" enctype="multipart/form-data"> 
                @csrf
                <div class="form-perfil__conteudo">
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="nomeCompleto" class="form-perfil__label">Nome Completo</label>
                            <input type="text" name="nome" value="{{$candidato->nome}}" required class="form-perfil__input" id="nomeCompleto">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            
                            <label for="cpf" class="form-perfil__label">CPF</label>
                            <input type="text" value="{{$candidato->cpf}}" name="cpf" required class="form-perfil__input-col02" id="cpf">
                        </div>
                        <div class="col-xs-6">
                            <label for="email" class="form-perfil__label">E-mail</label>
                            <input type="email" value="{{$candidato->email}}" required name="email" class="form-perfil__input-col02" id="email">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="telefone" class="form-perfil__label">Telefone</label>
                            <input type="tel" value="{{$candidato->telefone}}" name="telefone" class="form-perfil__input-col02" id="telefone">
                        </div>
                        <div class="col-xs-6">
                            <label for="celular01" class="form-perfil__label">Celular</label>
                            <input type="tel" value="{{$candidato->celular}}" required class="form-perfil__input-col02" name="celular" id="celular01">
                        </div>

                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="estadoCivil" class="form-perfil__label">Estado Civil</label>
							<select name="estadoCivil" id="estadoCivil" id="estadoCivil" class="form-perfil__select-col02">
								@foreach(estadoCivil() as $estado)
                                    {{$selected = $estado->id == $candidato->estado_civil ? 'selected' : ''}}
										<option {{$selected}} value="{{$estado->id}}">{{$estado->estado_civil}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label for="nascimento" class="form-perfil__label">Data Nascimento</label>
                            <input type="text" value="{{MySqlToData($candidato->nascimento)}}" required name="nascimento" placeholder="dd/mm/aaaa" class="form-perfil__input-col02" id="nascimento">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="cep" class="form-perfil__label">CEP*</label>
                            <input type="text" value="{{$candidato->cep}}" name="cep" class="form-perfil__input-col02" id="cep">
                        </div>
                        <div class="col-xs-6">
                            <label for="endereco" class="form-perfil__label">Endereço*</label>
                            <input type="text" value="{{$candidato->endereco}}" required class="form-perfil__input-col02" name="endereco" id="endereco">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="estado" class="form-perfil__label">Estado</label>
                            <select name="estado" id="estado" onchange="carregaCidades(this, 1)" required class="form-perfil__select-col02 listaEstados">
                                <option value="" selected>Selecione um Estado</option>
                                @foreach($estados as $estado)
                                    @php $selected = "" @endphp
                                    @if(sizeof($cidades))
                                        @php $selected = $cidades[1]->CT_UF == $estado->UF_ID ? 'selected' : '' @endphp
                                    @endif
                                        <option {{$selected}} value="{{$estado->UF_UF}}">{{$estado->UF_NOME}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label for="cidade" class="form-perfil__label">Cidade*</label>
                            <select name="cidade" class="form-perfil__select-col02" required id="listaCidades">
                                @foreach($cidades as $cidade)
                                    <option value=""></option>
                                    @php $selected = $candidato->cidade == $cidade->CT_ID ? 'selected' : '' @endphp
                                    <option {{$selected}} value="{{$cidade->CT_ID}}" >{{urldecode($cidade->CT_NOME)}}</option>
                                @endforeach
                            </select>								
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="instagram" class="form-perfil__label">Instagram</label>
                            <input type="text" value="{{$candidato->instagram}}" name="instagram" class="form-perfil__input-col02" id="instagram">
                        </div>
                        <div class="col-xs-6">
                            <label for="twitter" class="form-perfil__label">Twitter</label>
                            <input type="text" value="{{$candidato->twitter}}" name="twitter" class="form-perfil__input-col02" id="twitter">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                            <div class="col-xs-12">
                                <div class="form-perfil__img">
                                    @if(!empty($candidato->foto_perfil))                                   
                                        <img src="{{asset('./admin/upload/candidatos/'.$candidato->foto_perfil)}}" alt="" width="150px" height="150px" >
                                        <br>
                                    @endif                                    
                                    <span>Imagem De Perfil</span>
                                    <input type="file" name="imgPerfil">
                                </div>
                            </div>
                        </div>
                        <div class="row form-perfil__row form-perfil__row-bg">
                            <div class="form-perfil__titulo">
                                <h3>Senha de Acesso</h3>
                            </div>
                            <div class="col-xs-9">
                                <label for="senhaAtual" class="form-perfil__label">Senha Atual</label>
                                <input type="password" name="senha" class="form-perfil__input-col02" id="senhaAtual">
                            </div>
                            {{-- <div class="col-xs-6">
                                <input type="checkbox" id="mostrarSenha">
                                <label for="mostraSenha">Mostrar Senha</label>
                            </div> --}}
                        </div>
                            <div class="row form-perfil__row form-perfil__row-bg" style="margin-top:-61px">
                                <div class="col-xs-6">
                                    <label for="novaSenha" name="senhaN" class="form-perfil__label">Nova Senha</label>
                                    <input type="password" class="form-perfil__input-col02" name="password" id="password">
                                </div>
                                <div class="col-xs-6">
                                    <label for="repetirSenha" class="form-perfil__label">Repetir Nova Senha</label>
                                    <input type="password" name="password_confirmation" class="form-perfil__input-col02" id="repetirSenha">
                                </div>                                   
                            </div>                           
                        <div class="row form-perfil__row">
                            <div class="col-xs-12">
                                <div class="form-perfil__acao">
                                    <div class="form-perfil__acao-visualizar">
                                        <a href="">Visualizar Currículo</a>
                                    </div>
                                    <div class="form-perfil__acao-submit">
                                        <input type="submit" id="acaoSubmit" value="Salvar">
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection