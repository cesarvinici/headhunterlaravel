@extends('layouts.candidatoHeader')
@section("title", 'Candidatos')
@section('content')
<div class="col-xs-9">
        <section class="conteudo">                
            <div class="conteudo-header">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-xs-9">
                        <div class="conteudo-header__titulo">
                            <h2>Olá {{$user->nome}}, seja bem vindo novamente!</h2>
                        </div>
                        <div class="conteudo-header__data">
                            <p><?=strftime('%A, %d de %B de %Y', strtotime('today'));?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="conteudo-painel conteudo-768">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="conteudo-painel__titulo">
                            <div class="conteudo-painel__titulo-mensagem">
                                <h3>Minhas Mensagens</h3>
                            </div>
                            <div class="conteudo-painel__titulo-icone">
                                <div class="icone icn-email-m-verde"></div>
                            </div>
                            <div class="conteudo-painel__titulo-numero">
                                <span style="color:#8ac83b;">02</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="conteudo-painel__titulo">
                            <div class="conteudo-painel__titulo-compromissos">
                                <h3>Meus Compromissos</h3>
                            </div>
                            <div class="conteudo-painel__titulo-icone">
                                <div class="icone icn-calendario-m-azul"></div>
                            </div>
                            <div class="conteudo-painel__titulo-numero">
                                <span style="color:#6abaeb;">02</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="conteudo-painel__titulo">
                            <div class="conteudo-painel__titulo-meusalertas">
                                <h3>Meus Alertas</h3>
                            </div>
                            <div class="conteudo-painel__titulo-icone">
                                <div class="icone icn-sino-m-vermelho"></div>
                            </div>
                            <div class="conteudo-painel__titulo-numero">
                                <span style="color:#cc242e;">05</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nav-box  conteudo-480">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="candidatos-perfil">
                                <div class="box__conteudo-box">
                                    <div class="box__conteudo-box-titulo">
                                        <div class="icone">
                                            <div class="icn-user-menu-m-branco"></div>
                                        </div>
                                        <div class="texto">Meu Perfil</div>
                                        <div class="icone">
                                            <div class="icn-select-menu-m-branco"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box__conteudo-nav">
                                    <div class="box__conteudo-nav-item">
                                        <a href="{{route('editarCdd')}}" class="box__conteudo-nav-link">Meus Dados e Senha de Acesso</a>
                                    </div>
                                    <div class="box__conteudo-nav-item">
                                    <a href="{{route('cadastracv')}}" class="box__conteudo-nav-link">Currículo</a>
                                    </div>
                                    <div class="box__conteudo-nav-item">
                                        <a href="" class="box__conteudo-nav-link">Anexos</a>
                                    </div>
                                    <div class="box__conteudo-nav-item">
                                    <a href="{{route('cddCancelarConta')}}" class="box__conteudo-nav-link">Cancelar Conta</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection