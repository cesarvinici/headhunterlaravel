@extends('layouts.candidatoHeader')
@section('title', 'Solicitar Serviços')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Avaliação Comportamental</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <div class="servicos">
                <div class="servicos__header">
                    <div class="servicos__header-titulo">
                        <p>Solicite uma proposta de prestação de serviço ou contate-nos para um atendimento personalizado.</p>
                    </div>
                </div>
                <div class="servicos__conteudo">
                    <div class="row">
                        <div class="col-xs-6">
                            AVALIAÇÃO DISC
                        </div>
                        <div class="col-xs-6">
                            AVALIAÇÃO 360º
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection