@extends('layouts.candidatoHeader')
@section('title', 'Cadastrar Curriculum')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Pesquisar Vagas</h2>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__voltar">
                        <div class="conteudo-header__voltar-icone">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <div class="box-consulta">
                <div class="box-consulta__box">
                    <form action="{{route('filtraVagas')}}" method="post">
                        @csrf
                        <div class="row box-consulta__box-row">
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <input type="text" autocomplete="off" id="tituloCargo" style="height: 64px; width: 170px;" name="funcao" placeholder="FUNÇÃO">
                                    <input type="hidden" name="idCargo" value="" id="">
                                    <p style="color: red" class="empty-message"></p>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <div class="titulo">REMUNERAÇÃO</div>
                                    <div id="range-slider"></div>
                                    <div class="valor">
                                        <input type="text" name="salario" readonly id="amount" style="border:0;font-weight:bold;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <select name="areaAtuacao" id="areaAtua">
                                        <option value="">ÁREA DE ATUAÇÃO</option>
                                        @foreach($segmentos as $segmento)
                                            <option value="{{$segmento->id}}" >{{urldecode($segmento->segmento)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-limpar">
                                    <input type="reset" value="Limpar">
                                </div>
                            </div>
                        </div>

                        <div class="row box-consulta__box-row">
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <input type="text" style="height: 64px; width: 170px;" name="localidade" placeholder="CIDADE">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <select name="dataAnun" id="dataAnun">
                                        <option value="" selected disabled>DATA DO ANÚNCIO</option>
                                        <option value="">Valor 02</option>
                                        <option value="">Valor 03</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <select id="tipoVaga" name="regime">
                                        <option value="" selected disabled>TIPO DE VAGA</option>
                                        @foreach($regimeContratacao as $regime)
                                            <option value="{{$regime->id}}">{{urldecode($regime->regime)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-submit">
                                    <input type="submit" value="Consultar">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="resul">
                <div class="resul__header">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="resul__header-numero">
                                <p>{{sizeof($vagas)}} resultados encontrados</p>
                            </div>
                        </div>
                        {{-- <div class="col-xs-6">
                            <div class="resul__header-filtro">
                                <p>Organizar por: <span class="data">Data</span> | <span class="cargo">Cargo</span> | <span class="valor">Valor</span></p>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="resul__conteudo">
                    <div class="resul__titulo">
                        <div class="resul__col-cargo">
                            <h3>Título do Cargo</h3>
                        </div>
                        <div class="resul__col-local">
                            <h3>Localidade</h3>
                        </div>
                        <div class="resul__col-salario">
                            <h3>Salário</h3>
                        </div>
                    </div>
                    @forelse($vagas as $vaga)
                        <div id="candidatos-vagas" class="resul__mostra">
                            <div class="resul__mostra-resumo">
                                <div class="resul__col-cargo resul__col-cargo-1">
                                    <div class="agrupar">
                                        <div class="nome">{{urldecode($vaga->cargo)}}</div>
                                        <div class="data">Publicado em {{MySqlToData($vaga->created_at)}}</div>
                                    </div>
                                </div>
                                <div class="resul__col-local resul__col-local-1">
                                    <div class="icone">
                                        <div class="icn-resul-local-p-branco"></div>
                                    </div>
                                    <div class="local">{{urldecode($vaga->CT_NOME)}}</div>
                                </div>
                                <div class="resul__col-salario resul__col-salario-1">
                                    <div class="agrupar">
                                        @if($vaga->naoExibirSalario)
                                            CONFIDENCIAL
                                        @else
                                            <div class="min">R$ {{IntToMoney($vaga->salario)}}</div>
                                        @endif                                    
                                        {{-- <div class="max">Máx.: R$ 8.000,00</div> --}}
                                    </div>
                                </div>
                                <div class="resul__col-visualizar resul__col-visualizar-1" onclick="visualizar({{$vaga->id}})">
                                    <div class="icone">
                                        <div class="icn-resul-lupa-p-branco"></div>
                                    </div>
                                    <div class="texto">Visualizar Vaga</div>
                                </div>
                                <div class="resul__col-candidatarse resul__col-candidatarse-1" onclick="CandidatoVaga({{$vaga->id}})">
                                    <div class="icone">
                                        <div class="icn-resul-cadd-m-branco"></div>
                                    </div>
                                    <div class="texto">Candidatar-se</div>
                                </div>
                            </div>
                            <div class="resul__mostra-conteudo mostra{{$vaga->id}}">
                                <div class="resul__mostra-conteudo-bg">
                                    <div class="row resul__mostra-row">
                                        <div class="col-xs-3">
                                            <div class="resul__mostra-conteudo-titulo">
                                                <h3>Dados da Empresa:</h3>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="resul__mostra-conteudo-texto">
                                                <p>
                                                    @if($vaga->sigilo)
                                                        Confidencial
                                                    @else							
                                                        <strong>EMPRESA: </strong>{{$vaga->nome_fantasia}}<br>
                                                        <strong>SEGMENTO:</strong> {{urldecode($vaga->segmento)}}<br>
                                                        <strong>LOCALIZAÇÃO: </strong>{{mb_strtoupper(urldecode($vaga->CT_NOME))}}/{{$vaga->UF_UF}}<br>
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row resul__mostra-row">
                                        <div class="col-xs-3">
                                            <div class="resul__mostra-conteudo-titulo">
                                                <h3>Descrição da Função:</h3>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="resul__mostra-conteudo-texto">
                                                <p>
                                                    {{$vaga->principais_atividades}}
                                                </p>                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row resul__mostra-row">
                                        <div class="col-xs-3">
                                            <div class="resul__mostra-conteudo-titulo">
                                                <h3>Perfil &amp; Exigências:</h3>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="resul__mostra-conteudo-texto">
                                                <p><strong>FORMAÇÃO DESEJADA:</strong> {{$vaga->formacao_exigida}}</p>
                                                <p>
                                                    <strong>REQUISITOS DESEJÁVEIS:</strong> {{$vaga->requisitos}}
                                                </p>
                                                <p>
                                                <strong>CONHECIMENTOS E APTIDÕES NECESSÁRIAS:</strong>
                                                @foreach(getAptidoes($vaga->id) as $aptidao)
                                                    </p>									
                                                        {{$aptidao->aptidoes}} - {{nivelAptidao($aptidao->nivel_requerido)}}                                        
                                                    </p>
                                                @endforeach
                                                <p><strong>IDIOMAS:</strong></p>    
                                                @foreach(getIdiomas($vaga->id) as $idioma)
                                                    <p>{{mb_strtoupper($idioma->idioma)}} - {{nivelIdioma($idioma->nivel)}}</p>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row resul__mostra-row">
                                        <div class="col-xs-3">
                                            <div class="resul__mostra-conteudo-titulo">
                                                <h3>Salário &amp; Benefícios:</h3>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="resul__mostra-conteudo-texto">
                                                <div class="vagas__resul-descricao">
                                                    <p><strong>SALÁRIO:</strong> R$ {{intToMoney($vaga->salario)}}</p>
                                                    <p><strong>BENEFICIOS:</strong> {{$vaga->beneficios}}</p>
                                                    <p><strong>COMISSÃO:</strong> {{$vaga->comissao}} </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                            <p class="text-danger">Não existem vagas Cadastradas no sistema</p>
                    @endforelse
                    {{-- <div id="candidatos-vagas" class="resul__mostra">
                        <div class="resul__mostra-resumo">
                            <div class="resul__col-cargo resul__col-cargo-1">
                                <div class="agrupar">
                                    <div class="nome">Gerente de RH</div>
                                    <div class="data">Publicado em 17/05/2017</div>
                                </div>
                            </div>
                            <div class="resul__col-local resul__col-local-1">
                                <div class="icone">
                                    <div class="icn-resul-local-p-branco"></div>
                                </div>
                                <div class="local">Fortaleza</div>
                            </div>
                            <div class="resul__col-salario resul__col-salario-1">
                                <div class="agrupar">
                                    <div class="min">Min.: R$ 6.000,00</div>
                                    <div class="max">Máx.: R$ 8.000,00</div>
                                </div>
                            </div>
                            <div class="resul__col-visualizar resul__col-visualizar-1">
                                <div class="icone">
                                    <div class="icn-resul-lupa-p-branco"></div>
                                </div>
                                <div class="texto">Visualizar Vaga</div>
                            </div>
                            <div class="resul__col-candidatarse resul__col-candidatarse-1">
                                <div class="icone">
                                    <div class="icn-resul-cadd-m-branco"></div>
                                </div>
                                <div class="texto">Candidatar-se</div>
                            </div>
                        </div>
                        <div class="resul__mostra-conteudo">
                            <div class="resul__mostra-conteudo-bg">
                                <div class="row resul__mostra-row">
                                    <div class="col-xs-3">
                                        <div class="resul__mostra-conteudo-titulo">
                                            <h3>Dados da Empresa:</h3>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="resul__mostra-conteudo-texto">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                                                et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                                ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
                                                esse cillum dolore eu fugiat nulla </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row resul__mostra-row">
                                    <div class="col-xs-3">
                                        <div class="resul__mostra-conteudo-titulo">
                                            <h3>Descrição da Função:</h3>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="resul__mostra-conteudo-texto">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                                                et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                                ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
                                                esse cillum dolore eu fugiat nulla </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row resul__mostra-row">
                                    <div class="col-xs-3">
                                        <div class="resul__mostra-conteudo-titulo">
                                            <h3>Perfil & Exigências:</h3>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="resul__mostra-conteudo-texto">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                                                et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                                ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
                                                esse cillum dolore eu fugiat nulla </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row resul__mostra-row">
                                    <div class="col-xs-3">
                                        <div class="resul__mostra-conteudo-titulo">
                                            <h3>Salário & Benefícios:</h3>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="resul__mostra-conteudo-texto">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                                                et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                                ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
                                                esse cillum dolore eu fugiat nulla </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
<script>
    function CandidatoVaga(vaga)
    {
        if(confirm("Confirma o interesse pela vaga?"))
        {
            $.get('/dashboard/candidato/candidata/'+vaga, function(data)
            {
                if(data == 'OK')
                {
                    alert('Candidatura realizada com sucesso!');
                }
                else if(data == 'existe')
                {
                    alert("Você já se candidatou para esta vaga.");
                }
            });
        }
    } 
</script>
@endsection