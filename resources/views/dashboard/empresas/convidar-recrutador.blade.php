@extends('layouts.empresaHeader')
@section('title', 'Convidar Recrutador')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Headhunters De Preferência</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <div class="convite">
                <div class="convite__secao">
                    <div class="convite__secao__descricao">
                        <p>Trabalhe com os headhunters que julgar mais assertivo para o seu processo!</p>
                    </div>
                    <div class="convite__secao-titulo">
                        <p>01 - Selecione a vaga para qual você gostaria de convidar headhunters.</p>
                    </div>
                    <div class="convite__secao-search">
                    <form method="POST" action="{{url('dashboard/empresa/convidar-recrutador')}}">
                        @csrf
                            <div class="col-xs-5">
                                <div class="row">
                                    <input type="text" id="tituloCargo" autocomplete="off">
                                    <input type="hidden" name="idCargo" value="">
                                    <button type="submit">ICN</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                    @php
                        $arrayIds = []
                    @endphp                  
                    @forelse($vagas as $vaga)
                    @php array_push($arrayIds, $vaga->id) @endphp
                        <div class="row">
                            <div class="col-xs-1">
                                <input type="radio" value="{{$vaga->id}}" id="{{$vaga->id}}" name="vaga" class="vaga">
                            </div>
                            <div class="convite__secao-resul01" onclick="radioCheck({{$vaga->id}})">
                                <div class="col-xs-4">
                                    <div class="row">
                                        <div class="convite__secao-cargo">
                                            <div class="nome">
                                            <span>{{urldecode($vaga->cargo)}}</span>
                                            </div>
                                            <div class="data">
                                                <span>Publicado em {{MysqlToData($vaga->updated_at)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <div class="row">
                                        <div class="convite__secao-local">
                                            <div class="icone">ICN</div>
                                            <div class="nome">
                                                <span>{{$vaga->cidade}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="row">
                                        <div class="convite__secao-headhunters">
                                            <div class="numero">
                                                <span>{{headhuntersVaga($vaga->id)}}</span>
                                            </div>
                                            <div class="nome">
                                                <span>Headhunters</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="row">
                                        <div class="convite__secao-candidatos">
                                            <div class="numero">
                                            <span>{{contaCandidatos($vaga->id)}}</span>
                                            </div>
                                            <div class="nome">
                                                <span>Candidatos</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                        <p class="text-danger" style="margin-top:10px">Não foram encontrados resultados para pesquisa</p>                       
                    @endforelse
                </div>
                <div class="convite__secao">
                    {{-- <form method="post" action="{{url('dashboard/empresa/convidar-recrutador')}}">                   
                        <div class="convite__secao-titulo">
                            <p>02 - Selecione os headhunters.</p>
                        </div>
                        <div class="convite__secao-search">
                            <div class="col-xs-5">
                                <div class="row">
                                    <input type="text">
                                    <button type="submit">ICN</button>
                                </div>
                            </div>
                            <div class="col-xs-5">
                                <div class="row">
                                    <input type="text">
                                    <button type="submit">ICN</button>
                                </div>
                            </div>
                        </div>
                    </form> --}}
                </div>
                <div class="convite__secao">
                    <div class="convite__secao-header">
                        <div class="row">
                            {{-- <div class="col-xs-6">
                                <div class="resul__header-numero">
                                    @isset($vaga->id)
                                        <p>resultados encontrados</p>
                                    @endisset
                                </div>
                            </div> --}}
                            {{-- <div class="col-xs-6">
                                <div class="resul__header-filtro">
                                    <p>Organizar por: <span class="cidade">Cidade</span> | <span class="alfabetica">Ordem Alfabética</span> | <span class="valor">Valor</span></p>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="convite__secao">
                    <div class="col-xs-5"></div>
                    <div class="col-xs-3">
                        <div class="row">
                            <div class="convite__secao-resul-score-titulo">
                                <span>Pesquisa De Satisfação</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="row">
                            <div class="convite__secao-resul-like-titulo">
                                <span>Convidar</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="row">
                            <div class="convite__secao-resul-deslike-titulo">
                                <span>Aceitou Convite?</span>
                            </div>
                        </div>
                    </div>
                    <div id="headhunters">
                        {{-- @include('dashboard.empresas.listaheadhuntersconvite') --}}
                    </div>                   
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
<script>
    function radioCheck(id)
    {
        document.getElementById(id).checked = true;
       var element = document.getElementById('headhunters');
       $(element).children().remove();
       $.get('/dashboard/empresas/mostaHeadhunters/'+id, function(data)
        {

           $(element).append(data);        
        })
       
      // element.innerHTML = '<p>Hello</p>';

    }
    function convidarHH(headhunter)
    {
       if(confirm("Deseja convidar o headhunter para a vaga selecionada?"))
       {
            vaga = $("input[name='vaga']:checked").val();
            if(!vaga)
            {
                alert("Vaga não foi selecionada")
            }
            else
            {
                $.get('/dashboard/empresa/convidar-recrutador/'+vaga+'/'+headhunter, function(data)
                {
                    if(data == 'existe')
                    {
                        alert("Este headhunter já foi convidado para esta vaga");
                        return true;
                    }
                    if(data == 'ok')
                    {
                        alert("Headhunter convidado com sucesso");
                    }
                    else
                    {
                        alert("Houve um problema, tente novamente");
                    }
                });
                
            }
       }
    }
</script>
@endsection