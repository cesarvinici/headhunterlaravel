@extends('layouts.empresaHeader')
@section('title', 'Adicionar Empresa')
@section('content')
<div class="col-xs-9">
    <div style="margin-top:10px" class="row col-md-6 col-md-offset-3">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger">
                <ul>
                    <li>
                        {{Session::get('error')}}
                    </li>
                </ul>				
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li>
                        {{Session::get('success')}}
                    </li>
                </ul>				
            </div>
        @endif
    </div>
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Cadastro Da Empresa:</h2>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__voltar btn-voltar">
                        <div class="conteudo-header__voltar-icone ">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <form action="" id="form-add-empresa" method="post" enctype="multipart/form-data" >
                @csrf
                <div class="form-perfil__conteudo">            
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="razaoSocial" class="form-perfil__label">Razão Social</label>
                            <input type="text" value="{{old('razaoSocial')}}" name="razaoSocial" class="form-perfil__input " id="razaoSocial">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="nomeFantasia" class="form-perfil__label">Nome Fantasia</label>
                            <input type="text" name="nomeFantasia" value="{{old('nomeFantasia')}}" class="form-perfil__input " id="nomeFantasia">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="cnpj" class="form-perfil__label">CNPJ*</label>
                            <input type="text" value="{{old('cnpj')}}" name="cnpj" class="form-perfil__input " id="cnpj">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="cep" class="form-perfil__label">CEP*</label>
                            <input type="text" value="{{old('cep')}}" name="cep" class="form-perfil__input-col02" id="cep">
                        </div>
                        <div class="col-xs-6">
                            <label for="endereco" class="form-perfil__label">Endereço*</label>
                            <input type="text" value="{{old('endereco')}}" name="endereco" class="form-perfil__input-col02" id="endereco">
                        </div>
                        
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="numero" class="form-perfil__label">Número*</label>
                            <input type="text" value="{{old('numero')}}" name="numero" class="form-perfil__input-col02" id="numero">
                        </div>
                        <div class="col-xs-6">
                            <label for="cidade" class="form-perfil__label">Cidade*</label>
                                <select id="listaCidades" name="cidade" class="form-perfil__input-col02">
                            <option value="">...</option>
                            </select>
                        </div>          

                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="bairro" class="form-perfil__label">Bairro*</label>
                            <input type="text" value="{{old('bairro')}}" name="bairro" class="form-perfil__input-col02" id="bairro">
                        </div>
                        <div class="col-xs-6">
                            <label for="estados" class="form-perfil__label">Estado*</label>
                            <select name="estado" id="estado" onchange="carregaCidades(this, 1)" class="form-perfil__select-col02 listaEstados">
                                <option selected>Selecione um Estado</option>
                                @foreach($estados as $estado)
                                    <option value="{{$estado->UF_UF}}">{{$estado->UF_NOME}}</option>
                                @endforeach                       
                            </select>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="telefone" class="form-perfil__label">Telefone</label>
                            <input type="tel" value="{{old('telefone')}}" name="telefone" class="form-perfil__input-col02 telefone" id="telefone">
                        </div>
                        <div class="col-xs-6">
                            <label for="website" class="form-perfil__label">Website</label>
                            <input type="url" value="{{old('website')}}" name="website" class="form-perfil__input-col02" id="website">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="segmento" class="form-perfil__label">Segmento*</label>
                            <select class="form-perfil__select-col02" name="segmento">
                                @foreach($segmentos as $segmento)
                                    <option value='{{$segmento->id}}' >{{urldecode($segmento->segmento)}}</option>
                                @endforeach
                            </select>	
                        </div>
                        <div class="col-xs-6">
                                <label for="cidade" class="form-perfil__label">Nº de Funcionários (Matriz + Filial)</label>
                            <select class="form-perfil__input-col02" name="nFuncionarios">
                                @foreach($nFuncionarios as $funcionarios)
                            <option value="{{$funcionarios->id}}" >{{$funcionarios->quantidade}}</option>
                                @endforeach
                            </select>
                            
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="logotipo" class="form-perfil__label">Logotipo*</label>
                            <input type="file" name="logotipo" class="form-perfil__input-col" id="logotipo">
                        </div>
                    </div>

                    <div class="form-perfil__titulo form-perfil__titulo-azul">
                        <h3>Responsável Financeiro</h3>
                    </div>

                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="nomeResp" class="form-perfil__label">Nome Completo*</label>
                            <input type="text" value="{{old('nomeResp')}}" name="nomeResp" class="form-perfil__input " id="nomeResp">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="cargo" class="form-perfil__label">Cargo*</label>
                            <input type="text" value="{{old('cargoResp')}}" name="cargoResp" class="form-perfil__input " id="cargo">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="email" class="form-perfil__label ">E-mail</label>
                            <input type="email" value="{{old('emailResp')}}" name="emailResp" class="form-perfil__input-col02 " id="email">
                        </div>
                        <div class="col-xs-6">
                            <label for="telefoneRamal" class="form-perfil__label">Telefone | Ramal</label>
                            <input type="tel" value="{{old('telResp')}}" name="telResp" class="form-perfil__input-col02 telefone" id="telefoneRamal">
                        </div>
                    </div>
                    <div class="mensagem"></div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="form-perfil__acao">
                                    <div class="form-perfil__acao-submit">
                                            <input type="submit" id="btn-add-empresa" name="salvarEmp" value="Salvar">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection