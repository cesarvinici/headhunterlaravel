@extends('layouts.empresaHeader')
@section("title", 'Historico Faturamento')
@section("content")
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Histótico De Faturamento</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <div class="historico-faturamento">
                <div class="historico-faturamento__conteudo">
                    <div class="col-xs-4">
                        <div class="row"><div class="historico-faturamento__titulo">Recrutamento</div></div>
                    </div>
                    <div class="col-xs-2">
                        <div class="row"><div class="historico-faturamento__titulo">Data Emissão</div></div>
                    </div>
                    <div class="col-xs-2">
                        <div class="row"><div class="historico-faturamento__titulo">Data Vencimento</div></div>
                    </div>
                    <div class="col-xs-2">
                        <div class="row"><div class="historico-faturamento__titulo">Status</div></div>
                    </div>
                    <div class="col-xs-2">
                        <div class="row"><div class="historico-faturamento__titulo">Download</div></div>
                    </div>
                    <div class="historico-faturamento__row">
                        <div class="col-xs-4">
                            <div class="row">
                                <div class="historico-faturamento__cargo">
                                    <div class="nome">
                                        <span>Gerente Comercial</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="row">
                                <div class="historico-faturamento__emissao">
                                    <div class="data">
                                        <span>28/08/17</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="row">
                                <div class="historico-faturamento__vencimento">
                                    <div class="data">
                                        <span>31/08/17</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="row">
                                <div class="historico-faturamento__status">
                                    <div class="status">
                                        <span>Em Atraso</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="row">
                                <div class="historico-faturamento__nf">
                                    <div class="texto"><span>NF 1515</span></div>
                                    <div class="icone">ICN</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="row">
                                <div class="historico-faturamento__boleto">
                                    <div class="texto"><span>Boleto</span></div>
                                    <div class="icone">ICN</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="historico-faturamento__row">
                        <div class="col-xs-4">
                            <div class="row">
                                <div class="historico-faturamento__cargo">
                                    <div class="nome">
                                        <span>Gerente Comercial</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="row">
                                <div class="historico-faturamento__emissao">
                                    <div class="data">
                                        <span>28/08/17</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="row">
                                <div class="historico-faturamento__vencimento">
                                    <div class="data">
                                        <span>31/08/17</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="row">
                                <div class="historico-faturamento__status">
                                    <div class="status">
                                        <span>Em Atraso</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="row">
                                <div class="historico-faturamento__nf">
                                    <div class="texto"><span>NF 1515</span></div>
                                    <div class="icone">ICN</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="row">
                                <div class="historico-faturamento__boleto">
                                    <div class="texto"><span>Boleto</span></div>
                                    <div class="icone">ICN</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection