@extends('layouts.empresaHeader')
@section('title', 'Dashboard Empresa')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Olá {{$user->nome}}, seja bem vindo(a) novamente!</h2>
                    </div>
                    <div class="conteudo-header__data">
                        <p><?=  strftime('%A, %d de %B de %Y', strtotime('today'));?></p>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__img">
                        @if(!empty($user->imagem_perfil))
                            <img src="{{asset('./admin/upload/empresas/'.$user->imagem_perfil)}}" class="img-thumbnail" style="width: 100px; height: 100px;" alt="">
                        @else
                            <img src="{{asset('./admin/imgs/icons/img-perfil-candidato.png')}}" class="img-thumbnail"  alt="">
                        @endif              
                    </div>
                </div>
            </div>
        </div>
            <div class="conteudo-painel">
                <div class="notificaoes">
                    <div class="notificacoes__conteudo">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="notificacoes__row">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="notificacoes__icone">
                                                <div class="icn-email-m-verde"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="notificacoes__texto">
                                                <span>Minhas Mensagens</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="notificacoes__numero">
                                                <span style="color:#89c93b;font-size:18px;">02</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="notificacoes__row">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="notificacoes__icone">
                                                <div class="icn-calendario-m-azul"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="notificacoes__texto">
                                                <span>Meus Compromissos</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="notificacoes__numero">
                                                <span style="color:#6abaec;font-size:18px;">02</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="notificacoes__row">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="notificacoes__icone">
                                                <div class="icn-sino-m-vermelho"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="notificacoes__texto">
                                                <span>Meus Alertas</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="notificacoes__numero">
                                                <span style="color:#cd242e;font-size:18px;">05</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="notificacoes__row">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="notificacoes__icone">
                                                <div class="icn-mala-m-preto"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="notificacoes__texto">
                                                <span>Vagas em Aberto</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="notificacoes__numero">
                                                <span style="color:#282828;font-size:18px;">06</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="notificacoes__acesso">
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="notificacoes__acesso-nav">
                                    <li class="notificacoes__acesso-item">
                                        <a href="#" class="notificacoes__acesso-link">Contrato De Prestação de Serviço</a>
                                    </li>
                                    <li class="notificacoes__acesso-item">
                                        <a href="/dashboard/empresa/meu-perfil" class="notificacoes__acesso-link">Meu Perfil E Senha De Acesso</a>
                                    </li>
                                    <li class="notificacoes__acesso-item">
                                        <a href="/dashboard/empresa/cadastro-empresa" class="notificacoes__acesso-link">Cadastro Da Empresa</a>
                                    </li>
                                    <li class="notificacoes__acesso-item">
                                        <a href="/dashboard/empresa/manutencao-vagas" class="notificacoes__acesso-link">Manutenção vagas cadastradas</a>
                                    </li>
                                    @if($user->tipo == 1)
                                        <li class="notificacoes__acesso-item">
                                            <a href="/dashboard/empresa/usuarios" class="notificacoes__acesso-link">Usuários Da Conta</a>
                                        </li>
                                    @endif
                                    <li class="notificacoes__acesso-item">
                                        <a href="/dashboard/empresa/cancelar-conta" class="notificacoes__acesso-link">Cancelar Conta</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>


@endsection