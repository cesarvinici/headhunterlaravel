{{-- lista os headhunters que serão convidados para trabalhas nas vagas --}}
@forelse($headhunters as $headhunter)                    
    <div class="convite__secao-resul02" style="margin-bottom:15px">
        <div class="col-xs-1">
            <div class="row">
                <div class="convite__secao-resul-icone">ICONE</div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="row">
                <div class="convite__secao-resul-recrutador">
                    <div class="nome">
                        <span>{{urldecode($headhunter->nome)}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="row">
                <div class="convite__secao-resul-score">
                    <div class="icone">ICN</div>
                    <div class="texto">
                        <span>Média Das Avaliações: 4</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2 pointer" onclick="convidarHH({{$headhunter->id}})">
            <div class="row">
                <div class="convite__secao-resul-like">
                    <div class="icone">ICN LIKE</div>
                </div>
            </div>
        </div>
        <div class="col-xs-2 pointer">
            <div class="row">
                <div class="convite__secao-resul-deslike">
                    <div class="icone">ICN DESLIKE</div>
                </div>
            </div>
        </div>
    </div>
    @empty
    <p>Não há headhunter disponível.</p>
@endforelse