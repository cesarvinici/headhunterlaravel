@extends('layouts.empresaHeader')
@section("title", 'Manutenção de Vagas')
@section('content')
    <div class="col-xs-9">
        <div class='row col-md-6 col-md-offset-3'>
            @if(Session::has('success'))
                <div class="alert alert-success">
                   <ul>
                       <li>{{Session::get('success')}}</li>
                   </ul>
                </div>
            @endif        
            @if(Session::has('error'))
                <div class="alert alert-danger">
                   <ul>
                       <li>{{Session::get('error')}}</li>
                   </ul>
                </div>
            @endif            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div> 
        <section class="conteudo">
            <div class="conteudo-header">
                <div class="row">
                    <div class="col-xs-9">
                        <div class="conteudo-header__titulo">
                            <h2>Consultar Vagas</h2>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="conteudo-header__voltar-icone">ICN</div>
                        <div class="conteudo-header__voltar btn-voltar">
                            <div class="conteudo-header__voltar-texto">Voltar</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="conteudo-painel">
                <div class="consulta">
                    <div class="consulta__filtro">
                        <div class="convite__secao-search">
                            <form method="post" id="filtroVagasEmp" >
                                @csrf
                                <div class="consulta__filtro-row">
                                    <div class="col-xs-6">
                                        <input type="hidden" id="editEmpresa" value="EMPRESAID">
                                        <div class="row">
                                            <input name="cod" type="text" placeholder="Código da Vaga">
                                            <button  class="filtroVagaEmp" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="row">
                                            <input name="cargo" autocomplete="off" type="text" id="tituloCargo" placeholder="Função">
                                            <input type="hidden" name="idCargo" value="" id="">
                                            <button  class="filtroVagaEmp" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="consulta__filtro-row">
                                    <div class="col-xs-6">
                                        <div class="row">
                                            <input type="text" name="cidade" placeholder="Cidade">
                                            <button  class="filtroVagaEmp" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="row">
                                            <input type="text" placeholder="Status da Vaga" name="status">
                                            <button  class="filtroVagaEmp"  type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="consulta__filtro-row">
                                    
                                    <div class="col-xs-6">
                                        <div class="row">
                                            <input type="text" placeholder="Tipo" name="regime">
                                            <button  class="filtroVagaEmp" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="manut-vagas-page">
                           @include('dashboard.empresas.vagasEmp')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection