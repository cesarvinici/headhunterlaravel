@extends('layouts.empresaHeader')
@section('title', 'Caixa de Saída')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-12 btn-voltar">
                    <div class="conteudo-header__voltar">
                        <div class="conteudo-header__voltar-icone">ICN</div>
                        <div class="conteudo-header__voltar-texto">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <div class="col-xs-5">
                <div class="caixa-entrada">
                    <div class="caixa-entrada__titulo">
                        <span>Caixa De Saída</span>
                    </div>
                    <div class="caixa-entrada__conteudo">
                        <ul class="caixa-entrada__nav">
                            <li class="caixa-entrada__item">
                                <a href="" class="caixa-entrada__link">CENTRAL: Aviso de Cobrança | 25/08/17 | 13:30</a>
                            </li>
                            <li class="caixa-entrada__item">
                                <a href="" class="caixa-entrada__link">CENTRAL: Aviso de Cobrança | 25/08/17 | 13:30</a>
                            </li>
                            <li class="caixa-entrada__item">
                                <a href="" class="caixa-entrada__link">CENTRAL: Aviso de Cobrança | 25/08/17 | 13:30</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-7">
                <div class="caixa-entrada">
                    <div class="caixa-entrada__titulo caixa-entrada__titulo-bg">
                        <span>De: Central | Data E Hora Do Envio: 25/08/17 | 13:30</span>
                    </div>
                    <div class="caixa-entrada__conteudo caixa-entrada__conteudo-bg">
                        <div class="caixa-entrada__assunto ">
                            <span>Assunto: Aviso de Cobrança</span>
                        </div>
                        <div class="caixa-entrada__texto">
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                        </div>
                        <div class="caixa-entrada__responder">
                            <a href="">Enviar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <form class="form-perfil" action="">
                <div class="form-perfil__conteudo">
                        <div class="row form-perfil__row">
                            <div class="form-perfil__conteudo-titulo">
                                <span>Entre Em Contato Com a Gente!</span>
                            </div>
                        </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="area" class="form-perfil__label">Área</label>
                            <select name="area" class="form-perfil__input" id="area">
                                <option value="">Valor 01</option>
                                <option value="">Valor 02</option>
                                <option value="">Valor 03</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="assunto" class="form-perfil__label">Assunto</label>
                            <input type="text" class="form-perfil__input" id="assunto">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="mensagem" class="form-perfil__label">Mensagem</label>
                            <textarea name="" class="form-perfil__input" id="mensagem"></textarea>
                        </div>
                    </div>                                    
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <div class="form-perfil__acao">
                                <div class="form-perfil__acao-submit">
                                    <input type="submit" id="acaoSubmit" value="Enviar">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form> 
        </div>
    </section>
</div>
@endsection