@extends('layouts.empresaHeader')
@section('title', 'Meus Dados')
@section('content')

<div class="col-xs-9">
    <div style="margin-top:10px" class="row col-md-6 col-md-offset-3">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger">
                <ul>
                    <li>
                        {{Session::get('error')}}
                    </li>
                </ul>				
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li>
                        {{Session::get('success')}}
                    </li>
                </ul>				
            </div>
        @endif
    </div>
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Meu Perfil</h2>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__voltar btn-voltar">
                        <div class="conteudo-header__voltar-icone">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto ">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <form class="form-perfil" action="" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-perfil__conteudo">
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="nomeCompleto" class="form-perfil__label">Nome</label>
                            <input type="text" name="nome" class="form-perfil__input" id="nomeCompleto" value="{{$user->nome}}">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="caergo" class="form-perfil__label">Cargo</label>
                            <input type="text" name="cargo" class="form-perfil__input" id="cargo" value="{{$user->cargo}}">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="email" class="form-perfil__label">E-mail</label>
                        <input type="email" name="email" class="form-perfil__input-col02" id="email" value="{{$user->email}}">
                        </div>
                        <div class="col-xs-6">
                            <label for="tipoUsuario" class="form-perfil__label">Tipo de Usuário</label>
                            <select name="tipoUsuario" name="tipoUsuario" class="form-perfil__select-col02" id="tipoUsuario">
                                <option value="1">Valor 01</option>
                                <option value="2">Valor 02</option>
                                <option value="3">Valor 03</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <div class="form-perfil__img">
                               
                                @if(!empty($user->imagem_perfil))
                                    <img src="{{asset('./admin/upload/empresas/'.$user->imagem_perfil)}}" style="width: 200px; height: 200px" class="img img-responsive img-thumbnail" alt=""><br>
                                    {{-- <button style="margin-top: 10px" class="btn btn-primary btn-sm" onclick="removeFotoPerfil({{$user->id}})">Remover</button> --}}
                                    <br>
                                    @endif  
                                    <span>Imagem De Perfil</span>
                                    <input type="file" name="imgPerfil" class="">
                            </div>
                        </div>
                    </div>
                    <div class="row form-perfil__row form-perfil__row-bg senha">
                        <div class="form-perfil__titulo">
                            <h3>Senha De Acesso</h3>
                        </div>
                        <div class="col-xs-6">
                            <label for="senhaAtual" class="form-perfil__label">Senha Atual:</label>
                            <input type="password" name="senhaAtual" class="form-perfil__input-col02" id="senhaAtual">
                        </div>
                    </div>
                    <div style="margin-top: -40px" class="row form-perfil__row form-perfil__row-bg senha">
                            <!--  <div class="col-xs-6">
                                    <label for="mostrarSenha"><input type="checkbox" id="mostrarSenha">Mostrar Senha</label>                            
                                </div> -->
                        <div class="col-xs-6">
                            <label for="repetirSenha" class="form-perfil__label">Nova Senha:</label>
                            <input type="password" name="novaSenha" class="form-perfil__input-col02" id="repetirSenha">
                        </div>
                        <div class="col-xs-6">
                            <label for="novaSenha" class="form-perfil__label">Repetir Nova Senha:</label>
                            <input type="password" name="novaSenhaC" class="form-perfil__input-col02" id="novaSenha">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <div class="form-perfil__acao">
                                <div class="form-perfil__acao-submit">
                                    <input type="submit" name="salvar" id="acaoSubmit" value="Salvar">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection