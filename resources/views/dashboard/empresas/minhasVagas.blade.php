@extends('layouts.empresaHeader')
@section('title', 'Vagas')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-6">
                    <div class="conteudo-header__numero">
                        <span><?= sizeof($vagas) ?> resultados encontrados</span>
                    </div>
                </div>
                <!--  <div class="col-xs-6">
                    <div class="conteudo-header__filtro">
                        <p>Organizar por: <span class="data">Data</span> | <span class="local">Local</span> </p>
                    </div>
                </div> -->
            </div>
        </div>
        <div id="headhunter-vagas" class="conteudo-painel">
            <div class="vagas">
                <div class="vagas__conteudo">
                    <div id="accordion">
                       @php $i = 0 @endphp
                       @foreach ($vagas as $vaga)
                        <!-- DADOS VAGA RESUMO -->
                        <div class="vagas__conteudo-resumo">
                            <div class="col-xs-3">
                                <div class="row">
                                    <div class="vagas__conteudo-col-cargo">
                                        <div class="agrupar">
                                            <div class="nome">
                                                <span>{{urldecode($vaga->cargo)}}</span>
                                            </div>
                                            <div class="data">
                                                <span>Publicado em {{mysqltoData($vaga->created_at)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="row">
                                    <div class="vagas__conteudo-col-local">
                                        <div class="icone">
                                            <div class="icn-resul-local-p-branco"></div>
                                        </div>
                                        <div class="local">
                                            <span>{{$vaga->cidade}} / {{$vaga->estado}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="row">
                                    <div class="vagas__conteudo-col-headhunters">
                                        <div class="agrupar">
                                            <div class="numero">
                                                <span>{{headhuntersVaga($vaga->id)}}</span>
                                            </div>
                                            <div class="nome">
                                                <span>Headhunters</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="row">
                                    <div class="vagas__conteudo-col-candidatos">
                                        <div class="agrupar">
                                            <div class="numero">
                                            <span>{{candidatosVaga($vaga->id)}}</span>
                                            </div>
                                            <div class="nome">
                                                <span>Candidatos</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1">
                                <div class="row">
                                    <div class="vagas__conteudo-col-abrir" onclick="mostaDados(this, {{$vaga->id}})">
                                        <div class="icone">
                                            <div class="icn-resul-acesso-p-branco setaVaga"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DADOS VAGA INFORMAÇÕES ADICIONAIS -->
                        <div class="dados-vaga vaga{{$vaga->id}} esconder">
                            <div class="vagas__informacoes">
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="vagas__informacoes-abas">
                                            <ul class="vagas__informacoes-tabs">
                                                <li class="active nav-tabs-item"><a data-toggle="tab" href="#analiseCv{{$vaga->id}}">Análise <br>Currícular</a></li>
                                                <li class="nav-tabs-item"><a data-toggle="tab" href="#headHunterAcao{{$vaga->id}}">Headhunter <br>Em Ação</a></li>
                                                <li class="nav-tabs-item"><a data-toggle="tab" href="#shotlistComp{{$vaga->id}}">Shortlist <br>Comparativo</a></li>
                                                <li class="nav-tabs-item"><a data-toggle="tab" href="#documentosCompr{{$vaga->id}}">Documentos <br>Comprobatório</a></li>
                                                <li class="nav-tabs-item"><a data-toggle="tab" href="#avaliacaoDisc{{$vaga->id}}">Avaliação <br>Disc</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="analiseCv{{$vaga->id}}" class="tab-pane fade in active">
                                                    <div class="vagas__informacoes-filtro">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <div class="vagas__informacoes-caixa">
                                                                    <select name="" id="">
                                                                        <option value="">Candidatos</option>
                                                                        <option value="">Pendente De Análise</option>
                                                                        <option value="">Aderente Ao Perfil</option>
                                                                        <option value="">Fora Do Perfil</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <div class="vagas__informacoes-caixa">
                                                                    <select name="" id="">
                                                                        <option value="">Score Ranqueamento</option>
                                                                        <option value="">Pendente De Análise</option>
                                                                        <option value="">Aderente Ao Perfil</option>
                                                                        <option value="">Fora Do Perfil</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <div class="vagas__informacoes-caixa">
                                                                    <select name="" id="">
                                                                        <option value="">Ações</option>
                                                                        <option value="">Pendente De Análise</option>
                                                                        <option value="">Aderente Ao Perfil</option>
                                                                        <option value="">Fora Do Perfil</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <div class="vagas__informacoes-caixa">
                                                                    <select name="" id="">
                                                                        <option value="">Headhunter</option>
                                                                        <option value="">Pendente De Análise</option>
                                                                        <option value="">Aderente Ao Perfil</option>
                                                                        <option value="">Fora Do Perfil</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <!-- CANDIDATOS DA VAGA -->
                                                    <div class="perfil-resul-acao">
                                                        @foreach(getCandidatosvaga($vaga->id) as $candidato)
                                                            <div class="vagas__informacoes-resultado-box" onclick="mostraInfoBox(this, {{$candidato->id}})">
                                                                <div class="row">
                                                                    <div class="col-xs-2">
                                                                        <div class="vagas__informacoes-img">
                                                                            @if(!empty($candidato->foto_perfil))
                                                                                <img src="{{asset('./admin/upload/candidatos/'.$candidato->foto_perfil)}}" style="height:75px; width:76px" alt="">
                                                                            @else
                                                                                <img src="{{asset('./admin/imgs/icons/perfil-img-resul.png')}}" alt="">
                                                                            @endif                                                                                                                                                    
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-4">
                                                                        <div class="vagas__informacoes-nome">
                                                                        <span>{{$candidato->nome}}</span>
                                                                        </div>
                                                                        <div class="vagas__informacoes-local">{{$candidato->cidade}} / {{$candidato->estado}}</div>
                                                                    </div>
                                                                    <div class="col-xs-3">
                                                                        <div class="vagas__informacoes-score">
                                                                        <div class="numero">80%</div>
                                                                        <div class="texto">score</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-3">
                                                                        <div class="vagas__informacoes-status-box">
                                                                            <span>Fora do Perfil <i class="fa fa-circle"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="vagas__informacoes-resultado-box-conteudo">
                                                                <div class="vagas__informacoes-resultado">
                                                                    <div class="row">
                                                                        <div class="col-xs-2">
                                                                            <div class="vagas__informacoes-img">
                                                                                @if(!empty($candidato->foto_perfil))
                                                                                    <img src="{{asset('./admin/upload/candidatos/'.$candidato->foto_perfil)}}" style="height:75px; width:76px" alt="">
                                                                                @else
                                                                                    <img src="{{asset('./admin/imgs/icons/perfil-img-resul.png')}}" alt="">
                                                                                @endif   
                                                                            </div>
                                                                            <div class="vagas__informacoes-score">
                                                                                <div class="numero">80%</div>
                                                                                <div class="texto">score</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-5">
                                                                            <div class="vagas__informacoes-nome">
                                                                                <span>{{$candidato->nome}}</span>
                                                                            </div>
                                                                            <div class="vagas__informacoes-local">
                                                                                {{$vaga->cidade}} / {{$vaga->estado}}
                                                                            </div>
                                                                            @php $experiencia = candidatoExperiencia($candidato->id);@endphp
                                                                            @if(sizeof($experiencia))
                                                                            @foreach($experiencia as $exp)
                                                                                <div class="vagas__informacoes-experiencias">
                                                                                    <div class="titulo">{{$loop->first ? 'Última Experiência:' : 'Experiência Anterior:'}}</div>
                                                                                    <div class="empresa-data"> <span> {{$exp->empresa}} - {{mesAno($exp->admissao)}} a {{mesAno($exp->desligamento)}} </span></div>
                                                                                    <div class="cargo">{{urldecode($exp->cargo)}}</div>
                                                                                </div>
                                                                            @endforeach
                                                                           
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-xs-5">
                                                                            <div class="vagas__informacoes-avaliacao">
                                                                                <div class="titulo">Avaliação Curricular</div>
                                                                                <div class="status">
                                                                                    <p>Aderente Ao Perfil</p>
                                                                                    <p>Fora Do Perfil</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="vagas__informacoes-agendamento">
                                                                                <div class="texto">Solicitar Agendamento Da Entrevista</div>
                                                                                <div class="icone">
                                                                                    <div class="icn-calendario-m-azul"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="vagas__informacoes-mensagem">
                                                                                <div class="texto">Enviar Mensagem Para o Headhunter</div>
                                                                                <div class="icone">
                                                                                    <div class="icn-perfil-email-p-azul"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="vagas__informacoes-headhunter">
                                                                            <div class="row">
                                                                                <div class="col-xs-2">
                                                                                <div class="titulo">
                                                                                    <span>Headhunter</span>
                                                                                </div>
                                                                                </div>
                                                                                <div class="col-xs-3">
                                                                                <div class="nome">
                                                                                <span>{{buscaHeadhunter($candidato->id, $vaga->id)}}</span>
                                                                                </div>
                                                                                </div>
                                                                            </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="vagas__informacoes-extra">
                                                                    <div class="vagas__informacoes-extra-titulo">
                                                                        <span>Evolução e Feedback Do Processo Seletivo</span>
                                                                    </div>
                                                                    <div class="vagas__informacoes-extra-conteudo">
                                                                        <div class="row">
                                                                            <div class="col-xs-4">
                                                                                <div class="vagas__informacoes-extra-etapas">
                                                                                    <div class="titulo">Etapas</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <div class="vagas__informacoes-extra-etapas">
                                                                                    <div class="titulo">Data</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <div class="vagas__informacoes-extra-etapas">
                                                                                    <div class="titulo">Status</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                <div class="vagas__informacoes-extra-etapas">
                                                                                    <div class="titulo">Observação</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        {{-- <div class="row">
                                                                            <div class="col-xs-4">
                                                                                <div class="vagas__informacoes-extra-etapas">
                                                                                    <label for="">ICN</label>
                                                                                    <input type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <div class="vagas__informacoes-extra-data">
                                                                                    <input type="input">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-3">
                                                                                <div class="vagas__informacoes-extra-status">
                                                                                    <select name="" id="">
                                                                                        <option value="">Aprovado</option>
                                                                                        <option value="">Valor 02</option>
                                                                                        <option value="">Valor 03</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-3">
                                                                                <div class="vagas__informacoes-extra-obs">
                                                                                    <textarea name="" id=""></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div> --}}
                                                                        {{-- <div class="row">
                                                                            <div class="col-xs-4">
                                                                                <div class="vagas__informacoes-extra-etapas">
                                                                                    <label for="">ICN</label>
                                                                                    <input type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <div class="vagas__informacoes-extra-data">
                                                                                    <input type="input">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-3">
                                                                                <div class="vagas__informacoes-extra-status">
                                                                                    <select name="" id="">
                                                                                        <option value="">Aprovado</option>
                                                                                        <option value="">Valor 02</option>
                                                                                        <option value="">Valor 03</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-3">
                                                                                <div class="vagas__informacoes-extra-obs">
                                                                                    <textarea name="" id=""></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div> --}}
                                                                        @php
                                                                            $evol = getEvolucaoCdd($candidato->id, $vaga->id);
                                                                            // json_decode($vaga->etapas_processo_seletivo);                                                                            
                                                                        @endphp
                                                                        @if(sizeof($evol))
                                                                            @foreach($evol as $evolucao)
                                                                                <form method="post" class="form-evolucao{{$i}}">                                                                               
                                                                                    @csrf                                                                                
                                                                                    <div class="row">
                                                                                        <div class="col-xs-4">
                                                                                            <div class="vagas__informacoes-extra-etapas">
                                                                                                <label for="">ICN</label>
                                                                                                <select name="etapa">
                                                                                                    @foreach($etapas as $etapa)
                                                                                                        @if(in_array($etapa->id, json_decode($vaga->etapas_processo_seletivo)))
                                                                                                            @php $selected = $evolucao->etapa == $etapa->id ? 'selected' : 'disabled' @endphp
                                                                                                            <option {{$selected}} value="{{$etapa->id}}">{{urldecode($etapa->etapa)}}</option>
                                                                                                        @endif
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-2">
                                                                                            <div class="vagas__informacoes-extra-data">
                                                                                            <input type="text" name="data" class="data" readonly value="{{MySqlToData($evolucao->data)}}">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-2">
                                                                                            <div class="vagas__informacoes-extra-status">
                                                                                                <select name="status">
                                                                                                    <option {{$evolucao->status == 1 ? 'selected' : 'disabled'}} value="1">Aprovado</option>
                                                                                                    <option {{$evolucao->status == 2 ? 'selected' : 'disabled'}} value="2">Em analise</option>
                                                                                                    <option {{$evolucao->status == 3 ? 'selected' : 'disabled'}} value="3">Reprovado</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-3">
                                                                                            <div class="vagas__informacoes-extra-obs">
                                                                                                <textarea name="obs{{$i}}" readonly>{{$evolucao->observacoes}}</textarea>
                                                                                                <input type="hidden" value="{{$i}}" name="aux">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-1" style="left: -14px;">
                                                                                            <div class="vagas__informacoes-extra-obs">
                                                                                                {{-- <input type="submit" style="padding: 10px;
                                                                                                color: white; background: #3c6ba2; border:#3c6ba2" value="Salvar" onclick="salvar(this)"> --}}
                                                                                            </div>
                                                                                        </div>                                                                                    
                                                                                    </div>                                                                                    
                                                                                    {{-- <input type="hidden" name="cdd" value="{{$candidato->id}}">
                                                                                    <input type="hidden" name="vaga" value="{{$vaga->id}}">
                                                                                    <input type="hidden" name="hh" value="{{buscaHeadhunterId($candidato->id, $vaga->id)}}">  --}}
                                                                                </form>  
                                                                                @php $i++ @endphp 
                                                                            @endforeach
                                                                        @endif 
                                                                        @php 
                                                                            //Caso só exista um formulário preenchido pela empresa, então o código abaixo irá mostrar os formulários que faltam
                                                                        @endphp       
                                                                        @if(sizeof(json_decode($vaga->etapas_processo_seletivo)) > sizeof($evol))
                                                                            @for($x = 0; $x < sizeof(json_decode($vaga->etapas_processo_seletivo)) - sizeof($evol); $x++)
                                                                                <form method="post" class="form-evolucao{{$i}}">                                                                               
                                                                                    @csrf                                                                                    
                                                                                    <div class="row">
                                                                                        <div class="col-xs-4">
                                                                                            <div class="vagas__informacoes-extra-etapas">
                                                                                                <label for="">ICN</label>
                                                                                                <select name="etapa">
                                                                                                    @foreach($etapas as $etapa)
                                                                                                        @if(in_array($etapa->id, json_decode($vaga->etapas_processo_seletivo)))
                                                                                                            <option value="{{$etapa->id}}">{{urldecode($etapa->etapa)}}</option>
                                                                                                        @endif
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-2">
                                                                                            <div class="vagas__informacoes-extra-data">
                                                                                            <input type="text" name="data" class="data" value="{{date('d/m/Y')}}">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-2">
                                                                                            <div class="vagas__informacoes-extra-status">
                                                                                                <select name="status">
                                                                                                    <option value="1">Aprovado</option>
                                                                                                    <option value="2" selected>Em analise</option>
                                                                                                    <option value="3">Reprovado</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-3">
                                                                                            <div class="vagas__informacoes-extra-obs">
                                                                                                <textarea name="obs{{$i}}"></textarea>
                                                                                                <input type="hidden" value="{{$i}}" name="aux">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-1" style="left: -14px;">
                                                                                            <div class="vagas__informacoes-extra-obs">
                                                                                                <input type="submit" style="padding: 10px;
                                                                                                color: white; background: #3c6ba2; border:#3c6ba2" value="Salvar" onclick="salvar(this)">
                                                                                            </div>
                                                                                        </div>                                                                                    
                                                                                    </div>                                                                                    
                                                                                    <input type="hidden" name="cdd" value="{{$candidato->id}}">
                                                                                    <input type="hidden" name="vaga" value="{{$vaga->id}}">
                                                                                    <input type="hidden" name="hh" value="{{buscaHeadhunterId($candidato->id, $vaga->id)}}"> 
                                                                                </form>  
                                                                                @php $i++ @endphp                                                                          
                                                                            @endfor
                                                                        @endif
                                                                        <div class="row">
                                                                            <div class="col-xs-8">
                                                                                <div class="vagas__informacoes-extra-conteudo-titulo">
                                                                                    Contratação
                                                                                </div>
                                                                                <div class="vagas__informacoes-extra-admissao">
                                                                                    <div class="texto">
                                                                                        Data De Admissão
                                                                                    </div>
                                                                                    <div class="data">
                                                                                        <span>30/08/2017</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="vagas__informacoes-extra-garantia">
                                                                                    <div class="texto">
                                                                                        Garantia 30 Dias
                                                                                    </div>
                                                                                    <div class="data">
                                                                                        <span>30/08/2017</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                <div class="vagas__informacoes-extra-avaliacao">
                                                                                    <div class="titulo">
                                                                                        Avalição Do Headhunter
                                                                                    </div>
                                                                                    <div class="icone">ICN</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <!-- HEADHUNTER EM AÇÃO -->
                                                <div id="headHunterAcao{{$vaga->id}}" class="tab-pane fade">
                                                    @php $aux = '' @endphp
                                                    @foreach(infoHeadhunterVaga($vaga->id) as $headhunter)
                                                        @if($headhunter->id == $aux)
                                                            @continue
                                                        @else
                                                            @php $aux = $headhunter->id @endphp
                                                        @endif
                                                        <div class="headHunterPerfil">
                                                            <div class="headHunterPerfil-box">
                                                                <div class="row">
                                                                    <div class="col-xs-2">
                                                                        <div class="vagas__informacoes-img">
                                                                            @if(!empty($headhunter->imagem_perfil))
                                                                                <img src="{{asset('./admin/upload/'.$headhunter->imagem_perfil)}}" width="76px" height="75px" alt="">
                                                                            @else
                                                                                <img src="{{asset('./admin/imgs/icons/perfil-img-resul.png')}}" alt="">
                                                                            @endif                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-3">
                                                                        <div class="vagas__informacoes-nome">
                                                                            <span>{{$headhunter->nome}}</span>
                                                                        </div>
                                                                        <div class="vagas__informacoes-local">
                                                                            {{$headhunter->cidade}}, {{$headhunter->estado}}
                                                                        </div>
                                                                    </div>
                                                                    {{-- <div class="col-xs-3">
                                                                        <div class="vagas__informacoes-engajamento">
                                                                            <span>Enganjamento <br>28/08/17</span>
                                                                            <br>
                                                                            <span>Método Candidatura <br>Espotânea</span>
                                                                        </div>
                                                                    </div> --}}
                                                                    <div class="col-xs-4">
                                                                        <div class="vagas__informacoes-numero-box">
                                                                            <div class="row">
                                                                                <div class="col-xs-4">
                                                                                    <span>01</span>
                                                                                </div>
                                                                                <div class="col-xs-4">
                                                                                    <span>01</span>
                                                                                </div>
                                                                                <div class="col-xs-4">
                                                                                    <span>01</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="headHunterPerfil-conteudo">
                                                                <div class="vagas__informacoes-resultado">
                                                                    <div class="row">
                                                                        <div class="col-xs-3">
                                                                            <div class="vagas__informacoes-aba2-img">
                                                                                <img src="{{asset('./admin/imgs/icons/perfil-img-resul.png')}}" alt="">
                                                                            </div>
                                                                            <div class="vagas__informacoes-aba2-nomelocal">
                                                                            <div class="nome">{{$headhunter->nome}}</div>
                                                                                <div class="local">{{$headhunter->cidade}}, {{$headhunter->estado}}</div>
                                                                            </div>
                                                                            <div class="vagas__informacoes-aba2-engajamento">
                                                                                <p>Engajamento</p>
                                                                            <p>{{MysqlToData($headhunter->engajamento)}}</p>
                                                                            </div>
                                                                            <div class="vagas__informacoes-aba2-metodo">
                                                                                <p>Método</p>
                                                                                <p>Candidatura
                                                                                <br>Espotânea</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-9">
                                                                            {{-- @foreach(getCddHeadhunterVaga($vaga->id, $headhunter->id) as $candidato) --}}
                                                                                <div class="row">
                                                                                    <div class="col-xs-5">
                                                                                        <div class="vagas__informacoes-aba2-recrutados">
                                                                                            <div class="titulo">
                                                                                                <span>Candidatos
                                                                                            <br>Recrutados</span>
                                                                                            </div>
                                                                                            <div class="nomes">
                                                                                                @foreach(getCddHeadhunterVaga($vaga->id, $headhunter->id) as $candidato)
                                                                                                    {{$candidato->nome}}
                                                                                                @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <div class="vagas__informacoes-aba2-dataenvio">
                                                                                            <div class="titulo">
                                                                                                <span>Data Do
                                                                                                <br>Envio</span>
                                                                                            </div>
                                                                                            <div class="datas">
                                                                                                @foreach(getCddHeadhunterVaga($vaga->id, $headhunter->id) as $candidato)
                                                                                                    <p>{{MysqlToData($candidato->created_at)}}</p>
                                                                                                @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-4">
                                                                                        <div class="vagas__informacoes-aba2-status">
                                                                                            <div class="titulo">
                                                                                                <span>Status</span>
                                                                                            </div>
                                                                                            <div class="status">
                                                                                                @foreach(getCddHeadhunterVaga($vaga->id, $headhunter->id) as $candidato)
                                                                                                    <p style="color:#8ac93a;">Aderente Ao Perfil
                                                                                                        <i class="fa fa-circle"></i>
                                                                                                    </p>
                                                                                                @endforeach
                                                                                                
                                                                                                {{-- <p style="color:red;">Fora Do Perfil
                                                                                                    <i class="fa fa-circle"></i>
                                                                                                </p> --}}
                                                                                                {{-- <p style="color:#808080;">Pendente De Análise
                                                                                                    <i class="fa fa-circle"></i>
                                                                                                </p> --}}
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            {{-- @endforeach --}}
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="vagas__informacoes-aba2-acao">
                                                                                        <button>
                                                                                            <div class="icone">ICN</div>
                                                                                            <span>Avalie o Desempenho
                                                                                                <br>Do Headhunter</span>
                                                                                        </button>
                                                                                        <button>
                                                                                            <div class="icone">ICN</div>
                                                                                            <span>Adicionar aos
                                                                                                    <br>Meus Preferidos</span>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <div id="shotlistComp{{$vaga->id}}" class="tab-pane fade">
                                                    <div class="vagas__informacoes-aba3-bg">
                                                        <div class="vagas__informacoes-aba3-conteudo">
                                                            <div class="col-xs-4">
                                                                <div class="vagas__informacoes-aba3-tipo">
                                                                    <div class="titulo">
                                                                        <span>Tipo de Relatório</span>
                                                                    </div>
                                                                    <input type="checkbox" id="ind" name="individual">
                                                                    <label for="ind">Individual</label>
                                                                    <br>
                                                                    <input type="checkbox" id="comp" name="individual">
                                                                    <label for="comp">Quadro Comparativo</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <div class="vagas__informacoes-aba3-candidatos">
                                                                    <div class="titulo">
                                                                        <span>Selecione Os Candidatos</span>
                                                                    </div>
                                                                    @foreach(getCandidatosvaga($vaga->id) as $candidato)
                                                                        <input type="checkbox" id="{{$candidato->id}}" name="individual">
                                                                        <label for="{{$candidato->id}}">{{$candidato->nome}}</label>
                                                                        <br>
                                                                    @endforeach
                                                                    {{-- <input type="checkbox" name="individual">
                                                                    <label for="">Gilberto Studart</label>
                                                                    <br>
                                                                    <input type="checkbox" name="individual">
                                                                    <label for="">Carolina Lucena</label> --}}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <div class="vagas__informacoes-aba3-relatorio">
                                                                    <div class="titulo">
                                                                        <span>Relatório</span>
                                                                    </div>
                                                                    <div class="icone">ICN</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="documentosCompr{{$vaga->id}}" class="tab-pane fade">
                                                    <div class="vagas__informacoes-aba4-bg">
                                                        <div class="vagas__informacoes-aba4-conteudo">
                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <div class="vagas__informacoes-aba4-candidatos">
                                                                        <div class="titulo">
                                                                            <span>Candidatos</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vagas__informacoes-aba4-candidatos">
                                                                        @foreach(getCandidatosvaga($vaga->id) as $candidato)
                                                                            <a href="">{{$candidato->nome}}</a>
                                                                            <br>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <div class="vagas__informacoes-aba4-documentos">
                                                                        <div class="titulo">
                                                                            <span>Documentos Comprobatórios</span>
                                                                        </div>
                                                                        <div class="vagas__informacoes-aba4-acoes">
                                                                            @foreach(getCandidatosvaga($vaga->id) as $candidato)
                                                                                <a href="">Diploma Conclusão Nível Superior</a>
                                                                                <br>
                                                                                <a href="">Diploma Pós Graduação</a>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="avaliacaoDisc{{$vaga->id}}" class="tab-pane fade">
                                                    <div class="vagas__informacoes-aba5-bg">
                                                        <div class="vagas__informacoes-aba5-conteudo">
                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <div class="vagas__informacoes-aba4-candidatos">
                                                                        <div class="titulo">
                                                                            <span>Candidatos</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="candidatos">
                                                                        @foreach(getCandidatosvaga($vaga->id) as $candidato)
                                                                            <a href="">{{$candidato->nome}}</a>                                                                        
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <div class="vagas__informacoes-aba4-avaliacao">
                                                                        <div class="titulo">
                                                                            <span>Avaliação Disc</span>
                                                                        </div>
                                                                        <div class="vagas__informacoes-aba4-">
                                                                            <a href="">Não Consta Nos Registros!</a>
                                                                            <a href="">Não Consta Nos Registros!</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <div class="vagas__informacoes-aba4-acao">
                                                                        <div class="titulo">
                                                                            <span>Ação</span>
                                                                        </div>
                                                                        <div class="vagas__informacoes-aba4-acoes">
                                                                            <a href="">Visualizar</a>
                                                                            <a href="">Solicitar Disc</a>
                                                                            <a href="">Solicitar Disc</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <div class="row">
                                        <div class="vagas__boxs-lateral">
                                            <div class="vagas__boxs-lateral-row">
                                                <div class="vagas__boxs-lateral-titulo">
                                                    <span>Candidatos</span>
                                                </div>
                                            </div>
                                            <div class="vagas__boxs-lateral-row">
                                                <div class="vagas__boxs-lateral-box-01">
                                                    <div class="box-numero">
                                                    <span>{{sizeof(getCandidatosvaga($vaga->id))}}</span>
                                                    </div>
                                                    <div class="box-descricao">
                                                        <span>Pendentes De Análise</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vagas__boxs-lateral-row">
                                                <div class="vagas__boxs-lateral-box-02">
                                                    <div class="box-numero">
                                                        <span>03</span>
                                                    </div>
                                                    <div class="box-descricao">
                                                        <span>Aderentes Ao Perfil</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vagas__boxs-lateral-row">
                                                <div class="vagas__boxs-lateral-box-03">
                                                    <div class="box-numero">
                                                        <span>02</span>
                                                    </div>
                                                    <div class="box-descricao">
                                                        <span>Fora Do Perfil</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
<script>
    function salvar(element)
    {
        event.preventDefault();
        form = $(element).parent().parent().parent().parent().attr('class');
        $(element).prop("disabled",true);
        $.post('/dashboard/empresa/evoluirCdd', $('.'+form).serialize(), function(data)
        {
            if(data == 'ok')
            {
                alert('Informação salva com sucesso');
            }
        });
    }
</script>
@endsection