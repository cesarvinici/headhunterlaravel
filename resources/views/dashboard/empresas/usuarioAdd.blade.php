<div class="conteudo-painel">
        <form class="form-perfil" action="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-perfil__conteudo">
                <div class="row form-perfil__row">
                    <div class="col-xs-12">
                        <label for="nomeCompleto" class="form-perfil__label">Nome</label>
                    <input type="text" name="nome" value="{{old('nome')}}" class="form-perfil__input" id="nomeCompleto" >
                    </div>
                </div>
                <div class="row form-perfil__row">
                    <div class="col-xs-12">
                        <label for="caergo" class="form-perfil__label">Cargo</label>
                    <input type="text" name="cargo" value="{{old('cargo')}}" class="form-perfil__input" id="cargo">
                    </div>
                </div>
                <div class="row form-perfil__row">
                    <div class="col-xs-6">
                        <label for="email" class="form-perfil__label">E-mail</label>
                    <input type="email" name="email" value="{{old('email')}}" class="form-perfil__input-col02" id="email">
                    </div>
                    <div class="col-xs-6">
                        <label for="tipoUsuario" class="form-perfil__label">Tipo de Usuário</label>
                        <select name="tipoUsuario" name="tipoUsuario" class="form-perfil__select-col02" id="tipoUsuario">
                            <option value="1">Valor 01</option>
                            <option value="2">Valor 02</option>
                            <option value="3">Valor 03</option>
                        </select>
                    </div>
                </div>
                <div class="row form-perfil__row">
                    <div class="col-xs-12">
                        <div class="form-perfil__img">
                                <span>Imagem De Perfil</span>
                        <input type="file" name="imgPerfil" value="{{old('imgPerfil')}}" class="">
                        </div>
                    </div>
                </div>
                <div class="row form-perfil__row form-perfil__row-bg senha">
                    <div class="form-perfil__titulo">
                        <h3>Senha De Acesso</h3>
                    </div>
                    <div class="col-xs-6">
                            <label for="repetirSenha" class="form-perfil__label">Nova Senha:</label>
                            <input type="password" name="novaSenha" class="form-perfil__input-col02" id="repetirSenha">
                        </div>
                        <div class="col-xs-6">
                            <label for="novaSenha" class="form-perfil__label">Repetir Nova Senha:</label>
                            <input type="password" name="novaSenhaC" class="form-perfil__input-col02" id="novaSenha">
                        </div>
                </div>
                <div class="row form-perfil__row">
                    <div class="col-xs-12">
                        <div class="form-perfil__acao">
                            <div class="form-perfil__acao-submit">
                                <input type="submit" name="salvar" id="acaoSubmit" value="Salvar">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>