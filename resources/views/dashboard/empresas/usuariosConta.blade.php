@extends('layouts.empresaHeader')
@section('title', 'Usuários da Conta')
@section('content')
<div class="col-xs-9">
    <div style="margin-top:10px" class="row col-md-6 col-md-offset-3">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger">
                <ul>
                    <li>
                        {{Session::get('error')}}
                    </li>
                </ul>				
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li>
                        {{Session::get('success')}}
                    </li>
                </ul>				
            </div>
        @endif
    </div>
    <section class="conteudo">
        <div class="conteudo-header">            
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Usuários</h2>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__voltar btn-voltar">
                        <div class="conteudo-header__voltar-icone">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto ">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        @if(sizeof($usuarios))
            <div class="usuarios">               
                <div class="usuarios__tabela">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Cargo</th>
                                    <th colspan="2" style="width:10%;">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($usuarios as $usuario)
                                <tr>                        
                                    <td>{{$usuario->nome}}</td>
                                    <td>{{$usuario->email}}</td>
                                    <td>{{$usuario->cargo}}</td>
                                    <td class="usuarios__tabela-editar" onclick="editUserEmpresa({{$usuario->id}})"><i class="fa fa-edit"></i></td>
                                    <td class="usuarios__tabela-fechar" onclick="excluirUsuarioEmpresa({{$usuario->id}})"><i class="fa fa-close"></i></td>
                                </tr>
                                @endforeach                                 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
        <div class="conteudo-painel">
            @include('dashboard.empresas.usuarioAdd')
        </div>
    </section>
</div>
@endsection