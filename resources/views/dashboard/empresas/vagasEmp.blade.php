<div class="consulta__filtro-resul">
    <div class="row">
        <div class="col-xs-6">
            <div class="resul__header-numero">
            <p><span class="resultEnc">{{sizeof($vagas)}}</span> resultados encontrados</p>
            </div>
        </div>
        <!-- <div class="col-xs-6">
            <div class="resul__header-filtro">
                <p>Organizar por: <span class="cidade">Nome Fantasia</span> | <span class="alfabetica">Código</span> | <span class="valor">Razão Social</span></p>
            </div>
        </div> -->
    </div>
</div>
<div class="consulta__tabela">
    <div class="table-responsive tabelaManutVagas">
        <table class="table tabelaVagas tablesorter">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Função</th>
                    <th>Status</th>
                    <th>Cidade</th>
                    <th>Tipo</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="tbody"> 
                @foreach($vagas as $vaga)
                    <tr>
                        <td>{{$vaga->id}}</td>
                            <td>{{urldecode($vaga->cargo)}}</td>
                            <td>
                                <select class="form-control" onchange="alteraStatus({{$vaga->id}}, this)">
                                    @foreach(statusVaga() as $status)
                                    @php $selected = $status->id == $vaga->status_vaga ? 'selected' : ''; @endphp
                                        <option {{$selected}} value="{{$status->id}}">{{$status->status_vaga}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>{{urldecode($vaga->cidade)}}</td>
                            <td>{{urldecode($vaga->regime)}}</td>
                            <td><a href="/dashboard/empresa/manutencao-vagas/{{$vaga->id}}/edit"><i class="fa fa-external-link pointer"></i></a>
                            <img style="margin-left: 5px" class="pointer" onclick="deletaVaga({{$vaga->id}})" src="{{asset('./admin/imgs/icons/delete.png')}}">
                        </td>
                    </tr>
                @endforeach                                  
            </tbody>
        </table>
    </div>
</div>