@extends('layouts.headhunterHeader')
@section('title', 'Adicionar Vaga')
@section('content')

<div class="col-xs-9">


    <section class="conteudo">
            <div class="conteudo-header">
                    @if(Session::has('cad_vaga_ok'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{{Session::get('cad_vaga_ok')}}</li>
                        </ul>
                       
                    </div>
                @endif
            
                @if(Session::has('cad_vaga_fail'))
                    <div class="alert alert-danger">
                        <ul>
                            <li> {{Session::get('cad_vaga_fail')}}</li>
                        </ul>
                       
                    </div>
                @endif
                
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
    
             <div class="form-perfil__titulo form-perfil__titulo-azul">
                <h3>Cadastrar nova vaga</h3>
            </div>
    
            <div class="row">
              <div class="col-xs-6">
                <div class="conteudo-header__guia">
                    <div class="conteudo-header__guia-icone">ICN</div>
                    <div class="conteudo-header__guia-texto">Guia De Orientação</div>
                </div>
            </div>
        </div>
    </div>
    <div class="conteudo-painel">
        <!-- FORM 1 -->
        <div class="row form-perfil__row">
            <div class="col-xs-12">
                <div class="form-perfil__menu">
                    <a href="#" id="guia-1" class="tablinks tabs form-perfil__menu-link form-perfil__menu-link-ativo" onclick="novaGuia(event, 'form-1')">01 - Dados Da Vaga</a>
                    <a href="#" id="guia-2" class="tablinks tabls form-perfil__menu-link" onclick="novaGuia(event, 'form-2')">02 - Remuneração</a>
                    <a href="#" id="guia-3" class="tablinks tabls form-perfil__menu-link" onclick="novaGuia(event, 'form-3')">03 - Fee Recrutamento</a>
                </div>
            </div>
        </div>
        <div id="form-1" class="tab form-1">
    
        <form class="form-perfil" action="{{route('store.vagasHH')}}" method="post">
            @csrf
            <div class="form-perfil__conteudo">
                <div id="dadosVaga">
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <input type="checkbox" id="vagaPCD" name="vagaPCD">
                            <label for="vagaPCD">Vaga PCD</label>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="tituloCargo" class="form-perfil__label">Título do Cargo</label>
                            <input type="text" autocomplete="off" name="tituloCargo" class="form-perfil__input validatePage1 validate" id="tituloCargo">
                            <input type="hidden" name="idCargo" value="" id="">
                            <p style="color: red" class="empty-message"></p>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="numVagas" class="form-perfil__label">Nº de Vagas</label>
                            <input type="text" name="numVagas" class="form-perfil__input-col02 validatePage1 validate" id="numVagas">
                        </div>
                        <div class="col-xs-6">
                            <input type="radio" name="sigilosos" id="sigilosoSim" value="1">
                            <label for="sigilosoSim">Processo Sigiloso <small><br>(Dados da Empresa Oculto)</small></label>
                            <input type="radio" name="sigilosos" id="sigilosoNao" checked value="0" checked>
                            <label for="sigilosoNao">Processo Aberto</label>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <div class="form-perfil__coluna-label form-perfil__coluna-label-h-240px">
                                <span>Localidade da Vaga</span>
                            </div>
                            <div class="form-perfil__coluna-inputs">
                                <div class="form-perfil__coluna-inputs-row">
                                    <select name="empresa" onchange="getEmpresa()" class="form-perfil__select-col02 validatePage1 validate" id="empresa">
                                        <option value="" selected disabled>Selecione uma empresa</option>
                                        @if(!empty($empresas))
                                            @foreach($empresas as $empresa)
                                                <option value="{{$empresa->id}}"> {{$empresa->nome_fantasia}} - {{$empresa->cnpj}} </option>
                                            @endforeach
                                        @else
                                            <option selected disabled value="">É necessário cadastrar ao menos 1 empresa</option> 
                                        @endif                                      
                                    </select>
                                </div>
                                <div class="row">
                                        <div class="col-xs-6">
                                        <label for="cidade" class="form-perfil__label">Cidade*</label>
                                        <input type="text" name="cidade" readonly class="form-perfil__select-col04 validatePage1 validate" id="cidade" maxlength="16">
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="estado" class="form-perfil__label">Estado*</label>
                                        <input type="text" name="estado" readonly class="form-perfil__select-col04 validatePage1 validate" id="estado" maxlength="16">
                                    </div>
                                </div>
                                <!-- <div class="form-perfil__coluna-inputs-row">
                                <label for="empresa" class="form-perfil__label">Empresa</label>
                                <input type="text" class="form-perfil__input-col03" id="empresa">
                                </div> -->
                                <div class="form-perfil__coluna-inputs-row">
                                    <label for="segmento" class="form-perfil__label validatePage1 validate">Segmento</label>
                                    <input type="text" name="segmento" readonly class="form-perfil__input-col03 validatePage1 validate" id="segmento">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="formacaoExigida" class="form-perfil__label-textarea">Formação Exigida ou Desejada</label>
                            <textarea name="formacaoExigida" class="form-perfil__input-textarea validatePage1 validate" id="formacaoExigida" placeholder="Descreva a formação necessário ou desejada. Ex.: Formação Superior em Admnistração de Empresas. Desejável MBA em Marketing" maxlength="300" ></textarea>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="atividades" class="form-perfil__label-textarea">Principais Atividades</label>
                            <textarea name="principaisAtividades" class="form-perfil__input-textarea validatePage1 validate" id="atividades" placeholder="Descreva as principais atribuições e responsabilidades do cargo"></textarea>
                        </div>
                    </div>

                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="requisitos" class="form-perfil__label-textarea">Requisitos Necessários ou Desejáveis</label>
                            <textarea name="requisitos" class="form-perfil__input-textarea validatePage1 validate" id="requisitos" placeholder="Enumere as experiências ou especificações exigidas ou desejadas para a vaga."></textarea>
                        </div>
                    </div>    
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <div class="form-perfil__coluna-label form-perfil__coluna-label-h-285px">
                                <span>Conhecimento ou Aptidões Técnicas Necessário(a)s</span>
                            </div>
                            <div class="form-perfil__coluna-inputs" id="aptidoes">
                                <div class="form-perfil__coluna-inputs-row  div-conhecimentos" >
                                    <textarea name="conhecimento[]" class="validatePage1 validate"  id="" cols="79" rows="2"></textarea>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label for="">Nível Requerido:</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="radio" id="conhecimentonv11" name="nivelConhecimento1" value="1" checked>
                                            <label for="conhecimentonv11">Básico</label>
                                            <input type="radio" id="conhecimentonv12" name="nivelConhecimento1" value="2">
                                            <label for="conhecimentonv12">intermediário</label>
                                            <input type="radio" id="conhecimentonv13" name="nivelConhecimento1" value="3">
                                            <label for="conhecimentonv13">Avançado</label>
                                        </div>
                                    </div>
                                    {{-- <div class="row">
                                        <div class="col-xs-12">
                                            <img src="{{asset('admin/imgs/icons/add16px.png')}}" style="float: right; margin-right: 5px; display: inline;" class="btnaddNovaAptidao btnNovoItem">
                                        </div>    
                                    </div> --}}
                                </div>
                                <div class="form-perfil__coluna-inputs-row  div-conhecimentos" >
                                        <textarea name="conhecimento[]" class="validatePage1 validate"  id="" cols="79" rows="2"></textarea>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label for="">Nível Requerido:</label>
                                            </div>
                                            <div class="col-xs-8">
                                                <input type="radio" id="conhecimentonv21" name="nivelConhecimento2" value="1" checked>
                                                <label for="conhecimentonv21">Básico</label>
                                                <input type="radio" id="conhecimentonv22" name="nivelConhecimento2" value="2">
                                                <label for="conhecimentonv22">intermediário</label>
                                                <input type="radio" id="conhecimentonv23" name="nivelConhecimento2" value="3">
                                                <label for="conhecimentonv23">Avançado</label>
                                            </div>
                                        </div>
                                        {{-- <div class="row">
                                            <div class="col-xs-12">
                                                <img src="{{asset('admin/imgs/icons/add16px.png')}}" style="float: right; margin-right: 5px; display: inline;" class="btnaddNovaAptidao btnNovoItem">
                                            </div>    
                                        </div> --}}
                                    </div>
                                    <div class="form-perfil__coluna-inputs-row  div-conhecimentos" >
                                            <textarea name="conhecimento[]" class="validatePage1 validate"  id="" cols="79" rows="2"></textarea>
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <label for="">Nível Requerido:</label>
                                                </div>
                                                <div class="col-xs-8">
                                                    <input type="radio" id="conhecimentonv31" name="nivelConhecimento3" value="1" checked>
                                                    <label for="conhecimentonv31">Básico</label>
                                                    <input type="radio" id="conhecimentonv32" name="nivelConhecimento3" value="2">
                                                    <label for="conhecimentonv32">intermediário</label>
                                                    <input type="radio" id="conhecimentonv33" name="nivelConhecimento3" value="3">
                                                    <label for="conhecimentonv33">Avançado</label>
                                                </div>
                                            </div>
                                            {{-- <div class="row">
                                                <div class="col-xs-12">
                                                    <img src="{{asset('admin/imgs/icons/add16px.png')}}" style="float: right; margin-right: 5px; display: inline;" class="btnaddNovaAptidao btnNovoItem">
                                                </div>    
                                            </div> --}}
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <div class="form-perfil__coluna-label form-perfil__coluna-label-h-170px ">
                                <span>Conhecimento de outro idioma</span>
                            </div>
                            <div class="form-perfil__coluna-inputs" id="idiomas">
                                <div class="form-perfil__coluna-inputs-row">
                                    <select name="conhecimentoIdioma[]" id="idioma" class="form-perfil__select-col02">
                                            <option></option>
                                            @foreach($idiomas as $idioma)
                                                <option value="{{$idioma->id}}">{{$idioma->idioma}}</option>
                                            @endforeach                                            
                                    </select>
                                    {{-- <input type="checkbox" name="obrigatoriedadeIdioma[]" id="desejavel">
                                    <label for="desejavel">Desejável apenas, não mandatório</label> --}}
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label for="">Nível Requerido:</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="radio" name="nivelIdioma1" id="nvidioma10" value="0" checked>
                                            <label for="nvidioma10">Básico</label>
                                            <input type="radio" name="nivelIdioma1" id="nvidioma11" value="1">
                                            <label for="nvidioma11">intermediário</label>
                                            <input type="radio" name="nivelIdioma1" id="nvidioma12" value="2">
                                            <label for="nvidioma12">Avançado</label>
                                        </div>
                                        {{-- <div class="row">
                                            <div class="col-xs-12">
                                                <img src="{{asset('admin/imgs/icons/add16px.png')}}" style="float: right; margin-right: 150px; display: inline;" class="btnAddNovoIdioma btnNovoItem">
                                            </div>    
                                        </div> --}}
                                    </div>                                    
                                </div>
                                <div class="form-perfil__coluna-inputs-row">
                                        <select name="conhecimentoIdioma[]" id="idioma" class="form-perfil__select-col02">
                                                <option></option>
                                                @foreach($idiomas as $idioma)
                                                    <option value="{{$idioma->id}}">{{$idioma->idioma}}</option>
                                                @endforeach                                            
                                        </select>
                                        {{-- <input type="checkbox" name="obrigatoriedadeIdioma[]" id="desejavel">
                                        <label for="desejavel">Desejável apenas, não mandatório</label> --}}
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label for="">Nível Requerido:</label>
                                            </div>
                                            <div class="col-xs-8">
                                                <input type="radio" name="nivelIdioma2" id="nvidioma20" value="0" checked>
                                                <label for="nvidioma20">Básico</label>
                                                <input type="radio" name="nivelIdioma2" id="nvidioma21" value="1">
                                                <label for="nvidioma21">intermediário</label>
                                                <input type="radio" name="nivelIdioma2" id="nvidioma22" value="2">
                                                <label for="nvidioma22">Avançado</label>
                                            </div>
                                            {{-- <div class="row">
                                                <div class="col-xs-12">
                                                    <img src="{{asset('admin/imgs/icons/add16px.png')}}" style="float: right; margin-right: 150px; display: inline;" class="btnAddNovoIdioma btnNovoItem">
                                                </div>    
                                            </div> --}}
                                        </div>                                       
                                </div>
                                <div class="form-perfil__coluna-inputs-row">
                                        <select name="conhecimentoIdioma[]" id="idioma" class="form-perfil__select-col02">
                                                <option></option>
                                                @foreach($idiomas as $idioma)
                                                    <option value="{{$idioma->id}}">{{$idioma->idioma}}</option>
                                                @endforeach                                            
                                        </select>
                                        {{-- <input type="checkbox" name="obrigatoriedadeIdioma[]" id="desejavel">
                                        <label for="desejavel">Desejável apenas, não mandatório</label> --}}
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label for="">Nível Requerido:</label>
                                            </div>
                                            <div class="col-xs-8">
                                                <input type="radio" name="nivelIdioma3" id="nvidioma30" value="0" checked>
                                                <label for="nvidioma30">Básico</label>
                                                <input type="radio" name="nivelIdioma3" id="nvidioma31" value="1">
                                                <label for="nvidioma31">intermediário</label>
                                                <input type="radio" name="nivelIdioma3" id="nvidioma32" value="2">
                                                <label for="nvidioma32">Avançado</label>
                                            </div>
                                            {{-- <div class="row">
                                                <div class="col-xs-12">
                                                    <img src="{{asset('admin/imgs/icons/add16px.png')}}" style="float: right; margin-right: 150px; display: inline;" class="btnAddNovoIdioma btnNovoItem">
                                                </div>    
                                            </div> --}}
                                        </div>                                       
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="disponViagem" class="form-perfil__label ">Disponibilidade Para Viagens?</label>
                            <input type="radio" name="disponViagem" value="1" id="disponSim" checked>
                            <label for="disponSim">Sim</label>
                            <input type="radio" name="disponViagem" value="0" id="disponNao">
                            <label for="disponNao">Não</label>
                        </div>
                        <div class="col-xs-6">
                            <label for="qualFrequencia" class="form-perfil__label">Qual Frequencia</label>
                            <input type="text" name="frequenciaViagem" class="form-perfil__input-col02" id="qualFrequencia" maxlength="26">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <div class="form-perfil__acao">
                                <div class="form-perfil__acao-submit">
                                    <input type="submit" id="acaoSubmit" class=""  value="Prosseguir" onclick="event.preventDefault(); prosseguir('form-2', 'guia-2', 1);" > 
                                </div>
                                <!--  <div class="form-perfil__acao-prosseguir" >
                                    <a href="#" onclick="prosseguir('form-2', 'guia-2')">Prosseguir</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="mensagem">
                        <div class="mensagem__conteudo mensagem__conteudo-bg-cinza">
                            <div class="mensagem__conteudo-texto">
                                <p>Obs.: Não são permitidos anúncios que solicitem comprovação do tempo de experiência ou façam referência a idade, gênero, raça, religião, condições de saúde, sexo, situação familiar, estado de gravidez, opinião política, nacionalidade, origem social ou qualquer outra forma de discriminação, salvo quando a natureza da atividade a ser exercida assim o exigir.</p>
                            </div>
                        </div>
                    </div>
                </div> <!-- DADOS VAGA -->
            </div>
        </div>
        <!-- FORM 2 -->
        <div id="form-2" class="tab form-2" style="display: none">
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <input type="checkbox" name="NaoExibirSalario" id="salrioNao">
                    <label for="salrioNao">Não exibir o salário</label>
                    <input type="checkbox" name="NaoExibirBeneficios" id="beneficiosNao">
                    <label for="beneficiosNao">Não exibir os Benefícios</label>
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-6">
                    <label for="salarioMensal" class="form-perfil__label">Salário Mensal</label>
                    <input type="text" name="salarioMensal" class="form-perfil__input-col02 money" id="salarioMensal" placeholder="R$ 0,00">
                </div>
                <div class="col-xs-6">
                    <label for="faixaSalarial" class="form-perfil__label">Faixa Salarial</label>
                    <input type="text" name="faixaSalarial" class="form-perfil__input-col02 faixasal" id="faixaSalarial" placeholder="R$ 0,00 a R$ 0,00" maxlength="50">
                </div>
                <div class="divSalario" style="color: red"></div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="comissao" class="form-perfil__label">Comissão</label>
                    <input type="text" class="form-perfil__input" name="comissao" id="comissao" placeholder="Descreva o pacote completo de benefícios.">
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="beneficios" class="form-perfil__label">Benefícios</label>
                    <input type="text" class="form-perfil__input validate validatePage2" name="beneficios" id="beneficios" placeholder="Descreva o pacote de benefícios.">
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="regContr" class="form-perfil__label">Regime de Contratação</label>
                    <select name="regContr" class="form-perfil__input" id="regContr">
                        @foreach($regimes as $regime)
                            <option value="{{$regime->id}}">{{mb_strtoupper(urldecode($regime->regime))}}</option>
                        @endforeach                        
                    </select>
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="horarioTrab" class="form-perfil__label">Horário de Trabalho</label>
                    <input type="text" class="form-perfil__input validate validatePage2" name="horarioTrab" id="horarioTrab" placeholder="Ex.: Segunda a Quinta-feira das 08h as 18h | Sexta-feira das 08h as 17h" maxlength="50">
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="infoAdicionaisVagas" class="form-perfil__label">Informações Adicionais | Vaga</label>
                    <input type="text" name="infoAdicionaisVagas" class="form-perfil__input" id="infoAdicionaisVagas" placeholder="Ex. Possuir veiculo próprio, carteira propria de clientes, etc.">
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-6">
                    <label for="candEstados" class="form-perfil__label">Candidatos de Outros Estados?</label>
                    <input type="radio" name="candEstados" value="1" id="candestadosim">
                    <label for="candestadosim">Sim</label>
                    <input type="radio" name="candEstados" id="candestadonao" value="0" checked>
                    <label for="candestadonao">Não</label>
                </div>
                <div class="col-xs-6">
                    <label for="estRegi" class="form-perfil__label">Quais Estados ou Regiões?</label>
                    <input type="text" class="form-perfil__input-col02" name="estadoseRegioes" id="estRegi">
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="auxMudanca" class="form-perfil__label">Auxílio Mudança ou Moradia?</label>
                    <input type="text" class="form-perfil__input" name="auxMudanca" id="auxMudanca" placeholder="Se houver, especificar a política e/ou valores do auxílio.">
                </div>
            </div>
                <div class="mensagem"></div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <div class="form-perfil__acao">
                        <div class="form-perfil__acao-submit">
                            <input type="submit" id="acaoSubmit" value="Prosseguir"  onclick="event.preventDefault(); prosseguir('form-3', 'guia-3', 2);">
                        </div>
                        <!--
                        <div class="form-perfil__acao-prosseguir">
                                    <a href="#" onclick="prosseguir('form-3', 'guia-3')">Prosseguir</a>
                        </div>
                        -->
                    </div>
                </div>
            </div>
        </div>
    
        <!-- FORM 3 -->
        <div id="form-3" class="tab form-3" style="display: none">
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="nomeCompleto" class="form-perfil__label">Data Limite Envio de CV</label>
                    <input type="radio" name="dataLimite" id="dias5" value="5">
                    <label for="dias5">Até 5 dias (URGENTE)</label>
                    <input type="radio" name="dataLimite" id="dias10" value="10">
                    <label for="dias10">Até 10 dias</label>
                    <input type="radio" name="dataLimite" id="dias15" value="15">
                    <label for="dias15">Até 15 dias</label>
                    <input type="radio" name="dataLimite" id="dias20" value="20" checked>
                    <label for="dias20">Até 20 dias</label>
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <div class="form-perfil__coluna-label form-perfil__coluna-label-h-240px">
                        <span>Etapas do Processo Seletivo</span>
                    </div>
                    <div class="form-perfil__coluna-inputs">
                        <div class="form-perfil__coluna-inputs-row">
                            <div class="form__descricao">
                                <p>Favor descrever as etapas do processo seletivo na sua empresa. Exemplo: Entrevista com RH, Entrevista com Diretoria, Prova Pratica, etc</p>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($etapas as $etapa)
                                <div class="col-xs-2">         
                                <input type="checkbox" id="etapa{{$etapa->id}}" value="{{$etapa->id}}" name="etapasProcesso[]">
                                    <label for="etapa{{$etapa->id}}"><small>{{urldecode($etapa->etapa)}}</small></label>
                                </div> 
                            @endforeach                                    
                        </div>
                            <div class="form-perfil__coluna-inputs-row">
                                <label for="outros" class="form-perfil__label">Outros</label>
                                <input type="text" name="etapasProcessoOutros" class="form-perfil__input-col03" id="outros" maxlength="100" placeholder="Especificar etapas.">
                            </div>
                    </div>
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="feeRecrut" class="form-perfil__label">FEE de Recrutamento</label>
                    <select name="feeRecruts" class="form-perfil__input" id="feeRecrut">
                        @foreach($fee as $fee)
                            <option value="{{$fee->id}}">{{$fee->fee}} DO SALÁRIO DE CONTRATAÇÃO DO CANDIDATO</option>
                       @endforeach
                    </select>
                </div>
            </div>
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="limitCV" class="form-perfil__label">Limitação de CV HeadHunters</label>
                    <select class="form-perfil__select-col02" name="limitCV" style="width: 390px">
                        
                        @for ($i=1; $i <= 10 ; $i++)
                        
                            <option value="{{$i}}">{{$i}}</option>
                            
                        @endfor
                    </select>
                </div>
            </div>     
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="documentosRequeridos" class="form-perfil__label">Documentos Requeridos</label>
                    <input type="text" maxlength="255" name="documentosRequeridos" class="form-perfil__input" id="documentosRequeridos">
                </div>
            </div>  
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="responsavelRecrutamento" class="form-perfil__label">Responsável pelo recrutamento</label>
                    <select type="text" style="width: 46%" name="responsavelRecrutamento" class="form-perfil__select-col02" id="responsavelRecrutamento">
                        <option value="0" selected>Não selecionar um responsável</option>
                        @if($users)
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->nome}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div> 
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <label for="comentariosRecomendacoes" class="form-perfil__label">Comentários / Recomendações</label>
                    <input type="text" maxlength="300" name="comentariosRecomendacoes" class="form-perfil__input" id="comentariosRecomendacoes">
                </div>
            </div>
            <div class="mensagem"></div>                              
            <div class="row form-perfil__row">
                <div class="col-xs-12">
                    <div class="form-perfil__acao">
                        <div class="form-perfil__acao-submit">
                            <input type="submit" id="acaoSubmit" name="salvarVaga" class="salvarVaga" value="Salvar">
                        </div>
                    </div>
                </div>
            </div>                
        </div>
    </div>
        </form>
        </div>
</section>

</div>
@endsection