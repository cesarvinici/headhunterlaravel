@foreach($candidatos as $candidato)
<tr class="resultadoAnexosCddPartc"><td></td></tr>
<tr class="tr-row-superior">
    <td class="cell-td-titulo" rowspan="2">
        <span class="nome">{{$candidato->nome}}</span><br>
        <span class="data">{{MysqlToData($candidato->updated_at)}}</span>
    </td>
    <td class="cell-td-acao"><span>Editar</span></td>
    <td class="cell-td-acao"><span>Ativar</span></td>
    <td class="cell-td-acao"><span>Solicitar</span></td>
    <td class="cell-td-acao"><span>Anexar</span></td>
    <td class="cell-td-acao"><span>Solicitar</span></td>
    <td class="cell-td-acao"><span>Anexar</span></td>
    <td class="cell-td-acao"><span>Solicitar</span></td>
    <td class="cell-td-acao"><span>Upload</span></td>
</tr>
<tr class="tr-row-inferior">
    <td class="cell-td-acao">Solicitar</td>
    <td class="cell-td-acao"><i class="fa fa-upload"></i></td>
    <td class="cell-td-acao"><i class="fa fa-list-ul"></i></td>
    <td class="cell-td-acao"><i class="fa fa-bar-chart"></i></td>
    <td class="cell-td-acao"><i class="fa fa-file-text-o"></i></td>
    <td class="cell-td-acao"><i class="fa fa-upload"></i></td>
    <td class="cell-td-acao"><i class="fa fa-file-text-o"></i></td>
    <td class="cell-td-acao"><i class="fa fa-upload"></i></td>
</tr>
@endforeach