@extends('layouts.headhunter')
@section('title', 'Cadastro')
@section('content')

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">CADASTROS</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>

    <div class="row">
		<div class="col-lg-12">
            <!-- <div class="col-lg-6">
                <a href="./meus-dados"><h1 class="page-header"><i class="fa fa-user fa-fw"></i> Minha Conta</h1></a>
            </div>
            <div class="col-lg-6">
                <a href="#"><h1 class="page-header"><i class="fa fa-user fa-fw"></i> Cliente</h1></a>
            </div>
            <div class="col-lg-6">
                <a href="#"><h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> Vagas</h1></a>
            </div>
            <div class="col-lg-6">
                <a href="#"><h1 class="page-header"><i class="fa fa-users fa-fw"></i> Candidatos</h1></a>
            </div> -->



                <div class="col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            MINHA CONTA
                        </div>
                        <div class="panel-body">
                            <p>                <a href="./meus-dados"><h1 class="page-header"><i class="fa fa-user fa-fw"></i> Minha Conta</h1></a>
</p>
                        </div>
                    </div>
                </div>

				<div class="col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            CLIENTE
                        </div>
                        <div class="panel-body">
                            <p>                <a href="#"><h1 class="page-header"><i class="fa fa-user fa-fw"></i> Cliente</h1></a>
</p>
                        </div>
                    </div>
                </div>

				<div class="col-lg-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							VAGAS
						</div>
						<div class="panel-body">
							<p>                <a href="#"><h1 class="page-header"><i class="fa fa-briefcase fa-fw"></i> Vagas</h1></a>
</p>
						</div>

					</div>
				</div>

				<div class="col-lg-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							CANDIDATOS
						</div>
						<div class="panel-body">
							<p><a href="#"><h1 class="page-header"><i class="fa fa-users fa-fw"></i> Candidatos</h1></a></p>
						</div>
					</div>
				</div>
		</div>
		<!-- /.col-lg-12 -->
	</div>
</div>

@endsection
