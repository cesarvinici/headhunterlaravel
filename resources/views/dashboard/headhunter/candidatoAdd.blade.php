@extends('layouts.headhunterHeader')
@section('title', 'Adiciona Candidato')
@section('content')
<div class="col-xs-9">
	<section class="conteudo">
		<div class="conteudo-header">
				@if(Session::has('cad_cdd_ok'))
				<div class="alert alert-success">
					<ul>
						<li>
							{{Session::get('cad_cdd_ok')}}
						</li>
					</ul>			
				</div>
				@endif
				@if(Session::has('cad_cdd_fail'))
					<div class="alert alert-danger">
						<ul>
							<li>{{Session::get('cad_cdd_fail')}}</li>
						</ul>
						
					</div>
				@endif
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			<div class="row">
				<div class="col-xs-9">
					<div class="conteudo-header__titulo">
						<h2>Cadastrar Candidato</h2>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="conteudo-header__voltar-icone">ICN</div>
					<div class="conteudo-header__voltar">
						<div class="conteudo-header__voltar-texto btn-voltar-cad">Voltar</div>
					</div>
				</div>
			</div>
		</div>
		<div class="conteudo-painel">
			<form class="form-perfil" action="" method="post" id="form-candidato" enctype="multipart/form-data">
				@csrf
				<div class="form-perfil__conteudo">
					<div class="row form-perfil__row">
						<div class="col-xs-12">
							<input type="checkbox" name="pcd" id="cddPCD" {{old('cddPCD') == 'on' ? 'checked' : ''}} >
							<label for="cddPCD">CDD PCD</label>
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-12">

							<label for="nomeCompleto" class="form-perfil__label ">Nome Completo</label>
							<input type="text" value="{{old('nome')}}" required name="nome" class="form-perfil__input " id="nomeCompleto">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							
							<label for="cpf" class="form-perfil__label">CPF</label>
							<input type="text" required value="{{old('cpf')}}" name="cpf" class="form-perfil__input-col02 " id="cpf">
						</div>
						<div class="col-xs-6">
							<label for="email" class="form-perfil__label">E-mail</label>
							<input type="email" value="{{old('email')}}" name="email" required class="form-perfil__input-col02 " id="email">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="telefone" class="form-perfil__label">Telefone</label>
							<input type="tel" value="{{old('telefone')}}" name="telefone" class="form-perfil__input-col02" id="telefone">
						</div>
						<div class="col-xs-6">
							<label for="celular01" class="form-perfil__label">Celular</label>
							<input type="tel" value="{{old('celular')}}" class="form-perfil__input-col02" name="celular" id="celular01">
						</div>

					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="estadoCivil" class="form-perfil__label">Estado Civil</label>
							<select name="estadoCivil" id="estadoCivil" id="estadoCivil" class="form-perfil__select-col02">
								<option disabled selected value="">Estado Civil</option>
								@foreach(estadoCivil() as $estado)
									@php $selected = old('estadoCivil') == $estado->id ? 'selected' : '' @endphp								   
                                   <option {{$selected}} value="{{$estado->id}}">{{$estado->estado_civil}}</option>
                                @endforeach
                            </select>
						</div>
						<div class="col-xs-6">
							<label for="nascimento" class="form-perfil__label">Data Nascimento</label>
							<input type="text" value="{{old('nascimento')}}" name="nascimento" required placeholder="dd/mm/aaaa" class="form-perfil__input-col02 " id="nascimento">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="cep" class="form-perfil__label">CEP*</label>
							<input type="text" value="{{old('cep')}}" name="cep" class="form-perfil__input-col02" id="cep">
						</div>
						<div class="col-xs-6">
							<label for="endereco" class="form-perfil__label">Endereço*</label>
							<input type="text" value="{{old('endereco')}}" class="form-perfil__input-col02" name="endereco" id="endereco">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="estado" class="form-perfil__label">Estado</label>
							<select name="estado" id="estado" required onchange="carregaCidades(this, 1)" class="form-perfil__select-col02 listaEstados">
								<option selected>Selecione um Estado</option>
								@foreach($estados as $estado)
								@php $selected = $estado->UF_UF == old('estado') ? 'selected' : '' @endphp
									<option {{$selected}} value="{{$estado->UF_UF}}">{{$estado->UF_NOME}}</option>
                                @endforeach
							</select>
						</div>
						<div class="col-xs-6">
							<label for="cidade" class="form-perfil__label">Cidade*</label>
							<select name="cidade" class="form-perfil__select-col02 " id="listaCidades">
								<option value="" selected >...</option>								
							</select>								
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="facebook" class="form-perfil__label">Facebook</label>
							<input type="text" value="{{old('instagram')}}" name="instagram" class="form-perfil__input-col02" id="facebook">
						</div>
						<div class="col-xs-6">
							<label for="linkedin" class="form-perfil__label">Linkedin</label>
							<input type="text" value="{{old('twitter')}}" name="twitter" class="form-perfil__input-col02" id="linkedin">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-4">
							<div class="form-perfil__img">
								<img src="" alt="">
							</div>
							<label>Curriculo</label>
							<br>
							<input type="file" name="curriculo" id="curriculo">
						</div>
						<div class="col-xs-4">
							<div class="form-perfil__img">
								<img src="" alt="">
							</div>
							<label>Imagem do Perfil</label>
							<br>
							<input type="file" name="imgPerfil" id="docs">
						</div>
						<div class="col-xs-4">
							<div class="form-perfil__img">
								<img src="" alt="">
							</div>
							<label>Relatorios</label>
							<br>
							<input type="file" name="relatorios[]" multiple="" id="relatorios">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-12">
							<label for="objetivo" class="form-perfil__label">Objetivo</label>
							<input type="text" value="{{old('objetivo')}}" maxlength="500" name="objetivo" class="form-perfil__input" id="objetivo">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-12">
							<label for="resuQuali" class="form-perfil__label">Resumo das Qualidades</label>
							<textarea name="resumoQualidades" maxlength="2000" class="form-perfil__input" id="resuQuali">{{old('resumoQualidades')}}</textarea>
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="trabAtual" class="form-perfil__label">Trabalhando Atualmente</label>
							<input type="radio" name="trabAtual" {{old('trabAtual') ? 'checked' : ''}} value="1" id="trabAtualsim"><label for="trabAtualsim">Sim</label>
							<input type="radio" name="trabAtual" {{!old('trabAtual') ? 'checked' : ''}} checked value="0" id="trabAtualnao"><label for="trabAtualnao">Nao</label>
						</div>
						<div class="col-xs-6">
							<label for="" class="form-perfil__label">Disponibilidade de Contratação</label>
							<select name="dispContratacao" class="form-perfil__input-col02">
								@foreach(dispContratacao() as $disp)
									@php $selected = $disp->id == old('dispContratacao') ? 'selected' : '' @endphp
									<option {{$selected}} value="{{$disp->id}}">{{$disp->disponibilidade}}</option>
								@endforeach
							</select>
						</div>
					</div>
					
					<div class="row form-perfil__row">
						<div class="col-xs-12">
							<div class="form-perfil__coluna-label form-perfil__coluna-label-h-305px">
								<span>Formação</span>
							</div>
							<div class="form-perfil__coluna-inputs">
								<div class="form-perfil__coluna-inputs-row">
									<label for="instituicao" class="form-perfil__label">Instituição</label>
									<input type="text" value="{{old('instituicao')}}" name="instituicao" class="form-perfil__input-col03" id="instituicao">
								</div>
								<div class="form-perfil__coluna-inputs-row">
									<label for="curso" class="form-perfil__label">Curso</label>
									<input type="text" value="{{old('curso')}}" name="curso" class="form-perfil__input-col03" id="curso">
								</div>
								<div class="form-perfil__coluna-inputs-row">
									<label for="periodo" class="form-perfil__label">Período/Duração</label>
									<input type="text" value="{{old('duracao')}}" name="duracao" class="form-perfil__input-col03" id="periodo">
								</div>
								<div class="form-perfil__coluna-inputs-row">
									@foreach($formacoes as $formacao)
										@php $checked = old('nivel') == $formacao->id ? 'checked' : ''  @endphp
										<input type="radio" {{$checked}} name="nivel" value="{{$formacao->id}}" id="formacao{{$formacao->id}}"><label for="formacao{{$formacao->id}}">{{urldecode($formacao->nivel)}}</label> 
									@endforeach														
								</div>
								<div class="form-perfil__coluna-inputs-row">
									<label for="status" class="form-perfil__label">Status</label>
									@foreach($statusForm as $status)
										@php $checked = old('status') == $status->id ? 'checked' : ''  @endphp
										<input {{$checked}} type="radio" name="status" value="{{$status->id}}" id="status{{$status->id}}"><label for="status{{$status->id}}">{{urldecode($status->f_status)}}</label>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<div class="div-idiomas">
						@for($i = 0, $j=1; $i < 3; $i++, $j++)
							<div class="row form-perfil__row">
								<div class="col-xs-6">
									<label for="idioma" class="form-perfil__label">Conhecimento de outro idioma?</label>
									<select name="idioma[]" id="idioma" class="form-perfil__input-col02">
											<option></option>
											@foreach($idiomas as $idioma)
											@php $selected = old('idioma.'.$i) == $idioma->id ? 'selected' : '' @endphp										
												<option {{$selected}} value="{{$idioma->id}}">{{$idioma->idioma}}</option>
											@endforeach												
									</select>
								</div>
								<div class="col-xs-6">
									
								    <input type="radio" {{old('idiomaNivel'.$j) == 0 ? 'checked' : ''}} name="idiomaNivel{{$j}}" id="basico{{$j}}1" value="0" > <label for="basico{{$j}}1">Básico</label>
									<input type="radio" {{old('idiomaNivel'.$j) == 1 ? 'checked' : ''}} name="idiomaNivel{{$j}}" id="intermediario{{$j}}2" value="1"><label for="intermediario{{$j}}2">Intermediário</label>
									<input type="radio" {{old('idiomaNivel'.$j) == 2 ? 'checked' : ''}} name="idiomaNivel{{$j}}" id="avancado{{$j}}3" value="2" ><label for="avancado{{$j}}3">Avançado</label>
								</div>
								{{-- <div class="row">
									<div class="col-xs-12">
										<img src="../assets/imgs/icons/add16px.png" style="float: right; margin-right: 150px; display: inline;" class="btn-idioma-candidato btnNovoItem">
										<img src="../assets/imgs/icons/delete-button.png" style="float: right; margin-right: 5px; display: inline;" id="" class="btn-delete-idioma-cdd pointer">
									</div>    
								</div> --}}
							</div>
						@endfor
					</div>
					@for($i=0; $i < 3; $i++)
                            <div class="row form-perfil__row">
                                <div class="col-xs-12">
                                    <div class="form-perfil__coluna-label form-perfil__coluna-label-h-245px">
                                        @if($i == 0) <span>Última Experiência</span>
                                        @else <span>Experiência Anterior</span>
                                        @endif
                                    </div>
                                    <div class="form-perfil__coluna-inputs">
                                        <div class="form-perfil__coluna-inputs-row">
                                            <label for="empresa" class="form-perfil__label">Empresa</label>
                                            <input type="text" name="empresa[]" value="{{old('empresa.'.$i)}}" class="form-perfil__input-col03" id="empresa">
                                        </div>
                                        <div class="form-perfil__coluna-inputs-row">
                                            <label for="cargo" class="form-perfil__label">Cargo</label>
                                            <input type="text" autocomplete="off" name="cargo[]" value="{{old('cargo.'.$i)}}" class="form-perfil__input-col03 tituloCargo" id="cargo">
                                        	<input type="hidden" class="idcargo" name="idCargo[]" value="{{old('idCargo.'.$i)}}" id="">
                                            <p style="color: red" class="empty-message"></p>
                                        </div>
                                        <div class="form-perfil__coluna-inputs-row">
                                            <label for="descAtiv" class="form-perfil__label">Descrição de Atividades/Funções</label>
										<textarea name="atividades[]" maxlength="2000" class="form-perfil__input-col03" id="descAtiv">{{old('atividades.'.$i)}}</textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="dataAdmi" class="form-perfil__label">Data Admissão</label>
                                                <input type="text" name="admissao[]" value="{{old('admissao.'.$i)}}" placeholder="dd/mm/aaaa" class="form-perfil__input-col04 data-adm data" id="dataAdmi">
                                            </div>
                                            <div class="col-xs-6">
                                                <label for="dataDesliult" class="form-perfil__label">Data Desligamento</label>
                                                <input type="text" placeholder="dd/mm/aaaa" name="desligamento[]" value="{{old('desligamento.'.$i)}}" class="form-perfil__input-col04 datadeslig data" id="dataDesliult">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endfor

					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="pretSalarial" class="form-perfil__label ">Pretensão Salarial</label>
							<input type="text" value="{{old('pretensaoSalarial')}}" required name="pretensaoSalarial" class="form-perfil__input-col02 money " id="pretSalarial">
						</div>
						<div class="col-xs-6">
							<label for="nivelHierar" class="form-perfil__label">Nível de Hierarquia</label>
							<select name="nivelHierar" class="form-perfil__input-col02" id="nivelHierar">
								@foreach($nivelHierarquico as $nivel)
									@php $selected = $nivel->id == old('nivelHierar') ? 'selected' : '' @endphp
									<option {{$selected}} value="{{$nivel->id}}">{{$nivel->nivel}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="disViag" class="form-perfil__label">Disponibilidade Para Viagens?</label>
							<input type="radio" name="disViag" {{old('disViag') ? 'checked' : ''}} value="1" id="disViagsim"><label for="disViagsim">Sim</label> 
							<input type="radio" name="disViag" {{!old('disViag') ? 'checked' : ''}} value="0" id="disViagnao"><label for="disViagnao">Não</label>
						</div>
						<div class="col-xs-6">
							<label for="qualFreq" class="form-perfil__label">Qual a Frequência</label>
							<input type="text" name="qualFreq" value="{{old('qualFreq')}}" class="form-perfil__input-col02" id="qualFreq">
						</div>
					</div>					
					<div class="row form-perfil__row">							
						@foreach($publicidade as $publi)
							@php $checked = old('cv') == $publi->id ? 'checked' : '' @endphp
							<div class="col-xs-4">
								<label for="publ{{$publi->id}}">{{urldecode($publi->publicidade)}}</label>
								<input type="radio" name="cv" {{$checked}} id="publ{{$publi->id}}" value="{{$publi->id}}">
								<label for="publ{{$publi->id}}"><p>
									{{urldecode($publi->descricao)}}
								</p></label>
							</div>
						@endforeach
        			</div>
					<div class="mensagem">						
					</div>
					<div class="row form-perfil__row">
						<div class="form-perfil__acao">
							<div class="col-xs-6">
								<div class="form-perfil__acao-submit">
									<input type="submit" name="salvarCandidato" value="Salvar">
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
</div>
@endsection
@section('scripts')
	<script>
		window.onload = function()
		{
			if($("#cep").val())
			{
				$("#cep").trigger('blur');
			}
		}
	</script>
@endsection