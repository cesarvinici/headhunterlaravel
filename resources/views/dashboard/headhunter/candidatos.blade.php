@extends('layouts.headhunterHeader')
@section('title', 'Manutenção Candidatos')
@section('content')
<div class="col-xs-9">
    <div style="margin-top:10px">
        @if(Session::has('cad_cdd_ok'))
            <div class="alert alert-success">
                <ul>
                    <li>
                        {{Session::get('cad_cdd_ok')}}
                    </li>
                </ul>			
            </div>
        @endif
        @if(Session::has('cad_cdd_fail'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{Session::get('cad_cdd_fail')}}</li>
                </ul>
                
            </div>
        @endif
    </div>
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Candidatos Cadastrados</h2>
                    </div>
                </div>
                <div class="col-xs-3" style="display: inline;">
                    <div class="conteudo-header__voltar">
                        <div class="conteudo-header__voltar-icone">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto btn-voltar-cad">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <div class="usuarios">
                <div class="usuarios__tabela">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th colspan="2" style="width:10%;">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($candidatos as $candidato)
                                    <tr>
                                        <td>{{$candidato->nome}}</td>
                                        <td>{{$candidato->email}}</td>
                                        <td class="usuarios__tabela-editar"><a href="/dashboard/headhunter/candidatos/{{$candidato->id}}/editar"><i class="fa fa-edit"></i></a></td>
                                        <td class="usuarios__tabela-fechar" onclick="excluirCandidato({{$candidato->id}})"><i class="fa fa-close"></i></td>
                                    </tr>     
                                @endforeach                                                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection