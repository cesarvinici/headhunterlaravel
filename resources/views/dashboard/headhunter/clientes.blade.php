@extends('layouts.headhunterHeader')
@section('title', 'Consulta Clientes')
@section('content')
<div class="col-xs-9">
		
	<section class="conteudo">
		@if(Session::has('empresa_excluir_ok'))
			<div class="alert alert-warning">
				<ul>
					<li>
						{{Session::get('empresa_excluir_ok')}}
					</li>
				</ul>				
			</div>
		@endif
		<div class="conteudo-header">
			<div class="row">
				<div class="col-xs-9">
					<div class="conteudo-header__titulo">
						<h2>Cliente - Consulta</h2>
					</div>
				</div>
				<div class="col-xs-3 btn-voltar-cad">
					<div class="conteudo-header__voltar">
						<div class="conteudo-header__voltar-icone">
							<div class="icn-header-back-p-branco"></div>
						</div>
						<div class="conteudo-header__voltar-texto">Voltar</div>
					</div>
				</div>
			</div>
		</div>
		<div class="conteudo-painel">
			<div class="consulta">
				<div class="consulta__filtro">
					<form method="post" id="filtro-clientes">
						@csrf
						<div class="convite__secao-search">
							<div class="consulta__filtro-row">
								<div class="col-xs-6">
									<div class="row">
										<input type="text" name="nomeFantasia" placeholder="Nome Fantasia">
										<button type="submit" class="filtroCliente"><i class="fa fa-search"></i></button>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="row">
										<input type="text" name="razaoSocial"  placeholder="Razão Social">
										<button type="submit" class="filtroCliente"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
							<div class="consulta__filtro-row">
								<div class="col-xs-6">
									<div class="row">
										<input type="text" name="segmento" placeholder="Segmento de Atuação">
										<button type="submit" class="filtroCliente"><i class="fa fa-search"></i></button>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="row">
										<input type="text" name="cnpj" id="cnpj" placeholder="CNPJ">
										<button type="submit" class="filtroCliente"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
							<div class="consulta__filtro-row">
								<div class="col-xs-6">
									<div class="row">
										<input type="text" name="cidade" placeholder="Cidade">
										<button type="submit" class="filtroCliente"><i class="fa fa-search"></i></button>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="row">
										<input type="text" placeholder="Código" name="cod"> 
										<button type="submit" class="filtroCliente"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
						</div>
					</form>
						<div class="listaClientes">
						@include('dashboard.headhunter.listaClientes')
					</div>
					
				</div>
			</div>
		</div>
	</section>
</div>
@endsection