@extends('layouts.headhunterHeader')
@section('title', 'Adiciona Candidato')
@section('content')
<div class="col-xs-9">

	<div style="margin-top:10px">
		@if(Session::has('cad_cdd_ok'))
		<div class="alert alert-success">
			<ul>
				<li>
					{{Session::get('cad_cdd_ok')}}
				</li>
			</ul>			
		</div>
		@endif
		@if(Session::has('cad_cdd_fail'))
			<div class="alert alert-danger">
				<ul>
					<li>{{Session::get('cad_cdd_fail')}}</li>
				</ul>
				
			</div>
		@endif
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
	
	<section class="conteudo">
		<div class="conteudo-header">
			<div class="row">
				<div class="col-xs-9">
					<div class="conteudo-header__titulo">
						<h2>Cadastrar Candidato</h2>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="conteudo-header__voltar-icone">ICN</div>
					<div class="conteudo-header__voltar">
						<div class="conteudo-header__voltar-texto btn-voltar-cad">Voltar</div>
					</div>
				</div>
			</div>
		</div>
		<div class="conteudo-painel">
            <form class="form-perfil" action="/dashboard/headhunter/candidatos/{{$candidato->id}}/editar" method="post" id="form-candidato" enctype="multipart/form-data">
				@csrf
				<div class="form-perfil__conteudo">
					<div class="row form-perfil__row">
						<div class="col-xs-12">
                            <input type="checkbox" name="pcd" id="cddPCD" {{$candidato->pcd ? 'checked' : ''}}>
							<label for="cddPCD">CDD PCD</label>
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-12">

							<label for="nomeCompleto" class="form-perfil__label validate">Nome Completo</label>
							<input type="text" name="nome" value="{{$candidato->nome}}" class="form-perfil__input validate" id="nomeCompleto">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							
							<label for="cpf" class="form-perfil__label">CPF</label>
                            <input type="text" value="{{$candidato->cpf}}" name="cpf" class="form-perfil__input-col02 validate" id="cpf">
						</div>
						<div class="col-xs-6">
							<label for="email" class="form-perfil__label">E-mail</label>
							<input type="email" value="{{$candidato->email}}" name="email" class="form-perfil__input-col02 validate" id="email">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="telefone" class="form-perfil__label">Telefone</label>
							<input type="tel" value="{{$candidato->telefone}}" name="telefone" class="form-perfil__input-col02" id="telefone">
						</div>
						<div class="col-xs-6">
							<label for="celular01" class="form-perfil__label">Celular</label>
							<input type="tel" value="{{$candidato->celular}}" class="form-perfil__input-col02" name="celular" id="celular01">
						</div>

					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="estadoCivil" class="form-perfil__label">Estado Civil</label>
							<select name="estadoCivil" id="estadoCivil" id="estadoCivil" class="form-perfil__select-col02">
								@foreach(estadoCivil() as $estado)
                                    {{$selected = $estado->id == $candidato->estado_civil ? 'selected' : ''}}
										<option {{$selected}} value="{{$estado->id}}">{{$estado->estado_civil}}</option>
                                @endforeach
							</select>
						</div>
						<div class="col-xs-6">
							<label for="nascimento" class="form-perfil__label">Data Nascimento</label>
							<input type="text" value="{{date('d/m/Y', strtotime($candidato->nascimento))}}" name="nascimento" placeholder="dd/mm/aaaa" class="form-perfil__input-col02 validate" id="nascimento">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="cep" class="form-perfil__label">CEP*</label>
							<input type="text" value="{{$candidato->cep}}" name="cep" class="form-perfil__input-col02" id="cep">
						</div>
						<div class="col-xs-6">
							<label for="endereco" class="form-perfil__label">Endereço*</label>
							<input type="text" value="{{$candidato->endereco}}" class="form-perfil__input-col02" name="endereco" id="endereco">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="estado" class="form-perfil__label">Estado</label>
							<select name="estado" id="estado" onchange="carregaCidades(this, 1)" class="form-perfil__select-col02 listaEstados">
								<option selected>Selecione um Estado</option>
                                @foreach($estados as $estado)
                                    {{$selected = $candidato->CT_UF == $estado->UF_ID ? 'selected' : ''}}
										<option {{$selected}} value="{{$estado->UF_UF}}">{{$estado->UF_NOME}}</option>
                                @endforeach
							</select>
						</div>
						<div class="col-xs-6">
							<label for="cidade" class="form-perfil__label">Cidade*</label>
							<select name="cidade" class="form-perfil__select-col02 validate" id="listaCidades">
                                @foreach($cidades as $cidade)
                                    {{$selected = $cidade->CT_ID == $candidato->cidade ? 'selected' : ''}}
                                    <option {{$selected}} value="{{$cidade->CT_ID}}">{{mb_strtoupper(urldecode($cidade->CT_NOME))}}</option>
                                @endforeach                               								
							</select>								
                        </div>
                    </div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="facebook" class="form-perfil__label">Facebook</label>
                            <input type="text" value="{{$candidato->instagram}}" name="facebook" class="form-perfil__input-col02" id="instagram">
						</div>
						<div class="col-xs-6">
							<label for="linkedin" class="form-perfil__label">Linkedin</label>
							<input type="text" value="{{$candidato->twitter}}" name="twitter" class="form-perfil__input-col02" id="likedin">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-4">
							<div class="form-perfil__img">
								<img src="" alt="">
							</div>
							<label>Curriculo</label>
							<!-- <span><a href="#">Adicionar</a></span> | <span><a href="#">Alterar</a></span> --><br>
							@if(!empty($candidato->curriculum))
							<a target="__BLANK" href="/admin/curriculos/candidatos/{{$candidato->curriculum}}">Baixar</a>
							@endif
							<input type="file" name="curriculo" id="curriculo">
						</div>
						<div class="col-xs-4">
							@if($candidato->foto_perfil)
								<div class="form-perfil__img" style="">
									<img src="{{asset('admin/upload/candidatos/'.$candidato->foto_perfil)}}" class="img-thumbnail" style="height: 200px; width: 200px" alt="">
								</div>
							@endif							
							<label>Imagem do Perfil</label>
							<!-- <span><a href="#">Adicionar</a></span> | <span><a href="#">Alterar</a></span> --><br>
							<input type="file" name="imgPerfil" id="docs">
						</div>
						<div class="col-xs-4">
							<div class="form-perfil__img">
								<img src="" alt="">
							</div>
							<label>Relatorios</label>
							@if(sizeof($relatorios))
								@foreach($relatorios as $relatorio)
									<br><a target="__BLANK" href="{{asset('admin/upload/candidatos/relatorios/'.$relatorio->relatorio)}}">Baixar</a>
								@endforeach
							@endif
							<!-- <span><a href="#">Adicionar</a></span> | <span><a href="#">Alterar</a></span> --><br>
							<input type="file" name="relatorios[]" multiple="" id="relatorios">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-12">
							<label for="objetivo" class="form-perfil__label">Objetivo</label>
							<input type="text" maxlength="500" value="{{$candidato->objetivo}}" name="objetivo" class="form-perfil__input" id="objetivo">
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-12">
							<label for="resuQuali" class="form-perfil__label">Resumo das Qualidades</label>
							<textarea name="resumoQualidades" maxlength="2000" class="form-perfil__input" id="resuQuali">{{$candidato->resumo_qualidades}}</textarea>
						</div>
					</div>
					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="trabAtual" class="form-perfil__label">Trabalhando Atualmente</label>
							<input type="radio" name="trabAtual" value="1" id="trabAtualsim" {{$candidato->trabalha_atualmente ? 'checked' : ''}} ><label for="trabAtualsim">Sim</label>
							<input type="radio" name="trabAtual" value="0" id="trabAtualnao" {{!$candidato->trabalha_atualmente ? 'checked' : ''}} ><label for="trabAtualnao">Nao</label>
						</div>
						<div class="col-xs-6">
							<label for="" class="form-perfil__label">Disponibilidade de Contratação</label>
							<select name="dispContratacao" class="form-perfil__input-col02">
								@foreach(dispContratacao() as $disp)
									@php $selected = $disp->id == $candidato->disponivel_contratacao ? 'selected' : '' @endphp
									<option {{$selected}} value="{{$disp->id}}">{{$disp->disponibilidade}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<!-- <div class="row form-perfil__row">
						<div class="col-xs-12">
							<label for="" class="form-perfil__label">Vaga apresentada ao candidato em:</label>
							<input type="date" class="form-perfil__input" id="">
						</div>
					</div> -->
					<div class="row form-perfil__row">
						<div class="col-xs-12">
							<div class="form-perfil__coluna-label form-perfil__coluna-label-h-305px">
								<span>Formação</span>
							</div>
							<div class="form-perfil__coluna-inputs">
								<div class="form-perfil__coluna-inputs-row">
									<label for="instituicao" class="form-perfil__label">Instituição</label>
                                    <input type="text" value="{{$candidato->instituicao}}" name="instituicao" class="form-perfil__input-col03" id="instituicao">
								</div>
								<div class="form-perfil__coluna-inputs-row">
									<label for="curso" class="form-perfil__label">Curso</label>
                                    <input type="text" name="curso" value="{{$candidato->curso}}" class="form-perfil__input-col03" id="curso">
								</div>
								<div class="form-perfil__coluna-inputs-row">
									<label for="periodo" class="form-perfil__label">Período/Duração</label>
                                    <input type="text" value="{{$candidato->duracao}}" name="duracao" class="form-perfil__input-col03" id="periodo">
								</div>
								<div class="form-perfil__coluna-inputs-row">
                                    @foreach($formacoes as $formacao)
                                        @php $checked = $formacao->id == $candidato->escolaridade ? 'checked' : '' @endphp
										<input {{$checked}} type="radio" name="nivel" value="{{$formacao->id}}" id="formacao{{$formacao->id}}"><label for="formacao{{$formacao->id}}">{{urldecode($formacao->nivel)}}</label> 
									@endforeach														
								</div>
								<div class="form-perfil__coluna-inputs-row">
									<label for="status" class="form-perfil__label">Status</label>
                                    @foreach($statusForm as $status)
                                        @php $checked = $status->id == $candidato->status_escolaridade ? 'checked' : ''  @endphp
								        <input {{$checked}} type="radio" name="status" value="{{$status->id}}" id="status{{$status->id}}"><label for="status{{$status->id}}">{{urldecode($status->f_status)}}</label>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<div class="div-idiomas">
                        @for($i = 0, $j=1; $i < sizeof($cddIdiomas); $i++, $j++)
                            <div class="row form-perfil__row">
                                <div class="col-xs-6">
                                    <label for="idioma" class="form-perfil__label">Conhecimento de outro idioma?</label>
                                    <input type="hidden" name="idIdioma[]" value="{{$cddIdiomas[$i]->id}}">
                                    <select name="idioma[]" id="idioma" class="form-perfil__input-col02">
										<option></option>
											@foreach($idiomas as $idioma)
												@php $selected = $idioma->id == $cddIdiomas[$i]->idioma ? 'selected' : '' @endphp
												<option {{$selected}} value="{{$idioma->id}}">{{$idioma->idioma}}</option>
											@endforeach                                                
									</select>
                                </div>
                                <div class="col-xs-6">
                                <input type="radio" name="idiomaNivel{{$j}}" id="basico{{$j}}1" value="0" {{$cddIdiomas[$i]->nivel == 0 ? 'checked' : ''}} > <label for="basico{{$j}}1">Básico</label>
                                    <input type="radio" name="idiomaNivel{{$j}}" id="intermediario{{$j}}2" value="1" {{$cddIdiomas[$i]->nivel == 1 ? 'checked' : ''}}><label for="intermediario{{$j}}2">Intermediário</label>
                                    <input type="radio" name="idiomaNivel{{$j}}" id="avancado{{$j}}3" value="2" {{$cddIdiomas[$i]->nivel == 2 ? 'checked' : ''}}><label for="avancado{{$j}}3">Avançado</label>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-xs-12">
                                        <img src="../assets/imgs/icons/add16px.png" style="float: right; margin-right: 150px; display: inline;" class="btn-idioma-candidato btnNovoItem">
                                        <img src="../assets/imgs/icons/delete-button.png" style="float: right; margin-right: 5px; display: inline;" id="" class="btn-delete-idioma-cdd pointer">
                                    </div>    
                                </div> --}}
                            </div>
                        @endfor
						
							<div class="row form-perfil__row">
									<div class="col-xs-6">
										<label for="idioma" class="form-perfil__label">Conhecimento de outro idioma?</label>
										<select name="idioma[]" id="idioma" class="form-perfil__input-col02">
                                        
                                            <option></option>
                                                @foreach($idiomas as $idioma)
                                                    <option value="{{$idioma->id}}">{{$idioma->idioma}}</option>
                                                @endforeach													
										</select>
									</div>
									<div class="col-xs-6">

										<input type="radio" name="idiomaNivel{{sizeof($cddIdiomas)+1}}" id="basico{{sizeof($cddIdiomas)+1}}1" value="0" checked> <label for="basico{{sizeof($cddIdiomas)+1}}1">Básico</label>
										<input type="radio" name="idiomaNivel{{sizeof($cddIdiomas)+1}}" id="intermediario{{sizeof($cddIdiomas)+1}}2" value="1"><label for="intermediario{{sizeof($cddIdiomas)+1}}2">Intermediário</label>
										<input type="radio" name="idiomaNivel{{sizeof($cddIdiomas)+1}}" id="avancado{{sizeof($cddIdiomas)+1}}3" value="2"><label for="avancado{{sizeof($cddIdiomas)+1}}3">Avançado</label>
									</div>
									{{-- <div class="row">
										<div class="col-xs-12">
											<img src="../assets/imgs/icons/add16px.png" style="float: right; margin-right: 150px; display: inline;" class="btn-idioma-candidato btnNovoItem">
											 <img src="../assets/imgs/icons/delete-button.png" style="float: right; margin-right: 5px; display: inline;" id="" class="btn-delete-idioma-cdd pointer">
										</div>    
									</div> --}}
								</div>
                    </div>
                    @for($i=0; $i < sizeof($cddExperiencia); $i++)
                        <div class="row form-perfil__row">
                            <div class="col-xs-12">
								<input type="hidden" name="idExp[]" value="{{$cddExperiencia[$i]->id}}" >
                                <div class="form-perfil__coluna-label form-perfil__coluna-label-h-245px">
                                    @if($i == 0) <span>Última Experiência</span>
                                    @else <span>Experiência Anterior</span>
                                    @endif
                                </div>
                                <div class="form-perfil__coluna-inputs">
                                    <div class="form-perfil__coluna-inputs-row">
                                        <label for="empresa" class="form-perfil__label">Empresa</label>
                                        <input type="text" value="{{$cddExperiencia[$i]->empresa}}" name="empresa[]" class="form-perfil__input-col03" id="empresa">
                                    </div>
                                    <div class="form-perfil__coluna-inputs-row">
                                        <label for="cargo" class="form-perfil__label">Cargo</label>
                                        <input type="text" autocomplete="off" value="{{urldecode($cddExperiencia[$i]->cargo)}}" class="form-perfil__input-col03 tituloCargo" id="cargo">
                                    <input type="hidden" class="idcargo" name="idCargo[]" value="{{$cddExperiencia[$i]->idcargo}}" id="">
                                        <p style="color: red" class="empty-message"></p>
                                    </div>
                                    <div class="form-perfil__coluna-inputs-row">
                                        <label for="descAtiv" class="form-perfil__label">Descrição de Atividades/Funções</label>
                                    <textarea name="atividades[]" maxlength="2000" class="form-perfil__input-col03" id="descAtiv">{{$cddExperiencia[$i]->atividades}}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="dataAdmi" class="form-perfil__label">Data Admissão</label>
                                            <input type="text" name="admissao[]" value="{{ $cddExperiencia[$i]->admissao ? date('d/m/Y', strtotime($cddExperiencia[$i]->admissao)) : ''}}" placeholder="dd/mm/aaaa" class="form-perfil__input-col04 data-adm data" id="dataAdmi">
                                        </div>
                                        <div class="col-xs-6">
                                            <label for="dataDesliult" class="form-perfil__label">Data Desligamento</label>
                                            <input type="text" placeholder="dd/mm/aaaa" name="desligamento[]" value="{{$cddExperiencia[$i]->desligamento ? date('d/m/Y', strtotime($cddExperiencia[$i]->desligamento)) : ''}}" class="form-perfil__input-col04 datadeslig data" id="dataDesliult">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                   
					@for($i=0; $i < (3 - sizeof($cddExperiencia) ); $i++)
						<div class="row form-perfil__row">
							<div class="col-xs-12">
								<div class="form-perfil__coluna-label form-perfil__coluna-label-h-245px">
									@if($i == 0) <span>Última Experiência</span>
									@else <span>Experiência Anterior</span>
									@endif
								</div>
								<div class="form-perfil__coluna-inputs">
									<div class="form-perfil__coluna-inputs-row">
										<label for="empresa" class="form-perfil__label">Empresa</label>
										<input type="text" value="" name="empresa[]" class="form-perfil__input-col03" id="empresa">
									</div>
									<div class="form-perfil__coluna-inputs-row">
										<label for="cargo" class="form-perfil__label">Cargo</label>
										<input type="text" autocomplete="off" value="" class="form-perfil__input-col03 tituloCargo" id="cargo">
									<input type="hidden" class="idcargo" name="idCargo[]" value="" id="">
										<p style="color: red" class="empty-message"></p>
									</div>
									<div class="form-perfil__coluna-inputs-row">
										<label for="descAtiv" class="form-perfil__label">Descrição de Atividades/Funções</label>
									<textarea name="atividades[]" maxlength="2000" class="form-perfil__input-col03" id="descAtiv"></textarea>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<label for="dataAdmi" class="form-perfil__label">Data Admissão</label>
											<input type="text" name="admissao[]" value="" placeholder="dd/mm/aaaa" class="form-perfil__input-col04 data-adm data" id="dataAdmi">
										</div>
										<div class="col-xs-6">
											<label for="dataDesliult" class="form-perfil__label">Data Desligamento</label>
											<input type="text" placeholder="dd/mm/aaaa" name="desligamento[]" value="" class="form-perfil__input-col04 datadeslig data" id="dataDesliult">
										</div>
									</div>
								</div>
							</div>
						</div>
					@endfor
                    
					{{-- <div class="row form-perfil__row">
						<div class="col-xs-12">
							<div class="form-perfil__coluna-label form-perfil__coluna-label-h-245px">
								<span>Penúltima Experiência</span>
							</div>
							<div class="form-perfil__coluna-inputs">
								<div class="form-perfil__coluna-inputs-row">
									<label for="empresa" class="form-perfil__label">Empresa</label>
									<input type="text" name="penultimaExperiencia" class="form-perfil__input-col03" id="empresa">
								</div>
								<div class="form-perfil__coluna-inputs-row">
									<label for="cargo" class="form-perfil__label">Cargo</label>
									<input type="text" autocomplete="off" name="cargoPenultimaExperiencia" class="form-perfil__input-col03 tituloCargo" id="cargo">
									 <input type="hidden" class="idcargo" name="idCargo[]" value="" id="">
                            		<p style="color: red" class="empty-message"></p>
								</div>
								<div class="form-perfil__coluna-inputs-row">
									<label for="descAtiv" class="form-perfil__label">Descrição de Atividades/Funções</label>
									<textarea name="atividadesPenultimaExperiencia" class="form-perfil__input-col03" id="descAtiv"></textarea>
								</div>
								<div class="row">
									<div class="col-xs-6">
										<label for="dataAdmi" class="form-perfil__label">Data Admissão</label>
										<input type="text" name="admissaoPenultimaExperiencia" placeholder="dd/mm/aaaa" class="form-perfil__input-col04 data-adm data" id="dataAdmi">
									</div>
									<div class="col-xs-6">
										<label for="dataDeslipen" class="form-perfil__label">Data Desligamento</label>
										<input type="text" name="desligamentoPenultimaExperiencia" placeholder="dd/mm/aaaa" class="form-perfil__input-col04 datadeslig data" id="dataDeslipen">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row form-perfil__row">
						<div class="col-xs-12">
							<div class="form-perfil__coluna-label form-perfil__coluna-label-h-245px">
								<span>Antepenúltima Experiência</span>
							</div>
							<div class="form-perfil__coluna-inputs">
								<div class="form-perfil__coluna-inputs-row">
									<label for="empresa" class="form-perfil__label">Empresa</label>
									<input type="text" name="antepenultimaExperiencia" class="form-perfil__input-col03" id="empresa">
								</div>
								<div class="form-perfil__coluna-inputs-row">
									<label for="cargo" class="form-perfil__label">Cargo</label>
									<input type="text" autocomplete="off" name="cargoAntepenultimaExperiencia" class="form-perfil__input-col03 tituloCargo" id="cargo">
									 <input type="hidden" class="idcargo" name="idCargo[]" value="" id="">
                            		<p style="color: red" class="empty-message"></p>
								</div>
								<div class="form-perfil__coluna-inputs-row">
									<label for="descAtiv" class="form-perfil__label">Descrição de Atividades/Funções</label>
									<textarea name="atividadeAntepenultimaExperiencia" class="form-perfil__input-col03" id="descAtiv"></textarea>
								</div>
								<div class="row">
									<div class="col-xs-6">
										<label for="dataAdmiant" class="form-perfil__label">Data Admissão</label>
										<input type="text" name="admissaoAntepenultimaExperiencia" class="form-perfil__input-col04 data-adm data" id="dataAdmiant" placeholder="dd/mm/aaaa" max="{{date('Y-m-d')}}">
									</div>
									<div class="col-xs-6">
										<label for="dataDesliant" class="form-perfil__label">Data Desligamento</label>
										<input type="text" placeholder="dd/mm/aaaa" name="desligamentoAntepenultimaExperiencia" class="form-perfil__input-col04 datadeslig data" id="dataDesliant" max="{{date('Y-m-d')}}">
									</div>
								</div>
							</div>
						</div>
					</div> --}}

					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="pretSalarial" class="form-perfil__label ">Pretensão Salarial</label>
                            <input type="text" name="pretensaoSalarial" value="{{number_format(floatval($candidato->pretensao_salarial),2,",",".")}}" class="form-perfil__input-col02 money" id="pretSalarial">
						</div>
						<div class="col-xs-6">
							<label for="nivelHierar" class="form-perfil__label">Nível de Hierarquia</label>
							<select name="nivelHierar" class="form-perfil__input-col02" id="nivelHierar">
								@foreach($nivelHierarquico as $nivel)
									@php $selected = $nivel->id == $candidato->nivel_hierarquico ? 'selected' : ''  @endphp
									<option {{$selected}} value="{{$nivel->id}}">{{$nivel->nivel}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="row form-perfil__row">
						<div class="col-xs-6">
							<label for="disViag" class="form-perfil__label">Disponibilidade Para Viagens?</label>
							<input type="radio" name="disViag" value="1" id="disViagsim" {{$candidato->disposicao_viagem ? 'checked' : ''}} ><label for="disViagsim">Sim</label> 
							<input type="radio" name="disViag" value="0" id="disViagnao" {{!$candidato->disposicao_viagem ? 'checked' : ''}}><label for="disViagnao">Não</label>
						</div>
						<div class="col-xs-6">
							<label for="qualFreq" class="form-perfil__label">Qual a Frequência</label>
                            <input type="text" name="qualFreq" value="{{$candidato->frequencia_viagens}}" class="form-perfil__input-col02" id="qualFreq">
						</div>
					</div>
					
					<div class="row form-perfil__row">							
						@foreach($publicidade as $publi)
							<div class="col-xs-4">
                                @php $checked = $publi->id == $candidato->publicidade_curriculum ? 'checked' : '' @endphp
								<label for="publ{{$publi->id}}">{{urldecode($publi->publicidade)}}</label>
								<input type="radio" name="cv" id="publ{{$publi->id}}" value="{{$publi->id}}" {{$checked}} >
								<label for="publ{{$publi->id}}"><p>
									{{urldecode($publi->descricao)}}
								</p></label>
							</div>
						@endforeach
        			</div>
					<div class="mensagem">						
					</div>
					<div class="row form-perfil__row">
						<div class="form-perfil__acao">
							<div class="col-xs-6">
								<div class="form-perfil__acao-submit">
									<input type="submit" class="salvarCandidato" name="salvarCandidato" value="Salvar">
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
</div>
@endsection