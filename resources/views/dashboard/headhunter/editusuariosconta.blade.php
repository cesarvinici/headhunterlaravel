<form class="form-perfil" action="/dashboard/headhunter/usuarios/{{$user->id}}" method="post" id="" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <div class="form-perfil__conteudo">
        <div class="row form-perfil__row">
            <div class="col-xs-12">
                <label for="nomeCompleto" class="form-perfil__label">Nome</label>
            <input type="text" name="nome" value="{{$user->nome}}" class="form-perfil__input validate" id="nomeCompleto" required>
            </div>
        </div>
        {{-- <div class="row form-perfil__row">
            <div class="col-xs-12">
                <label for="sobrenome" class="form-perfil__label">Sobrenome</label>
                <input type="text" name="nome_sobrenome" class="form-perfil__input validate" id="sobrenome" required>
            </div>
        </div> --}}
        <div class="row form-perfil__row">
            <div class="col-xs-6">
                <label for="email" class="form-perfil__label">E-mail</label>
            <input type="email" value="{{$user->email}}" name="email" class="form-perfil__input-col02 validate" id="email" required>
            </div>
            <div class="col-xs-6">
                <label for="tipoUsuario" class="form-perfil__label">Tipo de Usuário</label>
                <select name="tipoUsuario" name="tipoUsuario" class="form-perfil__select-col02" id="tipoUsuario" required>
                        <option value="1">Valor 01</option>
                        <option value="2">Valor 02</option>
                        <option value="3">Valor 03</option>
                    </select>
            </div>
        </div>
        <div class="row form-perfil__row">
            <div class="col-xs-12">
                <div class="form-perfil__img">
                    @if($user->imagem_perfil)
                    <img src="{{asset('admin/upload/users/'.$user->imagem_perfil)}}" style="width: 200px; height: 200px" class="img img-responsive img-thumbnail"
                        alt=""><br>
                    @endif
                    <span>Imagem De Perfil</span>
                    <input type="file" name="imgPerfil" class="">
                </div>
            </div>
        </div>
        <div class="row form-perfil__row form-perfil__row-bg senha">
            <div class="form-perfil__titulo">
                <h3>Senha De Acesso</h3>
            </div>
            <div class="col-xs-12">
                <label for="senhaAtual" class="form-perfil__label validate">Senha:</label>
                <input type="password" name="senha" class="form-perfil__input-col02" id="senha">
            </div>
        </div>
        <div class="mensagem"></div>
        <div class="row form-perfil__row">
            <div class="col-xs-12">
                <div class="form-perfil__acao">
    
                    <div class="form-perfil__acao-submit">
                        
                        <input type="submit" class="" name="salvar" value="Salvar">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>