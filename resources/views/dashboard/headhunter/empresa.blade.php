@extends('layouts.headhunterHeader')
@section('title', 'Empresa')
@section('content')

<div class="col-xs-9">    
    <section class="conteudo">
        <div class="conteudo-header">
            @if(Session::has('cad_empresa_ok'))
                <div class="alert alert-success">
                    <ul>
                        <li>
                            {{Session::get('cad_empresa_ok')}}
                        </li>
                    </ul>
                    
                </div>
            @endif
            @if(Session::has('cad_empresa_fail'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{{Session::get('cad_empresa_fail')}}</li>
                    </ul>
                    
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Cadastro Da Empresa:</h2>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__voltar btn-voltar-cad">
                        <div class="conteudo-header__voltar-icone ">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        @if($empresa->id)
            @include('dashboard.headhunter.empresaEdit')
        @else
            @include('dashboard.headhunter.empresaAdd')
        @endif
    </section>
</div>
@endsection
@section('scripts')

    <script>
        window.onload = function()
        {
            if($("#cep").val())
            {
                $("#cep").trigger("blur");
            }
        }
    </script>

@endsection
