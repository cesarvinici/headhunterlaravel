
       <div class="conteudo-painel">
            <form action="{{route('update.empresaHH', ['id' => $empresa->id])}}" id="form-edit-empresa" method="post" enctype="multipart/form-data" >
                @csrf
                @method('PUT')
                <div class="cadastros-empresa__secao">
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="cadastros-empresa__secao-descricao">
                                <span>Como você atuará no markteplace?</span>
                            </div>
                        </div>
                        <div class="col-xs-8">
                            <input type="radio" id="vinc" {{$empresa->tipo_atuacao == '1' ? 'checked' : ""}} name="atuacao" value="1">
                            <label for="vinc">Vinculado a uma Consultoria de RH</label>
                            <input type="radio" id="aut" {{$empresa->tipo_atuacao == '2' ? 'checked' : ""}} name="atuacao" value="2">
                            <label for="aut">Profisisonal Autônomo</label>
                        </div>
                    </div>
                </div>
                <div class="form-perfil__conteudo">                                               
                    
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="razaoSocial" class="form-perfil__label">Razão Social</label>
                            <input type="text" value="{{$empresa->razao_social}}" name="razaoSocial" class="form-perfil__input validate" id="razaoSocial">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="nomeFantasia" class="form-perfil__label">Nome Fantasia</label>
                        <input type="text" value="{{$empresa->nome_fantasia}}" name="nomeFantasia" class="form-perfil__input validate" id="nomeFantasia">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="cnpj" class="form-perfil__label">CNPJ*</label>
                        <input type="text" value="{{$empresa->cnpj}}" name="cnpj" class="form-perfil__input validate" id="cnpjemp">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="cep" class="form-perfil__label">CEP*</label>
                        <input type="text" value="{{$empresa->endereco_cep}}" name="cep" class="form-perfil__input-col02" id="cep">
                        </div>
                        <div class="col-xs-6">
                            <label for="endereco" class="form-perfil__label">Endereço*</label>
                            <input type="text" value="{{$empresa->endereco_logradouro}}" name="endereco" class="form-perfil__input-col02" id="endereco">
                        </div>
                        
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="numero" class="form-perfil__label">Número*</label>
                            <input type="text" value="{{$empresa->endereco_numero}}" name="numero" class="form-perfil__input-col02" id="numero">
                        </div>
                        <div class="col-xs-6">
                            <label for="cidade" class="form-perfil__label">Cidade*</label>
                                <select id="listaCidades" name="cidade" class="form-perfil__input-col02">
                                @foreach($cidades as $cidade)
                                    {{$selected = $cidade->CT_ID == $empresa->cidade ? 'selected' : ''}}
                                <option {{$selected}} value="{{$cidade->CT_ID}}">{{$cidade->CT_NOME}}</option>
                                @endforeach
                            </select>
                        </div>          

                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="bairro" class="form-perfil__label">Bairro*</label>
                            <input type="text" value="{{$empresa->endereco_bairro}}" name="bairro" class="form-perfil__input-col02" id="bairro">
                        </div>
                        <div class="col-xs-6">
                            <label for="estados" class="form-perfil__label">Estado*</label>
                            <select name="estado" id="estado" onchange="carregaCidades(this, 1)" class="form-perfil__select-col02 listaEstados">
                                <option selected>Selecione um Estado</option>
                                @foreach($estados as $estado)
                                    {{ $selected = $estado->UF_ID == $empresa->estado ? 'selected' : ''}}
                                    <option {{$selected}} value="{{$estado->UF_UF}}">{{$estado->UF_NOME}}</option>
                                @endforeach                       
                            </select>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="telefone" class="form-perfil__label">Telefone</label>
                            <input type="tel" value="{{$empresa->telefone1}}" name="telefone" class="form-perfil__input-col02 telefone" id="telefone">
                        </div>
                        <div class="col-xs-6">
                            <label for="website" class="form-perfil__label">Website</label>
                            <input type="url" value="{{$empresa->website}}" name="website" class="form-perfil__input-col02" id="website">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="segmento" class="form-perfil__label">Segmento*</label>
                            <select class="form-perfil__select-col02" name="segmento">
                                @foreach($segmentos as $segmento)
                                    {{$selected = $segmento->id == $empresa->segmento ? 'selected' : ''}}
                                    <option {{$selected}} value='{{$segmento->id}}' >{{urldecode($segmento->segmento)}}</option>
                                @endforeach
                            </select>	
                        </div>
                        <div class="col-xs-6">
                                <label for="cidade" class="form-perfil__label">Nº de Funcionários (Matriz + Filial)</label>
                            <select class="form-perfil__input-col02" name="nFuncionarios">
                                @foreach($nFuncionarios as $funcionarios)
                                {{$selected = $funcionarios->id == $empresa->numero_funcionarios ? 'selected' : ''}}
                            <option {{$selected}} value="{{$funcionarios->id}}" >{{$funcionarios->quantidade}}</option>
                                @endforeach
                            </select>
                            
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="logotipo" class="form-perfil__label">Logotipo*</label>
                            <div class="form-perfil__img">
                                @if($empresa->logotipo)            
                                    <img src="{{asset('admin/upload/logotipo/'.$empresa->logotipo)}}" class="img-thumbnail" style="height: 200px; width: 200px">
                                @endif
                                <br>
                            </div>
                            <input type="file" name="logotipo" class="form-perfil__input-col" id="logotipo">
                        </div>
                    </div>

                    <div class="form-perfil__titulo form-perfil__titulo-azul">
                        <h3>Responsável Financeiro</h3>
                    </div>

                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="nomeResp" class="form-perfil__label">Nome Completo*</label>
                        <input type="text" value="{{$empresa->responsavel_financeiro_nome}}" name="nomeResp" class="form-perfil__input validate" id="nomeResp">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="cargo" class="form-perfil__label">Cargo*</label>
                            <input type="text" value="{{$empresa->responsavel_financeiro_email}}" name="cargoResp" class="form-perfil__input validate" id="cargo">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="email" class="form-perfil__label validate">E-mail</label>
                            <input type="email" value="{{$empresa->responsavel_financeiro_email}}" name="emailResp" class="form-perfil__input-col02 validate" id="email">
                        </div>
                        <div class="col-xs-6">
                            <label for="telefoneRamal" class="form-perfil__label">Telefone | Ramal</label>
                            <input type="tel" value="{{$empresa->responsavel_financeiro_telefone}}" name="telResp" class="form-perfil__input-col02 telefone" id="telResp">
                        </div>
                    </div>
                    <div class="mensagem"></div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="form-perfil__acao">
                                    <div class="form-perfil__acao-submit">
                                        <input type="submit" id="btn-edit-hhEmpresa" name="salvarEmp" value="Salvar">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div style="display:none">            
                {{-- DADOS BANCARIOS --}}
                <div class="row form-perfil__row">
                    <div class="row form-perfil__titulo form-perfil__titulo-azul">
                        <div class="col-xs-6">
                            <h3>Formas de Pagamento</h3>
                        </div>
                        <div class="col-xs-3">
                            <input type="radio" name="formaPagamento" class="" id="boleto">
                            <label for="boleto" class="">Boleto Bancário</label>
                        </div>
                        <div class="col-xs-3">
                            <input type="radio" class="" id="cartao">
                            <label for="cartao" name="formaPagamento" class="">Cartão de Crédito</label>
                        </div>
                    </div>
                </div>
                <div class="form-perfil__row-bg">
                    <div class="form-perfil__titulo">
                        <h3>Dados Para Faturamento</h3>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="banco" class="form-perfil__label">Banco:</label>
                            <select name="bancos" class="form-perfil__input-col02" id="banco">
                                <option value="">Valor 01</option>
                                <option value="">Valor 02</option>
                                <option value="">Valor 03</option>
                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label for="numeroCartao" class="form-perfil__label">Número do Cartão:</label>
                            <input type="text" class="form-perfil__input-col02" id="numeroCartao">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="nomeCartao" class="form-perfil__label">Número do Cartão:</label>
                            <input type="text" class="form-perfil__input" id="nomeCartao">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="ccv" class="form-perfil__label">CCV:</label>
                            <select name="ccvs" class="form-perfil__input-col02" id="ccv">
                                <option value="">Valor 01</option>
                                <option value="">Valor 02</option>
                                <option value="">Valor 03</option>
                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label for="validade" class="form-perfil__label">Validade:</label>
                            <input type="text" class="form-perfil__input-col02" id="validade">
                        </div>
                    </div>
                </div>

                <div class="row form-perfil__row">
                    <div class="row form-perfil__titulo form-perfil__titulo-azul">
                        <div class="col-xs-8">
                            <h3>Dados para Depósito | Recebimento:</h3>
                        </div>
                    </div>
                </div>
                <div class="form-perfil__row-bg">
                    <div class="form-perfil__titulo">
                        <h3>Dados Bancários</h3>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="banco" class="form-perfil__label">Banco:</label>
                            <select name="bancos" class="form-perfil__input-col02" id="banco">
                                <option value="">Valor 01</option>
                                <option value="">Valor 02</option>
                                <option value="">Valor 03</option>
                            </select>
                        </div>
                        <div class="col-xs-3">
                            <label for="numeroCartao" class="form-perfil__label">Agência:</label>
                            <input type="text" class="" id="numeroCartao">
                        </div>
                        <div class="col-xs-3">
                            <label for="numeroCartao" class="form-perfil__label">Conta:</label>
                            <input type="text" class="" id="numeroCartao">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="nomeCartao" class="form-perfil__label">Nome Completo:</label>
                            <input type="text" class="form-perfil__input" id="nomeCartao">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-6">
                            <label for="validade" class="form-perfil__label">CPF:</label>
                            <input type="text" class="form-perfil__input-col02" id="validade">
                        </div>
                    </div>
                </div>
                <div class="row form-perfil__row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-perfil__acao">
                                <div class="form-perfil__acao-submit">
                                    <input type="submit" id="acaoSubmit" value="Prosseguir">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


