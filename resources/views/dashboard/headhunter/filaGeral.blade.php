@foreach($candidatos as $candidato)
                      
<div class="resul__grupo-row resultFiltro ">
    <div class="box-perfil">
        <div class="box-perfil-row-01">
            <div class="row">
                <div class="col-xs-2"> 
                    @if(!empty($candidato->foto_perfil))                                        
                        <img src="{{asset('./admin/upload/candidatos/'.$candidato->foto_perfil)}}" style="width: 76px; height: 75px" alt="">
                    @else
                        <img src="{{asset('./admin/imgs/icons/perfil-img-resul.png')}}" alt="">
                    @endif
                        <div class="score">
                        <div class="pct">
                            <span>80%</span>
                        </div>
                        <div class="descricao">
                            <span>Score</span>
                        </div>
                    </div>
                    <div class="idade">
                        <div class="numero">
                            <span></span>
                        </div>
                        <div class="estado-civil">
                            <span></span>
                        </div>
                    </div>
                    <div class="perfil-cv">
                        <div class="descricao">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="nome-local">
                        <div class="nome">
                        <span>{{$candidato->nome}}</span>
                        </div>
                        <div class="local">
                            <span>{{mb_strtoupper(urldecode($candidato->CT_NOME))}}/{{$candidato->UF_UF}}</span>
                        </div>
                    </div>
                    <div class="experiencia">
                        @php $experiencia = candidatoExperiencia($candidato->id);@endphp
                       
                            @forelse($experiencia as $exp)
                                <div class="ultima">
                                        <div class="titulo">
                                            <span>{{$loop->first ? 'Última Experiência:' : 'Experiência Anterior:'}}</span>
                                        </div>
                                        <div class="empresa-data">
                                            <span> {{$exp->empresa}}  - {{mesAno($exp->admissao)}} a {{mesAno($exp->desligamento)}} </span>
                                        </div>
                                        <div class="cargo">
                                            <span>{{urldecode($exp->cargo)}}</span>
                                        </div>
                                    </div>
                             @empty
                             <div class="ultima">
                                <div class="titulo">
                                    <span>Candidato sem experiência.</span>
                                </div>
                             </div>
                            @endforelse
                       
                    </div>
                    <div class="formacao">
                        <div class="descricao">
                        <span>Formação: {{$candidato->curso}}<br>{{$candidato->instituicao}}</span>
                        </div>
                    </div>
                    <div class="idioma"> 
                        @if(sizeof(candidatoIdiomas($candidato->id)))  
                            @foreach(candidatoIdiomas($candidato->id) as $idioma)                                                
                                <span>{{urldecode($idioma->idioma)}}: {{nivelIdioma($idioma->nivel)}} </span><br>
                            @endforeach
                        @endif                                                    
                    </div>
                </div> 
            <div class="col-xs-2">
                @if(!empty($candidato->curriculum))
                    <div class="candidatos__cv">
                    <a href="{{asset('/admin/curriculos/candidatos/'.$candidato->curriculum)}}" target="_BLANK">
                            <div class="icn-perfil-cv-p-azul"></div>
                        </a>
                    </div>
                @else                                        
                    <div class="col-xs-2">
                        <div class="candidatos__cv">
                            <div class="icn-perfil-cv-p-azul"></div>
                        </div>
                    </div>                                        
                @endif
            </div>                                        
                <div class="col-xs-4">
                    <div class="avaliacao">
                        <div class="titulo">
                            <span>Avaliação Curricular</span>
                        </div>
                        <div class="status">
                            <div class="status-row">
                                <div class="icone">BTN</div>
                                <div class="descricao">Aderente Ao Perfil</div>
                            </div>
                            <div class="status-row">
                                <div class="icone">BTN</div>
                                <div class="descricao">Fora Do Perfil</div>
                            </div>
                        </div>
                        <div class="titulo">
                            <span>Ações</span>
                        </div>
                        <div class="acoes">
                            <div class="enviar-email">
                                <span>Enviar Email</span>
                            </div>
                            <div class="enviar-quest">
                                <span>Enviar Questionário Pré-Seleção</span>
                            </div>
                            <div class="solicitar-entrev">
                                <span>Solicitar Entrevista</span>
                            </div>
                            <div class="evoluir-cand">
                                <span>Evoluir Candidato Participante</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="perfil-extra">
                    <div class="titulo">
                        <span>Anexos</span>
                    </div>
                    <div class="icone">
                        ICN
                    </div>
                </div>
            </div>
        </div>
        <div class="box-perfil-row-boxs">
            <div class="row">
                <div class="col-xs-4">
                    <div class="anexos-box">
                        <span>Declaração</span>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="anexos-box">
                        <span>Avaliação DISC</span>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="anexos-box">
                        <span>Certificado A</span>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="avaliacao-logo">
                        <div class="descricao">
                            <span>Avalição Do Headhunter</span>
                        </div>
                        <div class="icone">
                            ICN
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
                <div class="add-fila pointer" onclick="addCddFila(this,{{$candidato->id}})">
                @csrf
                <div class="descricao ">
                    <span>ADICIONAR A FILA DE CANDIDATURA</span>
                </div>
                <div class="icone">
                    ICN
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach