@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form class="form-perfil" action="{{route('update.meus-dados')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-perfil__conteudo">
        <div class="row form-perfil__row">
            <div class="col-xs-12">
                <label for="nomeCompleto" class="form-perfil__label">Nome Completo*</label>
            <input type="text" class="validate form-perfil__input" name="nome" id="nomeCompleto" value="{{urldecode($user->nome)}}" required>
            </div>
        </div>
        <div class="row form-perfil__row">
            <div class="col-xs-12">
                <label for="cpf" class="form-perfil__label">CPF*</label>
            <input type="text" name="cpf" value="{{urldecode($user->cpf)}}" class="validate form-perfil__input" id="cpf" required>
            </div>
        </div>
        <div class="row form-perfil__row">
            <div class="col-xs-6">
                <label for="cep" class="form-perfil__label">CEP*</label>
                <input type="text" name="cep" value="{{urldecode($user->endereco_cep)}}" class="validate form-perfil__input-col02" id="cep" required>
            </div>
            <div class="col-xs-6">
                <label for="numero" class="form-perfil__label">Número*</label>
                <input type="text" name="numero" class="validate form-perfil__input-col02" value="{{urldecode($user->endereco_numero)}}" id="numero" required>
            </div>
        </div>
        <div class="row form-perfil__row">
            <div class="col-xs-6">
                <label for="endereco" class="form-perfil__label">Endereço*</label>
                <input type="text" name="endereco"  class="validate form-perfil__input-col02" id="endereco" value="{{urldecode($user->endereco_logradouro)}}"  required>
            </div>
            <div class="col-xs-6">
                <label for="bairro" class="form-perfil__label">Bairro*</label>
                <input type="text" name="bairro" class="validate form-perfil__input-col02" value="{{urldecode($user->endereco_bairro)}}"  id="bairro" required>
            </div>
        </div>
        <div class="row form-perfil__row">
            <div class="col-xs-6">
                <label for="cidade" class="form-perfil__label">Cidade*</label>
                <select name="cidade" class="form-perfil__select-col02" id="listaCidades">
                    <option value="" selected >Selecione uma Cidade</option>
                    @foreach ($cidades as $cidade)
                    {{$selected = $user->CT_ID == $cidade->CT_ID ? 'selected' : ''}}
                     <option {{$selected}}  value="{{$cidade->CT_ID}}">{{urldecode($cidade->CT_NOME)}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-6">
                <label for="estado" class="form-perfil__label">Estado*</label>
                <select name="estado" id="estado" onchange="carregaCidades(this, 1)" class="form-perfil__select-col02 listaEstados">
                    <option selected>Selecione um Estado</option>
                    @foreach ($estados as $estado)
                        {{$selected = $estado->UF_ID == $user->UF_ID ? 'selected' : '' }}
                        <option  {{$selected}} value="{{$estado->UF_UF}}">{{$estado->UF_NOME}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row form-perfil__row">
            <div class="col-xs-12">
                <label for="complemento" class="form-perfil__label">Complemento</label>
            <input type="text" name="complemento" class="form-perfil__input" value="{{urldecode($user->endereco_complemento)}}" id="complemento">
            </div>
        </div>					
        <div class="row form-perfil__row">
            <div class="col-xs-6">
                <label for="telefone" class="form-perfil__label">Telefone</label>
            <input type="text" name="telefone" class="form-perfil__input-col02" value="{{urldecode($user->telefone)}}"  id="telefone">
            </div>
            <div class="col-xs-6">
                <label for="celular" class="form-perfil__label">Celular*</label>
            <input type="text" name="celular" class="validate form-perfil__input-col02" id="celular01" value="{{urldecode($user->celular)}}"  required>
            </div>
        </div>
        <div class="row form-perfil__row">
            <div class="col-xs-12">
                <label for="email" class="form-perfil__label">E-mail*</label>
            <input type="email" name="email" id="email" class="validate form-perfil__input" value="{{urldecode($user->email)}}"  required>
            </div>
        </div>
        <div class="row form-perfil__row">
            <div class="col-xs-6">
                <div class="form-perfil__img">
                                        <!-- <span><a href="#">Adicionar</a></span> | <span><a href="#">Alterar</a></span> -->
                   
                    @if($user->imagem_perfil)
                    
                        <img src="{{asset('admin/upload/'.$user->imagem_perfil)}}" class="img-thumbnail" style="height: 200px; width: 200px">
                        
                    @endif
                    <br>
                    <span>Imagem Do Perfil</span>
                    <input type="file" name="imgPerfil">
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-perfil__img">
                
                        @if($user->curriculo)
                            <a href="{{asset('admin/curriculos/'.$user->curriculo)}}">Baixar Curriculum</a>
                        @endif
                    <br>
                    <span>Curriculum</span>
                     <!-- <span><a href="#">Adicionar</a></span> | <span><a href="#">Alterar</a></span><br> -->
                    <input type="file" id="curriculo" name="curriculum">
                </div>
            </div>
        </div>
        <div class="row form-perfil__row">
            <div class="col-xs-6">
                <label for="facebook" class="form-perfil__label">Facebook</label>
                <input type="text" class="form-perfil__input-col02" name="facebook" id="facebook" value="{{urldecode($user->facebook)}}">
            </div>
            <div class="col-xs-6">
                <label for="linkedin" class="form-perfil__label">Linkedin</label>
            <input type="text" class="form-perfil__input-col02" name="linkedin" id="linkedin" value="{{urldecode($user->linkedin)}}">
            </div>
        </div>
        <div class="row form-perfil__row">
            <div class="col-xs-6">
                <label for="instagram" class="form-perfil__label">Instagram</label>
            <input type="text" class="form-perfil__input-col02" name="instagram" id="instagram" value="{{urldecode($user->instagram)}}">
            </div>
            <div class="col-xs-6">
                <label for="twitter" class="form-perfil__label">Twitter</label>
            <input type="text" class="form-perfil__input-col02" name="twitter" id="twitter" value="{{urldecode($user->twitter)}}">
            </div>
        </div>
        <div class="row form-perfil__row form-perfil__row-bg senha">
            <div class="form-perfil__titulo">
                <h3>Senha de Acesso</h3>
            </div>
            <div class="col-xs-6">
                <label for="senhaAtual" class="form-perfil__label">Senha Atual</label>
                <input type="password" class="form-perfil__input-col02" name="senhaAtual" id="senhaAtual">
            </div>
            {{-- <div class="col-xs-6">
                <input type="checkbox" id="mostrarSenha">
                <label for="mostraSenha">Mostrar Senha</label>
            </div> --}}
        </div>
        <div class="row form-perfil__row form-perfil__row-bg senha" style="margin-top:-63px">
            
            <div class="col-xs-6">
                <label for="novaSenha" class="form-perfil__label">Nova Senha</label>
                <input type="password" class="form-perfil__input-col02" name="novaSenha" id="novaSenha">
            </div>
            <div class="col-xs-6">
                    <label for="repetirSenha" class="form-perfil__label">Repetir Nova Senha</label>
                    <input type="password" class="form-perfil__input-col02" name="novaSenha_confirma" id="repetirSenha">
                </div>
        </div>
        <div class="mensagem"></div>
        <input type="hidden" name="salvar" value="Salvar">
        <div class="row form-perfil__row">
            <div class="col-xs-12">
                <div class="form-perfil__acao">
                    <div class="form-perfil__acao-submit">
                        <input type="submit" id="acaoSubmit" class="" name="salvar" value="Salvar">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@section('scripts')



@endsection