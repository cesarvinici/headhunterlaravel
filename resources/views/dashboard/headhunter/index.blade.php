@extends('layouts/headhunter')
@section('title', 'Dashboard')
@section('content')

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">DASHBOARD</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Minhas Vagas em Aberto
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>Cargo</th>
								<th>Empresa</th>
								<th>Segmento</th>
								<th>D.A</th>
							</tr>
						</thead>
						<tbody>
							@php $headhunter = $user->role == 1 ? $user->id : $user->headhunter @endphp
							@foreach($vagasGerais as $vaga)
								@if($vaga->headhunter == $headhunter) @continue @endif
								<tr class="odd gradeX">
									<td><a href="/dashboard/headhunter/painel-vagas/{{$vaga->id}}">{{urldecode($vaga->tituloCargo)}}</a></td>
									<td>{{urldecode($vaga->nome_fantasia)}}/{{urldecode($vaga->segmento)}}</td>
									<td>-</td>
									<td>-</td>
								</tr>
							@endforeach
							</tr>
							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
			</div>
				<!-- /.panel-body -->
		</div>
			<!-- /.panel -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					Convites (Últimos 90 Dias)
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">

							<div class="col-lg-6">
								<p>Recebi <span class="icone">{{sizeof($convites)}}</span></p><br>
								<p>Enviei <span class="icone">{{sizeof($convites)}}</span></p>
							</div>
							<div class="col-lg-6">
								<p>Aceitei <span class="icone">{{sizeof($convites)}}</span></p><br>
								<p>Aceitaram <span class="icone">{{sizeof($convites)}}</span></p>
							</div>


				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-6 -->
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					Vagas Marketplace (Últimos 90 dias)
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">

							<div class="col-lg-6">
								<p>Vagas em Ação <span class="icone">{{sizeof($convites)}}</span></p><br>
								<p>Vagas Suspensas <span class="icone">{{sizeof($convites)}}</span></p>
							</div>
							<div class="col-lg-6">
								<p>Vagas Finalizadas <span class="icone">{{sizeof($convites)}}</span></p><br>
								<p>Vagas em Aberto + 15 dias <span class="icone">{{sizeof($convites)}}</span></p>
							</div>


				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-6 -->

		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Vagas Internas (Últimos 90 Dias)
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">


							<div class="col-lg-4">
								<p>Vagas Cadastradas <span class="icone">{{sizeof($convites)}}</span></p><br>
								<p>Vagas Suspensas <span class="icone">{{sizeof($convites)}}</span></p><br>
							</div>
							<div class="col-lg-4">
								<p>Vagas em Aberto <span class="icone">{{sizeof($convites)}}</span></p><br>
								<p>Aceitaram <span class="icone">{{sizeof($convites)}}</span></p>
							</div>
							<div class="col-lg-4">
								<p>Vagas em Aberto <span class="icone">{{sizeof($convites)}}</span></p><br>
								<p>Aceitaram <span class="icone">{{sizeof($convites)}}</span></p>

							</div>


				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
	</div>
	<!-- /.row -->
</div>
<!-- /#page-wrapper -->


@endsection
