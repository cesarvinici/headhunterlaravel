<div id="listacandidatos">
    <div class="resul">
            <div class="resul__header">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="resul__header-numero">
                            <p>{{sizeof($candidatos)}} resultados encontrados</p>
                        </div>
                    </div>
                    <!--  <div class="col-xs-6">
                        <div class="resul__header-filtro">
                            <p>Organizar por: <span class="alfabetica">Orderm Alfabética</span> | <span class="salario">Pretensão</span> | <span class="localidade">Localidade</span></p>
                        </div>
                    </div> -->
                </div>
            </div>
            @foreach($candidatos as $candidato)
                <div class="pesquisar-candidato__resul">
                    <div class="pesquisar-candidato__resul-perfil">
                        <div class="vagas__informacoes-resultado">
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="vagas__informacoes-img">
                                        @if(!empty($candidato->foto_perfil))
                                            <img src="{{asset('./admin/upload/candidatos/'.$candidato->foto_perfil)}}" style="width: 76px; height: 75px" alt="">
                                        @else
                                            <img src="{{asset('./admin/imgs/icons/perfil-img-resul.png')}}" alt="">
                                        @endif
                                        
                                    </div>
                                    <div class="vagas__informacoes-score">
                                        <div class="numero" style="font-size:22px;">{{getIdade(MysqlToData($candidato->nascimento))}}</div>
                                        <div class="texto">{{$candidato->estado_civil}}</div>
                                        <br>
                                        <div class="texto">{{urldecode($candidato->publicidade)}}</div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="vagas__informacoes-nome">
                                        <span>{{$candidato->nome}}</span>
                                    </div>
                                    <div class="vagas__informacoes-local">
                                        {{mb_strtoupper(urldecode($candidato->CT_NOME))}}, {{$candidato->UF_UF}}
                                    </div>
                                    @php $experiencia = candidatoExperiencia($candidato->id) @endphp
                                    @if(sizeof($experiencia))
                                        <div class="vagas__informacoes-experiencias">
                                            @foreach($experiencia as $exp)
                                            <div class="titulo">{{$loop->first ? 'Última Experiência:' : 'Experiência Anterior:'}}</div>
                                                <div class="empresa-data"> {{$exp->empresa}} - {{MysqlToData($exp->admissao)}} a {{MysqlToData($exp->desligamento)}} </div>
                                                <div class="cargo">{{urldecode($exp->cargo)}}</div>
                                                {{-- <div class="titulo"></div>
                                                <div class="empresa-data"> {{$experiencia[1]->empresa}} - {{MysqlToData($experiencia[1]->admissao)}} a {{MysqlToData($experiencia[1]->desligamento)}}</div>
                                                <div class="cargo">{{urldecode($experiencia[1]->cargo)}}</div> --}}
                                            @endforeach

                                            <div class="idioma"> 
                                                @if(sizeof(candidatoIdiomas($candidato->id)))  
                                                    @foreach(candidatoIdiomas($candidato->id) as $idioma)                                                
                                                        <span>{{urldecode($idioma->idioma)}}: {{nivelIdioma($idioma->nivel)}} </span><br>
                                                    @endforeach
                                                @endif                                                    
                                            </div>
                                        </div>
                                    @else
                                    <div class="vagas__informacoes-experiencias">
                                            <div class="titulo">Candidato sem experiência</div>
                                        </div>
                                    @endif
                                </div>                                       
                                <div class="col-xs-2">
                                    <div class="candidatos__cv">
                                        @if(!empty($candidato->curriculum))
                                            <a href="{{asset('./admin/curriculos/candidatos/'.$candidato->curriculum)}}" target="_BLANK" >
                                            <div class="icn-perfil-cv-p-azul"></div>
                                            </a>
                                        @else
                                            <div class="icn-perfil-cv-p-azul"></div>
                                        @endif
                                    </div>
                                </div>                                  
                                                                                
                                <div class="col-xs-4">
                                    <div class="candidatos__atual">
                                        <div class="candidatos__atual-titulo">
                                            <span>Atualizado em {{MysqlToData($candidato->updated_at)}} </span>
                                        </div>
                                        <div class="candidatos__atual-box">
                                            <div class="agrupar">
                                                <div class="texto">
                                                    <span>{{$candidato->telefone}}</span><br>
                                                    <span>{{$candidato->celular}}</span>
                                                </div>
                                                <div class="icone">
                                                    <div class="icn-perfil-tel-p-azul"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="candidatos__atual-box">
                                            <div class="agrupar">
                                                <div class="texto">
                                                    <span>Remuneração a partir de: <br> R$  {{intToMoney($candidato->pretensao_salarial)}}</span>
                                                </div>
                                                <div class="icone">
                                                    <div class="icn-perfil-dolar-p-azul"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="candidatos__atual-box">
                                            <div class="agrupar">
                                                <div class="texto">
                                                    <span>Enviar Mensagem</span>
                                                </div>
                                                <div class="icone">
                                                    <div class="icn-perfil-email-p-azul"></div>
                                                </div>
                                            </div>
                                        </div>
                                                <!--  <div class="candidatos__atual-box">
                                                    <div class="agrupar">
                                                        <div class="adicionar">
                                                            <a href="#"><span>Adicionar Fila de Candidatura</span></a>
                                                        </div>
                                                    </div>
                                                </div> -->
                                    </div>
                                </div>
                            </div>                                    
                            @php $recrutamento = processoSelecaoCdd($candidato->id) @endphp
                            <div class="row">
                                <div class="col-xs-12">
                                    Recrutamento Atual:
                                    <ul>
                                        @forelse($recrutamento as $rec)     
                                            <li><strong>{{urldecode($rec->cargo)}}</strong> com o Headhunter <strong>{{$rec->nome}}</strong> 
                                                    há {{$rec->data}} dias.</li>
                                            @empty
                                                <li>Candidato não  está participando de nenhum processo seletivo atualmente.</li>
                                        @endforelse
                                    </ul>                                
                                </div>
                            </div>                                      
                        </div>
                        <div class="vagas__informacoes-extra">
                            <div class="vagas__informacoes-extra-titulo">
                                <span>Anexos</span>
                            </div>
                            <div class="vagas__informacoes-extra-conteudo">
                                <div class="vagas__informacoes-extra-anexos">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="vagas__informacoes-extra-anexos-box">
                                                <span>Declaração</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="vagas__informacoes-extra-anexos-box">
                                                <span>Avaliação DISC</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="vagas__informacoes-extra-anexos-box">
                                                <span>Certificado A</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="vagas__informacoes-extra-anexos-avaliacao">
                                                <div class="texto">
                                                    <span>Avalição Do Headhunter</span>
                                                </div>
                                                <div class="icone">
                                                    <img src="{{asset('./admin/imgs/icons/logo-acesso-avaliacao.png')}}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            {{$candidatos->appends(request()->except('_token'))->links()}}
        </div>
</div>
