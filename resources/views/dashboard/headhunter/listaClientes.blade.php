<div class="consulta__filtro-resul">
    <div class="row">
        <div class="col-xs-6">
            <div class="resul__header-numero">
            <p><span class="resultEnc">{{sizeof($empresas)}}</span> resultados encontrados</p>
            </div>
        </div>
        {{-- <div class="col-xs-6">
            <div class="resul__header-filtro">
                <p>Organizar por: <span class="cidade">Nome Fantasia</span> | <span class="alfabetica">Código</span> | <span class="valor">Razão Social</span></p>
            </div>
        </div> --}}
    </div>
</div>
<div class="consulta__tabela">
    <div class="table-responsive">
        <table class="table tabelaEmpresas tablesorter">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Razão Social</th>
                    <th>Nome Fantasia</th>
                    <th>CNPJ</th>
                    <th colspan="2" style="width:5%;"></th>
                </tr>
            </thead>
            <tbody class="tbody">
                @foreach($empresas as $empresa)
                <tr>
                    <td>{{$empresa->id}}</td>
                    <td>{{urldecode($empresa->razao_social)}}</td>
                    <td>{{urldecode($empresa->nome_fantasia)}}</td>
                    <td>{{$empresa->cnpj}}</td>
                    <td class="consulta__tabela-url">
                        <a href="{{route('edit.cliente', ['id' => $empresa->id])}}"><i class="fa fa-external-link"></i></a>
                        <td class="usuarios__tabela-fechar" onclick="deletaCliente({{$empresa->id}})"><i class="fa fa-close"></i></td>
                    </td>
                </tr>
                @endforeach									
            </tbody>
        </table>
    </div>
    <div style="float:right">
            {{ $empresas->links() }}
    </div>                        
</div>