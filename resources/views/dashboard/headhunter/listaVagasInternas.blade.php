<div class="resul">
    <div class="resul__header">
        <div class="row">
            <div class="col-xs-6">
                <div class="resul__header-numero">
                    <p><span class="qtdVagasInt">{{sizeof($headhunter)}}</span> resultados encontrados</p>
                </div>
            </div>
            <div class="col-xs-6">
                <!-- <div class="resul__header-filtro">
                    <p>Organizar por: <span class="data">Data</span> | <span class="cargo">Cargo</span> | <span class="valor">Valor</span></p>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="vagasInternas">                           
    @foreach($headhunter as $vaga)
        <div class="vagas__resul">
            <div class="col-xs-4">
                <div class="row">
                    <div class="vagas__marketplace-titulo">
                        <div class="agrupa">
                            <div class="nome">{{mb_strtoupper(urldecode($vaga->cargo))}}</div>
                        <div class="data">{{date('d/m/Y', strtotime($vaga->created_at))}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="row">
                    <div class="vagas__marketplace-local">
                        <div class="agrupa">
                            <div class="icone">
                                <div class="icn-resul-local-p-branco"></div>
                            </div>
                            <div class="local">
                                <span>{{mb_strtoupper(urldecode($vaga->CT_NOME))}}/{{$vaga->UF_UF}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="row">
                    <div class="vagas__marketplace-salario">
                        <div class="agrupar">
                            <div class="">{{number_format(floatval($vaga->salario),2,",",".")}}</div>
                        <!-- <div class="min">Mín.: R$ 6.000,00</div>
                            <div class="max">Máx.: R$ 8.000,00</div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="row">
                    <div class="vagas__marketplace-salarioPct">
                        <div class="agrupar">
                        <div class="pct">{{$vaga->fee}}</div>
                            <div class="descricao">SALÁRIO MÊS</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-1">
                <div class="row">
                    <div class="vagas__marketplace-icone">
                        <div class="row">
                            <a href="/dashboard/headhunter/painel-vagas/{{$vaga->id}}">
                                <div class="icone">
                                    <div class="icn-row-conecta-azul"></div>
                                </div>
                                <div class="descricao">
                                    <span>Encaminhar</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>                            
    @endforeach                           
</div>