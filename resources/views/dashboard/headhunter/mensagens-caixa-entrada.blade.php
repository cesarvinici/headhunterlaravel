@extends('layouts.headhunterHeader')
@section('title', 'Mensagens')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-12 btn-voltar">
                    <div class="conteudo-header__voltar">
                        <div class="conteudo-header__voltar-icone">ICN</div>
                        <div class="conteudo-header__voltar-texto">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <div class="col-xs-5">
                <div class="caixa-entrada">
                    <div class="caixa-entrada__titulo">
                        <span>Caixa De Entrada</span>
                    </div>
                    <div class="caixa-entrada__conteudo">
                        <ul class="caixa-entrada__nav">
                            @forelse($convites as $convite)
                                <li class="caixa-entrada__item">
                                    <a href="" class="caixa-entrada__link">Convite Vaga: {{$convite->cargo}} | {{MysqlToData($convite->created_at)}} | {{date('H:m', strtotime($convite->created_at))}} </a>
                                </li>
                                @empty
                                    <p>Sem mensagens na caixa de entrada</p>
                            @endforelse
                        </ul>                    
                    </div>
                </div>
            </div>
            <div class="col-xs-7">
                <div class="caixa-entrada">
                    @foreach($convites as $convite)
                        <div class="caixa-entrada__titulo-bg">
                            <span>De: Convite | Data E Hora Do Envio: {{MysqlToData($convite->created_at)}} | {{date('H:m', strtotime($convite->created_at))}}</span>
                        </div>
                        <div class="caixa-entrada__conteudo caixa-entrada__conteudo-bg">
                            <div class="caixa-entrada__assunto">
                                <span><strong>Assunto:</strong>Você foi convidado para trabalhar em uma vaga</span>
                            </div>
                            <div class="caixa-entrada__texto">
                                <p>Você foi convidado por para trabalhar em uma vaga:</p>
                                    <p>Vaga: {{urldecode($convite->cargo)}}</p>
                                    <p>Empresa: {{$convite->empresa}}</p>
                                    <p>Cidade: {{urldecode($convite->cidade)}}, {{$convite->estado}}</p>
                                    <p><a class="pointer" onclick="aceitaConvite({{$convite->id}})" >Clique aqui</a> para aceitar</p>
                                    <p><a class="pointer" onclick="recusaConvite({{$convite->id}})">Clique aqui</a> para recusar</p>
                            </div>
                            <div class="caixa-entrada__responder">
                                <a href="">Responder</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
<script>
    function aceitaConvite(convite)
    {
        $.get('/dashboard/ajax/aceitaConvite/'+convite, function(data)
        {
            if(data == 'ok')
            {
                alert("Convite aceito com sucesso");
            }  
        });
    }
    function recusaConvite(convite)
    {
        $.get('/dashboard/ajax/recusaConvite/'+convite, function(data)
        {
            if(data == 'ok')
            {
                alert('Convite recusado com sucesso');
            }
        });
    }
</script>

@endsection