@extends('layouts.headhunterHeader')
@section('title', 'Mensagens')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-6">
                    <a href="/dashboard/headhunter/mensagens/entrada">
                    <div class="conteudo-header__entrada pointer">
                        <div class="texto">
                            <span>Caixa de Entrada</span>
                        </div>
                        <div class="icone">ICN</div>
                        <div class="numero">
                            <span>{{sizeof($convites)}}</span>
                        </div>
                    </div>
                    </a>
                </div>
                <a href="/dashboard/headhunter/mensagens/saida"/>
                <div class="col-xs-6">
                    <div class="conteudo-header__saida pointer">
                        <div class="texto">
                            <span>Caixa de Saída</span>
                        </div>
                        <div class="icone">ICN</div>
                        <div class="numero">
                            <span>0</span>
                        </div>
                    </div>
                </div>
                </a>
            </div>
        </div>
        <div class="conteudo-painel">
            <form class="form-perfil" action="">
                <div class="form-perfil__conteudo">
                        <div class="row form-perfil__row">
                            <div class="form-perfil__conteudo-titulo">
                                <span>Entre Em Contato Com a Gente!</span>
                            </div>
                        </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="area" class="form-perfil__label">Área</label>
                            <select name="area" class="form-perfil__input" id="area">
                                <option value="">Valor 01</option>
                                <option value="">Valor 02</option>
                                <option value="">Valor 03</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="assunto" class="form-perfil__label">Assunto</label>
                            <input type="text" class="form-perfil__input" id="assunto">
                        </div>
                    </div>
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <label for="mensagem" class="form-perfil__label">Mensagem</label>
                            <textarea name="" class="form-perfil__input" id="mensagem"></textarea>
                        </div>
                    </div>                                    
                    <div class="row form-perfil__row">
                        <div class="col-xs-12">
                            <div class="form-perfil__acao">
                                <div class="form-perfil__acao-submit">
                                    <input type="submit" id="acaoSubmit" value="Enviar">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>                            
        </div>
    </section>
</div>
@endsection