@extends('layouts.headhunter') @section('title', 'Alterar Dados') @section('content')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">CADASTROS / MINHA CONTA</h1>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#dados-de-acesso">Dados de Acesso</a></li>
                <li><a data-toggle="tab" href="#meu-plano">Meu Plano</a></li>
                <li><a data-toggle="tab" href="#contrato">Contrato</a></li>
                <li><a data-toggle="tab" href="#usuarios">Usuários</a></li>
                <li><a data-toggle="tab" href="#taxa-sucesso">Taxa de Sucesso</a></li>
            </ul>

            <div class="tab-content well">
                <div id="dados-de-acesso" class="tab-pane fade in active">

                <form id="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-8">
                                <label>Nome Completo*</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-4">
                                <label>CPF*</label>
                                <input class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-2">
                                <label>CEP*</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Endereço*</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Imagem do Perfil*</label>
                                <input type="file" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Número</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-5">
                                <label>Complemento</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Bairro*</label>
                                <input class="form-control">
                            </div>

                        </div>

                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Estado</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Cidade</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Telefone*</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Celular*</label>
                                <input class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Linkedin*</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Facebook*</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Instagram*</label>
                                <input class="form-control">
                            </div>
                        </div> <br>

                        <div class="well-two">

                            <div class="row">
                                <form id="form">
                                <div class="form-group col-md-12">
                                    <label>Como você atuará no marketplace?</label>
                                    <label class="radio-inline">
                                        <input onclick="esconde_form_vinculado_empresa()" type="radio" id="profissional-autonomo" name="tipo-usuario">Profissional Autônomo
                                    </label>
                                    <label class="radio-inline">
                                        <input onclick="exibe_form_vinculado_empresa()" type="radio" id="vinculado-empresa" name="tipo-usuario">Vinculado a Consultoria de RH (empresa)
                                    </label>
                                </div>
                                </form>
                            </div>

                            <div id="form-vinculado-empresa">


                            <div class="row">
                            <div class="form-group col-md-8">
                                <label>Razão Social*</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-4">
                                <label>CNPJ*</label>
                                <input class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <label>Nome Fantasia*</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Imagem do Perfil*</label>
                                <input type="file" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>CEP*</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-8">
                                <label>Endereço*</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Número</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-5">
                                <label>Complemento</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Bairro*</label>
                                <input class="form-control">
                            </div>

                        </div>

                        <div class="row">
                            <div class="form-group col-md-3">
                            <label>Estado*</label>
                            <select class="form-control">
                                <option>Selecione o estado...</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                            </div>
                            <div class="form-group col-md-3">
                            <label>Cidade*</label>
                            <select class="form-control">
                                <option>Escolha sua cidade...</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Telefone*</label>
                                <input class="form-control">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Site*</label>
                                <input class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Segmento*</label>
                                <select class="form-control">
                                    <option>Escolha o segmento...</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Número de Funcionários*</label>
                                <select class="form-control">
                                    <option>Até 50 funcionários</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                              
                    </div>

                

                            </div>                            
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="text-center">Escolha o(s) segmento(s) que deseja atuar como recrutador na E-Headhunter:*</h3> <br>
                                    <div class="form-group col-md-4">
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Comercial e Marketing</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Finanças e Contabilidade</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Supply Chain | Logística</span>
                                        </label>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Publicidade e Propaganda</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Engenharia & Facilities</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Auditoria</span>
                                        </label>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Jurídico</span>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Comunicação</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Tecnologia da Informação</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Manutenção Industrial</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Segurança do Trabalho</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Área da Saúde</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Administração</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Todos Segmentos</span>
                                        </label>
                                        <br>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Recursos Humanos e DP</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Atendimento & Suporte</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Desempenho Empresarial
                                                        </span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Produção & Industrial
                                                        </span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Auditoria e Compliance</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                            <span>Trade & Inteligência de Mercado</span>
                                        </label>
                                        <br>
                                        <label class="left">
                                            <span>+</span>
                                            <input name="segmentos" type="checkbox" class="filled-in" />
                                        </label>
                                        <br>
                                    </div>
                                </div>

                                
                                <div class="row" >
                <div class="col-lg-4 col-centered"><br>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Alterar Senha de Acesso
                        </div>
                        <div class="panel-body">
                                <label>Nova Senha*</label>
                                <input class="form-control"><br>

                                <label>Repita a nova senha*</label>
                                <input class="form-control">
                    </div>
                    </div>
                </div></div>

                                <div class="form-group col-md-12 text-center"><br>
                                <button type="button" class="btn btn-primary btn-lg">Salvar Meus Dados</button>
                            </div>
                        </div>
                    </form>
                </div>
                

                    <div id="meu-plano" class="tab-pane fade">

                        <div class="row">
                            <div class="col-lg-4">
                                Escolha do Plano
                            </div>
                            <div class="col-lg-4">
                                Método de Pagamento
                            </div>
                            <div class="col-lg-4">
                                Detalhes do Faturamento
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                Escolha o Plano E-Headhunter
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">

                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Info Panel
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                    </div>
                                    <div class="panel-footer">
                                        Panel Footer
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Info Panel
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                    </div>
                                    <div class="panel-footer">
                                        Panel Footer
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        Info Panel
                                    </div>
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                    </div>
                                    <div class="panel-footer">
                                        Panel Footer
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="contrato" class="tab-pane fade">

                        <div class="row">

                            <div class="col-lg-12">
                                <div class="well">
                                    <h3>Termo de Contrato</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>

                                <h3>Cancelamento de Conta</h3>

                                <div class="alert alert-danger" role="alert">
                                    <p>Se você tem certeza de que não pretende mais usar o Marketplace E-HeadHunter e deseja excluir sua conta, cuidaremos disto para você. Lembramos que após a confirmação do encerramento você não poderá mais utilizar nossos recursos ou recuperar esta conta assim como os dados e informações nela adicionados.</p>
                                    <br>
                                    
                                    <p>Se ainda assim deseja continuar, clique em <a href="#">Excluir Minha Conta!</a>
                                    <button type="button" class="btn btn-primary btn-lg pull-right">Excluir Minha Conta</button>
                                        <br>

                                    </p>

                                </div>

                            </div>
                        </div>

                    </div>

                    <div id="usuarios" class="tab-pane fade">


                    <div id="col-md-12">
                            <nav class="nav nav-pills flex-column flex-sm-row text-center">
                                <a class="flex-sm-fill text-sm-center nav-link btn-primary" id="submenu-usuarios-gerenciamento-permissoes" href="#" style="float:left;padding:10px;">Gerenciamento de Permissões</a>
                                <a class="flex-sm-fill text-sm-center nav-link btn-primary" id="submenu-usuarios-cadastro-usuarios" href="#" style="float:left; margin: 0 10px; text-align:center;padding:10px;">Cadastro de Usuários</a>
                            </nav><br>

                            <div class="row">
                                <div id="gerenciamento-permissoes">
                                <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Serviços de Marketplace (Plano Básico)
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                    <thead>
                                                        <tr>
                                                            <th>Título do Grupo de Úsuários</th>
                                                            <th>Permissões</th>
                                                            <th>Abas</th>
                                                            <th>Ação</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr class="odd gradeX">
                                                            <td>-</td>
                                                            <td>-</td>
                                                            <td>-</td>
                                                            <td>-</td>
                                                            <td>-</td>
                                                        </tr>

                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalCadGrupoUsuarios">Cadastrar Grupo de Usuários</button>
                                                <div id="modalCadGrupoUsuarios" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                    
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Cadastrar Grupo de Usuários</h4>
                                                            </div>
                                                            <form>
                                                                <div class="modal-body">
                                                                    <p>Título do Grupo de Usuários*</p>
                                                                    <label>
                                                                        <input type="text" class="form-control"><br>
                                                                    </label>

                                                                    <p>Gerenciamento de Permissões (Abas):</p>
                                                                    <label class="">
                                                                        <input name="gerenciamento-permissoes" type="checkbox" class="filled-in" />
                                                                        <span>Cadastro Minha Conta</span>
                                                                    </label><br>
                                                                    <label class="">
                                                                        <input name="gerenciamento-permissoes" type="checkbox" class="filled-in" />
                                                                        <span>Cadastro Cliente</span>
                                                                    </label><br>
                                                                    <label class="">
                                                                        <input name="gerenciamento-permissoes" type="checkbox" class="filled-in" />
                                                                        <span>Cadastro Vaga</span>
                                                                    </label><br>
                                                                    <label class="">
                                                                        <input name="gerenciamento-permissoes" type="checkbox" class="filled-in" />
                                                                        <span>Cadastro Candidato</span>
                                                                    </label><br>
                                                                    <label class="">
                                                                        <input name="gerenciamento-permissoes" type="checkbox" class="filled-in" />
                                                                        <span>Aba Vagas</span>
                                                                    </label><br>
                                                                    <label class="">
                                                                        <input name="gerenciamento-permissoes" type="checkbox" class="filled-in" />
                                                                        <span>Aba Candidatos</span>
                                                                    </label><br>
                                                                    <label class="">
                                                                        <input name="grupos-usuarios" type="checkbox" class="filled-in" />
                                                                        <span>Aba Assessment</span>
                                                                    </label><br>
                                                                    <label class="">
                                                                        <input name="gerenciamento-permissoes" type="checkbox" class="filled-in" />
                                                                        <span>Aba Relatórios</span>
                                                                    </label><br>
                                                                    <label class="">
                                                                        <input name="gerenciamento-permissoes" type="checkbox" class="filled-in" />
                                                                        <span>Aba Mensagens</span>
                                                                    </label>
                                                                </div>
                                                                <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button> <button type="button" class="btn btn-primary" data-dismiss="modal">Salvar</button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                        </div>
                                                    </div>
                                            </div>
                                            
                                        </div>
                                </div>
                                <div id="cadastro-usuarios">
                               
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Listagem dos Usuários
                                        </div>

                                        <div class="panel-body">
                                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Usuário</th>
                                                        <th>Grupo</th>
                                                        <th>E-mail</th>
                                                        <th>Status</th>
                                                        <th>Ação</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr class="odd gradeX">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>

                                                    </tr>
                                                </tbody>
                                            </table>
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalCadNovoUsuario">Cadastrar Novo Usuário</button>
                                                <div id="modalCadNovoUsuario" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                    
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Cadastrar Novo Usuário</h4>
                                                            </div>
                                                            <form role="form">
                                                                <div class="modal-body">

                                                                <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label>Foto do Perfil*</label>
                                                                            <input type="file" class="form-control">
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label>Nome*</label>
                                                                            <input class="form-control">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label>Sobrenome*</label>
                                                                            <input class="form-control">
                                                                        </div>
                                                                    </div>

                                                                    
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label>Email*</label>
                                                                            <input class="form-control">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label>Grupo de Usuários*</label>
                                                                            <select class="form-control">
                                                                            <option>2</option>
                                                                            <option>3</option>
                                                                            <option>4</option>
                                                                            <option>5</option>
                                                                        </select>
                                                                        </div>
                                                                    </div>

                                                                    
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label>Login*</label>
                                                                            <input class="form-control">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label>Senha*</label>
                                                                            <input class="form-control">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button> <button type="button" class="btn btn-primary" data-dismiss="modal">Salvar</button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                        </div>
                                                    </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>


                        </div>


                        
                
                    <div id="taxa-sucesso" class="tab-pane fade">
                        
                        <div id="col-md-12" >
                            <nav class="nav nav-pills flex-column flex-sm-row text-center">
                                <a class="flex-sm-fill text-sm-center nav-link btn-primary" id="submenu-taxa-sucesso-servicos-marketplace" href="#" style="float:left;padding:10px;">Serviços Marketplace<br>Taxa de Sucesso</a>
                                <a class="flex-sm-fill text-sm-center nav-link btn-primary" id="submenu-taxa-sucesso-dados-bancarios" href="#" style="float:left; margin: 0 10px; text-align:center;padding:10px;">Dados Bancário<br> p/ Creditar Taxa de Sucesso</a>
                            </nav>

                            <div class="row">
                                <div id="servicos-marketplace">
                                    1s
                                </div>
                                <div id="dados-bancarios">
                               
                                Dados Bancário<br> p/ Creditar Taxa de Sucesso
                                </div>
                            </div>


                        </div>

                    </div>

                                
                            
                        
            </div>
                    @endsection