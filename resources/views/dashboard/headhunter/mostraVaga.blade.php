@extends('layouts.headhunterHeader')
@section('title', 'Vaga '.urldecode($vaga->cargo))
@section('content')
<div class="col-xs-9">
	<section class="conteudo">
		<div class="conteudo-header">
			<div class="row">
				<div class="col-xs-9">
				</div>
				<div class="col-xs-3">
					<div class="conteudo-header__voltar btn-voltar">
						<div class="conteudo-header__voltar-icone">
							<div class="icn-header-back-p-branco"></div>
						</div>
						<div class="conteudo-header__voltar-texto">Voltar</div>
					</div>
				</div>
			</div>
		</div>
		<div class="conteudo-painel">
			<div class="vagas__marketplace-pg-titulo">
				<h3>Recrute os melhores profissionais da sua network e <br>conquiste o prêmio de R$ 3.500,00.</h3>
			</div>
			<div class="vagas__resul">
				<div class="vagas__resul pointer" onclick="hideInfo()">
					<div class="col-xs-5">
						<div class="row">
							<div class="vagas__marketplace-titulo">
								<div class="agrupa">
									<div class="nome">{{urldecode($vaga->cargo)}}</div>
									<div class="data">{{MysqlToData($vaga->created_at)}}</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="row">
							<div class="vagas__marketplace-local">
								<div class="agrupa">
									<div class="icone">
										<div class="icn-resul-local-p-branco"></div>
									</div>
									<div class="local">
										<span>{{mb_strtoupper(urldecode($vaga->CT_NOME))}}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="row">
							<div class="vagas__marketplace-salario">
								<div class="agrupar">
                                    @if($vaga->naoExibirSalario)
                                        <div>CONFIDENCIAL</div>
                                    @else
								        <div class="">R$ {{intToMoney($vaga->salario)}}</div>
                                    @endif
                                    </div>
							</div>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="row">
							<div class="vagas__marketplace-salarioPct">
								<div class="agrupar">
									<div class="pct">{{$vaga->fee}}</div>
									<div class="descricao">Salário Mês</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="vagas__resul-row">
					<div class="row">
						<div class="col-xs-3">
							<div class="vagas__resul-titulo">
								<h3>Dados da Empresa:</h3>
							</div>
						</div>
						<div class="col-xs-9">
							<div class="vagas__resul-descricao">
								<p>
                                    @if($vaga->sigilo)
                                        Confidencial
                                    @else							
                                        <strong>EMPRESA: </strong>{{$vaga->nome_fantasia}}<br>
                                        <strong>SEGMENTO:</strong> {{urldecode($vaga->segmento)}}<br>
                                        <strong>LOCALIZAÇÃO: </strong>{{mb_strtoupper(urldecode($vaga->CT_NOME))}}/{{$vaga->UF_UF}}<br>
                                    @endif
                                </p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-3">
							<div class="vagas__resul-titulo">
								<h3>Descrição da Função:</h3>
							</div>
						</div>
						<div class="col-xs-9">
							<div class="vagas__resul-descricao">
								<p>
									{{$vaga->principais_atividades}}
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-3">
							<div class="vagas__resul-titulo">
								<h3>Perfil 	&amp; Exigências:</h3>
							</div>
						</div>
						<div class="col-xs-9">
							<div class="vagas__resul-descricao">
								<p><strong>FORMAÇÃO DESEJADA:</strong> {{$vaga->formacao_exigida}}</p>
								<p>
									<strong>REQUISITOS DESEJÁVEIS:</strong> {{$vaga->requisitos}}
								</p>
								<p>
								<strong>CONHECIMENTOS E APTIDÕES NECESSÁRIAS:</strong>
                                @foreach($aptidoes as $aptidao)
                                    </p>									
                                        {{$aptidao->aptidoes}} - {{nivelAptidao($aptidao->nivel_requerido)}}                                        
                                    </p>
                                @endforeach
                                <p><strong>IDIOMAS:</strong></p>    
                                @foreach($idiomas as $idioma)
                                    <p>{{mb_strtoupper($idioma->idioma)}} - {{nivelIdioma($idioma->nivel)}}</p>
                                @endforeach
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-3">
							<div class="vagas__resul-titulo">
								<h3>Salário &amp; Benefícios:</h3>
							</div>
						</div>
						<div class="col-xs-9">
							<div class="vagas__resul-descricao">
								<p><strong>SALÁRIO:</strong> R$ {{intToMoney($vaga->salario)}}</p>
								<p><strong>BENEFICIOS:</strong> {{$vaga->beneficios}}</p>
								<p><strong>COMISSÃO:</strong> {{$vaga->comissao}} </p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="vagas__mensagens">
				<div class="vagas__mensagens-abas">
					<ul class="nav nav-tabs">
						<li class="active">
							<a data-toggle="tab" href="!#encaminhar" class="vagas__mensagens-abas-encaminhar">
								<div class="agrupar">
									<div class="icone">
										<div class="icn-conecta-menu-m-branco"></div>
									</div>
									<div class="descricao">Encaminhar</div>
								</div>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="!#alerta" class="vagas__mensagens-abas-alerta">
								<div class="agrupar">
									<div class="icone">
										<div class="icn-aba-sino-branco"></div>
									</div>
									<div class="descricao">Alerta Radar</div>
								</div>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="!#submeter" class="vagas__mensagens-abas-submeter" onclick="$('#mostrarSubmeter').show()">
								<div class="agrupar">
									<div class="icone">
										<div class="icn-aba-icon-circ-branco"></div>
									</div>
									<div class="descricao">Submeter</div>
								</div>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="!#status" class="vagas__mensagens-abas-status">
								<div class="icone">
									<div class="icn-aba-excl-branco"></div>
								</div>
								<div class="descricao">Status</div>
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div id="encaminhar" class="tab-pane fade in active">
							<div class="vagas__mensagens-box">
								<form class="form-perfil" action="">	
									<div class="form-perfil__conteudo">
										<div class="row form-perfil__row">
											<div class="col-xs-12">
												<label for="destinatario" class="form-perfil__label">Para</label>
												<select name="" class="form-perfil__input" id="">
												<option value="ID">nome</option>	 
											</div>
										</div>
										<div class="row form-perfil__row">
											<div class="col-xs-12">
												<label for="email" class="form-perfil__label">E-mail</label>
												<input type="email" class="form-perfil__input" id="email">
											</div>
										</div>
										<div class="row form-perfil__row">
											<div class="col-xs-12">
												<label for="email" class="form-perfil__label">Mensagem</label>
												<textarea name=""  class="form-perfil__input" id="" cols="30" rows="10"></textarea>
											</div>
										</div>                                                        
										<div class="row form-perfil__row">
											<div class="col-xs-12">
												<div class="form-perfil__acao">
													<div class="form-perfil__acao-submit">
														<input type="submit" id="acaoSubmit" value="Enviar">
													</div>
													<!-- <div class="form-perfil__acao-cancelar">
														<a href="">Cancelar</a>
													</div> -->
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div id="alerta" class="tab-pane fade">
							<div class="vagas__mensagens-box">
								<form class="form-perfil" id="form-alerta" action="" method="post">
									@csrf
									<div class="form-perfil__conteudo">
										<div class="row form-perfil__row">
											<div class="col-xs-8">
												<div class="form-perfil-descricao">
													<span>Gostaria de receber alertas quando a empresa contratante alterar o status dessas vagas?</span>
												</div>
											</div>
											<div class="col-xs-4">							
											<input type="radio" name="alertaStatusVaga"  value="1" id="alertsim" {{isset($alertaStatus->recebeAlerta) ? 'checked' : ''}}><label for="alertsim">Sim</label> 
												<input type="radio" name="alertaStatusVaga"  value="0" id="alertnao" {{!isset($alertaStatus->recebeAlerta) ? 'checked' : ''}}  ><label for="alertnao">Não</label>
												
												<input type="hidden" name="idvaga" value="{{$vaga->id}}" >
												<input type="hidden" name="salvar-alerta-vaga" value="0">
												<input type="hidden" name="headhunter" value="{{$user->id}}">												
												@if(sizeof($hhAlertas))
													<input type="hidden" name="editaAlerta" value="{{sizeof($hhAlertas)}}">										
												@endif
											</div>
										</div>
										<div class="row form-perfil__row">																						
												@foreach($alertas as $alerta)
													@continue($alerta->id == 1)
													@php $checked = contaAlerta($alerta->id, $user->id, $vaga->id) ? 'checked' : '' @endphp
													<input {{$checked}} id="alerta{{$alerta->id}}" name="alerta[]" type="checkbox" value="{{$alerta->id}}"><label for="alerta{{$alerta->id}}">{{urldecode($alerta->alerta)}}</label><br>
												@endforeach
											<!-- <input type="checkbox"> Me alerte quando a empresa solicitar agenda de entrevista. <br>
											<input type="checkbox"> Me alerte quando essa vaga for finalizada. -->
										</div>                                                       
										<div class="row form-perfil__row">
											<div class="col-xs-12">
												<div class="form-perfil__acao">
													<div class="form-perfil__acao-submit">
														<input type="submit" id="acaoSubmit" onclick="alertaHeahHunter()" value="Salvar">
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div id="status" class="tab-pane fade">
							<div class="vagas__mensagens-box">sds</div>
						</div>
						<div id="submeter" class="fade">
							<div id="mostrarSubmeter" style="display: none">
                               @include('dashboard.headhunter.submeter')
						   </div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>
</div>	
@endsection