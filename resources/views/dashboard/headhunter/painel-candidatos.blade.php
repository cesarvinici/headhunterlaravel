@extends('layouts.headhunterHeader')
@section('title', 'Painel de Candidatos')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Pesquisar Candidato</h2>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__voltar btn-voltar-cad">
                        <div class="conteudo-header__voltar-icone">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto ">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <div class="box-consulta">
                <div class="box-consulta__box">
                    <form action="/dashboard/headhunter/painel-candidatos" id="filtro-painel-candidatos" method="post">
                        @csrf
                        <div class="row box-consulta__box-row">
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <select name="funcao" id="funcao">
                                        <option value="">FUNÇÃO</option>
                                        <option value="">Valor 02</option>
                                        <option value="">Valor 03</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <div class="titulo">PRETENSÃO A PARTIR</div>
                                    <div id="range-slider"></div>
                                    <div class="valor">
                                        <input type="text" class="" id="amount" readonly name="pretensaoSalarial" style="border:0;font-weight:bold;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <select name="areaAtua" id="areaAtua">
                                        <option value="">CONTRATO</option>
                                        <option value="">Valor 02</option>
                                        <option value="">Valor 03</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <select name="formacao" id="funcao">
                                        <option value="">FORMAÇÃO</option>
                                        @foreach($escolaridade as $form)
                                            <option value="{{$form->id}}">{{urldecode($form->nivel)}}</option>
                                        @endforeach                                         
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row box-consulta__box-row">
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <select name="estado" id="estado" onchange="carregaCidades(this, 1)" class="form-perfil__select-col02 listaEstados">
                                        <option selected value="">ESTADO</option>
                                        @foreach($estados as $estado)
                                            <option value="{{$estado->UF_ID}}">{{$estado->UF_NOME}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <select id="listaCidades" name="cidade" class="form-perfil__input-col02">
                                        <option value="">CIDADES</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <select name="idioma" id="idioma" class="form-perfil__select-col02 listaEstados">
                                        <option selected value="">IDIOMA</option>
                                        @foreach($idiomas as $idioma)
                                            <option value="{{$idioma->id}}">{{$idioma->idioma}}</option>
                                        @endforeach   
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="box-consulta__box-filtro">
                                    <select name="dispViagem" id="dataAnun">
                                        <option value="">DISPONIBILIDADE VIAGEM</option>
                                        <option value="0">NÃO</option>
                                        <option value="1">SIM</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row box-consulta__box-row">
                            <div class="col-xs-offset-9 col-xs-3">
                                <div class="box-consulta__box-submit">
                                    <input type="submit" name="submitFiltro" value="Buscar">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @include('dashboard.headhunter.listaCandidatos')
        </div>
    </section>
</div>
@endsection