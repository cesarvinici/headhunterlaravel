@extends('layouts/headhunterHeader')
@section('title', 'Dashboard')
@section('content')
<div class="col-xs-9">
	<section class="conteudo">
		<div class="conteudo-header">
			<div class="row">
				<div class="col-xs-9">
					<div class="conteudo-header__titulo">
						<h2>Vagas</h2>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="conteudo-header__voltar btn-voltar-cad">
						<div class="conteudo-header__voltar-icone">
							<div class="icn-header-back-p-branco"></div>
						</div>
						<div class="conteudo-header__voltar-texto">Voltar</div>
					</div>
				</div>
			</div>
		</div>
		<div class="conteudo-painel">
			<div class="vagas">
				<div class="vagas__notificaoes">
					<div class="row">
						<div class="col-xs-offset-1 col-xs-2">
							<div class="vagas__notificaoes-box">
								<div class="titulo">Minhas Vagas</div>
								<div class="icone">
									<div class="icn-mala-m-preto"></div>
								</div>
								<div class="numero">{{$minhasVagas}}</div>
							</div>
						</div>
						<div class="col-xs-2">
							<div class="vagas__notificaoes-box">
								<div class="titulo">Convites Novos</div>
								<div class="icone">
									<div class="icn-presente-m-verde"></div>
								</div>
								<div class="numero">{{$entrada}}</div>
							</div>
						</div>
						<div class="col-xs-2">
							<div class="vagas__notificaoes-box">
								<div class="titulo">Convites Enviados</div>
								<div class="icone">
									<div class="icn-acesso-m-azul"></div>
								</div>
								<div class="numero">02</div>
							</div>
						</div>
						<div class="col-xs-2">
							<div class="vagas__notificaoes-box">
								<div class="titulo">Histórico <br>Cdds Submetidos</div>
								<div class="icone">
									<div class="icn-cv-m-cinza"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-2">
							<div class="vagas__notificaoes-box">
								<div class="titulo">Meus Alertas</div>
								<div class="icone">
									<div class="icn-sino-m-vermelho"></div>
								</div>
								<div class="numero">{{$alertas}}</div>
							</div>
						</div>
					</div>
				</div>
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#marketplace">PESQUISA MARKETPLACE</a></li>
					<li><a data-toggle="tab" href="#minhasVagas">PESQUISA MINHAS VAGAS</a></li>
				</ul>
				<div class="tab-content">
					<div id="marketplace" class="tab-pane fade in active">
						<div class="box-consulta">
							<div id="formMarketplace" class="box-consulta__box">
								<form id="FormVagasMKtPlace" action="">
									@csrf
									<div class="row box-consulta__box-row">
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<input type="text" style="height: 64px; width: 100%;" name="palavraChave" placeholder="PALAVRA CHAVE">
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<div class="titulo">MÉDIA SALARIAL</div>
												<div id="range-sliderMKT"></div>
												<div class="valor">
													<input type="text" name="salario" id="amountMKT" style="border:0;font-weight:bold;">
												</div>
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<select name="areaAtuacao" id="areaAtua">
													<option value="">ÁREA DE ATUAÇÃO</option>
													@foreach($segmentos as $segmento)
														<option value="{{$segmento->id}}" >{{urldecode($segmento->segmento)}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<select name="taxarecut" id="funcao">
													<option value="">TX RECRUTAMENTO</option>													
													@foreach($fee as $taxa)
														<option value="{{$taxa->id}}">{{$taxa->fee}} DO SALÁRIO DE CONTRATAÇÃO DO CANDIDATO</option>
													@endforeach													                                                    
												</select>
											</div>
										</div>
									</div>
									<div class="row box-consulta__box-row">
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<input type="text" style="height: 64px; width: 100%;" name="empresa" placeholder="EMPRESA">
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<input type="text" autocomplete="off" id="tituloCargo" style="height: 64px; width: 100%;" name="funcao" placeholder="FUNÇÃO">
												<input type="hidden" name="idCargo" value="" id="">
												<p style="color: red" class="empty-message"></p>
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<input type="text" style="height: 64px; width: 100%;" name="localidade" placeholder="CIDADE">
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-submit">
												<input type="hidden" name="filtro" value="filtro">
												<input type="submit" onclick="filtroVagasMktPlace()" value="Buscar">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div id="listaVagasmktplace">
							@include('dashboard.headhunter.listaVagas')
						</div>
					</div>
					<div id="minhasVagas" class="tab-pane fade">
						<div class="box-consulta">
							<div class="box-consulta__box">
								<form id="FormVagasInternas" action="">
									@csrf
									<div class="row box-consulta__box-row">
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<input type="text" style="height: 64px; width: 100%;" name="palavraChave" placeholder="PALAVRA CHAVE">
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<div class="titulo">MÉDIA SALARIAL</div>
												<div id="range-sliderInt"></div>
												<div class="valor">
													<input type="text" name="salario" id="amountInt" style="border:0;font-weight:bold;">
												</div>
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<select name="areaAtuacao" id="areaAtua">
													<option value="">ÁREA DE ATUAÇÃO</option>
													@foreach($segmentos as $segmento)
														<option value="{{$segmento->id}}" >{{urldecode($segmento->segmento)}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<select name="taxarecut" id="funcao">
													<option value="">TX RECRUTAMENTO</option>
																						
													@foreach($fee as $taxa)
														<option value="{{$taxa->id}}">{{$taxa->fee}} DO SALÁRIO DE CONTRATAÇÃO DO CANDIDATO</option>
													@endforeach													                                                    
												</select>
											</div>
										</div>
									</div>
									<div class="row box-consulta__box-row">
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<input type="text" style="height: 64px; width: 100%;" name="empresa" placeholder="EMPRESA">
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<input type="text" style="height: 64px; width: 100%;" name="funcao" placeholder="FUNÇÃO">
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-filtro">
												<input type="text" style="height: 64px; width: 100%;" name="localidade" placeholder="CIDADE">
											</div>
										</div>
										<div class="col-xs-3">
											<div class="box-consulta__box-submit">
												<input type="hidden" name="filtro" value="filtro">
												<input type="submit" onclick="filtroVagasInternas()" value="Buscar">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div id="listaVagasInternas">
							@include('dashboard.headhunter.listaVagasInternas')
						</div>						
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


@endsection