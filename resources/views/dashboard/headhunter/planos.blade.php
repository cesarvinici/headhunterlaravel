@extends("layouts.headhunterHeader")
@section('title', 'Planos')
@section('content')
<div class="col-xs-9">
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Planos</h2>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__voltar btn-voltar">
                        <div class="conteudo-header__voltar-icone">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content__main">
            <div class="content__main">
                <form action="">
                    <div class="planos">
                        <div class="planos__conteudo">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="planos__coluna">
                                        <div class="planos__nome">
                                            <h3>...</h3>
                                        </div>
                                        <div class="planos__titulo">
                                            <span>Produtos</span>
                                        </div>
                                        <div class="planos__descricao">
                                            <span>Principais Diferenças Entre os Produtos</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Nº de Contas de Usuários</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Dashboard Marketplace</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Gestão de Clientes</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Acesso ao Banco de Candidatos</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Gestão de Processos</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Duração</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Preço</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="planos__coluna">
                                        <div class="planos__nome">
                                            <h3>TX Sucesso</h3>
                                        </div>
                                        <div class="planos__titulo">
                                            <span>Plano Básico</span>
                                        </div>
                                        <div class="planos__descricao">
                                            <span>Para profissionais independentes que desejam usar nosso sistema.</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Nº de Contas de Usuários</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Dashboard Marketplace</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Gestão de Clientes</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Acesso ao Banco de Candidatos</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Gestão de Processos</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Duração</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Preço</span>
                                        </div>
                                        <div class="planos__button">
                                            <span>Meu Plano</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="planos__coluna">
                                        <div class="planos__nome">
                                            <h3>TX Fixa</h3>
                                        </div>
                                        <div class="planos__titulo">
                                            <span>Plano Profissional</span>
                                        </div>
                                        <div class="planos__descricao">
                                            <span>Oferta TX fixa para empresas de consultorias ou Headhunters que pretendem gerenciar seus clientes, candidatos e processos.</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Nº de Contas de Usuários</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Dashboard Marketplace</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Gestão de Clientes</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Acesso ao Banco de Candidatos</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Gestão de Processos</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Duração</span>
                                        </div>
                                        <div class="planos__subtitulo">
                                            <span>Preço</span>
                                        </div>
                                        <div class="planos__button">
                                            <span>UPGRADE<br> Contratar</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="termos">
                        <div class="termos__conteudo">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="termos__texto">
                                        <p>
                                            TERMOS CONTRATUAIS:<br>
                                            Eu realizei contato com o candidato por telefone, e-mail ou entrevista, e ele(a) me autorizou a direcionar seu cv e informações adicionais para
                                            essa vaga. Acordei com o candidato (a) que não poderá ser encaminhado por outro HH para essa vaga e nem poderá estabelecer contato
                                            direto com a empresa contratante.
                                            Eu li e aceito os “termos e garantia” desse processo seletivo . Estou de acordo com a taxa de recrutamento fixada pela empresa contratante. O
                                            meu aceite implicará na assinatura eletrônica do contrato de prestação de serviço entre a empresa requisitante e a empresa que represento.
                                            Caso o cdd acima venha a ser contratado por essa empresa, ao termino da garantia emitirei uma nota de serviço.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="termos__checkbox">
                                        Li os termos e aceito. <input type="checkbox">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="termos__button">
                                        <input type="submit" value="Prosseguir">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection