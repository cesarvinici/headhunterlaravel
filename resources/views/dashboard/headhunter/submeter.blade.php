<div class="conteudo-painel">
    <div class="col-xs-11">
        <div class="row">
            <div class="vagas__abas">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#filaCand">Fila de Candidatura</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#candPartic" onclick="carregaCddParticipantes({{$vaga->id}})">Candidatos Participantes</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#anexosCdd" onclick="carregaAnexoCddParticipantes({{$vaga->id}})">Anexos Cdd Participantes</a>
                    </li>
                    <!--  <li>
                           <a data-toggle="tab" href="#manutVag">Manutenção da Vaga</a>
                       </li> -->
                </ul>
            </div>
            <div class="tab-content">
                <div id="filaCand" class="tab-pane fade in active">
                    <form action="" id="formFiltroFilaCdd">
                        @csrf
                        <input type="hidden" name="classe" value="1">
                        <input type="hidden" name="idvaga" id="idvaga" value="{{$vaga->id}}">
                        <div class="aba-header">
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="radio" id="todos" checked name="candidatos" value='0'><label for="todos">Todos</label>
                                    <input type="radio" id="candidatos" name="candidatos" value='1'><label for="candidatos">Meus Candidatos</label>
                                </div>
                                <div class="col-xs-6">
                                    <input type="search" name="pessoas" placeholder="Procurar por pessoas">
                                </div>
                            </div>
                        </div>
                        <div class="aba-filtro">
                            <div class="aba-filtro-input">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="aba-resul-row">
                                            <select name="funcao" id="">
                                                <option value="">Função</option>
                                                <option value="">Valor 01</option>
                                                <option value="">Valor 02</option>
                                            </select>
                                        </div>
                                        <div class="aba-resul-row">
                                            <select name="formacao" id="">
                                                <option value="" selected>Formação</option>
                                                @foreach($escolaridade as $escol)
                                                    <option value="{{$escol->id}}">{{urldecode($escol->nivel)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="aba-resul-row">
                                            <select name="estado" class="listaEstados" onchange="carregaCidades(this, 1)">
                                                <option value="" selected>Estado</option>
                                                @foreach($estados as $estado)
                                                    <option value="{{$estado->UF_ID}}">{{$estado->UF_NOME}}</option>
                                                @endforeach                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="aba-resul-row">
                                            <select name="funcao" id="">
                                                <option value="">Avaliação Curricular</option>
                                                <option value="">Valor 01</option>
                                                <option value="">Valor 02</option>
                                            </select>
                                        </div>
                                        <div class="aba-resul-row">
                                            <select name="sexo" id="">
                                                   <option value="">Sexo</option>
                                                   <option value="">Valor 01</option>
                                                   <option value="">Valor 02</option>
                                               </select>
                                        </div>
                                        <div class="aba-resul-row">
                                            <select name="cidade" id="listaCidades">
                                                   <option value="" selected>Cidade</option>
                                                   <option disabled>Selecione um estado</option>       
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="aba-filtro-consultar">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="consultar">
                                            <input type="submit" value="Consultar" onclick="filtrofilaGeral()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="resul__header">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="resul__header-numero ">
                                <p><span id="resultadosEnc">{{sizeof($candidatos)}}</span> resultados encontrados</p>
                                </div>
                            </div>
                            <!-- <div class="col-xs-6">
                                   <div class="resul__header-filtro">
                                       <p>Organizar por:
                                           <span class="data">Score</span> |
                                           <span class="cargo">Data</span> |
                                           <span class="valor">Ordem Alfabética</span>
                                           <span class="valor">Status</span>
                                       </p>
                                   </div>
                               </div> -->
                        </div>
                    </div>
                    <div class="resul__grupo filaCandidaturaGeral">
                       @include('dashboard.headhunter.filaGeral')
                    </div>
                    <div class="resul__footer">
                        <div class="paginacao"></div>
                    </div>
                </div>
                <div id="candPartic" class="tab-pane fade in">
                    <form action="" id="formFiltroCddParticipante">                            
                        @csrf
                        <input type="hidden" name="classe" value="1">
                        <input type="hidden" name="idvaga" id="idvaga" value="{{$vaga->id}}">
                        <div class="aba-header">
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="radio" id="todos1" checked name="candidatos" value='0' ><label for="todos1" >Todos</label>
                                    <input type="radio" id="candidatos1" name="candidatos" value='1' ><label for="candidatos1">Meus Candidatos</label> 
                                </div>
                                <div class="col-xs-6">
                                    <input type="search" name="pessoas" placeholder="Procurar por pessoas">
                                </div>
                            </div>
                        </div>
                        <div class="aba-filtro">
                            <div class="aba-filtro-input">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="aba-resul-row">
                                            <select name="funcao" id="">
                                                    <option value="">Função</option>
                                                    <option value="">Valor 01</option>
                                                    <option value="">Valor 02</option>
                                            </select>
                                        </div>
                                        <div class="aba-resul-row">
                                            <select name="formacao" id="">
                                                <option value="" selected>Formação</option>
                                                @foreach($escolaridade as $escol)
                                                    <option value="{{$escol->id}}">{{urldecode($escol->nivel)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="aba-resul-row">
                                            <select name="estado" class="listaEstados" onchange="carregaCidades(this, 2)">
                                                <option value="" selected>Estado</option>
                                                @foreach($estados as $estado)
                                                    <option value="{{$estado->UF_ID}}">{{$estado->UF_NOME}}</option>
                                                @endforeach                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="aba-resul-row">
                                            <select name="funcao" id="">
                                                    <option value="">Avaliação Curricular</option>
                                                    <option value="">Valor 01</option>
                                                    <option value="">Valor 02</option>
                                                </select>
                                        </div>
                                        <div class="aba-resul-row">
                                            <select name="sexo" id="">
                                                    <option value="">Sexo</option>
                                                    <option value="">Valor 01</option>
                                                    <option value="">Valor 02</option>
                                                </select>
                                        </div>
                                        <div class="aba-resul-row">
                                            <select name="cidade" id="listaCidades2">
                                                    <option value="" selected>Cidade</option>
                                                    <option disabled>Selecione um estado</option>       
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="aba-filtro-consultar">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="consultar">
                                            <input type="submit" value="Consultar" onclick="filtrofilaCddParticipantes()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="resul__header">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="resul__header-numero">
                                <p><span id="resultadosFilaPartic">0</span> resultados encontrados</p>
                                </div>
                            </div>
                            <!--  <div class="col-xs-6">
                                       <div class="resul__header-filtro">
                                           <p>Organizar por:
                                               <span class="data">Score</span> |
                                               <span class="cargo">Data</span> |
                                               <span class="valor">Ordem Alfabética</span>
                                               <span class="valor">Status</span>
                                           </p>
                                       </div>
                                   </div> -->
                        </div>
                    </div>
                        <div class="resul__grupo filtrofilaParticipantes">
                            @include('dashboard.headhunter.filaGeral')
                    </div>
                    <div class="resul__footer">
                        <div class="paginacaoParticipantes">
                            <!--  <span>Pág. 1 | 2 | 3 ></span> -->
                        </div>
                    </div>
                </div>
                <div id="anexosCdd" class="tab-pane fade in">
                    <form action="" id="form-cdd-anexos">
                        @csrf
                        <div class="aba-header">
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="radio" name="candidatos" value="0"  checked id="todos3"><label for="todos3">Todos</label>
                                    <input type="radio" name="candidatos" value="1" id="meus3"><label for="meus3">Meus Candidatos</label>
                                    <input type="hidden" name="idvaga" value="{{$vaga->id}}">
                                </div>
                                <div class="col-xs-6">
                                    <input type="search" name="pessoas" placeholder="Procurar por pessoas">
                                </div>
                            </div>
                        </div>
                        <div class="aba-filtro">
                            <div class="aba-filtro-input">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="aba-resul-row">
                                            <select name="funcao" id="">
                                                <option value="">Função</option>
                                                <option value="">Valor 01</option>
                                                <option value="">Valor 02</option>
                                            </select>
                                        </div>
                                        <div class="aba-resul-row">
                                                <select name="formacao" id="">
                                                       <option value="" selected>Formação</option>
                                                        @foreach($escolaridade as $escol)
                                                            <option value="{{$escol->id}}">{{urldecode($escol->nivel)}}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                            <div class="aba-resul-row">
                                                <select name="estado" class="listaEstados" onchange="carregaCidades(this, 3)">
                                                    <option value="" selected>Estado</option>
                                                    @foreach($estados as $estado)
                                                        <option value="{{$estado->UF_ID}}">{{$estado->UF_NOME}}</option>
                                                    @endforeach                                                
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="aba-resul-row">
                                                <select name="funcao" id="">
                                                       <option value="">Avaliação Curricular</option>
                                                       <option value="">Valor 01</option>
                                                       <option value="">Valor 02</option>
                                                   </select>
                                            </div>
                                            <div class="aba-resul-row">
                                                <select name="sexo" id="">
                                                       <option value="">Sexo</option>
                                                       <option value="">Valor 01</option>
                                                       <option value="">Valor 02</option>
                                                   </select>
                                            </div>
                                            <div class="aba-resul-row">
                                                <select name="cidade" id="listaCidades3">
                                                       <option value="" selected>Cidade</option>
                                                       <option disabled>Selecione um estado</option>       
                                                </select>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="aba-filtro-consultar">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="consultar">
                                            <input type="submit" value="Consultar" onclick="filtroAnexoCdd()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="resul__header">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="resul__header-numero">
                                    <p><span id="resultAnexoCddEnc">{{sizeof(carregaCandidatosdaFila($vaga->id))}}</span> resultados encontrados</p>
                                </div>
                            </div>
                            <!--  <div class="col-xs-6">
                                                   <div class="resul__header-filtro">
                                                       <p>Organizar por:
                                                           <span class="data">Score</span> |
                                                           <span class="cargo">Data</span> |
                                                           <span class="valor">Ordem Alfabética</span>
                                                           <span class="valor">Status</span>
                                                       </p>
                                                   </div>
                                               </div> -->
                        </div>
                    </div>
                    <div class="resul__grupo">
                            <div class="resul__grupo-inf">
                                    <div class="resul__grupo-inf-usuario">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <th class="cell-titulo"><span>Candidatos <br>Participantes</span></th>
                                                    <th class="cell-titulo" colspan="2"><span>Questionário <br>Pré-seleçao</span></th>
                                                    <th class="cell-titulo" colspan="2"><span>Avaliação <br>Disc</span></th>
                                                    <th class="cell-titulo" colspan="2"><span>Certificados</span></th>
                                                    <th class="cell-titulo" colspan="2"><span>Outros <br>Documentos</span></th>
                                                </tr>               
                                            <tbody id="anexos">
                                                @include('dashboard.headhunter.anexosFila')
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                    {{-- <div class="resul__footer">
                        <div class="paginacaoAnexos">
                            <span>Pág. 1 | 2 | 3 ></span>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-1">
    <div class="row">
        <div class="vagas__boxs-lateral">
            <div class="vagas__boxs-lateral-row">
                <div class="vagas__boxs-lateral-box-01">
                    <div class="box-numero">
                        <span>10</span>
                    </div>
                    <div class="box-descricao">
                        <span>Candidaturas</span>
                    </div>
                </div>
            </div>
            <div class="vagas__boxs-lateral-row">
                <div class="vagas__boxs-lateral-box-02">
                    <div class="box-numero">
                        <span>10</span>
                    </div>
                    <div class="box-descricao">
                        <span>Candidatos Participantes</span>
                    </div>
                </div>
            </div>
            <div class="vagas__boxs-lateral-row">
                <div class="vagas__boxs-lateral-box-03">
                    <div class="box-numero">
                        <span>02</span>
                    </div>
                    <div class="box-descricao">
                        <span>Ofertas</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>