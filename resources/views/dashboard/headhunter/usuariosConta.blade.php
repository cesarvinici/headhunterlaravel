@extends('layouts.headhunterHeader')
@section('title', 'Usuários da conta')
@section('content')
<div class="col-xs-9">
	<div style="margin-top:10px">
        @if(Session::has('cad_user_ok'))
        <div class="alert alert-success">
            <ul>
                <li>
                    {{Session::get('cad_user_ok')}}
                </li>
            </ul>			
        </div>
        @endif
        @if(Session::has('cad_user_fail'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{Session::get('cad_user_fail')}}</li>
                </ul>
                
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <section class="conteudo">
        <div class="conteudo-header">  

        {{-- <div class="row">            
            <div class="cold-md-12">
                <p class="text-danger bg-danger text-center" style="display: inline-block">Atenção, antes de cadastrar um novo usuário é necessário cadastrar uma empresa.</p>
            </div>               
        </div> --}}
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Usuário</h2>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__voltar btn-voltar-cad">
                        <div class="conteudo-header__voltar-icone">
                            <div class="icn-header-back-p-branco"></div>
                        </div>
                        <div class="conteudo-header__voltar-texto ">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        @if(sizeof($usuarios))
            <div class="usuarios">                  
                <div class="usuarios__tabela">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th colspan="2" style="width:10%;">Ação</th>
                                </tr>
                            </thead>
                            <tbody>                                
                                    @foreach($usuarios as $usuario)
                                        <tr>
                                            <td>{{$usuario->nome}}</td>
                                            <td>{{$usuario->email}}</td>
                                            <td class="usuarios__tabela-editar"><a href="/dashboard/headhunter/usuarios/{{$usuario->id}}/edit"><i class="fa fa-edit"></i></a></td>
                                            <td class="usuarios__tabela-fechar" onclick="excluirHeadHunterUser({{$usuario->id}})"><i class="fa fa-close"></i></td>
                                        </tr>     
                                    @endforeach                                                    
                            </tbody>
                        </table>
                    </div>
                </div>                    
            </div>
        @endif 
        <div class="conteudo-painel">
            @if(isset($user->id))
                <div class="col-xs-9" style="margin-bottom:10px">
                    <div class="conteudo-header__titulo" >
                        <h2>Editar Usuário</h2>
                    </div>
                </div>
                @include('dashboard.headhunter.editusuariosconta')
            @else
                <div class="col-xs-9" style="margin-bottom:10px">
                    <div class="conteudo-header__titulo">
                        <h2>Adicionar Usuário</h2>
                    </div>
                </div>
                @include('dashboard.headhunter.addusuarioconta')
            @endif
        </div>
    </section>
</div>
@endsection