@extends('layouts.headhunterHeader')
@section('title', 'Consulta Vagas')
@section('content')

<div class="col-xs-9">
        <div class='row'>
                @if(Session::has('cad_vaga_ok'))
                    <div class="alert alert-success">
                        {{Session::get('cad_vaga_ok')}}
                    </div>
                @endif
        </div>
    <section class="conteudo">
        <div class="conteudo-header">
            <div class="row">
                <div class="col-xs-9">
                    <div class="conteudo-header__titulo">
                        <h2>Consultar Vagas</h2>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="conteudo-header__voltar-icone">ICN</div>
                    <div class="conteudo-header__voltar btn-voltar-cad">
                        <div class="conteudo-header__voltar-texto">Voltar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-painel">
            <div class="consulta">
                <div class="consulta__filtro">
                    <div class="convite__secao-search">
                        <div class="consulta__filtro-row">
                            <div class="col-xs-6">
                                <div class="row">
                                    <input name="cod" type="text" placeholder="Código da Vaga">
                                    <button  class="filtroVaga" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="row">
                                    <input name="cargo" type="text" placeholder="Função">
                                    <button  class="filtroVaga" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="consulta__filtro-row">
                            <div class="col-xs-6">
                                <div class="row">
                                    <input type="text" name="empresa" placeholder="Cliente">
                                    <button  class="filtroVaga" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="row">
                                    <input type="text" placeholder="Status da Vaga" name="status">
                                    <button  class="filtroVaga"  type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="consulta__filtro-row">
                            <div class="col-xs-6">
                                <div class="row">
                                    <input type="text" name="cidade" placeholder="Cidade">
                                    <button  class="filtroVaga" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="row">
                                    <input type="text" placeholder="Tipo" name="regime">
                                    <button  class="filtroVaga" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="consulta__filtro-resul">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="resul__header-numero">
                                    <p><span class="resultEnc">2</span> resultados encontrados</p>
                                </div>
                            </div>
                            <!-- <div class="col-xs-6">
                                <div class="resul__header-filtro">
                                    <p>Organizar por: <span class="cidade">Nome Fantasia</span> | <span class="alfabetica">Código</span> | <span class="valor">Razão Social</span></p>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="consulta__tabela">
                        <div class="table-responsive tabelaManutVagas">
                            <table class="table tabelaVagas tablesorter">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Função</th>
                                        <th>Cliente</th>
                                        <th>Status</th>
                                        <th>Cidade</th>
                                        <th>Tipo</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="tbody">
                                    @foreach($vagas  as $vaga)
                                        <tr>
                                            <td>{{$vaga->id}}</td>
                                            <td>{{urldecode($vaga->cargo)}}</td>
                                            <td>{{$vaga->nome_fantasia}}</td>
                                            <td>
                                                <select class="form-control" onchange="alteraStatus({{$vaga->id}}, this)">
                                                    @foreach($status as $stat)
                                                        {{$selected = $stat->id == $vaga->status_vaga ? "selected" : ""}}
                                                        <option {{$selected}} value="{{$stat->id}}">{{$stat->status_vaga}}</option>
                                                    @endforeach                                                    
                                                </select>
                                            </td>
                                            <td>{{mb_strtoupper(urldecode($vaga->cidade))}}</td>
                                            <td>{{mb_strtoupper(urldecode($vaga->regime))}}</td>
                                            <td><a href="{{route('edit.vagasHH', ['id' => $vaga->id])}}"><i class="fa fa-external-link pointer"></i></a>
                                                <img style="margin-left: 5px" class="pointer" onclick="deletaVaga({{$vaga->id}})" src="{{asset('admin/imgs/icons/delete.png')}}">
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


@endsection