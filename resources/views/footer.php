<!-- include  -->
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo"><img src="../assets/img/icones/Layer1020.png" alt=""></div>
                </div>
                <div class="col-md-3">
                    <div class="contato">
                        +55 85 3045 8555</br>
                        Av. Desembargador Moreira, 2120</br>
                        Aldeota, Fortaleza - Ceará / Brasil
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="news">
                        <span>Receba novidades da HEADHUNTER</span>
                        <form id="newsletter">
                            <input type="email" placeholder="Digite seu e-mail aqui"></br>
                            <input type="submit" value="Enviar">
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="atendimento">
                        Horário de Atendimento</br>
                        Seg a Sex das 08:00hs às 18:00hs</br>
                        comercial@e-headhunter.com.br
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="../lib/bootstrap/js/bootstrap.js"></script>
</body>
</html>
