<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('admin/css/jquery-ui.css')}}">
    <link href="{{asset('admin/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
    <!-- font awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="{{asset('admin/js/tablesorter/themes/blue/style.css')}}">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        @yield('content')
      </div>
    </div>            
    @yield('scripts')
    <footer class="footer"></footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
    <!-- jQuery UI -->
    <script src="{{asset('admin/js/jquery-ui.min.js')}}"></script>
    <!-- MASK -->
    <script src="{{asset('admin/js/jquery.mask.min.js')}}"></script>
    <script src="{{asset('admin/js/siteMask.js')}}"></script>
    <!-- Table Sorter -->
    <script src="{{asset('admin/js/tablesorter/jquery.tablesorter.min.js')}}"></script>
    <!-- Demais JS do sistema -->
    <script src="{{asset('admin/js/main.js')}}"></script>
    <script src="{{asset('admin/js/validaform.js')}}"></script>
    <script src="{{asset('admin/js/validaCep.js')}}"></script>
    <!-- jQuery UI -->
    <script src="{{asset('admin/js/script-ui.js')}}"></script>
  </body>
</html>