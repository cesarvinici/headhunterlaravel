<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Dashboard - Candidatos @yield('title')</title>

        <!-- CSS -->
        <link rel="stylesheet" href="{{asset('./admin/css/jquery-ui.css')}}">
        <link href="{{asset('./admin/css/bootstrap.css')}}" rel="stylesheet">
        <link href="{{asset('./admin/css/style.css')}}" rel="stylesheet">
            
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- jQuery UI -->
        <script src="{{asset('./admin/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('./admin/js/script-ui.js')}}"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
<body>    
    <header class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-3">
                    <div class="header__logo">
                        <img src="{{asset('./admin/imgs/icons/logo-principal-admin.png')}}" alt="">
                    </div>
                </div>
                <div class="col-xs-9 header__menu-bg">
                    <div class="header__menu">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#NavMobile">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>                        
                        </button>
                        <div class="collapse navbar-collapse" id="NavMobile">
                            <ul class="header__nav navbar-nav">
                                <li class="header__nav-item">
                                    <a href="{{route('sobre')}}" class="header__nav-link">Sobre a E-Headhunter</a>
                                </li>
                                <li class="header__nav-item">
                                    <a href="{{route('vagas')}}" class="header__nav-link">Vagas</a>
                                </li>
                                <li class="header__nav-item">
                                    <a href="{{route('empresas')}}" class="header__nav-link">Empresas</a>
                                </li>
                                <li class="header__nav-item">
                                    <a href="{{route('headhunters')}}" class="header__nav-link">Headhunters</a>
                                </li>
                                <li class="header__nav-item">
                                    <a href="{{route('candidatos')}}" class="header__nav-link">Candidatos</a>
                                </li>
                                <li class="header__nav-item">
                                    <a href="{{route('assessment')}}" class="header__nav-link">Assessment Center</a>
                                </li>
                                <li class="header__nav-item header__nav-item-sair">
                                    <a href="{{route('logoutCdd')}}" class="header__nav-link header__nav-link--sair">Sair</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main class="principal">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-3 sidebar-bg">
                    <section class="row sidebar">
                        <nav class="sidebar__nav">
                            <ul class="sidebar__lista">
                                <li class="sidebar__item">
                                <a href="{{route('dashboardCandidato')}}" class="sidebar__item-link">Meu Perfil</a>
                                </li>
                                <li class="sidebar__item">
                                    <a href="{{route('cadastracv')}}" class="sidebar__item-link">Cadastrar Currículo</a>
                                </li>
                                <li class="sidebar__item">
                                    <a href="{{route('cddVaga')}}" class="sidebar__item-link">Candidatura a Vaga</a>
                                </li>
                                <li class="sidebar__item">
                                    <a href="{{route('servicos')}}" class="sidebar__item-link">Solicitar Serviços</a>
                                </li>
                            </ul>
                        </nav>
                    </section>
                </div>
                @yield('content')                        
            </div>            
        </div>      
    </main>
    <footer class="footer"></footer>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('./admin/js/bootstrap.min.js')}}"></script>
     <!-- MASK -->
     <script src="{{asset('./admin/js/jquery.mask.min.js')}}"></script>
     <script src="{{asset('./admin/js/siteMask.js')}}"></script>
     <!-- Table Sorter -->
     <script src="{{asset('./admin/js/tablesorter/jquery.tablesorter.min.js')}}"></script>

     <!-- Demais JS do sistema -->
     <script src="{{asset('./admin/js/main.js')}}"></script>
     <script src="{{asset('./admin/js/validaform.js')}}"></script>
     <script src="{{asset('./admin/js/validaCep.js')}}"></script>

    @yield('scripts')
</body>
</html>