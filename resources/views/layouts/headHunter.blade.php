
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{asset('admin/css/bootstrap-3.3.7.css')}}">

    <!-- MetisMenu CSS -->
    <link rel="stylesheet" href="{{asset('admin/css/metis-menu-1.1.3.css')}}">

    <!-- DataTables CSS -->
    <link rel="stylesheet" href="{{asset('admin/css/data-tables-bootstrap.css')}}">

    <!-- DataTables Responsive CSS -->
    <link rel="stylesheet" href="{{asset('admin/css/data-tables-responsive.css')}}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('admin/css/sb-admin-2.css')}}">

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="{{asset('admin/css/font-awesome.css')}}">

<link rel="stylesheet" href="{{asset('admin/css/eheadhunter.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        	<div class="herewego">
	            <div class="navbar-header">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="headhunter"><img src="{{asset('admin/imgs/logo-eheadhunter.png')}}" class="logo"></a>

	            </div>
	            <!-- /.navbar-header -->
                    <div class="barra-cima-bem-vindo">
                        <img src="{{asset('admin/imgs/avatar.png')}}" alt="Avatar" class="avatar">
                        <span class="mensagem-bem-vindo">Bem vindo(a), {{urldecode($user->nome)}}!</span><br>
                        <span class="mensagem-bem-vindo-data">{{ucfirst(strftime('%A, %d de %B de %Y', strtotime('today')))}}</span>
                    </div>
	            <ul class="nav navbar-top-links navbar-right">
	                <li class="nav-item active">
	                <a class="nav-link" href="#"><i class="fa fa-calendar fa-fw"></i><span class="here"> Entrevista Pendente</span></a>
	                </li>
	                                <li class="nav-item active">
	                <a class="nav-link" href="#"><i class="fa fa-envelope fa-fw"></i><span class="here"> Minhas Mensagens</span></a>
	                </li>
	                                <li class="nav-item active">
	                <a class="nav-link" href="#"><i class="fa fa-bell fa-fw"></i><span class="here"> Alerta</span></a>
	                </li>
	                <!-- /.dropdown -->
	                <li class="dropdown">
	                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
	                        <i class="fa fa-user fa-fw"></i> {{urldecode($user->nome)}} <i class="fa fa-caret-down"></i>
	                    </a>
	                    <ul class="dropdown-menu dropdown-user">
	                        <li><a href="#"><i class="fa fa-engine fa-gear"></i> Minha Conta</a>
	                        </li>
	                        <li><a href="#"><i class="fa fa-wechat fa-fw"></i> Suporte ao Cliente</a>
	                        </li>
	                        <li><a href="#"><i class="fa fa-question fa-fw"></i> FAQ</a>
	                        </li>
	                        <li class="divider"></li>
	                        <li><a href="/dashboard/logoutHeadhunter"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
	                        </li>
	                    </ul>
	                    <!-- /.dropdown-user -->
	                </li>
	                <!-- /.dropdown -->
	            </ul>
	            <!-- /.navbar-top-links -->

	            <div class="navbar-default sidebar" role="navigation">
	                <div class="sidebar-nav navbar-collapse">
	                    <ul class="nav" id="side-menu">
	                        <li>
	                            <a href="{{route('headhunters')}}" class="sidebar__item-link "><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
	                        </li>
                            <li>
                                <a href="{{route('cadastros')}}" class="sidebar__item-link  "><i class="fa fa-table fa-fw"></i> Cadastros</a>
                            </li>
	                        <li>
	                            <a href="{{route('painelVagas')}}" class="sidebar__item-link"><i class="fa fa-sitemap fa-fw"></i> Vagas</a>
	                        </li>
                            <li>
                                <a href="{{route('painelCandidatos')}}" class="sidebar__item-link  "><i class="fa fa-table fa-fw"></i> Candidatos</a>
                            </li>

                            <li>
                                <a href="" class="sidebar__item-link "><i class="fa fa-table fa-fw"></i> Assessment</a>
                            </li>
                            <li>
                                <a href="" class="sidebar__item-link "><i class="fa fa-files-o fa-fw"></i> Relatórios</a>
                            </li>
                            <li>
                                <a href="{{route('mensagensHH')}}" class="sidebar__item-link "><i class="fa fa-envelope fa-fw"></i> Mensagens</a>
                            </li>

	                    </ul>
	                </div>
	                <!-- /.sidebar-collapse -->
	            </div>
	            <!-- /.navbar-static-side -->
            </div>
        </nav>



        @yield('content')



    </div> <!-- FIM ROW -->
    </div><!--  FIM container-fluid   -->
    </main>

    <footer class="footer"></footer>


</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>

<script src="{{asset('admin/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('admin/js/jquery.mask.min.js')}}"></script>
<script src="{{asset('admin/js/siteMask.js')}}"></script>

<script src="{{asset('admin/js/main.js')}}"></script>
<script src="{{asset('admin/js/validaform.js')}}"></script>
<script src="{{asset('admin/js/validaCep.js')}}"></script>

<script src="{{asset('admin/js/script-ui.js')}}"></script>
@yield('scripts')


<script src="{{asset('admin/js/metis-menu-1.1.3.js')}}"></script>

<script src="{{asset('admin/js/data-tables-1.10.12.js')}}"></script>
<script src="{{asset('admin/js/data-tables-bootstrap.js')}}"></script>
<script src="{{asset('admin/js/data-tables-reponsive.js')}}"></script>

<script src="{{asset('admin/js/sb-admin-2.js')}}"></script>

<script>
$(document).ready(function() {
    $('#dataTables-example').DataTable({
        responsive: true
    });
});

$("#form-vinculado-empresa").hide();
function exibe_form_vinculado_empresa() {
    $("#form-vinculado-empresa").show();
}


function esconde_form_vinculado_empresa() {
    $("#form-vinculado-empresa").hide();
}
</script>
</body>

</html>
