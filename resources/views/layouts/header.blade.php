<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>HeadHunters - @yield('title')</title>   
    <link type="icon" rel="shortcut icon" href="img/icones/logo.png"/>
    
    <!-- css -->
    <link rel="stylesheet" href="{{asset('lib/jquery.bxslider/jquery.bxslider.css')}}">
    <link rel="stylesheet" href="{{asset('lib/bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="css/front-style.css">
    <link rel="stylesheet" href="css/admin-style.css">
    <link rel="stylesheet" href="lib/jquery/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.min.css">

    
    <!-- js -->
    <script src="lib/jquery/jquery-3.2.1.js"></script>
    <script src="lib/jquery/jquery-ui.min.js"></script>
    <script src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script src="js/fancybox/jquery.fancybox.min.js"></script>
    @yield('scripts')
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div id="site">
    <header id="topoheader-tophea--">
        <div class="container">
            <div class="row">
               <div class="col-md-12">
                    <div class="tophea-MidiasSociais">
                        <ul class="tophea-ListaMidiasSociais list-inline">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <nav id="navprincipal-navpri--">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="navpri-Logo">
                        <a href="./"><img src="img/icones/logo-headhunters.png" alt="Logo HeadHunters"></a>
                    </div>
                </div>
                <div class="col-md-8">
                    <ul class="navpri-ListaMenus list-inline">
                        <li><a href="{{route('sobre')}}">Sobre a E-Headhunter</a></li>
                        <li><a href="{{route('vagas')}}">Vagas</a></li>
                        <li><a href="{{route('empresas')}}">Empresas</a></li>
                        <li><a href="{{route('headhunters')}}">Headhunters</a></li>
                        <li><a href="{{route('candidatos')}}">Candidatos</a></li>
                        <li><a href="{{route('assessment')}}">Assessment Center</a></li>
                    </ul>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-LoginHeader">
                        <a href="admin/Empresas">Login</a>
                    </button>
                </div>
            </div>
        </div>
    </nav>


@yield('content')

<!-- include  -->
<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo"><img src="img/icones/Layer1020.png" alt=""></div>
                </div>
                <div class="col-md-3">
                    <div class="contato">
                        +55 85 3045 8555</br>
                        Av. Desembargador Moreira, 2120</br>
                        Aldeota, Fortaleza - Ceará / Brasil
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="news">
                        <span>Receba novidades da HEADHUNTER</span>
                        <form id="newsletter">
                            <input type="email" placeholder="Digite seu e-mail aqui"></br>
                            <input type="submit" value="Enviar">
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="atendimento">
                        Horário de Atendimento</br>
                        Seg a Sex das 08:00hs às 18:00hs</br>
                        comercial@e-headhunter.com.br
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="lib/bootstrap/js/bootstrap.js"></script>
</body>
</html>
