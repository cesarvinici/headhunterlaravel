<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>E-HeadHunter - @yield("title")</title>
  <link type="icon" rel="shortcut icon" href="{{asset('img/icones/logo.png')}}"/>

    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('admin/css/jquery-ui.css')}}">
    <link href="{{asset('admin/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
    <!-- font awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="{{asset('admin/js/tablesorter/themes/blue/style.css')}}">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <header class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-3">
                    <div class="header__logo">
                        <img src="{{asset('admin/imgs/icons/logo-principal-admin.png')}}" alt="">
                    </div>
                </div>
                <div class="col-xs-9 header__menu-bg">
                    <div class="header__menu">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#NavMobile">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="NavMobile">
                        <ul class="header__nav navbar-nav">
                            <li class="header__nav-item">
                                <a href="{{route("sobre")}}" class="header__nav-link">Sobre a E-Headhunter</a>
                            </li>
                            <li class="header__nav-item">
                                <a href="{{route('vagas')}}" class="header__nav-link">Vagas</a>
                            </li>
                            <li class="header__nav-item">
                                <a href="{{route('empresas')}}" class="header__nav-link">Empresas</a>
                            </li>
                            <li class="header__nav-item">
                                <a href="{{route("headhunters")}}" class="header__nav-link">Headhunters</a>
                            </li>
                            <li class="header__nav-item">
                                <a href="{{route("candidatos")}}" class="header__nav-link">Candidatos</a>
                            </li>
                            <li class="header__nav-item">
                                <a href="{{route("assessment")}}" class="header__nav-link">Assessment Center</a>
                            </li>
                            <li class="header__nav-item header__nav-item-sair">
                                <a href="/dashboard/logoutHeadhunter" class="header__nav-link header__nav-link--sair">Sair</a>
                            </li>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main class="principal">
        <div class="container-fluid">
            <div class="row">
            <div class="col-xs-3 sidebar-bg">
                    <section class="row sidebar">
                        <nav class="sidebar__nav">
                            <ul class="sidebar__lista">
                                <li class="sidebar__item">
                                    <a href="{{route('headhunters')}}" class="sidebar__item-link ">Dashboard</a>
                                </li>
                                <li class="sidebar__item">
                                    <a href="{{route('cadastros')}}" class="sidebar__item-link  ">Cadastros</a>
                                </li>
                                <li class="sidebar__item">
                                    <a href="{{route('painelVagas')}}" class="sidebar__item-link">Vagas</a>
                                </li>
                                <li class="sidebar__item">
                                    <a href="{{route('painelCandidatos')}}" class="sidebar__item-link  ">Candidatos | CV</a>
                                </li>
                                <li class="sidebar__item">
                                    <a href="" class="sidebar__item-link ">Painel de Controle | Estatísticas</a>
                                </li>
                                <li class="sidebar__item">
                                    <a href="{{route('mensagensHH')}}" class="sidebar__item-link ">Mensagens | Emails</a>
                                </li>
                                <li class="sidebar__item">
                                    <a href="" class="sidebar__item-link ">Histórico de Faturamento</a>
                                </li>
                                <li class="sidebar__item">
                                    <a href="" class="sidebar__item-link ">Relatórios</a>
                                </li>
                            </ul>
                        </nav>
                    </section>
                </div>

            @yield('content')

            </div> <!-- FIM ROW -->
  </div><!--  FIM container-fluid   -->
   </main>

    <footer class="footer"></footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
     <!-- jQuery UI -->
    <script src="{{asset('admin/js/jquery-ui.min.js')}}"></script>
    <!-- MASK -->
    <script src="{{asset('admin/js/jquery.mask.min.js')}}"></script>
    <script src="{{asset('admin/js/siteMask.js')}}"></script>
    <!-- Table Sorter -->
    <script src="{{asset('admin/js/tablesorter/jquery.tablesorter.min.js')}}"></script>

    <!-- Demais JS do sistema -->
    <script src="{{asset('admin/js/main.js')}}"></script>
    <script src="{{asset('admin/js/validaform.js')}}"></script>
    <script src="{{asset('admin/js/validaCep.js')}}"></script>

 <!-- jQuery UI -->
      <script src="{{asset('admin/js/script-ui.js')}}"></script>
    @yield('scripts')
  </body>
</html>
