<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Parallax Template - Materialize</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/materialize.css')}}">
  <link rel="stylesheet" href="{{asset('css/site.css')}}">
</head>
<body>
  <nav class="white" role="navigation">
    <div class="nav-wrapper container">
      <!-- {{asset('imgs/logo-eheadhunter.png')}} -->
      <a id="logo-container" href="#" class="brand-logo"><img src="http://localhost:8000/img/icones/logo-headhunters.png" style="height: 64px;margin-top: 6px;"></a>
      <ul class="right hide-on-med-and-down">
          <li><a href="{{route('sobre')}}">Sobre a E-Headhunter</a></li>
          <li><a href="{{route('vagas')}}">Vagas</a></li>
          <li><a href="{{route('empresas')}}">Empresas</a></li>
          <li><a href="{{route('headhunters')}}">Headhunters</a></li>
          <li><a href="{{route('candidatos')}}">Candidatos</a></li>
          <li><a href="{{route('assessment')}}">Assessment Center</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
          <li><a href="{{route('sobre')}}">Sobre a E-Headhunter</a></li>
          <li><a href="{{route('vagas')}}">Vagas</a></li>
          <li><a href="{{route('empresas')}}">Empresas</a></li>
          <li><a href="{{route('headhunters')}}">Headhunters</a></li>
          <li><a href="{{route('candidatos')}}">Candidatos</a></li>
          <li><a href="{{route('assessment')}}">Assessment Center</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>

     @yield('content')

     <footer class="page-footer indigo darken-3">
       <div class="container">
         <div class="row">
           <div class="col l3 s12 center">
            <img src="http://127.0.0.1:8000/img/icones/Layer1020.png" alt="">
           </div>
           <div class="col l3 s12 center">
               <p class="texto-rodape">+55 85 3045 8555<br>Av. Desembargador Moreira, 2120<br>Aldeota, Fortaleza - Ceará / Brasil</p>
           </div>

           <div class="col l3 s12 center">
               <span class="texto-rodape">Receba novidades da HEADHUNTER</span>
                 <form id="newsletter">
                     <input type="email" placeholder="Digite seu e-mail aqui"></br>
                     <input type="submit" value="Enviar">
                 </form>

           </div>

           <div class="col l3 s12">
               <p class="texto-rodape">Horário de Atendimento<br>Seg a Sex das 08:00hs às 18:00hs<br>comercial@e-headhunter.com.br</p>
           </div>

         </div>
       </div>
     </footer>
  <script src="{{asset('js/jquery-2.1.1.js')}}"></script>
  <script src="{{asset('js/materialize.js')}}"></script>
  <script src="{{asset('js/init.js')}}"></script>

  </body>
</html>
