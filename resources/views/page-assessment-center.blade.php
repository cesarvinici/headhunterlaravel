@extends('layouts.header')
@section('title', 'Assessment Center')
@section('content')
    <section id="imgdestaque-imgdes--">
        <div class="imgdes-Imagem">
           <div class="img">
               <img src="assets/img/icones/Slide_Assessment_Center_1600x360px_JPG.jpg" alt="">
           </div>
        </div>
        <div class="titulo-pagina">
            <h2>Assessment Center</h2>
        </div>
    </section>    
    <section id="assessment-center">
        <div class="container">
            <div class="conteudo">
                <div class="row">
                   <div class="texto">
                        <p>Os Assessments apoiam as empresas na identificação e entendimento dos perfis profissionais através do mapeamento de competências comportamentais e auxiliam na identificação dos pontos fortes (principais talentos) e fracos (necessidade de acompanhamento) que um candidato ou colaborador possa ter.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="box-left">
                        <div class="img text-center">
                            <img src="assets/img/icones/page-asse-center-img-01.png" alt="">
                        </div>
                        <div class="descricao">
                            <h3>Avaliação DISC</h3>
                            <p>O método de Avaliação DISC trata-se de análises comportamentais estabelecidas por meio de quatro fatores (Dominância, Influência, Estabilidade e Conformidade) que visam à percepçao de cada pessoa em relação ao seu ambiente de trabalho.</p>
                        </div>
                        <div class="btn-acao">
                            <span>Solicitar avaliação DISC</span>
                        </div>
                    </div>
                    <div class="box-right">
                        <div class="img text-center">
                            <img src="assets/img/icones/page-asse-center-img-02.png" alt="">
                        </div>
                        <div class="descricao">
                            <h3>Avalicação 360º</h3>
                            <p>A avaliação 360º, também conhecida como feedback 360º, é uma ferramenta de avaliação de desempenho que permite a avaliação do funcionário por todos a sua volta, como superiores, subordinados, prestadores de serviços, clientes e pelo próprio avaliado.</p>
                        </div>
                        <div class="btn-acao">
                            <span>Solicitar avaliação 360º</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="sub-texto">
                        <p>Ferramentas que combinam altos padrões de qualidade técnica com um sistema integrado e flexível para atender às necessidades da sua empresa.</p>
                        <p style="color:#003399;">Contate-nos para uma atendimento personalizado!</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection