@extends('layouts.header')
@section('title', 'Candidatos')
@section('content')
    <section id="imgdestaque-imgdes--">
        <div class="imgdes-Imagem">
           <div class="img">
               <img src="assets/img/icones/Slide_Candidatos_1600x360px_JPG.jpg" alt="">
           </div>
        </div>
        <div class="titulo-pagina" style="background-color:#0099db;">
            <h2>Candidatos</h2>
        </div>
    </section>
    
    <section id="candidatos">
        <div class="container">
            <div class="conteudo">
                <div class="row">
                   <div class="texto">
                        <p>A confiança que você deposita na E-Headhunter é motivo de orgulho para nós, acima de tudo é uma responsabilidade que traduzimos no compromisso de toda equipe.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="titulo">
                        <h3>Seja Muito Bem Vindos!</h3>
                    </div>
                    <div class="lista-infor">
                        <div class="box">
                            <div class="icn icn-curriculo"></div>
                            <div class="nome">
                                <span>Cadastre seu currículo</span>
                            </div>
                            <div class="descricao">
                                <p>Inscreva-se gratuitamente em nossa Base de Talentos.</p>
                            </div>
                        </div>
                        <div class="box">
                            <div class="icn icn-consulta"></div>
                            <div class="nome">
                                <span>Consulte nossas oporunidades</span>
                            </div>
                            <div class="descricao">
                                <p>Verifique vagas de trabalhos que correspondem ao seu perfil e candidate-se.</p>
                            </div>
                        </div>
                        <div class="box">
                            <div class="icn icn-revisao"></div>
                            <div class="nome">
                                <span>Solicite uma revisão curricular</span>
                            </div>
                            <div class="descricao">
                                <p>Solicite uma proposta para que um de nossos especialistas o auxilie na construção ou revisão do seu currículo. </br>(Serviço Opcional)</p>
                            </div>
                        </div>
                        <div class="box">
                            <div class="icn icn-avalicao"></div>
                            <div class="nome">
                                <span>Solicite uma avaliação comportamental</span>
                            </div>
                            <div class="descricao">
                                <p>Solcite uma proposta para conhecero seu perfil DISC. </br>(Serviço Opcional)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="acesso-dashboard">
        <div class="container">
            <div class="conteudo">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <div class="form">
                            <div class="titulo">
                                <h3>Acesse Empresa | Dashboard</h3>
                            </div>
                            @if(Session::has('error'))
                            <div class="alert alert-danger">
                                    <ul>
                                        <li>{{ Session::get('error') }}</li>                        
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('loginCdd')}}" method="post">
                                @csrf
                                <div class="grupo">
                                    <div class="label">
                                        <span>Login</span>
                                    </div>
                                    <div class="input">
                                        <input type="text" name="email" required>
                                    </div>
                                </div>
                                <div class="grupo">
                                    <div class="label">
                                        <span>Senha</span>
                                    </div>
                                    <div class="input">
                                        <input type="password" name="senha" required>
                                    </div>
                                </div>
                                <div class="acoes">
                                    <div class="submit-cadastrar"><a href="candidatos/create" data-fancybox data-type="iframe">Cadastre-se</a></div>
                                    <div class="submit"><input type="submit" value="Entrar"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        <div class="logo text-center"><img src="assets/img/icones/logo-page-headhunters.png" alt=""></div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
<script type="text/javascript">
    $(function()
    {
            $("[data-fancybox]").fancybox({
                afterClose : function( instance, current ) {
                    location.href='candidatos';
                }
            });
    })
</script>
@endsection
