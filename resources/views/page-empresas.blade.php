@extends('layouts.header')
@section('content')
    <section id="imgdestaque-imgdes--">
        <div class="imgdes-Imagem">
           <div class="img">
               <img src="{{asset('img/icones/Slide_Empresas_1600x360px_JPG.jpg')}}" alt="">
           </div>
        </div>
        <div class="titulo-pagina" style="background-color:#0099db;">
            <h2>E-Headhunter para Empresas</h2>
        </div>
    </section>    
    <section id="empresas">
        <div class="container">
            <div class="conteudo">
                <div class="row">
                   <div class="texto">
                        <p>Ao publicar suas vagas na plataforma, os Recrutadores serão notificados e convidados a participar do processo. Eles avaliarão os candidatos através de técnicas de busca ativa (Headhunter) ou busca passiva (banco de dados) sendo desafiados a preencher até mesmo as vagas mais complexas e específicas da sua empresa.</p>
                        <p>O conjunto de ferramentas online é totalmente gratuíto, restrigindo o seu investimento ao pagamento da taxa de recrutamento atrelada ao sucesso da contratação (política de satisfação garantida).</p>
                        <p>Nosso objetivo é fornecer as empresas um acesso incrivelmente rápido e eficiente a Recrutadores (Agências ou Headhunters Autônomos), permitindo-as personalizar, alinhar padrões, práticas e etapas dos seus processos seletivos, bem como monitorar a evolução e medir a eficácia dos processos.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>    
    <section id="acesso-dashboard">
        <div class="container">
            <div class="conteudo">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <div class="form">
                            <div class="titulo">
                                <h3>Acesse Empresa | Dashboard</h3>
                            </div>
                            @if(Session::has('error'))
                            <p class="text-danger">{{Session::get('error')}}</p> 
                          @endif
                            <form action="loginEmpresa" method="post">
                                @csrf
                                <div class="grupo">
                                    <div class="label">
                                        <span>Login</span>
                                    </div>
                                    <div class="input">
                                        <input name="login" type="text">
                                    </div>
                                </div>
                                <div class="grupo">
                                    <div class="label">
                                        <span>Senha</span>
                                    </div>
                                    <div class="input">
                                        <input name="senha" type="password">
                                    </div>
                                </div>
                                <div class="acoes">
                                    <div class="submit-cadastrar"><a href="nova-empresa/create" data-fancybox data-type="iframe" >Cadastre-se</a></div>
                                    <div class="submit"><input type="submit" name="loginEmpresa" value="Entrar"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        <div class="logo text-center"><img src="{{asset('img/icones/logo-page-headhunters.png')}}" alt=""></div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
<script type="text/javascript">
    $(function()
    {
            $("[data-fancybox]").fancybox({
                afterClose : function( instance, current ) {
                    location.href='empresas';
                }
            });
    })
</script>
@endsection