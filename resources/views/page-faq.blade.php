@extends('layouts.header')
@section('title', 'FAQ')
@section('content')
<section id="imgdestaque-imgdes--">
    <div class="imgdes-Imagem">
        <div class="img">
            <img src="assets/img/icones/Slide_FAQ_1600x360px_JPG.jpg" alt="">
        </div>
    </div>
    <div class="titulo-pagina">
        <h2>FAQ</h2>
    </div>
</section>
<script>
    $(function() {
        $("#tab").accordion({
            heightStyle: "content",
        });
    });
</script>
<section id="page-faq">
    <div class="container-fluid">
        <div class="conteudo">
            <div class="row">
                <div id="tab">
                    <div class="header">
                        <div class="titulo">Quanto tempo demora para receber candidatos de uma vaga divulgada?</div>
                        <div class="icone"></div>
                    </div>
                    <div class="texto">
                        <p>Poucas horas ou dias. Uma vez que você cadastra sua vaga, um e-mail de notificação será enviada para os Headhunters e agências associadas na plataforma que iniciarão os trabalhos de captação e identificação de talentos. Portanto, você deverá visualizar os resultados do trabalho da equipe rapidamente, geralmente em poucos dias. No entanto, dependerá também das especificações do perfil traçado, do salário oferecido, localização da vaga e da taxa de recrutamento firmada para engajar os recrutadores no processo seletivo. Se você tiver dúvidas, solicite orientação à nossa equipe de relacionamento que detêm um visão geral de mercado quando as médias de remuneração praticadas no seu estado.</p>
                    </div>
                    
                    <div class="header">
                        <div class="titulo">Quanto custa realizar um processo de recrutamento na E-Headhunter?</div>
                        <div class="icone"></div>
                    </div>
                    <div class="texto">
                        <p>Poucas horas ou dias. Uma vez que você cadastra sua vaga, um e-mail de notificação será enviada para os Headhunters e agências associadas na plataforma que iniciarão os trabalhos de captação e identificação de talentos. Portanto, você deverá visualizar os resultados do trabalho da equipe rapidamente, geralmente em poucos dias. No entanto, dependerá também das especificações do perfil traçado, do salário oferecido, localização da vaga e da taxa de recrutamento firmada para engajar os recrutadores no processo seletivo. Se você tiver dúvidas, solicite orientação à nossa equipe de relacionamento que detêm um visão geral de mercado quando as médias de remuneração praticadas no seu estado.</p>
                    </div>
                    
                    <div class="header" style="background-color:#fff;border:none;">
                        <div class="titulo">Quais as formas de pagamentos aceitas?</div>
                        <div class="icone"></div>
                    </div>
                    <div class="texto">
                        <p>Poucas horas ou dias. Uma vez que você cadastra sua vaga, um e-mail de notificação será enviada para os Headhunters e agências associadas na plataforma que iniciarão os trabalhos de captação e identificação de talentos. Portanto, você deverá visualizar os resultados do trabalho da equipe rapidamente, geralmente em poucos dias. No entanto, dependerá também das especificações do perfil traçado, do salário oferecido, localização da vaga e da taxa de recrutamento firmada para engajar os recrutadores no processo seletivo. Se você tiver dúvidas, solicite orientação à nossa equipe de relacionamento que detêm um visão geral de mercado quando as médias de remuneração praticadas no seu estado.</p>
                    </div>
                    
                    <div class="header">
                        <div class="titulo">Quais as formas de pagamentos aceitas?</div>
                        <div class="icone"></div>
                    </div>
                    <div class="texto">
                        <p>Poucas horas ou dias. Uma vez que você cadastra sua vaga, um e-mail de notificação será enviada para os Headhunters e agências associadas na plataforma que iniciarão os trabalhos de captação e identificação de talentos. Portanto, você deverá visualizar os resultados do trabalho da equipe rapidamente, geralmente em poucos dias. No entanto, dependerá também das especificações do perfil traçado, do salário oferecido, localização da vaga e da taxa de recrutamento firmada para engajar os recrutadores no processo seletivo. Se você tiver dúvidas, solicite orientação à nossa equipe de relacionamento que detêm um visão geral de mercado quando as médias de remuneração praticadas no seu estado.</p>
                    </div>
                    
                    <div class="header" style="background-color:#fff;border:none;">
                        <div class="titulo">Quais as formas de pagamentos aceitas?</div>
                        <div class="icone"></div>
                    </div>
                    <div class="texto">
                        <p>Poucas horas ou dias. Uma vez que você cadastra sua vaga, um e-mail de notificação será enviada para os Headhunters e agências associadas na plataforma que iniciarão os trabalhos de captação e identificação de talentos. Portanto, você deverá visualizar os resultados do trabalho da equipe rapidamente, geralmente em poucos dias. No entanto, dependerá também das especificações do perfil traçado, do salário oferecido, localização da vaga e da taxa de recrutamento firmada para engajar os recrutadores no processo seletivo. Se você tiver dúvidas, solicite orientação à nossa equipe de relacionamento que detêm um visão geral de mercado quando as médias de remuneração praticadas no seu estado.</p>
                    </div>
                    
                    <div class="header">
                        <div class="titulo">Quais as formas de pagamentos aceitas?</div>
                        <div class="icone"></div>
                    </div>
                    <div class="texto">
                        <p>Poucas horas ou dias. Uma vez que você cadastra sua vaga, um e-mail de notificação será enviada para os Headhunters e agências associadas na plataforma que iniciarão os trabalhos de captação e identificação de talentos. Portanto, você deverá visualizar os resultados do trabalho da equipe rapidamente, geralmente em poucos dias. No entanto, dependerá também das especificações do perfil traçado, do salário oferecido, localização da vaga e da taxa de recrutamento firmada para engajar os recrutadores no processo seletivo. Se você tiver dúvidas, solicite orientação à nossa equipe de relacionamento que detêm um visão geral de mercado quando as médias de remuneração praticadas no seu estado.</p>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="form-contato">
                   <h4>Nos envie uma mensagem com sua dúvida.</h4>
                    <form action="">
                        <div class="grupo">
                            <div class="label">
                                <span>Nome</span>
                            </div>
                            <div class="input">
                                <input type="text">
                            </div>
                        </div>
                        <div class="grupo">
                            <div class="label">
                                <span>E-mail</span>
                            </div>
                            <div class="input">
                                <input type="email">
                            </div>
                        </div>
                        <div class="grupo">
                            <div class="label-msg">
                                <span>Mensagem</span>
                            </div>
                            <div class="input-msg">
                                <textarea></textarea>
                            </div>
                        </div>
                        <div class="submit">
                            <input type="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection