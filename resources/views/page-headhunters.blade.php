@extends('layouts/site')
@section('title', "Headhunters")
@section('content')


<div id="index-banner" class="parallax-container">
  <div class="section no-pad-bot">
  </div>
  <div class="parallax"><img src="img/icones/Slide_Recrutadores_1600x360px_JPG.jpg" alt=""></div>
</div>
  <div class="container">

      <div class="section">

        <div class="row">
          <div class="col s12 center">
            <h2>E-Headhunter para Recrutadores</h2>
            <p>O Headhunter é uma plataforma de serviço verdadeiramente baseada no mérito, onde os Recrutadores terão acesso a diversas oportunidades de trabalho e poderão competir na identificação de perfis personalizados que correspondam às expectativas dos clientes.</p>
            <p>O mais assertivo, que tem o seu candidato escolhido, recebe uma premiação em dinheiro compatível à faixa de remuneração mensal do contratado pelo cliente, destacada como PRÊMIO.<br>A decisão sobre qual projeto participar é de competência do Headhunter.</p>
          </div>
        </div>
      </div>
    </div>
    <div id="index-banner" class="parallax-container">
      <div class="section no-pad-bot">
        <div class="container">
           <div class="row">
              <div class="col s12 m6 l5">
                  <div class="form white grey-text" style="border-radius: 10px;">

                    <p class="titulo-login-site center">Acesse Empresa | Dashboard</p>

                      @if(Session::has('login_fail'))
                      <div class="card-panel red white-text">{{Session::get('login_fail')}}</div>
                    @endif
                      {{Form::open((array('url'=>'loginHeadhunter')))}}
                      <!-- <form action="admin/logaUsuario.php" method="post"> -->
                              <div class="input-field col s12">
                              {{Form::email('login',"",['required'])}}
                              <label for="last_name">Login</label>
                            </div>


                            <div class="input-field col s12">
                            {{Form::password('senha',['required'])}}
                            <label for="last_name">Senha</label>
                          </div>

                          <div class="btns-submit-headhunters">

                            <!-- <input type="submit" value="Entrar" name="loginHeadHunter" class="waves-effect indigo darken-3 btn-large">               <!-- <a ><input type="submit" value="Entrar" name="loginHeadHunter"></a> -->
                            <button class="waves-effect indigo darken-3 btn-large center" type="submit" name="action">Entrar</button>
                            <a href="{{route('cadastroHH')}}" class="waves-effect grey lighten-2 black-text btn-large center" >Cadastre-se</a>

                          </div>
                    {{Form::close()}}
                      <!-- </form> -->
                  </div>
              </div>

              <div class="col s12 m6 l6 hide-on-small-only">
                  <div class="centraliza-logo-login">
                      <img src="http://127.0.0.1:8000/img/icones/logo-page-headhunters.png" alt="">
                  </div>
              </div>
          </div>

          <br><br>
        </div>
      </div>
      <div class="parallax"><img src="http://127.0.0.1:8000/img/icones/bg-page-secao-headhunters.png" alt=""></div>
    </div>


  </div>

@endsection
