@extends('layouts.header')
@section('title', 'Sobre')
@section('content')
    <section id="imgdestaque-imgdes--">
        <div class="imgdes-Imagem">
           <div class="img">
               <img src="assets/img/icones/Slide_Sobre_1600x360px_JPG.jpg" alt="">
           </div>
        </div>
        <div class="titulo-pagina">
            <h2>Saiba um pouco mais sobre a E-Headhunter</h2>
        </div>
    </section>
    <section id="sobre">
        <div class="container">
            <div class="conteudo">
                <div class="row">
                    <div class="texto">
                        <div class="texto-left">
                            <p>A empresa foi fundada por Headhunters que perceberam a necessidade de uma plataforma web segura e otimizada onde as empresas pudessem divulgar suas vagas e receber candidatos elegíveis após um processo minucioso de análise de perfil, permitindo-as assim contratar os melhores talentos sem perder tempo e dispor recursos analisando um quantitativo elevado de currículo sem aderência a vaga.</p>
                        </div>
                    </div>
                    <div class="texto">
                        <div class="texto-right">
                            <p>Por outro lado, os Recrutadores (consultorias ou Headhunters autônomos) possuem através do site uma constante fonte de oportunidades de trabalho e ferramentas para gerenciar seus processos.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="missao-visao">
        <div class="container">
           <div class="conteudo">
                <div class="row">
                    <div class="nome">
                        <div class="missao-nome"><h3>Missão</h3></div>
                    </div>
                    <div class="descricao">
                        <div class="missao-descricao"><p>Conectar empresas, recrutadores e candidatos promendo soluções acessíveis ao atingimento dos seus objetivos.</p></div>
                    </div>
                </div>
                <div class="row">
                    <div class="descricao">
                        <div class="visao-descricao"><p>Ser o principal parceito nos processos de recrutamento de profissionais colaborando com empresas de pequeno, médio e grande porte.</p></div>
                    </div>
                    <div class="nome">
                        <div class="visao-nome"><h3>Visão</h3></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            $('.slider-valores').bxSlider({
              minSlides: 1,
              maxSlides: 1,
              slideWidth: 640,
            });
        });
    </script>
    <section id="valores">
        <div class="container">
            <div class="conteudo">
                <div class="row">
                    <div class="col-md-12">
                        <div class="slider">
                            <h3>Valores</h3>
                            <ul class="slider-valores">
                                <li>
                                    <div class="item">
                                        <h4>Agilidade</h4>
                                        <h5>Garantir que os processos acontençam com agilidade.</h5>
                                    </div>
                                </li>
                                <li>
                                    <div class="item">
                                        <h4>Compromisso</h4>
                                        <h5>Garantir que os processos acontençam com agilidade.</h5>
                                    </div>
                                </li>
                                <li>
                                    <div class="item">
                                        <h4>Transparência</h4>
                                        <h5>Garantir que os processos acontençam com agilidade.</h5>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="acessos">
        <div class="container">
            <div class="conteudo">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo text-center"><img src="assets/img/icones/logo-pagina-sobre.png" alt=""></div>
                        <div class="slogan"><p>Uma estratégia abrangente para aquisição de talentos!</p></div>
                    </div>
                </div>
                <div class="row">
                    <div class="box">
                        <div class="icone text-center">
                            <img src="assets/img/icones/icone-como-funciona.png" alt="">
                        </div>
                        <div class="titulo"><h3>Como Funciona</h3></div>
                        <div class="descricao"><p>Conheça nosso formato de trabalho</p></div>
                    </div>
                    <div class="box">
                        <div class="icone text-center">
                            <img src="assets/img/icones/icone-faq.png" alt="">
                        </div>
                        <div class="titulo"><h3>F.A.Q</h3></div>
                        <div class="descricao"><p>Esclareça suas dúvidas com o F.A.Q (Perguntas Frequentes) ou se preferir, entre em contato conosco!</p></div>
                    </div>
                    <div class="box">
                        <div class="icone text-center">
                            <img src="assets/img/icones/icone-termo-compromisso.png" alt="">
                        </div>
                        <div class="titulo"><h3>Termo de Compromisso</h3></div>
                        <div class="descricao"><p>Tudo o que fazemos tem como objetivo a satisfação de nossos clientes. Mas, nós vamos além da satisfação, pois conquistamos a confiança de todos com quem mantemos relacionamentos e isto se traduz em fidelização.</p></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection