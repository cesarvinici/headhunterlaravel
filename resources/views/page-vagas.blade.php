@extends('layouts.header')
@section("title", 'Vagas')
@section('content')
    <section id="pagina-vagas">
        <div class="conteudo">
           <div class="img-bg">
               <img src="assets/img/icones/Slide_Vagas_1600x360px_JPG.jpg" alt="">
           </div>
        </div>
        <div class="titulo" style="background-color:#406ead;">
            <h2>RECRUTE: selecione as vagas que você gostaria de trabalhar e que acredita suprir!</h2>
        </div>
    </section>
    <section id="busca">
        <div class="container">
            <div class="row">
                <div class="base-busca">
                   <div class="form">
                        <form action="">
                            <div class="col-md-5">
                                <div class="posicao">
                                    <input type="text" value="POSIÇÃO">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="cidade">
                                    <input type="text" value="CIDADE">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="submit">
                                    <input type="submit" value="Buscar">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="base-resul">
                    <div class="col-md-6">
                        <div class="num">
                            <span>56 resultados encontrados</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="filtro">
                            <span>Organizado por: <span class="filtro-ativo">Data</span> | <span>Valor</span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="resul">
        <div class="container-fluid">
            <div class="row">
                <div class="col-resul">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-resul-nome">
                                <i class="fa fa-list-ul"></i> <span>Filtros</span>
                            </div>
                            <form>
                                <div class="conteudo-filtro">
                                    <div class="nav">
                                        <div class="menu">
                                            <span class="nome">Função</span><span class="icone">+</span>
                                        </div>
                                    </div>
                                    <div class="nav">
                                        <div class="menu">
                                            <span class="nome">Área</span><span class="icone">+</span>
                                        </div>
                                    </div>
                                      <script>
                                          $( function() {
                                            $( "#slider-range" ).slider({
                                              range: true,
                                              min: 0,
                                              max: 999,
                                              values: [ 75, 300 ],
                                              slide: function( event, ui ) {
                                                $( "#amountMin" ).val( "R$ " + ui.values[ 0 ] + ",00" );
                                                $( "#amountMax" ).val("R$ " + ui.values[ 1 ] + ",00" );
                                              }
                                            });
                                           $( "#amountMin" ).val( "R$ " + $( "#slider-range" ).slider( "values", 0 ) + ",00");
                                           $( "#amountMax" ).val("R$ " + $( "#slider-range" ).slider( "values", 1 ) + ",00");
                                          } );
                                      </script>
                                    <div class="nav">
                                        <div class="menu">
                                            <span class="nome">Salário a partir</span>
                                            <div class="slider-filtro">
                                               <div class="min">
                                                   <span>Min</span></br>
                                                   <input type="text" id="amountMin" readonly>
                                               </div>
                                               <div class="slider">                                               
                                                   <div id="slider-range"></div>
                                               </div>
                                               <div class="max">
                                                   <span>Max</span></br>
                                                   <input type="text" id="amountMax" readonly>
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="nav">
                                        <div class="menu">
                                            <span class="nome">Localidade</span><span class="icone">+</span>
                                        </div>
                                    </div>
                                    <div class="nav">
                                        <div class="menu">
                                            <span class="nome">Contrato</span><span class="icone">+</span>
                                        </div>
                                    </div>
                                    <div class="nav">
                                        <div class="menu">
                                            <span class="nome">Recrutamento</span><span class="icone">+</span>
                                        </div>
                                    </div>
                                    <div class="submit">
                                        <input type="submit" value="buscar">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-resul">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-resul-nome"><span>Título do Cargo</span></div>
                            <div class="conteudo-cargo">
                               <div class="cargo">
                                    <div class="nome"><span>Gerente de RH</span></div>
                                    <div class="data"><span>Publicado em 17/05/2017</span></div>
                                </div>
                               <div class="cargo resul-bg-none">
                                    <div class="nome"><span>Gerente de RH</span></div>
                                    <div class="data"><span>Publicado em 17/05/2017</span></div>
                                </div>
                               <div class="cargo">
                                    <div class="nome"><span>Gerente de RH</span></div>
                                    <div class="data"><span>Publicado em 17/05/2017</span></div>
                                </div>
                               <div class="cargo resul-bg-none">
                                    <div class="nome"><span>Gerente de RH</span></div>
                                    <div class="data"><span>Publicado em 17/05/2017</span></div>
                                </div>
                               <div class="cargo">
                                    <div class="nome"><span>Gerente de RH</span></div>
                                    <div class="data"><span>Publicado em 17/05/2017</span></div>
                                </div>
                               <div class="cargo resul-bg-none">
                                    <div class="nome"><span>Gerente de RH</span></div>
                                    <div class="data"><span>Publicado em 17/05/2017</span></div>
                                </div>
                               <div class="cargo">
                                    <div class="nome"><span>Gerente de RH</span></div>
                                    <div class="data"><span>Publicado em 17/05/2017</span></div>
                                </div>
                               <div class="cargo resul-bg-none">
                                    <div class="nome"><span>Gerente de RH</span></div>
                                    <div class="data"><span>Publicado em 17/05/2017</span></div>
                                </div>
                               <div class="cargo">
                                    <div class="nome"><span>Gerente de RH</span></div>
                                    <div class="data"><span>Publicado em 17/05/2017</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-resul">
                    <div class="col-md-1">
                        <div class="row">
                            <div class="col-resul-nome"><span>Localidade</span></div>
                            <div class="conteudo-local">
                               <div class="local">
                                    <div class="nome"><span><i class="fa fa-map-marker"></i> Fortaleza</span></div>
                                </div>
                               <div class="local resul-bg-none">
                                    <div class="nome"><span><i class="fa fa-map-marker"></i> Fortaleza</span></div>
                               </div>
                               <div class="local">
                                    <div class="nome"><span><i class="fa fa-map-marker"></i> Fortaleza</span></div>
                                </div>
                               <div class="local resul-bg-none">
                                    <div class="nome"><span><i class="fa fa-map-marker"></i> Fortaleza</span></div>
                               </div>
                               <div class="local">
                                    <div class="nome"><span><i class="fa fa-map-marker"></i> Fortaleza</span></div>
                                </div>
                               <div class="local resul-bg-none">
                                    <div class="nome"><span><i class="fa fa-map-marker"></i> Fortaleza</span></div>
                               </div>
                               <div class="local">
                                    <div class="nome"><span><i class="fa fa-map-marker"></i> Fortaleza</span></div>
                               </div>
                               <div class="local resul-bg-none">
                                    <div class="nome"><span><i class="fa fa-map-marker"></i> Fortaleza</span></div>
                               </div>
                               <div class="local">
                                    <div class="nome"><span><i class="fa fa-map-marker"></i> Fortaleza</span></div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-resul">
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-resul-nome"><span>Salário</span></div>
                            <div class="conteudo-salario">
                               <div class="salario">
                                    <span>Mín.: R$ 6.000,00</span></br>
                                    <span>Máx.: R$ 8.000,00</span>
                                </div>
                               <div class="salario resul-bg-none">
                                    <span>Mín.: R$ 6.000,00</span></br>
                                    <span>Máx.: R$ 8.000,00</span>
                                </div>
                               <div class="salario">
                                    <span>Mín.: R$ 6.000,00</span></br>
                                    <span>Máx.: R$ 8.000,00</span>
                                </div>
                               <div class="salario resul-bg-none">
                                    <span>Mín.: R$ 6.000,00</span></br>
                                    <span>Máx.: R$ 8.000,00</span>
                                </div>
                               <div class="salario">
                                    <span>Mín.: R$ 6.000,00</span></br>
                                    <span>Máx.: R$ 8.000,00</span>
                                </div>
                               <div class="salario resul-bg-none">
                                    <span>Mín.: R$ 6.000,00</span></br>
                                    <span>Máx.: R$ 8.000,00</span>
                                </div>
                               <div class="salario">
                                    <span>Mín.: R$ 6.000,00</span></br>
                                    <span>Máx.: R$ 8.000,00</span>
                                </div>
                               <div class="salario resul-bg-none">
                                    <span>Mín.: R$ 6.000,00</span></br>
                                    <span>Máx.: R$ 8.000,00</span>
                                </div>
                               <div class="salario">
                                    <span>Mín.: R$ 6.000,00</span></br>
                                    <span>Máx.: R$ 8.000,00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-resul">
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-resul-nome"><span>Tx Recrutamento</span></div>
                            <div class="conteudo-recru">
                               <div class="recru">
                                    <span>40%</span></br>
                                    <span>Salário Mês</span>
                                </div>
                               <div class="recru resul-bg-none">
                                    <span>40%</span></br>
                                    <span>Salário Mês</span>
                                </div>
                               <div class="recru">
                                    <span>40%</span></br>
                                    <span>Salário Mês</span>
                                </div>
                               <div class="recru resul-bg-none">
                                    <span>40%</span></br>
                                    <span>Salário Mês</span>
                                </div>
                               <div class="recru">
                                    <span>40%</span></br>
                                    <span>Salário Mês</span>
                                </div>
                               <div class="recru resul-bg-none">
                                    <span>40%</span></br>
                                    <span>Salário Mês</span>
                                </div>
                               <div class="recru">
                                    <span>40%</span></br>
                                    <span>Salário Mês</span>
                                </div>
                               <div class="recru resul-bg-none">
                                    <span>40%</span></br>
                                    <span>Salário Mês</span>
                                </div>
                               <div class="recru">
                                    <span>40%</span></br>
                                    <span>Salário Mês</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-resul">
                    <div class="col-md-1">
                        <div class="row">
                            <div class="col-resul-nome"></div>
                            <div class="conteudo-acao">
                               <div class="acao">
                                    <div class="icone"></div>
                                </div>
                               <div class="acao">
                                    <div class="icone"></div>
                                </div>
                               <div class="acao">
                                    <div class="icone"></div>
                                </div>
                               <div class="acao">
                                    <div class="icone"></div>
                                </div>
                               <div class="acao">
                                    <div class="icone"></div>
                                </div>
                               <div class="acao">
                                    <div class="icone"></div>
                                </div>
                               <div class="acao">
                                    <div class="icone"></div>
                                </div>
                               <div class="acao">
                                    <div class="icone"></div>
                                </div>
                               <div class="acao">
                                    <div class="icone"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="paginacao">
            <span>PAG. 1 | 2 | 3 ></span>
        </div>
    </section>
    
@endsection