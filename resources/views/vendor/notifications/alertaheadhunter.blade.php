@component('mail::message')
# @lang('Olá')    
<p>Informamos que a vaga abaixo sofreu uma atualização no seu status.</p>
@component('mail::table')
    | ID       | Vaga    | Empresa     | Status |
    |-----:-------- |------:-------|------:-------|---:-----|
    | {{$vaga->id}} | {{urldecode($vaga->cargo)}} | {{$vaga->nome_fantasia}} | {{$vaga->status_vaga}} |

@endcomponent

@lang('Até logo'),<br>Equipe E-{{ config('app.name') }}

@endcomponent

