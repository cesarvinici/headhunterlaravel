@component('mail::message')
# @lang('Olá,')    
<p>Você foi convidado para trabalhar em uma vaga.</p>
@component('mail::table')
    | Vaga       |  Empresa | Data limite para contratação | 
    | ---------- |:---------| ---------------:|
    |{{$convite->cargo}}| {{$convite->nome_fantasia}} | {{MysqlToData($convite->data_limite)}}
@endcomponent

<p>Acesse sua caixa de mensagem para aceitar ou recusar o convite.</p>


@lang('Até logo'),<br>Equipe E-{{ config('app.name') }}

@endcomponent
