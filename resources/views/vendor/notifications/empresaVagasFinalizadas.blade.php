@component('mail::message')
# @lang('Olá')    
<p>Informamos que uma ou mais vagas foram finalizadas pois esgotou-se o prazo para contratação</p>
@component('mail::table')
    | ID       | Vaga         | Data Vencimento  |
    | ------------- |:-------------:| --------:|
    @foreach($vagas as $vaga)
    | {{$vaga->id}} | {{urldecode($vaga->cargo)}} | {{MySqlToData($vaga->data_limite)}} |
    @endforeach
@endcomponent


@lang('Até logo'),<br>Equipe E-{{ config('app.name') }}

@endcomponent
