@component('mail::message')
# @lang('Olá')    
<p>Atualização na fila de candidatura de um de seus candidatos.</p>
{{-- @component('mail::table')
    | Candidato | Vaga | Empresa | Etapa | Data | Status |
    |:--------------------------|:---------------------------------|:------------------------|:---------------------------------|:----------------------------------|:--------------------------------------|
    | {{$evolucao->candidato}} | {{urldecode($evolucao->cargo)}} | {{$evolucao->empresa}} | {{urldecode($evolucao->etapa)}} | {{MysqlToData($evolucao->data)}} | {{statusEvolucao($evolucao->status)}} |
@endcomponent --}}
<table class="table table-bordered table-striped" style="width:130%">
    <tr>
        <th>Candidato</th>
        <th>Vaga</th>
        <th>Empresa</th>
        <th>Etapa</th>
        <th>Data</th>
        <th>Status</th>
    </tr>
    <tr>
        <td>{{$evolucao->candidato}}</td>
        <td>{{urldecode($evolucao->cargo)}}</td>
        <td>{{$evolucao->empresa}}</td>
        <td>{{urldecode($evolucao->etapa)}}</td>
        <td>{{MysqlToData($evolucao->data)}}</td>
        <td>{{statusEvolucao($evolucao->status)}}</td>
    </tr>
</table>

@if(! empty($evolucao->observacoes))
    <strong>Observações: </strong> <p>{{$evolucao->observacoes}}</p>
@endif

@lang('Até logo'),<br>Equipe E-{{ config('app.name') }}

@endcomponent

