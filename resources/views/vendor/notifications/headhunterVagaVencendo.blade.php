@component('mail::message')
# @lang('Olá')    
<p>Uma ou mais vagas estão prestes a vencer.</p>
@component('mail::table')
    | ID       | Vaga    | Empresa     | Data Vencimento  |
    |-----:-------- |------:-------|------:-------|---:-----|
    @foreach($vagas as $vaga)
    | {{$vaga->id}} | {{urldecode($vaga->cargo)}} | {{$vaga->nome_fantasia}} | {{MySqlToData($vaga->data_limite)}} |
    @endforeach
@endcomponent


@lang('Até logo'),<br>Equipe E-{{ config('app.name') }}

@endcomponent

