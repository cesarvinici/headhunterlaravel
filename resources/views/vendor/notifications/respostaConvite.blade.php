@component('mail::message')
# @lang('Olá,')    
<p>Um headhunter respondeu ao seu convite.</p>
@component('mail::table')
  |  Código | Cargo       |  Headhunter | Data limite | Status Convite | 
  |:--------|:------------|:---------|:----------------|:--------------|
  | {{$convite->id}} |{{$convite->cargo}}| {{$convite->nome}} | {{MysqlToData($convite->data_limite)}} | {{getStatusConvite($convite->status_convite)}}
@endcomponent

@lang('Até logo'),<br>Equipe E-{{ config('app.name') }}

@endcomponent
