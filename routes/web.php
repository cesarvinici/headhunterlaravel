<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//public views
Route::get('/', function () {
    return view('index');
});

Route::get('/cadastro/headhunter', function(){return view('cadastro.cadastroHeadhunter');})->name('cadastroHH');
Route::get('sobre', function(){return view('page-sobre');})->name('sobre');
Route::get('vagas', function(){return view('page-vagas');})->name('vagas');
Route::get('assessment-center', function(){return view('page-assessment-center');})->name('assessment');

//Headhunter
Route::get('headhunters', 'HeadhunterController@page_headhunter')->name('headhunters'); 
Route::get('dashboard/headhunter/index','HeadhunterController@index');
Route::get('dashboard/headhunter/','HeadhunterController@index');
Route::get('dashboard/headhunter/cadastros', 'HeadhunterController@cadastro')->name('cadastros');
Route::get('dashboard/headhunter/meus-dados', 'HeadhunterController@edit')->name('edit.meus-dados');
Route::post('dashboard/headhunter/meus-dados', 'HeadhunterController@update')->name('update.meus-dados');
Route::get('dashboard/logoutHeadhunter', 'HeadhunterController@logout');
Route::post('loginHeadhunter', 'HeadhunterController@loginHeadhunter');
Route::post('/cadastro/headhunter', 'HeadhunterController@store');
Route::get('dashboard/headhunter/mensagens', 'HeadhunterController@mensagens')->name('mensagensHH');
Route::get('dashboard/headhunter/mensagens/entrada', 'ConviteEmpresaHeadhunterController@getConvites');

Route::get('dashboard/headhunter/mensagens/saida', function()
{
    return view('dashboard.headhunter.mensagens-caixa-saida');
});

Route::get('dashboard/headhunter/planos', function(){return view('dashboard.headhunter.planos');})->name('planos.headhunter');

Route::post('dashboard/headhunters/criaAlerta', 'HeadhunterAlertaController@criaAlerta');
//HeadhunterEmpresa
Route::resource('dashboard/headhunter/empresa','HeadhunterEmpresaController')->name('index', 'get.empresaHH')
                                                                             ->name('store', 'store.empresaHH')
                                                                             ->name('update', 'update.empresaHH');
Route::get('dashboard/headhunter/deletar-conta', function(){ return view('dashboard.headhunter.cancelarConta');})->name('deletaContaHH');

//Headhunter cliente
Route::resource('dashboard/headhunter/clientes', 'HeadhunterClienteController')->name('index', 'index.clientes')
                                                                               ->name('create', 'create.cliente')
                                                                               ->name('store', 'store.cliente')
                                                                               ->name('edit', 'edit.cliente')
                                                                               ->name('update', 'update.cliente');
Route::get('dashboard/headhunter/cliente/remover/{id}', 'HeadhunterClienteController@destroy');
Route::post('dashboard/ajax/filtroClienteConsulta', 'HeadhunterClienteController@filtro');

//HeadHunterUser
Route::resource('dashboard/headhunter/usuarios', 'HeadhunterUsuarioController');
Route::get('dashboard/headhunter/usuarios/{id}/delete','HeadhunterUsuarioController@destroy');

//Vaga Headhunter
Route::get('dashboard/headhunter/painel-vagas', 'VagaHeadhunterController@painelVagas')->name('painelVagas');
Route::get('dashboard/headhunter/painel-vagas/{id}', 'VagaController@show');
Route::resource('dashboard/headhunter/vagas', 'VagaHeadhunterController')->name('index', 'index.vagasHH')
                                                                          ->name('create', 'create.vagasHH')
                                                                          ->name('edit', 'edit.vagasHH')
                                                                          ->name('update', 'update.vagasHH')
                                                                          ->name('store', 'store.vagasHH');
  
//Vagas
    
    //Ajax
    Route::get('dashboard/ajax/alteraStatusVaga/{id}/{status}', 'VagaController@alteraStatus');
    Route::get('dashboard/ajax/deletaVaga/{id}', 'VagaController@destroy');
    Route::post("/dashboard/ajax/filtroVagasMktPlace", 'VagaHeadhunterController@filtroVagasMktPlace');
    Route::post("dashboard/ajax/filtroVagasInternas", 'VagaHeadhunterController@filtroVagasInternas');
//Empresas
    Route::get('dashboard/ajax/getInfoEmpresa/{id}', 'EmpresaController@buscaInfoEmpresa');
//Candidatos
Route::any('dashboard/headhunter/painel-candidatos', 'HeadhunterCandidatoController@filtro')->name('painelCandidatos');
//Candidatos Headhunter
Route::get('dashboard/headhunter/candidatos/adicionar', 'HeadhunterCandidatoController@create');
Route::get('dashboard/headhunter/candidatos', 'HeadhunterCandidatoController@index');
Route::get('dashboard/headhunter/candidatos/{id}/editar', 'HeadhunterCandidatoController@edit');
Route::get('dashboard/headhunter/candidatos/{id}/delete', 'HeadhunterCandidatoController@destroy');
Route::post('dashboard/headhunter/candidatos/adicionar', 'HeadhunterCandidatoController@store');
Route::post('dashboard/headhunter/candidatos/{id}/editar', 'HeadhunterCandidatoController@update');

//Cargos
    //Ajax
        Route::get('dashboard/ajax/cargos/{cargo}', 'CargoController@getCargos');
        Route::get('dashboard/ajax/cargos/buscaid/{cargo}', 'CargoController@getIdCargo');
//Cidades
    //Ajax
        Route::get("dashboard/ajax/listaCidades/{id}", 'CidadeController@show');

//Fila Candidatura
    //Ajax
    Route::get('dashboard/ajax/carregaFilaParticipantes/{vaga}', 'FilaCandidaturaController@mostraCandidatosVaga');
    Route::post('dashboard/ajax/filtroFilaCdd', 'FilaCandidaturaController@filtroFilaCdd');
    Route::get('dashboard/ajax/addCddFila/{vaga}/{candidato}', 'FilaCandidaturaController@addCddFila');
    Route::post('dashboard/ajax/filtrofilaCddParticipantes', 'FilaCandidaturaController@filtroCddParticipantes');
    Route::post('dashboard/ajax/filtrocddAnexos', 'FilaCandidaturaController@filtroCddAnexos');
    Route::get('dashboard/ajax/mostraCddAnexos/{id}', 'FilaCandidaturaController@mostraCddAnexos');

//Empresa
Route::resource('nova-empresa', 'EmpresaUsuarioController');
Route::get('empresas', 'EmpresaController@pageEmpresas')->name('empresas');
Route::resource('dashboard/empresa/', 'EmpresaController');
Route::post('loginEmpresa', 'EmpresaUsuarioController@loginUsuario');
Route::get('dashboard/empresa/meu-perfil', 'EmpresaUsuarioController@edit');
Route::post('dashboard/empresa/meu-perfil', 'EmpresaUsuarioController@update');
Route::get('dashboard/empresa/logout', 'EmpresaUsuarioController@logout');
Route::get('dashboard/empresa/cadastro-empresa', 'EmpresaController@create');
Route::post('dashboard/empresa/cadastro-empresa', 'EmpresaController@store');
Route::post('/dashboard/empresa/edita-empresa', 'EmpresaController@update')->name('empresa.edit');

Route::resource('dashboard/empresa/manutencao-vagas', 'VagaController');
Route::post('dashboard/ajax/filtraVagasEmp', 'VagaController@filtraVagasEmp');
Route::get('dashboard/empresa/cadastro-vaga', 'VagaController@create');
Route::post('dashboard/empresa/cadastro-vaga', 'VagaController@store');

Route::resource('dashboard/empresa/usuarios', 'EmpresaUsuarioController');
Route::get('dashboard/empresa/editUser/{id}', 'EmpresaUsuarioController@editaUsuario');
Route::post('dashboard/empresa/usuariosEdit', 'EmpresaUsuarioController@updateUsuario');
Route::get('dashboard/empresas/removeUsuario/{id}', 'EmpresaUsuarioController@destroy');
Route::get('dashboard/empresa/convidar-recrutador', 'EmpresaVagaController@inviteRecrut' );
Route::post('dashboard/empresa/convidar-recrutador', 'EmpresaVagaController@filtraParaConvite');
Route::resource('dashboard/empresa/minhas-vagas', 'EmpresaVagaController');

Route::get('dashboard/empresa/mensagens-da-central', function()
{
    return view('dashboard.empresas.mensagens-central');
});
Route::get('dashboard/empresa/mensagens-da-central/caixa-de-entrada', function()
{
    return view('dashboard.empresas.mensagens-central-caixa-entrada');
});
Route::get('dashboard/empresa/mensagens-da-central/caixa-de-saida', function()
{
    return view('dashboard.empresas.mensagens-central-caixa-saida');
});
Route::get('dashboard/empresa/historico-faturamento', function()
{
    return view('dashboard.empresas.historico-faturamento');
});
Route::get('/dashboard/empresa/cancelar-conta', function()
{
    return view('dashboard.empresas.cancela-conta');
});

Route::post('dashboard/empresa/evoluirCdd', 'EmpresaVagaController@evolucaoCdd');
// Convite Empresa
Route::get('dashboard/empresa/convidar-recrutador/{vaga}/{headhunter}', 'ConviteEmpresaHeadhunterController@store');
Route::get('dashboard/ajax/aceitaConvite/{id}', 'ConviteEmpresaHeadhunterController@aceitaConvite');
Route::get('dashboard/ajax/recusaConvite/{id}', 'ConviteEmpresaHeadhunterController@recusaConvite');
Route::get('dashboard/empresas/mostaHeadhunters/{id}', 'ConviteEmpresaHeadhunterController@mostaHeadhunters');
// Emails
Route::get('emails/vagasVencendo/{hash}', 'EmailController@vagasVencendoEmpresa');
Route::get('/mailable', function () {
// mostra como fica os emails que serão enviados.

});
// Candidatos
Route::resource('candidatos', 'CandidatoController')->name('index','candidatos')->name('create', 'cadastrar')->name('store', 'CddStore');
Route::post('/dashboard/candidato/login', 'CandidatoController@login')->name('loginCdd');
Route::get('/dashboard/candidato/', 'CandidatoController@dashboard')->name('dashboardCandidato');
Route::get('/dashboard/candidato/logout', 'CandidatoController@logoutCdd')->name('logoutCdd');
Route::get('/dashboard/candidato/editar', 'CandidatoController@edit')->name('editarCdd');
Route::post('/dashboard/candidato/editar', 'CandidatoController@update')->name('editarCdd');
Route::get('/dashboard/candidato/cadastrar-curriculum', 'CandidatoController@cadastraCv')->name('cadastracv');
Route::post('/dashboard/candidato/cadastrar-curriculum', 'CandidatoController@editaCv')->name('editaCv');
Route::get('/dashboard/candidato/vagas', 'CandidatoController@vagas')->name('cddVaga');
Route::post('/dashboard/candidato/vagas', 'CandidatoController@filtroVagas')->name('filtraVagas');
Route::get('/dashboard/candidato/candidata/{vaga}', 'CandidatoController@candidatoVaga')->name('candidatar-se');
Route::get('/dashboard/candidato/servicos', 'CandidatoController@servicos')->name('servicos');
Route::get('/dashboard/candidato/cancelar-conta', 'CandidatoController@cancelarConta')->name('cddCancelarConta');
Route::get('/dashboard/candidato/cancelar-conta/delete', 'CandidatoController@destroy')->name("deletarConta");


